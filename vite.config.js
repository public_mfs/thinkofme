import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import Vue from '@vitejs/plugin-vue'
import Markdown from 'unplugin-vue-markdown/vite'
import prism from 'markdown-it-prism'

import doneRight from 'markdown-it-toc-done-right'
import anchor from 'markdown-it-anchor'

//CSS兼容使用autoprefixer添加前缀
import autoprefixer from 'autoprefixer';

// ElementUI Plus 自动按需导入
// 按需自动引入配置完之后，在组件中可直接使用，不需要引用和注册，这里已经实现了按需自动引入Element-plus组件
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';

// 自动引入图标组件
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'

// Gzip压缩
import viteCompression from 'vite-plugin-compression'

export default defineConfig({
  base: "/think/",//需要和nginx中配置的location保持一致

  server: {
    // host: 'localhost',
    host: '192.168.31.113',//将 host 设置为内网 IPv4 地址，可以令手机通过局域网访问
    port: '9999',
    proxy: {
      '/getCityId/': {
        target: 'http://webapi.weatherol.com/',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/getCityId/, ''),
      },
      '/weatherCommon': {
        target: 'http://zsta.weatherol.com/v1/weather/common',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/weatherCommon/, ''),
      },
    },
  },

  plugins: [
    Vue({
      include: [/\.vue$/, /\.md$/],
    }),
    //转码markdown为html并且添加目录/锚点等
    Markdown({
      markdownItSetup(md) {
        // for example
        md.use(prism);
        md.use(anchor);
        md.use(doneRight);
      },
    }),
    AutoImport({
      resolvers: [
        ElementPlusResolver(),

        // 自动导入图标组件
        IconsResolver({
          prefix: 'Icon',
        }),
      ],
    }),
    Components({
      resolvers: [
        ElementPlusResolver(),

        // 自动注册图标组件
        IconsResolver({
          enabledCollections: ['ep'],
        }),
      ],
    }),
    Icons({
      autoInstall: true,
    }),

    // Gzip压缩
    viteCompression({
      filter: /\.(js|css|json|txt|html|ico|svg|png|jpg|jpeg|bin|gltf)(\?.*)?$/i, // 需要压缩的文件
      threshold: 1024, // 文件容量大于这个值进行压缩，单位是bit
      algorithm: 'gzip', // 压缩方式
      ext: 'gz', // 后缀名
      deleteOriginFile: false, // 压缩后是否删除压缩源文件
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  css: {
    postcss: {
      plugins: [
        // 配置 autoprefixer
        autoprefixer({
          overrideBrowserslist: [
            // 'Android 4.1',
            // 'iOS 7.1',//兼容 iOS 7.1 及以上版本的 Safari 浏览器。
            // 'Chrome > 31',//兼容 Chrome 31 及以上版本。
            // 'ff > 31',
            // 'ie >= 8',//兼容 Internet Explorer 8 及以上版本。
            // '> 1%',

            // "> 1%",
            "last 2 versions",
            "not ie <= 8",
          ],
          grid: true,//为 CSS 中的网格布局自动添加前缀
        }),
      ],
    },
  }
})

