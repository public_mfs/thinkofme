# 注释

```text
/**
 * 城市的stationid查询；http://webapi.weatherol.com/v1/city/searchCityInfo?domestic=1&city=%E5%85%B0%E5%B7%9E&language=cn&type=searchCity&complete=1&key=CWk8TkM8
 * 24小时预报：http://zsta.weatherol.com/v1/weather/common?type=forecast24h&key=CWk8TkM8&stationid=101160101
 * 当前天气：http://zsta.weatherol.com/v1/weather/common?type=current&key=CWk8TkM8&stationid=101160101
 * 昨天：http://zsta.weatherol.com/v1/weather/common?type=yesterday&key=CWk8TkM8&stationid=101160101
 * 7内的天气预报：http://zsta.weatherol.com/v1/weather/common?type=forecast7d&key=CWk8TkM8&stationid=101160101
 * 各种乱七八糟的指数：http://zsta.weatherol.com/v1/weather/common?type=index9type&key=CWk8TkM8&stationid=101160101
 * 空气质量：http://zsta.weatherol.com/v1/weather/common?type=air&key=CWk8TkM8&stationid=101160101
 * AQI:
 * {
    "PM25": "24",//
    "PM10": "37",
    "CityName": "娄底",
    "lng": "112.002",
    "AQI": "37",
    "O3": "113",
    "SO2": "10",
    "LevelIndex": "优",
    "NO2": "6",
    "MaxPoll": "—",
    "CO": "0.8",
    "AQILEVEL": "优",//√
    "lat": "27.73"
}
1、一级： 空气污染指数 ≤50优级   #009966
2、二级： 空气污染指数 ≤100良好  #ffde33
3、三级： 空气污染指数 ≤150轻度污染  #ff9933
4、四级： 空气污染指数 ≤200中度污染 #cc0033
5、五级： 空气污染指数 ≤300重度污染 #660099
6、六级：空气污染指数＞300严重污染  #7e0023
 * 日出日落：http://zsta.weatherol.com/v1/weather/common?type=riseset&key=CWk8TkM8&stationid=101160101
 * 不知道干嘛的(知道了，天气提醒，比如预警什么的，没有则返回空数组)/：http://zsta.weatherol.com/v1/weather/common?type=alert&key=CWk8TkM8&stationid=101160101（返回数据：{"message":"Query success","result":[{"w":[],"stationid":"101160101","updateTime":"2024-04-27 00:23"}],"code":"ok"}）
 */


 /**
 * 跳转百度：https://weathernew.pae.baidu.com/weathernew/pc?query=娄底市天气&srcid=4982
 * 24小时天气：#hourCvs
 * 15天天气：#dayCvs  参数：15_day_forecast
 * 40天天气：#weatherFuture  参数：long_day_forecast
 */


// 这个可以让我们分段展示，通过滑动图标查看后续的数据，但是性能很差看起来也很卡顿，放弃掉这种做法，换做下面的隔2个点显示一个label
// dataZoom: [
//   {
//     type: "slider",
//     show: false, //不显示下方的滑动条
//     start: 0,
//     end: 39, // 一次只显示x个数据点
//     xAxisIndex: 0,
//     filterMode: "filter", // 设置为 'filter' 可以按照类别缩放
//   },
//   {
//     type: "inside",
//     xAxisIndex: 0,
//     filterMode: "filter", // 设置为 'filter' 可以按照类别缩放
//   },
// ],

Weather：
1. 路由激活/失活时适时添加/移除窗口resize监听
2. resize监听事件防抖处理


docker run -d -i --name think_of_me_runner --restart always -v /my_project/think_of_me/runner_config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner


gitlab-runner register         --non-interactive         --executor "docker"         --docker-image alpine:latest         --url "https://gitlab.com/"         --registration-token "GR1348941Rqf8Mv1cfUMp8SfFxSw_"         --name "think_of_me_runner"         --run-untagged="true"         --tag-list "think_of_me_tag"         --locked="false"         --access-level="not_protected"



 5:02:46
查询天气按钮加上自动缩小 √

 5:02:47
添加白色body背景 √

 5:02:47
标题，ico √

直辖市查询出错修改 √
左边菜单栏去掉多余的border √
:title属性去除，改为elementUI Plus得tooltip组件 √
```
