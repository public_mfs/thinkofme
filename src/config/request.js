import axios from 'axios';

const request = axios.create({
    baseURL: '',
    timeout: 3000
});
request.interceptors.request.use(config => config, err => {
    Promise.reject(err);
});
request.interceptors.response.use(res => {
    return Promise.resolve(res.data);
}, err => {
    Promise.reject(err);
});

export default request;