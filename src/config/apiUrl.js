let commonPrefix;
if (import.meta.env.DEV) {
    commonPrefix = ``;
} else {
    const hrefObj = new URL(location.href);
    commonPrefix = hrefObj.protocol + '//' + hrefObj.hostname + '/thinkBackEnd';
}
function weatherCommonUrls(type, key, stationid) {
    return `${commonPrefix}/weatherCommon?type=${type}&key=${key}&stationid=${stationid}`;
}
function getCityUrl(where, zKey) {
    return `${commonPrefix}/getCityId/v1/city/searchCityInfo?domestic=1&city=${where}&language=cn&type=searchCity&complete=1&key=${zKey}`
}

export {
    weatherCommonUrls,
    getCityUrl
}