const prefix = "./publicAssets/images/";
function getIcon(type, typePm) {
    let result = '';

    const map = new Map([
        ['晴', ['0', 'sunny']],
        ['多云', ['1', 'cloudy']],
        ['阴', ['2', 'overcast']],
        ['小雨', ['7', 'raindrops']],
        ['阵雨', ['7', 'raindrops']],
        ['中雨', ['8', 'raindrops']],
        ['大雨', ['40', 'raindrops']],
        ['暴雨', ['41', 'raindrops']],
        ['小雪', ['26', 'snowland']],
        ['中雪', ['27', 'snowland']],
        ['大雪', ['27', 'snowland']],
        ['暴雪', ['27', 'snowland']],
        ['雨夹雪', ['19', 'raindrops']],
        ['霾', ['20', 'dust']],
        ['雾', ['33', 'fog']]
    ]);

    result = map.get(type);
    if (!result) {
        result = map.get(typePm);
    }
    if (result) {
        result = [prefix + result[0] + ".png", prefix + 'ic_bg_static_' + result[1] + ".jpg"];
        return result;
    }
    else {
        return [prefix + '888' + ".png", prefix + 'ic_bg_static_cloudy.jpg'];
    }
}

function getPColor(api) {
    if (!api) return 'rgba(0,0,0,0)';
    if (api <= 50) {
        return "#009966"; // 优级
    } else if (api <= 100) {
        return "#ffde33"; // 良好
    } else if (api <= 150) {
        return "#ff9933"; // 轻度污染
    } else if (api <= 200) {
        return "#cc0033"; // 中度污染
    } else if (api <= 300) {
        return "#660099"; // 重度污染
    } else {
        return "#7e0023"; // 严重污染
    }
}

// 防抖
function deBounce(fnName, t) {
    let timer;
    return function (...args) {
        clearTimeout(timer);
        timer = setTimeout(fnName, t, ...args);
    }
}

export {
    getIcon,
    getPColor,
    deBounce
}