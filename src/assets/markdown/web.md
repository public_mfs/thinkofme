# 前端

[TOC]

## TypeScript

### 对比JS

1. 添加了类型系统的 JavaScript
2. TypeScript是**静态类型**，编译阶段就报错；JavaScript是动态类型，没有编译阶段，执行时才会类型报错
3. TypeScript 可以编译为 JavaScript
4. TypeScript 非常适用于大型项目——类型系统让大型项目更易维护、更少bug
5. TypeScript 拥有很多编译选项，类型检查的严格程度由你决定

### 安装和编译

1. 安装、编译

    ```shell
    npm install -g typescript //安装
    tsc --init //生成一个tsconfig.json配置文件
    tsc //编译，默认会编译项目下所有的ts文件，可以通过配置tsconfig.json中的exclude或者include字段来控制编译文件
    tsc hello.ts //tsc编译hello.ts为hello.js
    ```

2. 编译时进行类型检查，即使编译出错，也会生成结果文件
3. 编译选项/设置——tsconfig.json文件

    ```json
    {
        "compilerOptions": {//编译选项，比如输出目录，strictNullChecks进行null/undefined严格检查，m默认未false
            "outDir": "./built",
            "allowJs": true,
            "target": "es5",
            "strictNullChecks": false
    },
        "include": ["./src/**/*"]
    }
    ```

### 5种基本数据类型+any任意值

1. **any类型上访问任何属性/任何方法都是可以的**
2. **undefined、null可以赋值给任何类型，但是void不可以**，但是如果开启了编译选项strictNullChecks，则不能随意赋值给其他类型（any、unknown类型除外）

    ```typescript
    //5种基本类型
    let a: number = 2;
    let b: boolean = (a > 2);
    let n1: number = 0b1011;//支持es6的二进制、八进制等等
    let str1: string = `你好我是${a}`;
    let un: undefined = undefined;
    let nu: null = null;
    a = undefined;//undefined、null可以赋值给任何类型，但是void不可以
    function void_fun(): void { }//可以用 void 表示没有任何返回值的函数

    let an1: any = "ss";
    an1 = 22;//任意值的类型可以改变，类似于js
    let an2 = "ddd";//变量如果在声明的时候，未指定其类型，那么它会被识别为任意值类型：
    ```

### 类型推论

1. 对于**没有明确指定类型**的，如果声明的时候：
     * 声明同时赋值：被推测为赋值的类型
     * 只声明不赋值：被推测为any，后续不管如何赋值都不进行类型检查

     ```typescript
     let an3;//an3为any类型
     let an4 = 1;//an4后续只能为number类型
     an3 = "dsd";
     an3 = 333;
     ```

### 联合类型

1. 变量可以指定多种类型，使用 | 分隔每个类型：`let union : string | number | boolean = 23;`
2. **不确定联合类型到底是何种类型的时候**，调用类型的属性或者方法必须各类型**共有**的，否则会报错

    ```typescript
    function set_union(a: number | string): number {
        return a.toString().length;//.toString()是number和string类型共有的方法
    }
    ```

3. 联合类型赋值后也会进行类型推论，被推测为赋值的类型，但是可以通过重复赋值为被推测为另外一个类型（包含在联合类型中）

### 对象的类型：接口

1. 接口对**类**的形状（属性）和行为（所以也可以包含方法）进行抽象，类的形状必须和接口**完全一致**,属性比接口形状多/少都会报错
2. 可以使用**可选属性**`?:`来定义某些**不确定是否**存在的属性

    ```typescript
    interface my_interface {
        readonly name: string;
        age: number;
        is_graduated: boolean | number;
        has_boyfriend?: boolean;
        [propName: string]: number | boolean | string;
    }
    let tom: my_interface = {
        name: "tom",
        age: 24,
        is_graduated: 1
    }
    interface my_interface2 {
        fun(arg: string): void;
    }
    ```

3. 任意属性：使用` [propName: string]: number `定义任意属性取 number 类型的值，其他属性必须是任意属性类型限制类型的子集
4. 只读属性：在接口的属性前加` readonly `修饰符，**第一次给对象赋值的时候就必须给只读属性赋值！** 并且后续不可以改动

### 数组类型：4种数组表示方法

```typescript
//数组的4种表示方法
let arr1:number[]=[1,3,4];//类型+方括号[]
let arr2:Array<number>=[1,1,3,4,7];// **泛型表示数组

// interface like_arr {//接口表示数组（不常用，但是常用来表示类数组）
//     [index: number]: any;
//     length?: number;//可选属性，可以表示数组，又可以表示类数组
// }
let arr4: like_arr = [1, 3, 4, 5];
console.log(arr4);
function arr_like(arg1: string | number, arg2: number): void {
    let args: like_arr = arguments;//接口表示类数组
    console.log(args);
}
arr_like(1, 3);
```

### 函数类型

1. 函数表达式，完整的函数表达式如下

    ```typescript
    let fun2: (arg1: string, arg2: string) => number = function (arg1: string, arg2: string): number {
        return parseInt(arg1) + parseInt(arg2.toString());
    }
    ```

2. 在 TypeScript 的类型定义中，=> 用来表示函数的定义，左边是输入类型，需要用括号括起来，右边是返回值类型。与ES6的箭头函数不是一个东西。

    ```typescript
    (输入的参数以及类型) => 返回类型
    ```

3. 接口定义函数的形状+可选参数+参数默认值：可选参数必须放在所有必选参数后面，否则报错

    ```typescript
    interface fun_inter {
        (arg1: number, arg2: number): number;
    }
    let fun3: fun_inter;//接口定义函数形状
    //这里fun3虽然检测通过，但是实际使用fun3的时候只能传fun_inter中定义的两个参数，arg3与arg4传进来会报错→类型必须与其实现的接口形状保持完全一致
    fun3 = function (arg1: number, arg2: number, arg3?: number, arg4: number = 1): number {
        return arg1 + arg2;
    }
    console.log(fun3(1, 3));

    let push = function (arr: any[], ...rest: any[]) {
        rest.forEach(function (item: any): void {
            arr.push(item);
        })
    }
    let arr5: any[] = [];
    push(arr5, 1, 3, 4, 5, '3');
    console.log(arr5);
    ```

4. 重载：使用重载定义多个 reverse 的函数类型，精确的表达，例如：输入为数字的时候，输出也应该为数字，输入为字符串的时候，输出也应该为字符串（后面会知道可以用泛型来实现）

    ```typescript
    function reverse(x: number): number;
    function reverse(x: string): string;
    function reverse(x: number | string): number | string | void {
        if (typeof x === 'number') {
            return Number(x.toString().split('').reverse().join(''));
        } else if (typeof x === 'string') {
            return x.split('').reverse().join('');
        }
    }
    ```

### 类型断言（手动指定一个类型）

兼容：存在两个接口A、B，其中A包含的属性/方法在B内都能找到，此外B可能还会有自己额外的属性/方法，则A兼容B，类似于父类兼容子类
**子类实例可以赋值给父类变量，父类实例不能赋值给子类变量**

1. 语法

    ```typescript
    值 as 类型 //推荐
    //or
    <类型>值   //不推荐，可能和泛型混淆，jsx语法也会歧义
    ```

2. 用途
    1. 将一个联合类型（class1 | class2）**断言**成其中的一个类型，比如class1。使用断言后避免调用方法或者使用深层次属性

        ```typescript
        function assertion1(arg1: number[] | number): string {
            return (arg1 as number[]).join(','); //传入非number[]类型的参数会报错
        }
        console.log(assertion1([22, 33]));
        ```

    2. 将一个父类**断言**成一个更具体的子类

        ```typescript
        interface ApiError extends Error {
            code: number;
        }
        interface HttpError extends Error {
            statusCode: number;
        }

        function isApiError(error: Error) : boolean {
            if (typeof (error as ApiError).code === 'number') {
                return true;
            }
            return false;
        }
        ```

    3. 将任何类型断言成`any`：将一个变量断言为 any 可以说是解决 TypeScript 中类型问题的最后一个手段。它极有可能掩盖了真正的类型错误，所以如果不是非常确定，就不要使用`as any`。不能滥用`as any`，另一方面也不要完全否定它的作用

        ```typescript
        // window.age = 222; //报错
        (window as any).age = 222;
        console.log((window as any).age);
        ```

    4. 将any断言成一个具体类型，比如函数fun返回值为any，调用该函数时使用`const result = fun() as Cat`，其中，Cat是一个接口；提高代码的可维护性
3. 阶段总结
    1. 联合类型可以被断言为其中一个类型
    2. 父类可以被断言为子类 / 子类可以被断言成父类
    3. 任何类型都可以被断言为 any
    4. any 可以被断言为任何类型
    5. 要使得 A 能够被断言为 B，只需要 A 兼容 B 或 B 兼容 A 即可
4. 类型断言的限制
    1. 不是所有类型都可以互相断言的，需要二者**兼容**，比如：Cat包含Animals的所有属性/方法，此外还有自己的独特的方法/属性，则称Animals兼容Cat，彼此可以互相断言
5. 双重断言
    1. `my_cat1 as any as fish` 将任何一个类型通过`as any as`作为中转转换成另外一个类型
6. 类型断言vs类型声明（类型声明比类型断言更加严格）
    1. 对于animals、cat来说，类型断言只需要其中一者兼容另外一者即可，但是类型转换需要目标类型兼容被转换变量的类型

        ```typescript
        interface cat {
            name: string,
            run: string
        }
        interface animals {
            name: string
        }
        //some_animal和my_cat1赋值操作省略
        let v_cat1: cat = some_animal as cat;//正确，animals兼容cat，可以互相断言
        let v_animal1: animals = my_cat1 as animals;

        // let v_cat2: cat = some_animal;//报错，类型转换cat不兼容animals
        let v_animal2: animals = my_cat1;//正确，animals兼容cat
        ```

    2. 为了增加代码的质量，我们最好优先使用类型声明，这也比类型断言的 as 语法更加优雅
7. 类型断言vs泛型：最优解决方案→泛型

    ```typescript
    //优化前：类型断言
    function getCacheData(key: string): any {
        return (window as any).cache[key];
    }
    interface Cat {
        name: string;
        run(): void;
    }
    const tom = getCacheData('tom') as Cat;
    tom.run();

    //优化后：泛型
    function getCacheData<T>(key: string): T {
        return (window as any).cache[key];
    }
    const tom = getCacheData<Cat>('tom');
    tom.run();
    ```

### 声明文件

1. 对于我们引入的第三方库比如jQuery，要直接在TS中使用必须要用declare声明，否则TS无法使识别：declare仅用于编译时的检查，编译结果中会被删除
2. 使用declare来声明一个变量/函数/类/接口/枚举类型/命名空间等

    ```typescript
    declare let jQuery: (selector: string) => any;
    //function/class/interface/enum/namespace/type...
    declare class declare_class {
        name: string;
        say(): void;
    }
    declare namespace my_namespace {
        const version: string;
        class space_class {
            name: string;
            say(): void;
        }
        function my_fun(): void;
    }
    ```

3. 通常将所有的声明语句放到一个`.d.ts`（必须是这个后缀）后缀的文件中
4. 由于声明文件只是为了编译检查的时候使用，因此里面**只包含各种类型/形状**，不会有其具体实现，如果写了具体实现会报错，其具体实现应该是在引进的第三方库中实现的

### 内置对象

1. ECMAScript标准的内置类型：Boolean、Error、Date、RegExp等，可以将变量设置为这些类型
2. DOM 和 BOM 提供的内置对象有：Document、HTMLElement、Event、NodeList 等。

### 类型别名：type + 字符串字面量类型

```typescript
//为string类型和（my_interface|my_interface2）联合类型取别名
type my_string = string;
type my_inter = my_interface | my_interface2;

type str_list = 'str1' | 'str2' | 'zouzhu';
function test_str(arg: str_list): void { }
// test_str('zou');//报错，必须是str_list字符串字面量类型中的一个
test_str('str1');//正确
```

### 枚举：enum

1. 枚举内部成员的值默认为数字0开始递增，并且可以反向映射
2. 未手动赋值的枚举项会**接着**上一个枚举项递增
3. 成员赋值重复不会被ts察觉，所以不要有覆盖的情况
4. 枚举项有两种类型：常数项（只能是一个直接的数值/字符串等）和计算所得项（可以有计算），使用`const enum`声明一个只有常数项的枚举

    ```typescript
    enum my_enum1 { one = 999, two, three, four, five = <any>'邹竹', six = <any>'me'.split('') };
    console.log(`one:${my_enum1['one']}  two:${my_enum1['two']}  three:${my_enum1['three']}   six:${JSON.stringify(my_enum1['six'])}`);// 999 1000 1001 ["m","e"]
    const enum my_const_enum1 { up = 1, down, left, right = "d" };//const enum成员不可以包含如.length、.split之类的计算
    ```

### 元组：[string, number, boolean]

1. 数组合并了相同类型的对象，而元组（Tuple）合并了不同类型的对象。

    ```typescript
    // let tom1: [string, number, boolean] = ["", 22, false, 44];//报错，初始化赋值的时候，元素个数不能越界
    let tom1: [string, number] = ["", 22];
    tom1.push(233);//虽然越界了，但是不会报错，
    // tom1.push(true);//报错，添加的越界元素必须是元组种每个类型的联合类型
    console.log(tom1);
    tom1[0] = 'Tom';
    tom1[1] = 25;

    tom1[0].slice(1);
    tom1[1].toFixed(2);
    console.log(tom1);
    ```

### 类

1. 三大特性
    1. 封装：隐藏细节，只暴露对外接口
    2. 继承：子类继承父类
    3. 多态：对父类的同一个方法不同子类有不同的实现
2. 三种访问修饰符：public、protected、private，JS中就没有这三种关键字
    1. public是公开的，外部可访问
    2. protected是受保护的，只有**类内部和子类内部**可以访问
    3. private私有的，只有本类内部可以访问
3. readonly修饰符：写在其它修饰符最后，并且实例不可以修改该属性值
4. abstract修饰符：抽象类、抽象方法
    1. 抽象类是不允许被实例化的
    2. **抽象方法在子类中必须被实现**，抽象方法不能定义在非抽象类上
5. 继承：与ES6一样使用extends

    ```typescript
    abstract class creature {
        public readonly name: string;
        protected age: number = 24;
        constructor(name: string) {
            this.name = name;
        }
        public abstract hello(): void;
    }
    class human extends creature {
        constructor(name: string) {
            super(name);
        }
        public hello(): void {//不实现hello方法会报错
            console.log(`Hi, I am ${this.name}, age ${this.age}`);
        }
    }
    // let me: creature = new human('zouzhu');//正确，因为子类型实例可以赋值给父类型变量
    let me: human = new human('zouzhu');
    me.hello();
    ```

### 类与接口

1. 将多个类的共同行为/属性提取成接口，类定义时通过`implements`实现接口
2. 接口可以继承接口
3. **接口也可以继承类**：接口其实继承的是这个类的**类型（类中除了构造函数、静态方法/属性以外的部分）**，也就是其实例属性和实例方法
4. 注意区分类与接口的语法，接口中属性/方法声明使用`:`，但是类中属性/方法声明使用`=`，并且类中的变量不要加let/var

    ```typescript
    interface water {
        water_prop: string;
        swim(): void;
        // swim: () => void; //与上面的写法等价
    }
    interface land_father {
        land_prop: string;
    }
    interface land extends land_father {//接口可以继承接口
        run(): void;
    }
    class frog implements water, land {
        land_prop: string = "跑步";
        water_prop: string = "游泳";
        constructor(other_prop?: string) {//构造函数不会被接口继承，同理静态属性、静态方法也不会被接口继承
            other_prop ? this.other_props = other_prop : "";
        }
        swim(): void {
            console.log("I'm swiming!");
        }
        run(): void {
            console.log("I'm running!");
        }
        other_reaction?(): void {
            console.log("Other reaction!");
        }
        other_props?: string;
    }
    let f1 = new frog();
    f1.swim();
    f1.run();
    f1.other_reaction();
    console.log(f1.water_prop, f1.land_prop);
    //接口可以继承类
    interface child_inter extends frog { }
    let child_obj: child_inter = {//接口可以继承类
        land_prop: "",
        water_prop: "",
        swim() { },
        run() { },
        other_props: "额外的属性"
    }
    ```

### 泛型

1. 在函数后添加`<T>`，T用于表示任意的输入类型，可以定义返回值的类型为：`Array<T>`来限制返回值的每一项都为同一个类型（T类型），而避免使用any允许每一项都为any类型
2. 使用`<T, T1, T2>`形式来定义多个类型的参数

    ```typescript
    function create_array<T, T1, T2>(length: number, value: T): Array<T> {
        let result = [];
        for (let i = 0; i < length; i++) {
            result[i] = value;
        }
        return result;
    }
    console.log(create_array(5, 44));
    ```

3. 泛型约束，限制泛型的类型/形状，使用extends+interface
    1. 直接约束
    2. 各个参数之间也可以互相约束

        ```typescript
        interface length_arg {
            length: number;
        }
        function test_generics2<T extends length_arg>(arg: T): void { }
        // test_generics2(0);//报错，0不包含length属性
        test_generics2("");

        function test_generics3<T extends T1, T1>(child: T, parent: T1): void { }
        test_generics3({ name: 'zouzhu', age: 24 }, { name: 'zouzhu' })// 第一个参数是第二个参数的子类
        ```

4. 泛型接口，接口名称后面加上`<T>`

    ```typescript
    //使用泛型定义一个函数的形状
    interface my_array<T> {
        (length: number, value: T, ext?: any): Array<T>;
    }
    let create_my_array: my_array<any>;//泛型接口
    create_my_array = function <T>(length: number, value: T): Array<T> {
        let result = [];
        for (let i = 0; i < length; i++) {
            result.push(value);
        }
        return result;
    }
    console.log(create_my_array(4, '你好'));
    ```

5. 泛型类 + 泛型参数默认类型：类名后加`<T>`或者`<T = number/string/...>`

    ```typescript
    class my_class<T = number>{//默认的类型为number
        public inner_array: T;
        print() {
            console.log(typeof this.inner_array);
        }
        constructor(arg: T) {
            this.inner_array = arg;
        }
    }
    let ob1 = new my_class<number>(32);//由构造函数可知这里的参数类型必须和泛型类型一致
    ob1.print();
    ```

## Docker小结

1. 下载安装：**注意如果直接使用官方的安装教程`yum install -y docker`会出问题，安装的会是1.13.1版本（docker version查看）这个版本必然会出问题，甚至无法拉取很多镜像**

    ```bash
    yum install yum-utils -y
    yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
    yum install docker-ce  docker-ce-cli -y
    ```

2. 运行docker服务：`sudo systemctl start docker`
3. Docker下载后直接运行docker version，会提示Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?说明docker服务未运行，启动docker服务后，再次docker version提示消失

    ```bash
    [root@VM-8-14-centos ~]# docker version
    Client: Docker Engine - Community
    Version:           23.0.1
    API version:       1.42
    Go version:        go1.19.5
    Git commit:        a5ee5b1
    Built:             Thu Feb  9 19:49:07 2023
    OS/Arch:           linux/amd64
    Context:           default
    Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
    [root@VM-8-14-centos ~]# sudo systemctl start docker
    [root@VM-8-14-centos ~]# docker version
    Client: Docker Engine - Community
    Version:           23.0.1
    API version:       1.42
    Go version:        go1.19.5
    Git commit:        a5ee5b1
    Built:             Thu Feb  9 19:49:07 2023
    OS/Arch:           linux/amd64
    Context:           default

    Server: Docker Engine - Community
    Engine:
    Version:          23.0.1
    API version:      1.42 (minimum version 1.12)
    Go version:       go1.19.5
    Git commit:       bc3805a
    Built:            Thu Feb  9 19:46:47 2023
    OS/Arch:          linux/amd64
    Experimental:     false
    containerd:
    Version:          1.6.19
    GitCommit:        1e1ea6e986c6c86565bc33d52e34b81b3e2bc71f
    runc:
    Version:          1.1.4
    GitCommit:        v1.1.4-0-g5fd4c4d
    docker-init:
    Version:          0.19.0
    GitCommit:        de40ad0

    ```

4. 容器操作

    ```bash
    docker run node /bin/echo "你好" 
    # docker + run 运行一个镜像
    # 以node镜像创建了一个容器，在容器里执行了 /bin/echo "你好"

    # 进入容器终端/伪终端
    docker run -i -t node /bin/bash # 启动容器
    # -t: 在新容器内指定一个伪终端或终端。
    # -i: 保持和 docker 容器内的交互，启动容器时，运⾏的命令# 结束后，容器依然存活，没有退出（默认是会退出，即停⽌的）
    # -d: 后台运⾏容器
    # -p: 宿主机:内部端口 映射
    # --rm: 容器在启动后，执⾏完成命令或程序后就销毁
    # --name: 给容器起⼀个自定义定义名称

    # exit 或者 ctrl+D 退出容器
    # 或者
    docker run -it node /bin/bash
    docker run --rm -v /home/user/myapp:/app my_image #启动一个名为 my_container 的 Docker 容器，并将主机上的 /home/user/myapp 目录挂载到该容器中的 /app 目录

    docker exec -it 容器id /bin/bash # 进入容器
    docker attach 容器id  # 进入容器
    docker rm 容器id # 删除已停止的Docker容器。
    docker rm -f 容器id # 删除容器
    docker container prune # 清理掉所有处于终止状态的容器

    docker ps # 查看运行的容器
    docker container list/ls #查看运行的容器
    docker ps -a # 查看所有的容器，正在运行的+已经停止的
    docker logs 容器id # 查看容器内的输出
    docker stop 容器id # 停止容器工作
    docker start 容器id # 启动一个已经停止工作的容器

    docker inspect 容器id # 查看该容器的配置

    docker-compose up
    version: '3'
    services:
    web:
        image: nginx
        ports:
        - "8080:80"
    db:
        image: mysql
        environment:
        MYSQL_ROOT_PASSWORD: password

    docker-compose: 使用Compose定义和运行多个相关容器。

    ```

5. 镜像操作

    ```bash

    docker pull 镜像名 # 从Docker Hub上获取/下载某个镜像，比如docker pull node
    docker build -t myimage .  # 以当前目录下的Dockerfile文件构建一个新的名为myimage的镜像
    docker images # 查看本地储存的所有镜像列表
    docker rmi myimage # 删除本地储存的node镜像
    docker push username/myimage # 将本地的Docker镜像推送到Docker Hub

    ```

6. [更多](https://blog.csdn.net/Albert__Einstein/article/details/125912187)

## Gitlab ci/cd

### 概念

1. 持续集成（CI）
    对于每次的分支提交/更改，都会自动且连续地构建和测试
2. 持续交付
    持续集成的下一步，手动部署上线
3. 持续部署（CD）
    类似于持续交互，但是是自动部署

### 安装和注册GitLab Runner

1. 安装，下面以RHEL/CentOS/Fedora云服务器为例讲解：其它系统安装详见[教程](https://docs.gitlab.com/runner/install/)
    1. linux打开终端并执行以下指令进行安装：

        ```bash
        su root

        curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
    
        yum install gitlab-runner
        ```

    2. 以下指令直接在linux系统进行注册，但是通常是在docker中注册，后面有docker内注册的流程

        ```bash
        sudo gitlab-runner register --url YOUR_URL --registration-token YOUR_TOCKEN

        # docker内注册，后面又有详细步骤
        docker run -d -i --name meme_me --restart always -v /srv/gitlab-runner/new_config2:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner
        # 使用docker进行注册一个runner容器，注册完成之后使用docker ps命令查看会有你注册的gitlab-runner容器，如果没有，检查是否有已经注册好的runner容器正在运行，并将其删除掉，网上很多教程都用一句指令来
        
        ```

        * 其中，`YOUR_URL`和`YOUR_TOCKEN`字段在`项目gitlab → settings → CI/CD → Runners → Specific runners`下获取，`url`通常是`https://gitlab.com/`，`tocken`为一串专属随机乱码；
        * `excutor`字段通常选择`docker`；
        * `default docker image`默认的docker镜像
2. gitlab-runner注册流程如下，下面的命令默认你已经在linux centos上安装好了gitlab-runner以及Docker，以下指令在linux终端运行
   1. 创建并运行一个runner容器

        ```bash
        docker run -d -i --name home_runner_docker_name --restart always -v /srv/gitlab-runner/new_config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner
        # 此时建立了一个名为xxx的runner容器，docker ps可以看到
        ```

   2. 进入容器并且注册runner，**注意runner本身并不是一个容器，它只是一个可执行程序，它在我们创建的runner容器内部运行**

        ```bash
        docker ps # 找到你的runner容器id（镜像是gitlab/gitlab-runner）并且复制下来，找不到的话可能是你之前就注册过了runner容器，把之前的runner容器停止或删除
        docker exec -it 容器id /bin/bash # 进入runner容器

        gitlab-runner register         --non-interactive         --executor "docker"         --docker-image alpine:latest         --url "https://gitlab.com/"         --registration-token "GR13489412J34EFgRL5Ubs52ijePq"         --name "home_runner"         --run-untagged="true"         --tag-list "home_tag"         --locked="false"         --access-level="not_protected" # runner容器中执行runner注册代码，url、registration-token记得换成自己项目配置的，name、tag-list也是，注册成功会提示success
        ```

   3. 挂载目录卷，我们在步骤1中把容器的/etc/gitlab-runner挂载在了主机的/srv/gitlab-runner/new_config上，该目录下面保存的是runner的配置文件config.toml，两个目录会始终保持一致，如下修改了volumes之后就配置完毕！！

        ```bash
        exit # 2中我们进入了容器执行注册，现在先退出容器
        cd /srv/gitlab-runner/new_config # 找到runner配置目录
        vi config.toml # 修改config.toml，找到你的runner，把 volumes = ["/cache"] 改为 volumes = ["/cache", "/usr/bin/docker:/usr/bin/docker", "/var/run/docker.sock:/var/run/docker.sock"] ，令runner引用宿主机的Docker环境

        # 其它操作，不用输入，仅仅是用法
        # gitlab-runner list # 列出注册的所有runner
        ```

   4. 注意事项：
      1. 许多注册教程都是一句指令将docker run以及register指令合并在一起进行runner注册，但是我的服务器运行了后容器会直接停止，即使加-i也没用，我这个方法没区别，只是将两个步骤分开来，而且可以保证容器不会被自动关闭
      2. 注册时指定tag非常重要，因为如果不指定tag，yml脚本会随机找一个runner运行而不是你注册的那个runner（有可能是gitlab自带的runner），因此job的tag和runner的tag要保持一致，同一个tag的job才会在对应的runner中执行
   5. 报错：
      1. **ERROR: Failed to remove network for build。**
         1. 原因：启动runner时未将本地docker.sock文件挂载到容器，容器中无法调用宿主机docker环境；
         2. **解决：修改启动命令，例如：docker run -d --name fitlab-runner --restart always -v /opt/gitlab_runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab-runner**
         3. 参考地址：<https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/>
      2. Cannot connect to the Docker daemon at unix:/var/run/docker.sock.
         1. 原因：Gitlab Runner任务作业中调用的镜像无Docker环境，也无法调用宿主机的Docker;
         2. 解决：修改/usr/gitlab-runner/new_config2/config.toml的volumes为3.中的值，重启Runner 容器；

### .gitlab-ci.yml文件 [配置关键字列表](https://docs.gitlab.cn/jh/ci/yaml/)

1. yml脚本示例以及对应的Dockerfile示例
    yml脚本

    ```yml
    image: node:alpine # 指定要运行作业的默认镜像，会以该镜像生成一个容器来执行job的script

    stages:
        - install
        - build
        - deploy

    cache:
        key: zacks_home_key
        paths:
            - node_modules

    job_install:
        stage: install
        tags:
            - dont_cry_tag
        script:
            - npm install

    job_build:
        stage: build
        tags:
            - dont_cry_tag
        script:
            - npm run build

    job_deploy:
        stage: deploy
        image: docker # 指定镜像为docker，创建一个docker容器来执行job的script
        tags:
            - dont_cry_tag
        script:
            - docker build -t zacks_home_image . #这里会以当前目录下的Dockerfile文件（下面有例子）为镜像创建一个容器，并且会存放在宿主机目录（linux）中运行，因为我们挂在了docker.sock目录到宿主机
            - if [ $(docker ps -aq --filter name=zacks_home_container) ]; then docker rm -f zacks_home_container;fi
            - docker run -d -p 8083:80 -v /srv/gitlab-runner/new_backup_nginx.conf:/etc/nginx/nginx.conf --name zacks_home_container zacks_home_image # 把宿主机的8083端口映射到容器的80端口，这个时候宿主机运行 docker ps 会发现有一个名为zacks_home_container的容器在运行
            # 此外我们还挂载了nginx.conf文件，用于开启容器的gzip压缩，如果不需要开启gzip，可以将上面命令中的 -v /srv/gitlab-runner/new_backup_nginx.conf:/etc/nginx/nginx.conf 这一段去掉
            # 记得要放行宿主机的8083端口，不是宝塔面板的端口（对应操作系统的8083），而是腾讯云面板的端口（对应云平台的8083），需要进入腾讯云的首页进行配置，否则使用ip:8083仍然无法访问你的网页

    ```

    对应的Dockerfile文件

    ```Dockerfile
    FROM node:latest as builder
    WORKDIR /app
    COPY package.json .
    RUN npm install
    COPY . .
    RUN npm run build

    FROM nginx:latest
    COPY --from=builder /app/dist /usr/share/nginx/html
    ```

2. 脚本/命令会被gitlab-runner自动执行，分为多个stage，每个stage(阶段：默认值有` .pre/build/test/deploy/.post `这5个阶段)可以有多个job。同一阶段的作业并行运行，下一阶段的作业在上一阶段的作业成功完成后运行。未指定stage的作业会被分配到test（默认）阶段。**任务顺序并不是按照job定义顺序执行，而是按照阶段定义的顺序执行。**

    ```text
    graph LR
    A[pipeline]-->B[stage: build]
    A-->C[stage: deploy]
    A-->D[stage: other_stages]
    B-->E[job: job1]
    B-->F[job: job2]
    ```

3. workflow关键字（放在顶层定义）来确定是否创建流水线，**和job中的only字段/rules字段类似**。比如以下配置，表示当commit标题不以draft结尾 或者 是一个和并请求 或者 提交的分支是默认分支（现在的默认分支是main）时会运行pipeline

    ```bash
    workflow:
    rules:
        - if: $CI_COMMIT_TITLE =~ /-draft$/
        when: never
        - if: $CI_PIPELINE_SOURCE == "merge_request_event"
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    ```

4. 语法验证：项目ci/cd→pipeline→CI lint 或者 vscode的GitLab Workflow扩展

## Vue全家桶

### Vue2

#### 用法

1. 生命周期：生命周期可以理解为三部分，第一部分为初始化 Vue 实例的过程，第二部分为数据渲染，更新 Dom 的过程，第三部分为销毁实例的过程，每一个部分都有对应的钩子函数来完成对应的部分过程
   1. beforeCreate：会在Vue实例创建前（未创建实例，所以data等选项都无法得到，更不能访问this）、props 解析之后立即调用
   2. created：在Vue实例创建完成后调用（已创建实例所以data、methods等都可以访问），但是模板还未获取，因此**无法操作DOM**，`$el`未初始化（`undefined`）
   3. beforeMount：数据挂载前调用，已经获取模板（模板编译完成生成虚拟DOM），但是数据未挂载到实际DOM上
   4. mounted：数据挂载完成，data.message 成功渲染，页面完成渲染（一般在此阶段进行ajax操作）
   5. beforeUpdate：data变化时，DOM更新前调用
   6. updated：data变化时，DOM更新后调用
   7. beforeDestroy：组件销毁前（清除定时器，解绑自定义的DOM事件，比如：window、scroll）
   8. destroyed：组件销毁
2. 发送请求应该选择哪个生命周期？
   1. 理论上，created、beforeMount、mounted都可以发送请求，因为实例已经创建，data可以随时改变。实际开发中通常选择created和mounted这两个选项，但是如果是需要操作DOM，由于在created中还未获取模板，无法操作DOM，则应该在mounted中发起请求（或者使用`this.$nextTick()`）。实际上如果不考虑DOM操作，请求放在created和mounted中并没有什么明显的区别，但是放在created中请求具有前置性，仅仅单纯的获取数据可能还是会快那么一点点。
3. 父子组件生命周期执行顺序：
   1. 挂载阶段：父 beforeCreate -> 父 created -> 父 beforeMount -> 子 beforeCreate -> 子 created -> 子 beforeMount -> 子 mounted -> 父 mounted  。需要等待子组件挂载完毕后父组件才能挂载完。
   2. 更新阶段：父 beforeUpdate -> 子 beforeUpdate -> 子 updated -> 父 updated  。 与挂载阶段类似。
   3. 销毁阶段：父 beforeDestroy -> 子 beforeDestroy -> 子 destroyed -> 父 destroyed  。
4. 自定义指令
   1. 注册一个v-focus指令的代码示例，包含5个钩子：

        ```javascript
        //全局指令注册
        Vue.directive('focus', {});
        //局部指令注册
        directives: {
            focus: {
                // 指令的定义
                bind: function(...args){},//指令第一次绑定到元素时调用。在这里可以进行一次性的初始化设置
                inserted: function (el, binding, vnode) {//// 当被绑定的元素插入到 DOM 中时调用inserted钩子
                    el.focus();
                },
                update: function(...args){},//所在组件的 VNode 更新时调用
                componentUpdated: function(){},//指令所在组件的 VNode 及其子 VNode 全部更新后调用。
                unbind: function(...args){},//指令与元素解绑时调用
            }
        }
        ```

   2. 其中，每个钩子的参数列表args包含以下几个对象：
      1. `el`：指令绑定的DOM元素
      2. `binding`：数据集对象，包括以下属性：
         1. `name`：指令名，不包括 v- 前缀。
         2. `value`：指令的绑定值，例如：v-my-directive="1 + 1" 中，绑定值为 2。
         3. `oldValue`：指令绑定的前一个值，仅在 update 和 componentUpdated 钩子中可用。无论值是否改变都可用。
         4. `expression`：字符串形式的指令表达式。例如 v-my-directive="1 + 1" 中，表达式为 "1 + 1"。
         5. `arg`：传给指令的参数，可选。例如 v-my-directive:foo 中，参数为 "foo"。
         6. `modifiers`：一个包含修饰符的对象。例如：v-my-directive.foo.bar 中，修饰符对象为 { foo: true, bar: true }。

#### 响应式原理

##### MVVM

1. 数据变化，视图也会自动变化，不像传统的MVC模式需要控制层来手动操作DOM改变视图
2. Vue的数据变化是非侵入性的：不需要调用其他任何API（比如react的 `this.setState`或者小程序的 `this.setData` ：为侵入式的），而是直接操作data数据
3. 为什么说Vue数据变化是非侵入式的呢？因为他的data的property被Object.defineProperty劫持了setter/getter，只要直接执行data.property读写操作就能够触发对应的getter/setter函数

##### Object.defineProperty()

1. 用法示例

    ```javascript
    Object.defineProperty(
        obj,
        'prop_name',
        {
            //value: undefined, //设置属性值，和get互斥，一起设置会报错
            //writable: false, //是否可以被赋值运算符改变，为false改值操作无效
            //configurable: false, //该属性能否被删除
            set: undefined,
            get: undefined,
            //enumerable: false, //是否可以被枚举，为true时可以被for(let key in obj)或者Object.keys()遍历
        })
    ```

2. 设置getter和setter的属性key，进行key++操作时会同时访问getter（先）和setter（后）属性
3. **缺点**：不能够直接在Object.defineProperty的getter/setter中对obj的属性进行读写，因此需要使用一个临时变量来保存这个属性值

##### defineReactive函数

使用闭包保存属性值 + `Object.defineProperty()`劫持setter/getter，相当于对`Object.defineProperty()`进行了一次封装，使用闭包提供了一个临时变量对getter/setter进行拦截

```javascript
function defineReactive(data,key,val){//val相当于一个初始值，是闭包中保存变量的中转值    
    Object.defineProperty(data,key,{
        configurable:true,//设置成可配置，比如可以被delete        
        enumerable:true,//可枚举，可以被Object.keys()或者for in遍历        
        get(){            
            console.log("你访问obj的"+key+"属性");            
            return val;        
        },        
        set(new_value){            
            console.log(`你试图修改obj的${key}属性`);            
            if(new_value===val){ return; }            
            val=new_value;       
        }    
    })
}
let obj={};
defineReactive(obj, 'my_key',10);//vue2为对象设置一个响应式属性，初始值为10
```

##### 递归侦测对象全部属性

1. **Observer类**

   1. Observer是什么：将一个正常的obj转化成任何属性（嵌套/非嵌套）都能被侦测到的工具类——**观察者**，也就是每个层级的属性都是响应式的
   2. 嵌套对象的内部属性无法通过defineReactive实现响应式，因为Object.defineProperty无法对obj.m.n属性进行劫持的能力：~~Object.defineProperty(obj, 'm.n', {set(){}})~~，需要递归侦测

2. **observe函数**
   1. 具体实现

       ```text
       A[observe方法, 参数是value]
       B[value有没有__ob_, 其实__ob__就是用于存放Observer]
       A-->B
       B--如果没有__ob__属性-->C[new一个Observer添加到value.__ob__上,<br/> 通过在Observer构造函数中调用def函数实现]
       D[Observer内包含一个walk方法: <br/>遍历下一层属性并且逐个设置defineReactive]
       C--value不是数组-->D
       C--value是数组-->G[将value的原型设置成新建的arrayMethods对象,<br/> 对象内重写了push等数组方法]
       E[设置某个属性值的时候, 触发set的new_value也需要observe]
       D-->E-->A
       ```

3. 递归顺序：是循环调用，而不是自己调用自己：`F[observe]-->G[Observer类]-->H[defineReactive函数]-->F[observe]`

##### 数组的响应式原理

1. 修改原型并且遍历数组的每个元素，并且observe每个元素
2. 对数组的7个方法进行改写：push、pop、shift（删除）、unshift、splice、sort、reverse，这7个方法（methodsToPatch）都是定义在Array.prototype上。如何改写？
    1. 以Array.prototype为原型创建一个对象，对新创建的arrayMethods对象的上述7中方法进行重写，需要用保存数组原有的push等方法，因为我们只需要在原有的7个方法上添加观察操作

        ```javascript
        var arrayProto = Array.prototype;
        var arrayMethods = Object.create(arrayProto);
        methodsToPatch.forEach(function (method) {
            const original = arrayProto[method];//保存原有的数组操作
            def(arrayMethods, method, function () {
                console.log ( "对methodsToPatch数组的7个方法进行重写的代码" ) ;
                console.log ( "function内的this指向数组本身，this时包含__ob__属性的" ) ;
            })
        })
        ```

    2. 在Observer类的constructor判断value的类型，**修改为如果是对象则仍然执行this.walk(value)，如果是数组则设置 `value.__proto__=arrayMethods`**（或者也可以用es6的 `Object.setPrototypeOf(value, arrayMethods)`）将value的原型设置为新建的arrayMethods对象
    3. **push、unshift、splice三种方法需要特殊处理**，因为可能会造成数组元素增加，需要对新增的元素也进行observe观察（`[...arguments].slice()`取新添加的元素数组），由于def函数重写push等方法时，function内的this仍然指向数组本身，因此可以直接使用`this.__ob__.observeArray`进行新增元素数组的观察
    4. 重写function的结尾记得要返回 original.apply(this, arguments)的结果

##### 核心流程（依赖收集/触发）

1. 用到数据的地方(组件)称为依赖
2. **具体响应流程**：
   1. **添加dep实例**：初始化时，首先递归遍历对象的每一层每一个属性，为每个属性都添加一个`dep`实例，`dep`中保存了`watcher`列表，最开始这个列表是空的
   2. **getter中收集依赖**：假如模板中有个对象/对象属性组成的字符串，会创建一个对应的`watcher`订阅者，构造函数中会解析该字符串并得到对应的数据（官网称为手动“touch”一下，即触发`getter`），并且把当前的`watcher`实例赋值给一个全局对象`Dep.target` → `getter`被触发，执行依赖收集：`dep.depend(Dep.target)`，在当前的`watcher`列表中添加当前的`watcher`（也就是`Dep.target`）
   3. **setter中触发依赖**当改变数据时，触发数据的setter，调用`dep.notify()`，该函数会遍历当前数据的watcher列表，并且执行对应的update方法 → 执行对应的回调函数
3. 其它：
   1. **Dep在哪儿实例化**
      1. `defineReactive`中：
         1. 使用闭包保存了一个实例，这是为了管理普通对象的每个属性的依赖而服务的
      2. `Observer`构造函数中
         1. 服务数组、以及增删属性的方法`Vue.set`、`Vue.delete`：
            1. 数组：数组不能和普通对象那样通过`Object.defineProperty()`劫持getter/setter，所以对更改原数组的7个方法进行依赖更新时需要一个额外的的dep实例来处理
            2. `Vue.set`：对象新增的属性并没有被添加Observer实例（因为Vue2中的响应性是数据初始化的时候就建立好了，直接动态添加的属性不具有响应性），因此需要额外使用`dep`来设置相应性
            3. `Vue.delete`：`Object.defineProperty()`无法拦截对象属性的删除操作，因此删除对象属性需要额外的方法`Vue.delete`来实现响应性
   2. 依赖触发和依赖收集的位置
      * dep.depend：
             1. defineProperty(data, key, val)的**getter**中 `dep.depend()`
             2. defineProperty的getter中如果子元素是一个对象 `childOb.dep.depend();`
             3. ...
      * dep.notify（4个地方调用）：
          1. defineProperty(data, key, val)的**setter**中 `dep.notify()`
          2. 数组重写的7个方法中：`ob.dep.notify()`
          3. `Vue.set`以及`Vue.delete`中：`ob.dep.notify();`
   3. Watcher是一个中介，保存了所watch对象的依赖回调函数，每当数据发生变化时通知组件的Render Function更新组件

##### 虚拟Dom：使用JS表达Dom结构

1. 变迁
   1. jq时代：DOM操作 → 视图更新
   2. vue时代：**数据改变 → 虚拟DOM（计算）→ 操作真实的DOM → 视图更新虚拟DOM就是通过js模拟dom结构**
2. V8引擎执行JS速度比直接操作Dom要快很多，因此使用类似于AST抽象语法树的概念将真实的Dom结构转化成一个个JS对象，使用JS表达Dom结构
    * **tag**：标签名为对象的tag属性
    * **props**：标签的style、class等转化成props属性内的style、className等子属性
    * **children**：标签的子标签数组
    * ...
3. 虚拟Dom先计算完成之后，再一次性更新实际Dom结构（以最小代价--diff算法--来实现Dom更新，提升性能）
4. 为什么要使用虚拟Dom？
    1. 数据驱动视图，Dom操作交给框架来操作
    2. 高效地控制Dom操作（diff算法）

##### 虚拟Dom核心：diff算法（vue2版本）

```javascript
//以下是vue源码的注释，vue的diff算法参考了Snabbdom
  /**
   * Virtual DOM patching algorithm based on Snabbdom by
   * Simon Friis Vindum (@paldepind)
   * Licensed under the MIT License
   * https://github.com/paldepind/snabbdom/blob/master/LICENSE
   *
   * modified by Evan You (@yyx990803)
   */
```

1. 介绍
   1. 找出两个对象（把DOM看程两棵语法树）之间的差异，目的是尽可能做到节点复用
   2. 传统的diff算法复杂度达到O(n^3)，效率低下
   3. vue对diff的优化：优化后的时间复杂度为O(n)
       1. 只比较同一个层级，不会跨级比较
       2. 标签名不同，直接删除，不继续在该节点往下进行深度比较
       3. 标签名相同且key值相同的时候，不会继续进行深度比较

2. 具体实现

   1. vnode（虚拟节点）结构：真实Dom的AST

       ```javascript
       var VNode = function VNode (
           tag, // 标签名称
           data, // 标签属性（事件、属性等）
           children, //子元素
           text, //children和text只会有一个，必有一个为undefined，也就是说标签内要么就是子元素数组，要么就是字符串
           elm, // 对应的真是Dom元素
           context, 
           componentOptions,
           asyncFactory
       )
       ```

   2. 实现一个patch函数

      1. patch ( oldVnode: Vnode | Element , vnode: Vnode)使用场景：
          * 会在**首次页面渲染**的时候执行一次，将vnode渲染到真实Dom（对应Element）上；
          * 其次`patch(vnode, new_vnode)`会将新的vnode替换掉老的vnode；
      2. patch实现流程：

          ```text
          A[patch函数接收oldVnode和vnode两个参数, 判断oldVnode是否为vnode类型]
          A--不是-->B[oldVnode=创建一个空的vnode并且关联Dom]
          C[通过sameVnode函数判断oldVnode和vnode是否为相同的Vnode  比较key/tag/等]
          A--是-->C
          B-->C
          C--是-->D[patchVnode]
          C--否-->E[创建新的Dom元素, 插入新的Dom元素并且移除老的Dom元素]
          ```

   3. patchVnode（包含大量的if else运算）
      1. 比较oldVnode和vnode
      2. 如果相同，直接返回
      3. 如果不同，一切以新的vnode为准
      4. 新vnode有children，旧的需要删除原来的text，添加新的children，或者更新children（更新children使用updateChildren函数）
      5. 新vnode有text，旧的需要删除原来的children，添加新的text，或者更新text
      6. PS：children 和 text 是互斥的，不会同时存在。

   4. updateChildren函数

      1. 判断sameVnode( oldStartVnode, newStartVnode)老的开始虚拟节点和新的开始虚拟节点是否同一个，是同一个的话指针往右移动（*startVnode往右移，*endVnode往左移）
      2. 同上继续判断：老结束vs新结束、老开始vs新结束、老结束vs新开始
      3. 以上4中sameVnode情况都没有命中，则比较key值
      4. 拿到新开始的key，在老children里找对应的key，没有找到则创建新元素；如果老children中找到了对应的key，再对比tag是否相等
      5. 如果tag不相等，那么说明仍然不是相同的节点，同样创建新的元素并插入
      6. 如果tag相等（key相等+tag相等），和patch函数一样执行patchVnode

##### 原理总结

1. 响应式原理总结：
   1. 判断数据是否为对象/数组（为数据添加Observer实例、并且执行defineReactive()函数，数组修改7个原型方法）
      1. 是普通对象则遍历对象每一个属性，使用Object.defineProperty()方法拦截getter/setter
      2. 是数组则修改该数组能够引起数据变化的7个原型方法
      3. 是其它的基本类型，则不需要处理
   2. getter中收集依赖，setter中触发依赖（Dep使用发布订阅模式实现，当数据发生变化时，会循环依赖列表，对每个依赖执行回调update）
      1. dep.depend（收集依赖方法）：
          1. defineProperty(data, key, val)的**getter**中 `dep.depend()`
          2. defineProperty的getter中如果子元素是一个对象 `childOb.dep.depend();`
      2. dep.notify（触发依赖/回调方法，4个地方调用，更新数据需要引起视图更新，通过此方法实现） ：
          1. defineProperty(data, key, val)的**setter**中 `dep.notify()`
          2. 数组重写的7个方法中：`ob.dep.notify()`
          3. `Vue.set`以及`Vue.delete`中：`ob.dep.notify();`
2. diff算法总结（基于snabbdom）：
   1. 节点抽象为对象，DOM抽象为一颗语法树
   2. 更新DOM树时会对两棵树每个节点都进行比较，如果数据不一致，一切以新节点的数据为准执行创建、修改、删除等
   3. 复用节点：**key相等，tag相等则视为同一个元素**，无需修改

### Vue3

#### Vue3用法

1. 初始化：`npm init vue@latest`
    1. 创建项目报错：`TypeError: (0 , import_node_util.parseArgs) is not a function`，node版本更新到18以上即可：[下载nvm](https://github.com/coreybutler/nvm-windows/releases) → 安装 + 重启（否则无法使用nvm指令）
       1. nvm list ，查看已经安装的node版本，`*`表示当前正在使用的版本
       2. nvm list available，查看可安装的node版本
       3. nvm install 14.14.0，安装某个固定版本
       4. nvm use 14.14.0，使用某个固定版本的use
2. 入口main.js

    ```javascript
    import './assets/main.css'

    import { createApp } from 'vue'
    import App from './App.vue'
    import router from './router'

    const app = createApp(App);
    app.use(router);
    app.mount('#app');
    ```

3. 通过createApp来创建一个实例，而当一个页面很大时，应该创建多个实例，每一个实例负责各自的部分功能，避免一个实例太大了
4. 创建实例
   1. 通过Vue.createApp创建**根组件**：`createApp(App).use(store).use(router).mount('#app')`
   2. 如下：mount函数返回的不是应用本身，而是根组件实例（vm通常用来表示组件实例）

       ```javascript
       const app = Vue.createApp({
       data() {
           return { count: 4 }
       }})
       const vm = app.mount('#app')
       console.log(vm.count) // => 4
       ```

   3. 生命周期与vue2的不同之处：**beforeUnmount、unmounted**替换原来的**beforeDestroy以及destroyed**
   4. 不要在选项或者回调中使用箭头函数，会报undefined错误：Uncaught TypeError: Cannot read property of undefined
5. 单文件组件，使用`<script setup></script>`进行开发，代码写在`<script setup></script>`内，如果使用的是选项式API，仍然可以使用`setup()`选项来插入和集成组合式API代码
6. 模板语法
   1. 一次性插值：`v-once`，第一次绑定之后就丧失响应性： `<span v-once>{{msg}}</span>`
   2. `v-html`可能会非常危险，容易导致XSS攻击，**只对可信内容使用 HTML 插值，绝不要将用户提供的内容作为插值**
   3. `v-bind`
   4. `v-on`
   5. . `v-for`
   6. `v-if/v-else`
   7. 修饰符（`.prevent`调用`event.preventDefault()`）
7. data属性以及方法
   1. 组件data是一个函数，保存在组件实例的`$data`中：`vm.$data`
   2. methods
   3. 防抖节流：官网推荐使用lodash等库实现
8. Class与Style的绑定
   1. `v-bind:class`语法与vue2保持一致
   2. 组件my_comp只有单个根元素（假定为一个带efg的class的div），则父组件中的`<my_comp class="abc"></my_comp>` 的class会被应用到该根元素上（属性透传）：`<div class="efg abc"></div>`
   3. 如果有多个根元素则不会透传，需要子组件手动从$attrs.class获取
   4. `v-vind:style`语法与vue2保持一致

    ```html
    <div v-bind:style="{ color: activeColor, fontSize: fontSize + 'px' }"></div>
    ```

9. 条件渲染
   1. `v-if/v-else/v-else-if` ： 使用带`v-if`的`template`元素包裹，最终`template`不会被渲染
   2. `v-show`
   3. `v-if`与`v-show`对比：
       * v-if是控制渲染，组件被销毁或者重建，**内部的状态会被重置和清除**，v-show是控制css的display样式为默认值/none，**组件内部的状态不会被清除**，并**且v-show不能用在多根节点组件以及template标签上**，如果需要，需要把组件改为单个根节点的组件在外层包裹一个`<div></div>`
       * v-if初始条件为假的时候不进行渲染，直到为真的时候才渲染；v-show不论初始条件真假总会被渲染，仅仅进行css切换。一般来说，v-if 有更高的切换开销，而 v-show 有更高的初始渲染开销。因此，如果需要非常频繁地切换，则使用 v-show 较好；如果在运行时条件很少改变，则使用 v-if 较好。
       * **vue2/vue3中的区别**：同级使用，在vue2中v-for的优先级更高，vue3中v-if的优先级更高，并且vue3中会报错。（vue2源码使用if/else判断对v-for的判断条件在v-if前，vue3中使用switch判断并且对v-if的判断case在v-for前）
10. 列表渲染
    1. `v-for="item in items"`也可以使用`v-for="item of items"`更符合js的迭代器用法
    2. 也可以遍历对象的属性（以`Object.keys()`结果进行遍历）：`v-for="(value, name, index) in myObject"`
    3. 你需要为每项提供一个唯一的 key（字符串/数组类型），作为 Vue 的虚拟 DOM 算法的提示，以保持对节点身份的持续跟踪。这样 Vue 就可以知道何时能够重用和修补现有节点，以及何时需要对它们重新排序或重新创建
11. 事件处理
    1. `v-on:click`/`@click`，接收原生的Dom Event：调用方法时传入`$event`
    2. 多事件处理器：`@click="one($event), two($event)"`，可以同时触发两个事件，不过必须加上括号()否则会把它看成普通的js变量语句（同vue2）
    3. 按键修饰符 ······
12. 表单输入绑定
    1. text 和 textarea 元素使用 value property 和 input 事件；
    2. checkbox 和 radio 使用 checked property 和 change 事件；
    3. select 字段将 value 作为 prop 并将 change 作为事件。
13. 组件基础 + 深入 + props
    1. 全局注册：`app.component` ；局部注册：通过components选项来引入自定义的组件，与vue2保持一致
    2. **props是单向数据流**，单向下行绑定：父级 prop 的更新会向下流动到子组件中，但是反过来则不行。
    3. **props对象是只读的**。如果在子组件内部改变prop，控制台vue会发出警告。如果子组件有需要改动，可以把prop赋值到data/computed中，但是引用类型仍然会改动父组件状态，因此应该避免修改任何 prop

        ```javascript
        const props = defineProps(['my_id']);
        console.log("props：", props);
        console.log("isReactive(props)/isProxy/isRef/isReadonly：", isReactive(props), isProxy(props), isRef(props), isReadonly(props));
        //true true false true
        //props是一个代理，即和一个reactive()定义的响应式对象是一样的

        //isReactive：是否为 reactive() 或 shallowReactive() 创建的代理
        //isProxy：是否是由 reactive()、readonly()、shallowReactive() 或 shallowReadonly() 创建的代理
        //isRef：是否为 ref
        //isReadonly：是否为只读对象。只读对象的属性可以更改，但他们不能通过传入的对象直接赋值。通过 readonly() 和 shallowReadonly() 创建的代理都是只读的，因为他们是没有 set 函数的 computed() ref。
        ```

    4. 监听子组件事件/自定义事件：同vue2一样使用`emit`，**但是vue3新增了emits选项：用来定义一个组件可以向其父组件触发的事件**
    5. 组件中使用v-model
        1. 父组件
            * prop：`:value` -> `:modelValue='my_value'`；
            * 事件：`@input` -> `@update:modelValue='my_value=$event'`
        2. 子组件中要定义默认的props:`modelValue`，并且需更改父组件值时调用`$emit('update:modelValue',$event.target.value)`
    6. 动态组件：使用conponent元素和`is=xx`来定义一个动态组件,使用`keep-alive`标签来缓存组件的状态

        ```html
        <keep-alive>
        <component :is="currentTabComponent"></component>
        </keep-alive>
        ```

    7. 异步组件：使用`defineAsyncComponent`包裹一个返回值为`Promise`的函数，如：

        ```javascript
        const AsyncComp = defineAsyncComponent(() =>
        import('./components/AsyncComponent.vue')
        )
        ```

    8. 封装一个可复用的组件，需要满足什么条件？（低耦父传校不改，抛出事件父处理）
       1. 低耦合，组件之间的依赖越小越好
       2. 最好从父级传入信息，不要在公共组件中请求数据
       3. 传入的数据要进行校验
       4. 不要直接修改父组件的状态
       5. 处理事件的方法写在父组件中
       6. 合理的扩展性：插槽等等
14. `defineProps` 和 `defineEmits` 都是**只**能在` <script setup> `中使用的编译器宏。他们**不需要导入**，且会随着 `<script setup>` 的处理过程一同被编译掉，选项式写法需要使用props、emits选项。
15. ref
    1. ref优点
        1. ref不像**reactive**一样**只能处理对象类型**，它可以处理任何类型。如果将一个对象obj赋值给ref，`ref(obj)`会将obj包装成一个带`.value`属性的ref对象，并且会在内部调用`reactive()`将该对象转为具有**深层**次响应式的对象。在setup内部访问的时候需要通过`.value`访问（除非是嵌套在一个响应式对象内），所有对 `.value` 的操作都将被追踪，并且写操作会触发与之相关的副作用。在模板中访问则会自动解包，不使用.value访问
        2. ref作为参数传递的时候，**通常**传递的是一个带value属性的响应式对象（整个ref都会传递），因此不会丢失响应性

        ```javascript
        const o1 = {
            a: 1
        }
        const ref_obj1 = ref(o1);
        console.log(ref_obj1, isRef(ref_obj1), isReactive(ref_obj1), isProxy(ref_obj1));// RefImpl true false false
        console.log(ref_obj1.value, isRef(ref_obj1.value), isReactive(ref_obj1.value), isProxy(ref_obj1.value));// Proxy{a: 1} false true true

        const o2 = "abc";
        const ref_sp = ref(o2);
        console.log(ref_sp, isRef(ref_sp), isReactive(ref_sp), isProxy(ref_sp));// RefImpl true false false
        console.log(ref_sp.value, isRef(ref_sp.value), isReactive(ref_sp.value), isProxy(ref_sp.value));// abc false false false

        const o3 = "zzz";
        const temp = reactive(o3);//控制台警告，不能将原始值转化为reactive，本用法无效
        console.log(temp, isReactive(temp));//zzz false

        setTimeout(() => {
            ref_obj1.value.a++;
            setTimeout(() => {
                const temp = { b: 22 };
                ref_obj1.value = temp;//这个替换也是响应式的
            }, 2000);
        }, 2000)
        ```

    2. 在模板中，ref仅会对顶层属性解包，也就是说只有ref包裹在对象最外层，才能够合理解包（不写.value），如果内层是ref而最外层不是，则解包会出问题，比如`const result = ref({ b: 1 });`时模板中的`{{result.b + 1}}`会显示2，而如果是`const result = { b: ref(1) }`则会显示[object Object]1
    3. 有时候如果数据结构太大，并且内部的很多细节并不需要监听，则可以使用shallowRef来进行优化，shallowRef只会对对象的第一层属性进行响应式监听，不会深层递归地转为响应式
    4. 当ref被嵌套在响应式对象内部时，会自动解包，表现得和普通属性一样

        ```javascript
        const count = ref(0)
        const state = reactive({
            count
        })

        console.log(state.count) // 0

        state.count = 1
        console.log(count.value) // 1
        ```

    5. 响应式数组/Map类型中的ref元素不会被解包，需要通过.value来手动解包

        ```javascript
        const books = reactive([ref('Vue 3 Guide')])
        // 这里需要 .value
        console.log(books[0].value)

        const map = reactive(new Map([['count', ref(0)]]))
        // 这里需要 .value
        console.log(map.get('count').value)
        ```

16. reactive
    1. reactive创建响应式对象/数组(**reactive本质就是原对象的一个代理，ref返回一个带value属性的对象并且劫持了对象的getter/setter**)，对基本类型无效，如果是定义在setup函数内部，需要通过return该响应式对象才能在模板内部使用；或者更流行的方式是使用带setup标记的script标签来写代码，这样就会自动把标签内部第一层的变量暴露，更简单方便
    2. reactive() 将**深层地**转换对象：当访问嵌套对象时，它们也会被 reactive() 包装。*当 ref 的值是一个对象时，ref() 也会在内部调用它*

        ```javascript
        function ref(target) {
            // 如果将对象分配为ref值，则将它被处理为深层的响应式对象。故调用reactive函数
            target = reactive(target)
            return {
                // 保存target数据保存起来
                _value: target,
                get value() {
                    console.log('劫持到了读取数据')
                    return this._value
                },
                set value(val) {
                    console.log('劫持到了修改数据,准备更新界面', val)
                    this._value = val
                }
            }
        }
        ```

    3. reactive默认是深层响应式，即嵌套对象每一层都是响应式的，如果只需要对象根级别属性响应式，可以使用shallowReactive
    4. shallowReactive(obj)只对对象第一层进行一次代理，不关心内部嵌套对象，基本类型直接返回
    5. reactive(obj)在shallowReactive实现的基础上递归每个属性/数组每个元素，判断其是否为对象/数组，是对象/数组则需要额外再代理该对象（即遍历所有属性并对每个属性调用reactive），直到遇到基本类型直接返回

        ```javascript
        const state = shallowReactive({
            foo: 1,
            nested: {
                bar: 2
            }
        });
        // 更改状态自身的属性是响应式的
        state.foo++;
        //顶层是响应式的
        console.log(isReactive(state));//true
        // ...但下层嵌套对象不会被转为响应式
        console.log(isReactive(state.nested)); // false
        // 顶层往下不是响应式的
        state.nested.bar++;
        function increment() {
            state.nested.bar++;//方法改变state.nested内部属性改变不会触发响应式更新，只有模板内其它响应式属性改变了，这个非响应式数据的视图才会随着一起更新
        }
        ```

    6. 对同一个对象的调用多次reactive()，都会返回同一个代理：`reactive(obj) === reactive(obj)` ； 对一个代理调用reactive()也会返回该代理本身：`const r = reactive(obj); console.log(reactive(r) === r); // true`
    7. reactive深层响应式对象内部的嵌套对象也默认是响应式，所以**动态添加的属性（值为对象）也是响应式**，无需和Vue2那样使用`Vue.set` 或者 `this.$set`；并且下面例子中虽然将raw赋值给了me.nested，d但是此时me.nested会是一个Proxy，所以`me.nested === raw`会返回false

        ```javascript
        const me = reactive({});
        const raw = { a: "hello" };
        setTimeout(() => {
            me.nested = raw;//两秒钟后视图也会动态更新
            console.log(me.nested === raw);// false
        }, 2000);
        ```

    8. reactive局限性：
        1. **只对对象类型有效，基本类型无效**
        2. **不能替换reactive内部的对象，替换了的话原引用对象的模板/DOM数据会丧失响应性！！**（其实很好理解，reactive本质是一个proxy，变量存放的是这个Proxy的一个**引用**，模板数据是和Proxy本身进行响应，而不是这个变量，因此替换掉目标对象的话，引用指向的是新对象的代理，而原对象的响应式由于没有被引用，自然就丢失了；而ref由于把目标对象盛放在一个ref对象的value属性上，替换目标对象只是影响了ref的value属性而已，而ref对象本身就是响应式的，因此可以整体替换。reactive替换之后原对象Proxy虽然失去了响应性，变成了“孤立”的对象（reactive对象不再引用原对象），原对象仍可能被其他部分代码或组件引用，从而使其保持在内存中，但是Vue会清理这些无用的对象，不会轻易导致内存泄漏）

            ```javascript
            let state = reactive({ a: 1, b: 2 });
            const abc = ref({ aref: 1, bref: 2 });
            setTimeout(() => {
                state = reactive({ a: 2, b: 10 });
                abc.value = { aref: 2, bref: 10 };
                console.log(state, abc);//两个变量的值都变了，但是reactive对应的视图并不会更新！！！！
            }, 3000);
            ```

        3. 将响应式对象的属性（前提条件是该属性为原始类型）赋值或解构至本地变量时，或是将该**属性**传入一个函数时，我们会失去响应性；ref作为参数传递的时候，**通常**传递的是一个带value属性的响应式对象（整个ref都会传递），因此不会丢失响应性

            ```javascript
            const s = reactive({
                count: 1,
                obj: { a: 1 }
            });
            setTimeout(() => {
                let count = s.count;
                count++;//普通属性（原始类型）赋值/解构到本地时，会失去响应性，s.count对应的视图不会更新
                const obj = s.obj;
                obj.a++;//对象赋值/解构，不会影响，s.obj.a对应视图更新
            }, 2000);
            ```

17. 计算属性：`const comp_data = computed(() => { return some_data.some_prop })`
    1. 计算属性接收一个getter函数，返回一个缓存的**ref，注意，返回值是一个ref**
    2. **计算属性默认是仅可读的**，也不应该修改（如果仅仅传入一个getter函数，修改的时候会提示警告并且修改失败：Write operation failed: computed value is readonly）；但是如果特殊场景必须修改，可以传入一个带getter和setter的对象，这样可以通过comp.value = "some_value"来调用setter，从而实现组件上的v-model：

        ```javascript
        const comp = computed({
            get() {
                return rr.discription;
            },
            set(new_val) {
                rr.discription = new_val;
            }
        });

        //可以用来实现组件上的v-model，只需要绑定modelValue属性并且setter中触发update:modelValue方法即可
        const props = defineProps(['modelValue'])
        const emit = defineEmits(['update:modelValue'])

        const value = computed({
            get() {
                return props.modelValue
            },
            set(value) {
                emit('update:modelValue', value)
            }
        })
        ```

18. 表单中的v-model
    1. v-model在表单中其实就是一个语法糖：`<input :value="text" @input="text = $event.target.value">`
    2. v-model在表单中会忽略DOM元素设置的value，checked，selected属性，而以js中绑定的数据作为正确的数据源
    3. v-model的修饰符：`v-model.lazy=""`、`v-model:name.lazy=""`
       1. v-model.lazy：数据只会在 "change" 事件触发（输入框失去焦点或按下回车键）时更新，而不是在每次 "input" 事件触发时立即更新
       2. v-model.number：输入自动转化为数字
       3. v-model.trim：去掉输入内容两端的空格
19. 组件上的v-model
    1. v-model在组件上被展开成如下形式：

        ```vue
        <!-- 原来是： -->
        <CustomInput v-model="searchText" />
        <!-- 展开是： -->
        <CustomInput
            :modelValue="searchText"
            @update:modelValue="newValue => searchText = newValue"
        />
        ```

        ```vue
        <!-- CustomInput.vue -->
        <script setup>
        defineProps(['modelValue'])//在选项式写法中使用props选项
        defineEmits(['update:modelValue'])//选项式写法中使用emits选项
        </script>

        <template>
        <input
            :value="modelValue"
            @input="$emit('update:modelValue', $event.target.value)"
        />
        </template>
        ```

    2. 需要通过`defineProps`或者`props`选项指定prop，默认为`modelValue`，需要通过`defineEmits`或者`emits`选项指定事件名称，默认为`update:modelValue`
    3. 通过`$emit('update:modelValue', $event.target.value)`来触发事件和传递参数
    4. v-model参数可以修改：prop名称和事件名称都可以修改而不是用默认值，即绑定prop为other_name，触发事件设置为`update:other_name`即可，和默认的v-model相比只有模板中调用时需要加入参数这一个区别：`v-model:other_name = "some_value"`
    5. 由于v-model可以设置自定义参数，因此我们可以在组件上同时绑定多个v-model参数用以双向绑定不同值
    6. 可以使用带getter/setter的computed来实现组件上的v-model，详见计算属性中的代码例子，只要在setter中触发`update:modelValue`事件即可，在getter中返回modelValue即可
    7. v-model可以自定义修饰符并且对其进行处理，通过modelModifiers这个prop来进行操作，如果在模板中使用了某个修饰符v-model.abc，那么modelModifiers.abc为true
    8. 自定义组件使用v-model与vue2.x有区别
       1. 属性由`value`改为了`modelValue`
       2. 默认事件名称由`input`改为了`update:modelValue`
       3. v-bind 的 .sync 修饰符和组件的 model 选项已移除，可在 v-model 上加一个参数代替
          1. 复习：Vue2中的v-bind.sync：
             1. 默认情况下，v-bind传递prop数据流是单向的（只有父组件能修改，子组件不能修改），但是有时候需要对某一个 prop 进行“双向绑定”，此时应该使用v-bind.sync
             2. 用法

                ```vue
                <!-- 父组件中 -->
                <text-document
                :title="doc.title"
                @update:title="doc.title = $event"
                ></text-document>
                <!-- 或者，v-bind:title.sync就是上面代码的语法糖 -->
                <text-document v-bind:title.sync="doc.title"></text-document>

                <!-- 子组件中 -->
                this.$emit('update:title', newTitle);
                <!-- 可以看出来v-bind.sync和Vue3的v-model很像，而且同样也可以绑定多个参数，因此Vue3直接使用新版的v-model代替了它 -->
                ```

             3. 与Vue2的v-model对比
                1. 都是实现双向绑定，都是语法糖
                2. v-bind.sync更灵活，能绑定多个prop，而v-model只能用一个
       4. 现在可以在同一个组件上使用多个 v-model 绑定
       5. 现在可以自定义 v-model 修饰符
20. 生命周期：使用组合式API时，会用setup来代替beforeCreate和created两个阶段
    1. beforeCreate（组合式API中没有本阶段）
    2. created（组合式API中没有本阶段）
    3. beforeMount
    4. mounted
    5. beforeUpdate
    6. updated
    7. beforeUnmount
    8. unmounted
21. 侦听器watch
    1. 形式：`watch(val, fun(new_val, old_val){}, options)`
    2. 返回值是一个停止侦听函数，可以加上括号调用来**停止侦听**，注意监听的val是ref时，回调函数中的new_val会自动解包
    3. 一个关键点是，侦**听器必须用同步语句创建**：如果用异步回调创建一个侦听器，那么它不会绑定到当前组件上，你必须手动停止它，以防内存泄漏。
    4. **数据源val可以是一个 ref (包括计算属性)、一个响应式对象reactive、一个 getter 函数、或多个数据源组成的数组**，但是**不能直接侦听响应式对象的属性值（除非该属性也是一个ref、响应式对象...）**（比如`watch(watch_obj.value.b.c, function(){})`），如果想直接监听深层属性，需要用getter函数返回该属性并监听该getter —— `watch(() => watch_obj.value.b.c, function(){})`（*这点不像Vue2，Vue2是可以直接监听深层路径的表达式的*）
    5. options对象有几个属性：deep（深层侦听），immediate（初始化时也会触发侦听，默认是false），flush（Vue3新增，回调执行时机，默认值是"pre"，即DOM更新之前执行，可以改为"post"以延迟至DOM更新之后执行）

    ```javascript
    const x = ref(0)
    const y = ref(0)

    // 单个 ref
    watch(x, (newX) => {
        console.log(`x is ${newX}`)
    })

    const obj = reactive({ count: 0 })

    //不能直接侦听响应式对象的属性值
    //watch(obj.count, (count) => {
    //    console.log(`count is: ${count}`)
    //})
    // // 错误，因为 watch() 得到的参数是一个 number，可以通过提供一个getter函数来解决这个问题

    // getter 函数
    watch(
        () => obj.count,
        (count) => {
            console.log(`count is: ${count}`)
        }
    )

    // 多个来源组成的数组
    watch([x, () => y.value], ([newX, newY]) => {
        console.log(`x is ${newX} and y is ${newY}`)
    })
    ```

22. watchEffect侦听器
    1. 形式：`watchEffect(fun(){}, options)`
    2. 作用：**立即运行**（不需要加immediate选项）一个函数fun()，同时响应式地追踪其依赖，并在依赖更改时重新执行fun()
    3. 返回值是一个停止侦听函数，可以加上括号调用来停止侦听
    4. 一个关键点是，侦听器必须用同步语句创建：如果用异步回调创建一个侦听器，那么它不会绑定到当前组件上，你必须手动停止它，以防内存泄漏。
    5. 回调触发时机：`options.flush`（回调执行时机，默认值是"pre"，即DOM更新之前执行，可以改为"post"以延迟至DOM更新之后执行）

    ```javascript
    watchEffect(async () => {
        const response = await fetch(
            `https://jsonplaceholder.typicode.com/todos/${todoId.value}`
        )
        //会自动追踪totoId.value并侦听
        data.value = await response.json()
    })
    ```

23. watch VS watchEffect
    1. watch 只追踪明确侦听的数据源。它不会追踪任何在回调中访问到的东西。另外，仅在数据源确实改变时才会触发回调。watch 会避免在发生副作用时追踪依赖，因此，我们能更加精确地控制回调函数的触发时机。
    2. watchEffect，则会在副作用发生期间追踪依赖。它会在同步执行过程中，自动追踪所有能访问到的响应式属性。这更方便，而且代码往往更简洁，但有时其响应性依赖关系会不那么明确。
    3. 对于有多个依赖项的侦听器来说，使用 watchEffect() 可以消除手动维护依赖列表的负担。此外，如果你需要侦听一个嵌套数据结构中的几个属性，watchEffect() 可能会比深度侦听器更有效，因为它将只跟踪回调中被使用到的属性，而不是递归地跟踪所有的属性。
24. watch VS computed
    * 相同点：
       1. watch和computed都是以Vue的依赖追踪机制为基础的，它们都试图处理这样一件事情：当某一个数据（称它为依赖数据）发生变化的时候，所有依赖这个数据的“相关”数据“自动”发生变化，也就是自动调用相关的函数去实现数据的变动。
    * 不同点：
       1. computed支持缓存，只有依赖数据发生改变,才会重新进行计算;而watch不支持缓存，数据变，直接会触发相应的操作。
       2. computed不支持异步（不能发请求） ，当computed内有异步操作时无效，无法监听数据的变化;而watch支持异步
       3. computed属性值会默认走缓存，是基于它们的响应式依赖进行缓存的，而watch监听的函数接收两个参数，第一个参数是最新的值，第二个参数是输入之前的值
       4. computed中的属性都有一个get和一 个set方法，当数据变化时，调用set方法
       5. computed必须return，watch不需要
       6. computed时多对一或者一对一的，一对多的时候一般用watch（多个依赖计算得到一个属性使用computed，一个属性变化操作其它的属性/方法使用watch）
25. 访问DOM模板/模板引用
    1. 某些情况下，我们仍然需要直接访问底层 DOM 元素。要实现这一点，我们可以在DOM元素上使用特殊的 ref 属性：`<input ref="input">`，和 v-for 的 key 类似。它允许我们在一个特定的 DOM 元素或子组件实例被挂载后，获得对它的直接**引用**
    2. 组合式API使用ref时必须暴露一个同名的ref值，选项式API则使用this.$refs.ref_name进行访问，如果多个DOM元素包含同一个ref值，则应该使用同名的**数组**ref

    ```vue
    <script setup>
    import { ref, onMounted } from 'vue'

    // 声明一个 ref 来存放该元素的引用
    // 必须和模板里的 ref 同名
    const input = ref(null)

    onMounted(() => {
        input.value.focus()
    })
    </script>

    <template>
        <input ref="input" />
    </template>
    ```

26. 组件
    1. 组件引入的选项式写法和Vue2一样需要import组件并且在components选项中注册，但是setup中只需要引入即可直接在模板中使用
    2. 一个组件需要显式声明它所接受的 props，这样 Vue 才能知道外部传入的哪些是 props，哪些是[透传 attribute](#touchuanAttribute)
    3. `<script setup>`传递通过`const props = defineProps(['prop1', 'prop2'])`来定义props，而`setup(props, context){}`选项式写法需要定义props选项，前者的第一个参数就是我们定义的props选项，在模板上的用法和Vue2保持一致

        ```javascript
        export default {
            props: ['title'],
            setup(props) {
                console.log(props.title)
            }
        }
        ```

    4. 事件监听
       1. 在`<script setup>`中通过`defineEmits(['event1', 'event2', ...])`来定义子组件需要抛出/触发的自定义事件，返回值就是一个`$emit`，因为setup本身无法访问`$emit`（因为无法访问this，也就是组件实例）；而使用`setup(props, ctx){}`函数或者选项式写法，则需要使用emits选项，并且setup函数通过第二个参数ctx的emit方法来触发事件

            ```vue
            <script setup>
            const emit = defineEmits(['inFocus', 'submit']);//返回一个和$emit相同作用的函数，可以用于抛出事件

            function buttonClick() {
                emit('submit')
            }
            </script>
            ```

            ```javascript
            export default {
                emits: ['enlarge-text'],
                setup(props, ctx) {
                    ctx.emit('enlarge-text')
                }
            }
            ```

       2. 事件校验例子，其实也可以通过子组件内部的方法手动校验，通过之后再emit也是同样的：

            ```vue
            <script setup>
            const emit = defineEmits({
            // 没有校验
            click: null,

            // 校验 submit 事件
            submit: ({ email, password }) => {
                if (email && password) {
                    return true
                } else {
                    console.warn('Invalid submit event payload!')
                    return false
                }
            }
            })

            function submitForm(email, password) {
                emit('submit', { email, password })
            }
            </script>
            ```

    5. 插槽
    6. 动态组件 `:is`，我们可以通过 `<KeepAlive>` 组件强制被切换掉的组件仍然保持“存活”的状态
    7. 组件注册：
       1. 全局注册，任意组件中可用

            ```javascript
            import { createApp } from 'vue'
            import MyComponent from './App.vue'

            const app = createApp({})

            app.component(
                // 注册的名字
                'MyComponent',
                // 组件的实现
                {
                    /* ... */
                }
            )
            app.component('MyComponent', MyComponent)
            ```

       2. 局部注册：使用`<script setup>`语法只需要import就可以直接使用，选项式写法则需要使用components选项手动/显示注册
27. <a id="touchuanAttribute"></a>透传attribute属性
    1. “透传 attribute”指的是传递给一个组件，却没有被该组件声明为 props 或 emits 的**attribute 或者 v-on 事件监听器（注意值和监听器都是可以的）**。最常见的例子就是 class、style 和 id
    2. 当一个组件以单个元素为根作渲染时，透传的 attribute 会自动被添加到根元素上

        ```vue
        <!-- <MyButton> 的模板，<MyButton> 并没有将 class 声明为一个它所接受的 prop -->
        <button>click me</button>

        <!-- 父组件引用MyButton，并且传入了 class： -->
        <MyButton class="large" />
            
        <!-- 最后渲染出的 DOM 结果是： -->
        <button class="large">click me</button>
        <!-- class 被视作透传 attribute，自动透传到了 <MyButton> 的根元素上 -->
        ```

    3. 如果一个子组件的根元素已经有了 class 或 style attribute，它会和从父组件上继承的值（即透传得到的class、style）合并，并且，父组件传递的class会排在子组件自身的class后面
    4. 同样的，v-on事件监听器也会被继承
    5. 多层透传：祖先组件、父组件、子组件，如果父组件的根节点就是子组件，那么祖先组件头传到父组件的attribute会继续传递到子组件，除非包含 父组件上声明过的 props 或是针对 emits 声明事件的 v-on 侦听函数，换句话说，声明过的 props 和侦听函数被父组件“消费”了。
    6. 禁止自动继承父组件的透传attribute（比如需要在根节点以外的节点使用attribute而非根节点）：设置选项`inheritAttrs: false`，如果是`<script setup>`写法，则需要使用`defineOptions({inheritAttrs: false})`来定义该选项
    7. **模板中可以通过`$attrs`来访问透传的attribute**（在` <script setup> `中使用 `useAttrs()` API 来访问，在setup函数中使用`ctx.attrs`来访问），这个 `$attrs` 对象包含了除组件所声明的 props 和 emits 之外的所有其他 attribute，例如 class，style，v-on 监听器等等，比如可以通过设定 `inheritAttrs: false` 和使用`v-bind="$attrs"` 来实现非根元素继承attribute
    8. 考虑到性能因素，**`$attrs`并不是响应式的**（`isReactive(attrs)/isProxy/isRef/isReadonly： false false false false`），如果你需要响应性，可以使用 prop。或者你也可以使用 onUpdated() 使得在每次更新时结合最新的 attrs 执行副作用
    9. **多个根节点的组件没有自动 attribute 透传行为**。如果 `$attrs` 没有被显式绑定，将会抛出一个运行时警告
28. 插槽
    1. 插槽内容可以是任意的元素/组件
    2. **插槽内容可以访问到父组件的数据作用域，无法访问子组件的数据**（父组件模板中的表达式只能访问父组件的作用域；子组件模板中的表达式只能访问子组件的作用域），以下两个message都同时为父组件中的message

        ```vue
        <span>{{ message }}</span>
        <FancyButton>{{ message }}</FancyButton>
        ```

    3. 插槽默认内容，如果父组件调用子组件时未提供插槽内容，则会使用默认内容`<slot>Submit <!-- 默认内容 --></slot>`
    4. 具名插槽：
       1. 带name属性的插槽，可以区分多个插槽，`<slot name='slot_name1'> </slot>   <slot name='slot_name2'> </slot>`，父组件使用时通过`<template v-slot:slot_name1>`来传入对应的具名插槽内容，没有提供name的插槽会有一个默认名称default
       2. `v-slot:slot_name1`可以在模板中简写为：`#slot_name1`
       3. 当一个组件同时接收默认插槽和具名插槽时，所有位于顶级的非 `<template>` 节点都被隐式地视为默认插槽的内容
    5. 作用域插槽
       1. 为了解决插槽无法访问子组件的数据而产生
       2. 在子组件定义插槽时，可以像传递props一样，向一个插槽出口传递attributes：`<slot :text="greetingMessage" :count="1"></slot>`
       3. 父组件接受插槽时`v-slot="slotProps"`，其中slotProps(可以是任意名称，不一定非要slotProps)就是子组件传递给插槽的所有属性组成的对象，可以在插槽内部内容中直接使用
       4. 一些组件可能只包括了逻辑而不需要自己渲染内容，视图输出通过作用域插槽全权交给了消费者组件。我们将这种类型的组件称为**无渲染组件**

            ```vue
            <MyComponent v-slot="slotProps">
                {{ slotProps.text }} {{ slotProps.count }}
            </MyComponent>
            ```

       5. 可以给`v-slot="slotProps"`添加一个name参数变为具名作用域插槽`v-slot:slot_name1="slotProps"`或着缩写为：`#slot_name1="slotProps"`
29. 依赖注入
    1. 通常父组件向后代传递数据时会使用props，但是props需要在每一级链路都手动地传递一遍，即使有些中间组件根本不关心这些props，并且可能会影响到更多这条路上的组件，这一问题被称为“prop 逐级透传”
    2. provide提供的数据在其所有的后代组件中都能被访问并且注入（inject）组件内部，无论镶嵌多深
    3. 如果**提供的值是一个 ref，注入进来的会是该 ref 对象，而不会自动解包为其内部的值**。这使得注入方组件能够通过 ref 对象保持了和供给方的响应性链接
    4. 用法：`<script setup>`以及setup函数中：先引入provide函数，然后调用：`provide(key, value)`；在选项式写法中是一个provide选项，值为一个包含key和value对象，value可以是任意值（普通值、ref等等），或者provide可以是一个函数，这个函数可以访问this，并且返回一个对象。对于简单类型值来说，**这种注入不会保持响应性，如果要保持响应性，应该使用computed函数**：

        ```javascript
        // this.message是一个（非ref、非reactive等响应式值）/简单类型
        provide() {
            return {
                // 显式提供一个计算属性
                message: computed(() => this.message)
            }
        }
        ```

    5. 应用级provide，`app.provide(key, value)`，该应用内所有组件都能够使用
    6. 注入inject：子组件中通过`inject(key)`获取祖先组件提供的数据
    7. inject默认值：如果祖先组件都没有提供指定数据，可以通过`inject(key, "default value")`来为设置一个默认的注入值
    8. 在一些场景中，默认值可能需要通过调用一个函数或初始化一个类来取得。为了避免在用不到默认值的情况下进行不必要的计算或产生副作用，我们可以使用工厂函数来创建默认值：`const value = inject('key', () => new ExpensiveClass(), true)`，第三个参数表示默认值应该被当作一个工厂函数
    9. 当提供 / 注入响应式的数据时，建议尽可能将任何对响应式状态的变更都保持在供给方组件中（这一点类似于props），如果子组件硬是要改，可以由祖先组件提供一个更改其数据的方法，子组件调用那个方法即可
    10. 如果你想确保提供的数据不能被注入方的组件更改，你可以使用 readonly() 来包装提供的值。

        ```vue
        <script setup>
        import { ref, provide, readonly } from 'vue'

        const count = ref(0)
        provide('read-only-count', readonly(count))
        </script>
        ```

    11. 大型项目中的provide数据为了避免命名冲突，可以使用Symbol作为key，将所有key保存在一个文件中，并且在提供和注入组件内都引入对应的文件和key即可
30. 异步组件defineAsyncComponent
    1. 异步组件在Vue2中通过返回 Promise 的函数来创建的`const asyncModal = () => import('./Modal.vue')` ；vue3中需要使用`defineAsyncComponent`方法包裹 `const asyncModal = defineAsyncComponent(() => import('./Modal.vue'))`或者`const asyncModalWithOptions = defineAsyncComponent({loader: () => import('./Modal.vue') })`

    ```javascript
    const AsyncComp = defineAsyncComponent({
        // 加载函数
        loader: () => import('./Foo.vue'),

        // 加载异步组件时使用的组件
        loadingComponent: LoadingComponent,
        // 展示加载组件前的延迟时间，默认为 200ms,这是因为在网络状况较好时，加载完成得很快，加载组件和最终组件之间的替换太快可能产生闪烁，反而影响用户感受
        delay: 200,

        // 加载失败后展示的组件
        errorComponent: ErrorComponent,
        // 如果提供了一个 timeout 时间限制，并超时了
        // 也会显示这里配置的报错组件，默认值是：Infinity
        timeout: 3000
    })
    ```

31. 组合式函数（只能在`setup()`或者`<script setup>`中使用）
    1. 把重复使用的逻辑功能抽象成一个函数并且单独作为一个模块/JS文件，需要使用时和普通模块一样导入（组件重用模块，组合式函数重用逻辑）
    2. 命名：组合式函数约定用驼峰命名法命名，并以“use”作为开头
    3. 返回值：推荐的约定是组合式函数始终返回一个包含**多个 ref 的普通的非响应式对象**，这样该对象在组件中被解构为 ref 之后仍可以保持响应性
    4. 如果你的应用用到了服务端渲染 (SSR)，请确保在组件挂载后才调用的生命周期钩子中执行 DOM 相关的副作用，例如：`onMounted()`。这些钩子仅会在浏览器中被调用，因此可以确保能访问到 DOM
    5. 确保在 onUnmounted() 时清理副作用。举例来说，如果一个组合式函数设置了一个事件监听器，它就应该在 onUnmounted() 中被移除
    6. 组合式函数可以调用所属组件的生命周期函数
    7. 比较：
       1. 和 Mixin 的对比​
          1. Vue 2 的用户可能会对 mixins 选项比较熟悉。它也让我们能够把组件逻辑提取到可复用的单元里。然而 mixins 有三个主要的短板：
             1. **不清晰的数据来源**：当使用了多个 mixin 时，实例上的数据属性来自哪个 mixin 变得不清晰，这使追溯实现和理解组件行为变得困难。这也是我们推荐在组合式函数中使用 ref + 解构模式的理由：让属性的来源在消费组件时一目了然。
             2. **命名空间冲突**：多个来自不同作者的 mixin 可能会注册相同的属性名，造成命名冲突。若使用组合式函数，你可以通过在解构变量时对变量进行重命名来避免相同的键名。
             3. 隐式的跨 mixin 交流：多个 mixin 需要依赖共享的属性名来进行相互作用，这使得它们隐性地耦合在一起。而一个组合式函数的返回值可以作为另一个组合式函数的参数被传入，像普通函数那样。
          2. 基于上述理由，我们不再推荐在 Vue 3 中继续使用 mixin。保留该功能只是为了项目迁移的需求和照顾熟悉它的用户。
       2. 和无渲染组件的对比​
          1. 在组件插槽一章中，我们讨论过了基于作用域插槽的无渲染组件。我们甚至用它实现了一样的鼠标追踪器示例。
          2. 组合式函数相对于无渲染组件的主要优势是：组合式函数不会产生额外的组件实例开销。当在整个应用中使用时，由无渲染组件产生的额外组件实例会带来无法忽视的性能开销。
          3. 我们推荐在纯逻辑复用时使用组合式函数，在需要同时复用逻辑和视图布局时使用无渲染组件。
       3. 和 React Hooks 的对比​
          1. 如果你有 React 的开发经验，你可能注意到组合式函数和自定义 React hooks 非常相似。组合式 API 的一部分灵感正来自于 React hooks，Vue 的组合式函数也的确在逻辑组合能力上与 React hooks 相近。然而，Vue 的组合式函数是基于 Vue 细粒度的响应性系统，这和 React hooks 的执行模型有本质上的不同。这一话题在组合式 API 的常见问题中有更细致的讨论。
32. 自定义指令
    1. 一个自定义指令由一个包含类似组件生命周期钩子的对象来定义（和Vue2的bind、inserted、update不一样，Vue3的自定义指令更像一个“组件”，它拥有7个组件的生命周期，没有beforeCreate）。钩子函数会接收到指令所绑定元素、binding对象（包含旧值、新值、参数、修饰符等）、vNode等作为其参数，可以像内置指令v-bind、v-model这类指令一样在模板/DOM上使用
    2. 在 `<script setup>` 中，任何以 `v` 开头的驼峰式命名的变量都可以被用作一个自定义指令。比如，`vFocus` 即可以在模板中以 v-focus 的形式使用。在没有使用` <script setup> `的情况下，自定义指令需要通过 directives 选项注册
    3. 详见[Vue官网-自定义指令](https://cn.vuejs.org/guide/reusability/custom-directives.html#introduction)
33. [vueUse](https://vueuse.org/guide/)：一个日益增长的 Vue 组合式函数集合，包含大量的工具函数
34. 内置组件：
    1. `<Transition>`
       1. 通过特定的css class（v-enter-active、v-leave-active、v-leave-to等）用于为元素/组件添加过渡动画
       2. 触发时机：
          1. 由 v-if 所触发的切换
          2. 由 v-show 所触发的切换
          3. 由特殊元素 `<component>` 切换的动态组件
          4. 改变特殊的 key 属性
    2. `<KeepAlive>`
       1. 默认情况下，一个组件实例在被切换/替换掉后会被销毁。这会导致它丢失其中所有已变化的状态——**当这个组件再一次被显示时，会创建一个只带有初始状态的新实例**
       2. `<KeepAlive>` 默认会缓存内部的所有组件实例，但我们可以通过 include 和 exclude prop 来定制该行为
       3. 被`<KeepAlive>`包裹的组件会多出activated（组件被插入DOM中/被激活时调用）和deactivated（组件被移除/失活时触发）两个生命周期
       4. 更多细节详见：[Vue官网-KeepAlive](https://cn.vuejs.org/guide/built-ins/keep-alive.html#include-exclude)
    3. `<Teleport>`
       1. `teleport` 提供了一种干净的方法，允许我们控制在 DOM 中哪个父节点下渲染了 HTML

            ```html
            <!--控制div在body的第一级子元素显示，以body为父元素-->
            <teleport to="body">
                <div v-if="modalOpen" class="modal">
                </div>
            </teleport>
            ```

       2. `<Teleport>` 只改变了渲染的 DOM 结构，它不会影响组件间的逻辑关系。也就是说，如果 `<Teleport>` 包含了一个组件，那么该组件始终和这个使用了 `<teleport>` 的组件保持逻辑上的父子关系。传入的 props 和触发的事件也会照常工作
       3. 禁用Teleport：`<Teleport :disabled="isMobile">···</Teleport>`
35. SSR（服务端渲染）
    1. 在服务端直接渲染成 HTML 字符串，作为服务端响应返回给浏览器
    2. 优势：
       1. 更快的首屏加载：无需等到所有的 JavaScript 都下载并执行完成之后才显示，所以你的用户将会更快地看到完整渲染的页面。除此之外，数据获取过程在首次访问时在服务端完成，相比于从客户端获取，可能有更快的数据库连接。这通常可以带来更高的核心 Web 指标评分、更好的用户体验，而对于那些“首屏加载速度与转化率直接相关”的应用来说，这点可能至关重要。
       2. 统一的心智模型：你可以使用相同的语言以及相同的声明式、面向组件的心智模型来开发整个应用，而不需要在后端模板系统和前端框架之间来回切换
       3. 更好的 SEO：搜索引擎爬虫可以直接看到完全渲染的页面
    3. 可以使用框架更加通用与方便（比如Nuxt全栈框架）
    4. 更多细节详见[Vue官网-服务端渲染SSR](https://cn.vuejs.org/guide/scaling-up/ssr.html)
36. 使用Vue-Cli时，如果需要自定义ESLint选项可以通过 .eslintrc 或 **package.json 中的 `eslintConfig` 字段**来配置
37. [部署](https://cn.vuejs.org/guide/best-practices/production-deployment.html)
38. [Vue性能优化](https://cn.vuejs.org/guide/best-practices/performance.html)
39. 虚拟DOM树的优化：把模板中需要响应式的元素收集起来成为一个数组，当这个组件需要重新渲染时，只需要遍历这个打平的数组而非整棵树。这也就是我们所说的树结构打平，这大大减少了我们在虚拟 DOM 协调时需要遍历的节点数量。模板中任何的静态部分都会被高效地略过。

#### API参考

1. `nextTick()`：当你在 Vue 中更改响应式状态时，最终的 DOM 更新并不是同步生效的，而是由 Vue 将它们缓存在一个队列中，直到下一个“tick”才批量更新DOM。这样是为了确保每个组件无论发生多少状态改变，都仅执行一次更新。`nextTick()` 可以在状态改变后立即使用，以等待 DOM 更新完成。你可以传递一个回调函数作为参数，或者 await 返回的 Promise。总结而言，**nextTick方法允许你在Vue实例的DOM更新完成后执行回调函数**，以便你可以操作或获取最新的DOM状态。（Vue2中nextTick返回的对象虽然也有.then()方法，但是不是Promise对象，Vue3中的返回的是Promise）

    ```javascript
    <script setup>
    import { nextTick } from 'vue'

    function increment() {
        state.count++;
        //此时DOM并没有实时更新
        nextTick(() => {
            // 访问更新后的 DOM
        })
        //或者
        await nextTick();
    }
    </script>
    ```

2. `isRef(obj)`：obj是否是一个ref
3. `unref(val)`：返回非ref形式数据，语法糖，即返回值 `val = isRef(val) ? val.value : val`
4. `toRef`：
   1. 将值、getter函数、ref规范化为ref

        ```javascript
        // 按原样返回现有的 ref
        toRef(existingRef)
        // 创建一个只读的 ref，当访问 .value 时会调用此 getter 函数
        toRef(() => props.foo) 
        // 从非函数的值中创建普通的 ref
        // 等同于 ref(1)
        toRef(1)
        ```

   2. 基于响应式对象(reactive)上的一个属性，创建一个对应的 ref。这样**创建的 ref 与其源属性保持同步**：改变源属性的值将更新 ref 的值，反之亦然，**即便属性是一个基本类型**。注意和直接使用ref包裹属性值不同，如果ref包裹的值是基本类型，则不会和源属性保持同步，但是引用类型的话是同步的。

        ```javascript
        const state = reactive({
            foo: 1,
            bar: 2
        })
        // 双向 ref，会与源属性同步
        const fooRef = toRef(state, 'foo')
        // 更改该 ref 会更新源属性
        fooRef.value++
        console.log(state.foo) // 2
        // 更改源属性也会更新该 ref
        state.foo++
        console.log(fooRef.value) // 3
        ```

5. `mixins`
   1. 类型：`Array<Object>`
   2. 一个包含组件选项对象的数组，这些选项都将被混入到当前组件的实例中，包含的生命周期会在使用该mixins的组件之前调用
   3. . mixins存在相同的data会被组件内的data覆盖
   4. Vue3 继续支持 mixin 的同时，**组合式函数**是更推荐的在组件之间共享代码的方式，在使用`<script setup>`时可以直接导入并使用组合式函数，而如果使用的是选项式API则需要使用`setup(){}`选项并在内部setup()内部使用
6. `extends`
   1. 类型：Object
   2. 扩展，从实现的角度看，extends 几乎等同于 mixins，然而，extends 和 mixins 表达了不同的意图。mixins 选项主要用来组合功能，而 extends 主要用来考虑继承性
7. `setup()`
   1. 类型：Function
   2. 参数：setup(props, context)  
       1. **用的少，通常是在选项式API组件中需要集成组合式API代码（比如选项式API代码中使用组合式函数复用组件某些公共代码）时会用，一般来说：对于结合单文件组件使用的组合式 API，推荐通过 `<script setup>` 以获得更加简洁及符合人体工程学的语法**
       2. props是响应式的，**但注意如果你解构了 props 对象，解构出的变量将会丢失响应性。因此我们推荐通过 props.xxx 的形式来使用其中的 props**
       3. 如果确实需要解构props并且保持响应性可以使用`const {...}=toRefs(props)`，或者`const v=toRef(props, 'key')`，或者使用`computed(()=>props.xxx)`

            ```javascript
            import { toRefs, toRef } from 'vue'

            export default {
                setup(props) {
                    // 将 `props` 转为一个其中全是 ref 的对象，然后解构
                    const { title } = toRefs(props)
                    // `title` 是一个追踪着 `props.title` 的 ref
                    console.log(title.value)

                    // 或者，将 `props` 的单个属性转为一个 ref
                    const title = toRef(props, 'title')
                }
            }
            ```

       4. setup上下文（第二个参数context）包含attrs、slots 、emit以及expose（expose是一个函数，允许暴露特定的属性）

            ```javascript
            export default {
                setup(props, context) {
                    // 透传 Attributes（非响应式的对象，等价于 $attrs）
                    console.log(context.attrs)

                    // 插槽（非响应式的对象，等价于 $slots）
                    console.log(context.slots)

                    // 触发事件（函数，等价于 $emit）
                    console.log(context.emit)

                    // 暴露公共属性（函数）
                    console.log(context.expose)
                }
            }
            ```

       5. attrs 和 slots 都是有状态的对象，它们总是会随着组件自身的更新而更新。这意味着你应当避免解构它们，并始终通过 attrs.x 或 slots.x 的形式使用其中的属性。此外还需注意，和 props 不同，attrs 和 slots 的属性都不是响应式的
       6. expose 函数用于显式地限制该组件暴露出的属性，当父组件通过模板引用（`$ref`，即模板标签添加ref属性获得对组件的直接引用，和key一样）访问该组件的实例时，将仅能访问 expose 函数暴露出的内容
       7. 组件使用 props 比其他 property 更常见，并且很多情况下组件仅使用 props。
       8. 将 props 作为单独的参数可以使单独键入更容易，而不会弄乱上下文中其他 property 的类型。这也使得在具有 TSX 支持的 setup、render 和普通功能组件之间保持一致的签名成为可能。
   3. 调用时间：初始 prop 解析之后立即调用 setup，在生命周期方面，它是在 beforeCreate 钩子之前调用的。
   4. 在模板中访问从 `setup` 返回的 `ref` 时，它会自动浅层解包（第一层会解包，内嵌的ref不会），因此你无须再在template模板中为它写 .value。其它选项通过 this 访问时也会同样如此解包
8. `reactive`
9. Refs
   1. `ref('abc')`接受一个内部值并返回一个响应式且可变的 `ref` 对象，ref需要通过`.value`进行访问其值（`template`内除外）
   2. `toRef(props, 'key')`用来为源响应式对象上的某个 key 新创建一个 ref
   3. `toRefs(props)`将响应式对象转化成普通的对象，每一个property都是源property的ref

   ```javascript
   let ref_obj = ref('ref内部值');
   console.log(`ref_obj.value：${ref_obj.value}`);

   let toRef_prop_user = toRef(props, 'user');
   console.log(`toRef_prop_user.value: ${toRef_prop_user.value}`);//是一个ref，所以要用.value进行访问值

   let toRefs_obj = toRefs(props);
   console.log(`toRefs_obj.value: `);
   console.log(toRefs_obj.user);
   console.log(toRefs_obj.user.value);//具体属性是ref，所以还是需要加.value
   ```

#### 查漏补缺（非新用法）

1. v-bind使用动态参数：`<a v-bind:[a_attr]="'//www.baidu.com'">百度</a>`，其中a_attr是组件中定义的一个数据，类似的，v-on也可以这样使用动态参数，并且都支持动态参数的简写语法
2. 修饰符：`<form @submit.prevent="onSubmit">...</form>`，对form的submit事件调用event.preventDefault();
3. 非响应式属性（isReactive(obj)为false）在模板中并不是一成不变的，而是当调用方法改变该属性时，不会触发依赖/视图更新，但是**当模板中有其它响应式属性改变时，组件内包含非响应式属性的模板视图也会随之一起更新**，比如a=1是非响应式数据，b=1是响应式数据，按钮1绑定事件a++，按钮2绑定事件b++，连续点1按钮10次，此时a已经变成了11，但是视图上的a仍然是1，这时，点击2按钮一次，会发现b视图变成了2，同时a视图变成了11
4. v-for的key作用：
   1. Vue 的diff算法中， Vue 默认按照“就地更新”的策略来更新通过 v-for 渲染的元素列表，通过sameVnode函数判断新节点和旧节点是否为相同的节点：如果两个节点的key+tag都一致，则判断为相同节点而复用和更新该元素，而不需要重新创建和操作DOM元素，避免性能浪费
5. v-on是**事件监听器**
6. v-on绑定事件 my_method(...args, event)时，为了传递event给my_method以获取当前点击的元素，可以在模板中这样调用`@click = my_method(1, 2, 3, $event)`，即模板中通过$event传递event。当模板中不传递参数时，方法中的第一个参数默认是event
7. 事件修饰符
   1. .stop：阻止事件流进一步传递，即调用`event.stopPropgation()`
   2. .prevent：阻止事件默认行为，即调用`event.preventDefault()`
   3. .self：当 event.target 是元素本身时才会触发事件处理器
   4. .capture：捕获阶段触发，即addEventListner中的`options.capture`，默认false
   5. .once：是否仅触发一次，即`options.once`
   6. .passive：是否允许默认行为，即options.passive，为true时不能调用`event.preventDefault()`，默认为false
8. props传递的时候可以是静态的（不带v-bind，传递普通静态值），也可以是动态的（带v-bind，可以传递一个变量）
9. props都是单向数据流，props因父组件的更新而变化，要改变props应该抛出一个事件
10. **v-bind不带参数时，会将对象所有的属性当作props传入组件中**：`<BlogPost v-bind="post" />`相当于`<BlogPost :id="post.id" :title="post.title" />`

#### 杂项

1. webpack Treeshaking 的注意事项​：因为 `defineComponent()` 是一个函数调用，所以它可能被某些构建工具认为会产生副作用，如 webpack。即使一个组件从未被使用，也有可能不被 tree-shake。为了告诉 webpack 这个函数调用可以被安全地 tree-shake，我们可以在函数调用之前添加一个 `/*#__PURE__*/` 形式的注释：`export default /*#__PURE__*/ defineComponent(/* ... */)`。请注意，如果你的项目中使用的是 Vite，就不需要这么做，因为 Rollup (Vite 底层使用的生产环境打包工具) 可以智能地确定 defineComponent() 实际上并没有副作用，所以无需手动注释

### Vue3对比Vue2

1. 核心原理：

     ```javascript
     let bValue = null;
     Object.defineProperty(obj1, prop, 
        {
            get() {
                return bValue;
            },
            set(newValue) {
                bValue = newValue;
            }
        }
     )

     const handler = {
         get(target, prop) {
             console.log(`代理拦截了get，读取属性${prop}`);
             return Reflect.get(target, prop);
         },
         //新增属性+修改属性都是使用set
         set(target, prop, value) {
             console.log("代理拦截了set，并且该操作是新增属性/修改已有属性");
             if (prop in target) {//只要存在该属性，即使值是undefined也会返回true
                 console.log(`修改属性${prop},值为${value}`);
             } else {
                 console.log(`新增属性${prop},值为${value}`);
             }
             return Reflect.set(target, prop, value);
         },
         deleteProperty(target, prop) {
             console.log(`代理拦截了deleteProperty，删除属性${prop}`);
             return Reflect.deleteProperty(target, prop);
         }
     }
     const obj = {};
     const p = new Proxy(obj, handler);
     ```

   1. Vue2使用`Object.defineProperty()`而Vue3使用Proxy搭配Reflect实现，从用法上来看，前者接受并且拦截一个属性，也就是说，Vue2是从属性层面进行代理，**需要递归遍历**所有的属性并进行监听，性能相对较差；而Vue3的Proxy是接收一整个对象进行代理，只需要对子对象进行代理而不需要对所有子属性添加响应，效率更高，并且代码量上远远优于Object.defineProperty()的递归遍历数据劫持操作
   2. Vue2的Object.defineProperty只能拦截value、configurable、enumerable、writable、get、set这6个描述符，并且实际只拦截了get、set这两个，因此它能拦截的操作相对较少，并且它**只对初始对象里的属性有劫持**，对于**新增的属性无法自动添加监听**，同时也**无法监听对象属性的删除**，因此Vue2中专门准备了**Vue.set、Vue.delete**这两个方法来应对这种局限性；而Vue3中使用的Proxy可以拦截13种元操作，可以更加精细化劫持捕捉操作，并且**set和deleteProperty两个元操作可以对属性的新增和删除进行拦截，从而无需使用Vue.set和Vue.delete**；此外前者无法检测到数组的下标和length属性的变更，而后者可以自动监听到下标和length属性的变化
   3. diff算法优化
       1. Vue2：对整个虚拟DOM树进行比较，即使许多节点都保持不变
       2. Vue3对Diff的优化：把模板中需要响应式的元素收集起来成为一个数组，当这个组件需要重渲染时，只需要遍历这个打平的数组而非整棵树。这也就是我们所说的树结构打平，这大大减少了我们在虚拟 DOM 协调时需要遍历的节点数量。模板中任何的静态部分都会被高效地略过。
       3. 静态节点提升：Vue 3 在编译时会对组件的模板进行分析，将那些在组件更新过程中保持不变的静态节点提升到组件的 setup 函数之外，避免不必要的比较和更新。
       4. 缓存事件处理器：Vue 3 缓存了事件处理器，使得在每次更新时不需要重新创建新的函数，减少了内存的使用（在vue2.x中在绑定DOM事件时，例如@click，这些事件被认为是动态变量，所以每次更新视图的时候都会追踪它的变化，然后每次触发都要重新生成全新的函数，在Vue 3中，提供了事件缓存对象cacheHandlers，当cacheHandlers开启的时候，@click绑定的事件会被标记成静态节点，被放入cacheHandlers中，这样在视图更新时也不会追踪，当事件再次触发时，就无须重新生成函数，直接调用缓存的事件回调方法即可，在事件处理方面提升了Vue的性能）
   4. Vue3的Proxy对老版本浏览器不兼容，因此这是它相比Vue2的一个缺点
2. API实现的区别：
   1. v-model
3. 开发者使用：
   1. 多了很多的API可供选择导入和使用，最典型的代表就是Composition API
   2. Vue2新增或者修改一个需求，就需要分别在data，methods，computed里修改 ，滚动条反复上下移动；而Vue3的Compisition API可以让我们的新增功能的代码更加有序的组织在一起而不分散，从而更好实现“功能代码块”的概念，方便维护
4. 其它细节：
   1. **新增**：
      1. vue2从技术上讲并没有‘app’这个概念，我们定义的应用只是通过 new Vue() 创建的根 Vue 实例；vue3新的全局API：Vue.createApp：`const app = createApp({})`，
      2. vue2中任何全局改变 Vue 行为的 API 在vue3中都会移动到应用实例API上，如`Vue.use`变成了`app.use`，`Vue.mixin`变成`app.mixin`、`Vue.component`变成`app.component`等等
      3. 和 prop 类似，vue3新增`emits`选项来定义组件可触发的事件
      4. `teleport` 提供了一种干净的方法，允许我们控制在 DOM 中哪个父节点下渲染了 HTML

            ```html
            <!--控制div在body的第一级子元素显示，以body为父元素-->
            <teleport to="body">
                <div v-if="modalOpen" class="modal">
                </div>
            </teleport>
            ```

      5. **新增`setup`组合式API**
   2. **移除**
       1. `$children`被移除，推荐用`$refs`来访问子组件
       2. `$listeners`被移除，事件监听器变为`$attrs`的一部分
       3. `propsData`选项被移除
       4. 全局函数 set 和 delete 以及实例方法 `$set` 和 `$delete`。基于代理的变化检测已经不再需要它们了
       5. `$on，$off 和 $once`实例方法已被移除因此不再支持原来的事件总线模式，但是可以用第三方库实现事件总线
   3. **修改**
       1. 生命周期与vue2的不同之处：**beforeUnmount、unmounted**替换原来的**beforeDestroy以及destroyed**
       2. Vue 3 现在正式支持了**多根节点**的组件，也就是片段
       3. 在vue2中`v-for`的优先级更高，vue3中`v-if`的优先级更高，并且vue3中会报错。（vue2源码使用`if/else`判断对`v-for`的判断条件在`v-if`前，vue3中使用`switch`判断并且对`v-if`的判断`case`在`v-for`前）
       4. 自定义组件上，v-model prop 和事件默认名称已更改：
           * prop：`:value` -> `:modelValue`；
           * 事件：`@input` -> `@update:modelValue`
           * 组件现在现在可以绑定多个v-model
       5. `data`只能是返回对象的函数，不可以是对象
       6. 异步组件以前是通过返回 Promise 的函数来创建的`const asyncModal = () => import('./Modal.vue')` ；vue3中需要使用`defineAsyncComponent`方法包裹 `const asyncModal = defineAsyncComponent(() => import('./Modal.vue'))`或者`const asyncModalWithOptions = defineAsyncComponent({loader: () => import('./Modal.vue') })`  原先的component选项字段被替换成`loader`选项字段
       7. `$attrs` 现在包含了所有传递给组件的属性，包括 `class` 和`style`
       8. **自定义指令的生命周期和组件的生命周期保持一致，但是没有`beforeCreate()`**；访问组件实例通过binding参数而不是vnode参数
5. 缺点：不支持IE11，包括在Vue官方的[github文档上已明确指出放弃Vue3对IE11的支持计划](https://github.com/vuejs/rfcs/blob/master/active-rfcs/0038-vue3-ie11-support.md#contributing-to-ie11s-staying-power)，因为Proxy无法在ES5中实现也没有对应的polyfills，如果确实需要，那么使用Vue2

### 其它

#### 封装axios

1. 下载[axios](https://www.axios-http.cn/docs/example) → 创建axios实例 → 封装请求响应拦截器（`axios.interceptors.request`/`response.use(config=>{})`） → 处理请求/响应/抛出错误 → 封装接口

```javascript
//axios.js
//引入axios
import axios from 'axios';
//创建实例
const api = axios.create({
    baseURL: '',//请求地址的公共部分
    timeout: 3000//超时时间
})
//拦截器拦截axios请求和响应
api.interceptors.request.use(config => {
    return config;//config是请求的信息
}, err => {
    Promise.reject(err);//抛出错误
});

api.interceptors.response.use(res => {
    return Promise.resolve(res);//res是服务器返回结果
}, err => {
    Promise.reject(err);//抛出错误
});

export default api;

//request.js
//请求方法
import api from 'axios.js';
export const login = () => api({
    url: '',
    method: 'get',
    params: {}
})

//具体使用
import { login } from 'request.js';
login().then(res => {
    console.log(res);
})
```

#### 组件通信方法

1. props + `$emit`：通用，父子组件通信
2. provide + inject 依赖注入：通用，祖先组件与后代组件通信
3. vuex：通用，所有组件之间的通信
4. pinia
5. eventBus事件总线：父子/兄弟/跨组件组件通信，创建一个新的vue实例作为“bus”并挂载在Vue的原型上，实例中`this.$bus.$on`来监听事件，使用`this.$bus.$emit`来触发事件并且提交参数，vue2专用；**vue3由于`$on，$off 和 $once`实例方法已被移除因此不再支持原来的事件总线模式**，而是用第三方库实现，例如 mitt 或 tiny-emitter
6. `$parent + $children` 获取父组件子组件实例：vue2专用（非响应式），vue3不可用（`$children`被移除），vue3中可以用`$ref`来代替，但是也是非响应式的，*都不推荐使用*

### VueX

```javascript
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        test: "测试值",
    },
    getters: {
        get_test(state) {
            return `处理过的（${state.test}）`;
        }
    },
    mutations: {
        test(state) {
            console.log("mutations测试！state：" + JSON.stringify(state));
        },
        action_change(state, value) {
            state.test = 'action_change：' + value;
        }
    },
    actions: {
        test_action(context, param) {
            setTimeout(() => {
                context.commit('action_change', param);
            }, 3000);
        }
    },
    //这里使用不规范，实际开发中如果使用了模块化，就应该把所有数据都模块化，上面的选项都分割并且删除，而拆分到下面的modules选项中，这里仅仅为了演示用法而这样写
    modules:{
        module1: {
            state: {
                data1: "1"
            },
            getters: {},
            mutations: {},
            actions: {}
        },
        module2: {
            state: {
                data2: "2"
            },
            getters: {},
            mutations: {},
            actions: {}
        }
    }
})

store.commit('test');
export default store;
```

```javascript
methods: {
    //通过commit一个mutations来调用store中的mutations，更推荐使用mapMutations
    change_data(value) {
        this.$store.commit('change_test', value);
    },
    //通过dispatch一个actions来调用store中的actions，更推荐使用mapActions
    action_change(value) {
        this.$store.dispatch('test_action', value);
    },
    ...mapMutations(['alert_msg', 'change_msg'])
},
computed: {
    //modules模块内数据引入的方式，mutations等其他选项类似
    ...mapState({
        data1: (state) => state.module1.data1,
        data2: (state) => state.module2.data2
    }),
    //引入普通state数据
    ...mapState(['working_hard']),
    //引入普通的getters
    ...mapGetters(['ask']),
}
```

```javascript
//vue3中组合式API使用VueX
import { useStore } from 'vuex'

export default {
    setup () {
        const store = useStore()

        return {
            // 在 computed 函数中访问 state
            count: computed(() => store.state.count),

            // 在 computed 函数中访问 getter
            double: computed(() => store.getters.double)
            // 使用 mutation
            increment: () => store.commit('increment'),

            // 使用 action
            asyncIncrement: () => store.dispatch('asyncIncrement')
        }
    }
}
```

#### VueX概念

1. store容器，保存所有公用数据/状态
2. 组件 访问store实例：`this.$store`
3. 组件 访问state：`this.$store.state`，建议使用mapState辅助函数
4. 组件 访问getters：`this.$store.getters`，建议使用mapGetters辅助函数
5. 组件 提交mutations：`this.$store.commit(some_mutation, ...args)`，建议使用mapMutations辅助函数
6. 组件 分发actions：`this.$store.dispatch(some_action, ...args)`，建议使用mapActions辅助函数

#### state

1. 类似于组件中的data选项，用于存放数据

#### getters

1. 类似于组件中的computed选项，一个计算属性
2. 函数的第一个参数是**state**

#### mutations

1. 类似于组件中的methods，方法，但是不能执行异步，异步操作会导致视图和数据不一致的情况，应该使用actions来执行异步
2. 函数的第一个参数是**state**

#### actions

1. 提交mutations，可以执行异步
2. 函数的第一个参数是**一个与 store 实例具有相同方法和属性的 context 对象**，可以通过context.state访问state，但是不是store实例，只是代理了 store 一部分方法、属性，因为store是整个共享数据中心，而modules模块内部的context只能访问本模块的数据

    ```javascript
    let context = {
        dispatch: local.dispatch,
        commit: local.commit,
        getters: local.getters,
        state: local.state,
        rootGetters: store.getters,
        rootState: store.state
    }
    ```

#### modules

1. 把以上四个属性细分为更精细的块

### 路由

1. 如果只需要一个简单的页面路由，而不想为此引入一整个路由库，你可以通过**动态组件的方式模拟路由**，监听浏览器 hashchange 事件或使用 History API 来更新当前组件
2. 服务端路由：服务器根据用户访问的 URL 路径返回不同的响应结果，点击一个链接时，浏览器会从服务端获得全新的 HTML，然后重新加载整个页面
3. 客户端路由：客户端的 JavaScript 可以拦截页面的跳转请求，动态获取新的数据，然后在无需重新加载的情况下更新当前页面。利用诸如 History API 或是 hashchange 事件这样的浏览器 API 来管理应用当前应该渲染的视图
4. 模板的写法`<router-link :to="{ name: 'user', params: { username: 'erina' }}">链接</router-link>`
5. 通过 `this.$router` 的形式访整个问路由实例，并且以 `this.$route` 的形式访问当前路由（包含当前的params、path、query、name、hash等信息）

    ```javascript
    // Home.vue
    export default {
        computed: {
            username() {
                //this.$route访问当前路由
                return this.$route.params.username
            },
        },
        methods: {
            goToDashboard() {
                //this.$router访问整个路由实例
                if (isAuthenticated) {
                    this.$router.push('/dashboard')
                } else {
                    this.$router.push('/login')
                }
            },
        },
    }
    ```

6. `this.$router` 与直接使用通过 `createRouter` 创建的 router 实例完全相同。我们使用 `this.$router` 的原因是，我们不想在每个需要操作路由的组件中都导入路由。
7. 要在 setup 函数中访问路由，需要调用 `useRouter` 或 `useRoute` 函数
8. 路径参数：**`params` 不能与 `path` 一起使用：如果提供了 path，params 会被忽略**，query和path、name都能正常使用
   1. query传参和params传参的区别：
      1. Query传参：**使用query属性，参数会以查询字符串的形式附加在URL后面**，使用`?`来标记参数的起始位置，多个参数使用`&`分隔。示例：`/user?id=1&name=John`，接收方通过`this.$route.query`来获取参数值，**刷新参数不丢失**
      2. Params传参：**使用params属性，避免参数暴露在URL上**，此时router的path配置不用添加`/:参数名`，**刷新参数丢失**
      3. 动态路由传参（router路径拼接）：**参数会直接嵌套在URL路径中**（就像一个子路径，和query的问号注意甄别），使用冒号:来定义动态参数。示例：`/user/1`，刷新参数**不丢失**

    ```javascript
    // 这些都会传递给 `createRouter`
    const routes = [
        // 动态字段以冒号开始
        { path: '/users/:id', component: User },
    ]
    //现在像 /users/johnny 和 /users/jolyne 这样的 URL 都会映射到同一个路由。

    //query传参：
    //使用 <router-link> 传递 query 参数
    //<router-link :to="{ path: '/user', query: { id: '1', name: 'John' } }">User</router-link>
    //使用 router.push() 传递 query 参数
    //this.$router.push({ path: '/user', query: { id: '1', name: 'John' } });
    // URL: /user?id=1&name=John
    console.log(this.$route.query.id);   // Output: "1"
    console.log(this.$route.query.name); // Output: "John"
    //也能直接使用to拼接参数：<router-link to="/more?userid=你好">More</router-link>通过query获取
    //但是不能够在path后面拼接?参数，这样的话params和query都是无法获取到的<router-link :to="{path:'/more?userid=邹竹'}">More</router-link>

    //params传参：参数不会暴露在URL上，不能和path一起使用，只能使用name
    //路由配置：name:  'user'，无需使用冒号参数形式:id
    //this.$router.push({ name: 'user', params: { id: '1', name: 'John' } })
    console.log(this.$route.params.id); // Output: "1"

    //动态路由传参，即拼接path：
    // 路由配置：path: '/user/:id'
    // 使用 <router-link> 传递 params 参数
    //<router-link :to="{ path: '/user/1' }">User</router-link>
    // 使用 router.push() 传递 params 参数
    //this.$router.push({ path: '/user/1' });
    // URL: /user/1
    console.log(this.$route.params.id); // Output: "1"
    ```

9. 上面例子中像 `/users/johnny` 和 `/users/jolyne` 这样的 URL 都会映射到同一个路由，当从前者导航到后者时，用的是同一个组件，因此会复用该组件，同时也导致组件的生命周期钩子不会被重复调用，为了解决这个问题，可以`watch $route对象上的任意属性`，在这个场景中，就是 `watch $route.params`；或者也可以使用组件内的导航守卫`beforeRouteUpdate`
10. 以 `/` 开头的嵌套路径将被视为根路径。这允许你利用组件嵌套，而不必使用嵌套的 URL
11. 每个组件都可以有自己的嵌套路由，需要在该路由组件内部添加`<router-view></router-view>`，并且在总路由配置中为该路由添加`children`选项
12. 如果嵌套子路由的路径为`/user/:id/abc`，当我们导航到`/user/:id`时，子组件User内部的router-view中不会显示任何内容，因为我们的abc路径并没有匹配，这种情况下如果想让router-view也渲染一些东西，可以添加一个空的嵌套路径：

    ```javascript
    const routes = [
    {
        path: '/user/:id',
        component: User,
        children: [
            // 当 /user/:id 匹配成功
            // UserHome 将被渲染到 User 的 <router-view> 内部
            { path: '', component: UserHome },

            // ...其他子路由
        ],
    },
    ]
    ```

13. 编程式导航：
    1. `this.$router.push`：向 history 栈添加一个新的记录，当你点击 `<router-link>` 时，内部会调用这个方法，所以点击 `<router-link :to="...">`（声明式导航） 相当于调用 `this.$router.push(...)`（编程式导航）
    2. **`params` 不能与 `path` 一起使用：如果提供了 path，params 会被忽略**
    3. **使用query传参，匹配方式可以是 name和path，使用params传参，匹配方式只能是name**
    4. 属性 `to` 与 `router.push` 接受的对象种类相同，所以两者的规则完全相同
    5. `router.push()` 和所有其他导航方法都会返回一个 Promise，让我们可以等到导航完成后才知道是成功还是失败
    6. `router.push({ path: '/home', replace: true })`相当于`router.replace({ path: '/home' })`
    7. `this.$router.go(n)` 相当于 `window.history.go(n)`
    8. `router.push`、`router.replace` 和 `router.go` 是 `window.history.pushState`、`window.history.replaceState` 和 `window.history.go`（go是html4就有了，前两个是html5新增） 的翻版，它们确实模仿了 `window.history` 的 API
14. 命名路由：带name属性的路由，要链接到一个命名的路由，可以向 router-link 组件的 to 属性传递一个对象：`<router-link :to="{ name: 'user', params: { username: 'erina' }}">`，将导航到路径 `/user/erina`
15. 命名视图：一个路由下面包含多个带name属性的router-view，没有显式设置的router-view的name属性默认为default，由此可以显示多个视图，并且路由配置下的component（该路由只有一个组件、一个router-view）需要变为components（表示该路由下有多个组件、与多个组件对应的多个router-view）

    ```javascript
    const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
        path: '/',
        components: {
            default: Home,
            // LeftSidebar: LeftSidebar 的缩写
            LeftSidebar,
            // 它们与 `<router-view>` 上的 `name` 属性匹配
            RightSidebar,
        },
        },
    ],
    })
    ```

16. 重定向：

    ```javascript
    //当用户访问 /home 时，URL 会被 / 替换，然后匹配成 /
    const routes = [{ path: '/home', redirect: '/' }];

    //重定向的目标也可以是一个命名的路由
    const routes = [{ path: '/home', redirect: { name: 'homepage' } }];

    const routes = [
        {
            // /search/screens -> /search?q=screens
            path: '/search/:searchText',
            redirect: to => {
                // 方法接收目标路由作为参数
                // return 重定向的字符串路径/路径对象
                return { path: '/search', query: { q: to.params.searchText } }
            },
        },
        {
            path: '/search',
            // ...
        }
    ]
    ```

17. 别名（路由配置中的alias属性）：将 / 别名为 /home，意味着当用户访问 /home 时，URL 仍然是 /home，但会被匹配为用户正在访问 / 的组件
18. 路由组件传参 —— router的props属性：
    1. **当 props 设置为 true 时，route.params 将被设置为组件的 props，从而不需要组件中通过`$route.params.id`获取，降低组件与路由的耦合度**
    2. 对于有命名视图的路由（component变为components对象，并且包含多个组件和对应视图），你必须为每个命名视图定义 props 配置

    ```javascript
    const User = {
        // 请确保添加一个与路由参数完全相同的 prop 名
        props: ['id'],
        template: '<div>User {{ id }}</div>'
    }
    const routes = [{ path: '/user/:id', component: User, props: true }]

    //命名视图必须为每个命名视图定义 props 配置
    const routes = [
        {
            path: '/user/:id',
            components: { default: User, sidebar: Sidebar },
            props: { default: true, sidebar: false }
        }
    ]
    ```

19. 历史记录模式：
    1. Hash 模式：通过`createWebHashHistory()`创建
    2. HTML5 模式/History模式：通过`createWebHistory()`创建
    3. absctract模式：`createMemoryHistory()`创建
    4. Hash与History模式对比：
       1. Hash模式在内部传递的实际 URL 之前使用了一个哈希字符（`#`）。由于这部分 URL 从未被发送到服务器，所以它不需要在服务器层面上进行任何特殊处理。不过，它在 SEO 中确实有不好的影响。
       2. History模式更漂亮，但是直接访问路由或者刷新的时候会访问服务器，此时会返回404（vue-router是客户端路由而非服务器路由），需要在你的服务器上添加一个简单的回退路由。如果 URL 不匹配任何静态资源，它应提供与你的应用程序中的 index.html 相同的页面。
    5. 扩展：
       1. 在 URL 中，井号（#）被用作锚点（Anchor）或哈希（Hash）来标记文档内的特定位置。例如，`http://example.com/page#section1` 表示访问 `http://example.com/page` 页面，并将页面滚动到 id="section1" 的元素所在位置。
       2. 锚点在单页面应用（SPA）中也经常用于实现路由功能，例如 Vue Router 的 Hash 模式就使用了锚点来模拟路由路径，如 `http://example.com/#/about`
    6. Vue的三种路由模式（原理：更新视图但不重新请求页面，即客户端路由）
       1. hash模式（onhashchange方法，可以监听location.hash的变化）
          1. 使用 URL 的 hash 来模拟一个完整的 URL，于是当 URL 改变时，页面不会重新加载，其显示的网路路径中会有 “#” 号，有一点点丑。这是最安全的模式，因为他兼容所有的浏览器和服务器。由于#后面的 URL 从未被发送到服务器，所以它不需要在服务器层面上进行任何特殊处理也不会导致服务器返回404。不过，它在 SEO 中确实有不好的影响
       2. history模式
          1. 原理：HTML5的pushState，这个语句可以在浏览器历史记录里面添加一条记录并且更新URL但是不会引起刷新`history.pushState(state, title, url)`;
          2. state是一个状态，它的值是一个对象，在vue中是生成了一个key存进去了，这个state可以在`window.onpopstate=function(event)`中的`event.state`中获取该保存的state值，相当于将state和url关联起来了，通过onpopstate来获取每个url中的state
          3. 美化后的hash模式，会去掉路径中的 “#”。依赖于Html5 的history，pushState API,所以要担心IE9以及以下的版本。服务器需要处理（nginx：`location / {  try_files $uri $uri/ /index.html;}`），如果 URL 不匹配任何静态资源，它应提供与你的应用程序中的 index.html 相同的页面，否则将返回404错误
       3. abstract模式
          1. 支持所有javascript运行模式。如果发现没有浏览器的API，路由会自动强制进入这个模式
    7. hash和history模式对比总结
       1. history更美观
       2. hash兼容性更好
       3. 刷新页面：history需要后端配置回退路由，否则会返回404错误，hash不需要
       4. 原理不同
20. 导航守卫
    1. **router.beforeEach全局前置守卫**（全局router中进行配置）
       1. router.beforeEach

            ```javascript
            router.beforeEach((to, from) => {
                // ...
                //to: 即将要进入的目标$route
                //from: 当前导航正要离开的路由$route
                // 返回 false 以取消导航
                // 但是没有返回值或者返回undefined/true，正常跳转
                return false
            })

            //还可以返回一个路由对象
            //一个例子
            router.beforeEach(async (to, from) => {
                if (
                    // 检查用户是否已登录
                    !isAuthenticated &&
                    // ❗️ 避免无限重定向
                    to.name !== 'Login'
                ) {
                    // 将用户重定向到登录页面
                    return { name: 'Login' }
                }
            })
            ```

       2. 守卫方法的返回值情况如下
          1. 不返回任何值：导航将继续进行，相当于调用了 `next()`
          2. 返回 false：导航将会被中断，当前导航被取消，用户将保持在当前页面
          3. 返回一个跳转对象：你可以通过返回一个跳转对象来进行重定向或者导航到不同的页面
       3. 可选的第三个参数next（v3.x版本中参数next时存在的并且必须使用`next()`才可以跳转，v4.x的next变为可选参数）：**当存在next时候，必须手动调用next()以正常跳转**，不调用`next()`则不会跳转
       4. 当一个导航触发时，全局前置守卫按照创建顺序调用。守卫是异步解析执行，此时导航在所有守卫 resolve 完之前一直处于等待中
    2. **router.beforeResolve全局解析守卫**（全局router中进行配置）
       1. 和 router.beforeEach 类似，因为它在每次导航时都会触发，不同的是，解析守卫刚好会在导航被确认之前、所有组件内守卫和异步路由组件被解析之后调用
       2. router.beforeResolve 是获取数据或执行任何其他操作（如果用户无法进入页面时你希望避免执行的操作）的理想位置。

            ```javascript
            router.beforeResolve(async to => {
                if (to.meta.requiresCamera) {
                    try {
                        await askForCameraPermission()
                    } catch (error) {
                        if (error instanceof NotAllowedError) {
                            // ... 处理错误，然后取消导航
                            return false
                        } else {
                            // 意料之外的错误，取消导航并把错误传给全局处理器
                            throw error
                        }
                    }
                }
            })
            ```

    3. **router.afterEach全局后置钩子**（全局router中进行配置）
       1. 和守卫不同的是，全局后置钩子**不会接受 next 函数**也不会改变导航本身
       2. 可选的第三个参数failure：如果导航被阻止，导致用户停留在同一个页面上，由 router.push 返回的 Promise 的解析值将是 Navigation Failure，为一个falsy的值（通常为undefined），可以区分我们导航是否离开了当前位置

        ```javascript
        router.afterEach((to, from, failure) => {
            if (!failure) sendToAnalytics(to.fullPath)
        })
        ```

    4. **beforeEnter路由独享的守卫**（单个route内部配置）
       1. beforeEnter 守卫 只在进入路由时触发，参数（ params、query 或 hash）改变不会触发该守卫
       2. beforeEnter 的值可以是一个数组，包含多个守卫方法

        ```javascript
        const routes = [
            {
                path: '/users/:id',
                component: UserDetails,
                beforeEnter: (to, from) => {
                    // reject the navigation
                    return false
                },
            },
        ]
        ```

    5. 组件内三大导航守卫：**`beforeRouteEnter`、`beforeRouteUpdate`、`beforeRouteLeave`**
       1. 例子

            ```javascript
            const UserDetails = {
                template: `...`,
                beforeRouteEnter(to, from) {
                    // 在渲染该组件的对应路由被验证前调用
                    // 不能获取组件实例 `this` ！
                    // 因为当守卫执行时，组件实例还没被创建！
                    // 除非使用next的参数函数的第一个参数vm来手动获取实例，后面有范例
                },
                beforeRouteUpdate(to, from) {
                    // 在当前路由改变，但是该组件被复用时调用
                    // 举例来说，对于一个带有动态参数的路径 `/users/:id`，在 `/users/1` 和 `/users/2` 之间跳转的时候，
                    // 由于会渲染同样的 `UserDetails` 组件，因此组件实例会被复用。而这个钩子就会在这个情况下被调用。
                    // 因为在这种情况发生的时候，组件已经挂载好了，导航守卫可以访问组件实例 `this`
                },
                beforeRouteLeave(to, from) {
                    // 在导航离开渲染该组件的对应路由时调用
                    // 与 `beforeRouteUpdate` 一样，它可以访问组件实例 `this`
                },
            }

            //beforeRouteEnter无法访问this解决方法
            beforeRouteEnter (to, from, next) {
                next(vm => {
                    // 通过 `vm` 访问组件实例，vm即是this
                })
            }
            ```

       2. beforeRouteEnter 守卫 不能 访问 this，因为守卫在导航确认前被调用，因此即将登场的新组件还没被创建。不过，你可以通过传一个回调给 可选的第三个参数next 来访问组件实例。**在导航被确认的时候执行回调**，并且把组件实例作为回调方法的参数
       3. **beforeRouteEnter 是支持给 next 传递回调函数的唯一守卫**。对于 beforeRouteUpdate 和 beforeRouteLeave 来说，this 已经可用了，所以不支持 传递回调，因为没有必要了（**beforeRouteUpdate和beforeRouteLeave不支持next参数**）
    6. 回调函数支持可选参数`next`的：`beforeEach`、`beforeRouteEnter`
    7. **完整导航流程顺序**
       1. 导航被触发。
       2. 在失活的组件里调用 beforeRouteLeave 守卫。
       3. 调用全局的 beforeEach 守卫。
       4. 在重用的组件里调用 beforeRouteUpdate 守卫(2.2+)。
       5. 在路由配置里调用 beforeEnter。
       6. 解析异步路由组件。
       7. 在被激活的组件里调用 beforeRouteEnter。
       8. 调用全局的 beforeResolve 守卫(2.5+)。
       9. 导航被确认。
       10. 调用全局的 afterEach 钩子。
       11. 触发 DOM 更新。
       12. 调用 beforeRouteEnter 守卫中传给 next 的回调函数，创建好的组件实例会作为回调函数的参数传入
21. meta属性：路由元信息，**可以用于路由拦截**
    1. 有时，你可能希望将任意信息附加到路由上，如过渡名称、谁可以访问路由等。这些事情可以通过接收属性对象的meta属性来实现，并且它可以在路由地址和导航守卫上都被访问到
    2. meta可以通过`$route.meta`访问到
    3. 路由拦截例子

        ```javascript
        const router = createRouter({
            history: createWebHashHistory(),
            routes: [
                {
                    path: '/posts',
                    component: PostsLayout,
                    children: [
                        {
                            path: 'new',
                            component: PostsNew,
                            // 告诉钩子，必须经过身份验证的用户才能进入本路由
                            meta: { requiresAuth: true }
                        },
                        {
                            path: ':id',
                            component: PostsDetail
                            // 任何人都可以阅读文章
                            meta: { requiresAuth: false }
                        }
                    ]
                }
            ]
        })

        //全局前置守卫鉴权并且令没有登陆的重定向至登陆页面
        router.beforeEach((to, from) => {
            // 而不是去检查每条路由记录
            // to.matched.some(record => record.meta.requiresAuth)
            if (to.meta.requiresAuth && !auth.isLoggedIn()) {
                // 此路由需要身份验证，需要检查是否已登录
                // 如果没有登录，则重定向到登录页面
                return {
                    path: '/login',
                    // 保存我们所在的位置，以便以后再来
                    query: { redirect: to.fullPath },
                }
            }
        })
        ```

22. 获取数据：可以在导航完成之后获取（组件的created/mounted内获取），也可以在导航完成之前获取（组件内的路由导航守卫beforeRouteEnter中获取）
23. 组合式API中使用路由：
    1. 通过`useRouter()`和`useRoute()`函数来使用/代替选项式的`this.$router`和`this.$route`
    2. 模板中我们仍然可以访问 `$router` 和 `$route`，所以不需要在 `setup` 中返回 `router` 或 `route`
    3. route 对象是一个响应式对象，所以它的任何属性都可以被监听，但你应该避免监听整个 route 对象。在大多数情况下，你应该直接监听你期望改变的参数getter

        ```javascript
        // 当参数更改时获取用户信息
        watch(
            () => route.params.id,
            async newId => {
                userData.value = await fetchUser(newId)
            }
        )
        ```

    4. `setup`中`onBeforeRouteLeave`, `onBeforeRouteUpdate`作为组合式函数公开，但是setup中没有onBeforeRouteEnter这样的守卫函数，实在要用可以使用选项式写法或者多写一个script标签来使用`beforeRouteEnter`守卫（网友提供的方法，也可以使用`defineOptions`实现，感觉比较偏门，详见[这里](https://blog.csdn.net/oafzzl/article/details/125045087)）
24. 导航的滚动行为，**全局router中进行配置scrollBehavior选项**
    1. 可以像原生的`history.scrollRestoration`（有两个可选值：默认值`auto`：将恢复/保留用户已滚动到的页面上的位置，刷新页面默认会保留；   `manual`：不还原页上的位置。用户必须手动滚动到该位置）属性一样定义页面的滑动行为，只在支持 history.pushState 的浏览器中可用
    2. 示例

        ```javascript
        const router = createRouter({
            history: createWebHashHistory(),
            routes: [...],
            scrollBehavior (to, from, savedPosition) {
                // return 期望滚动到哪个的位置
            }
        })
        ```

    3. 需为router提供一个 scrollBehavior 方法，其中的to和from表示单个路由对象，和导航守卫的路由对象类似，函数返回值可以是如下值
       1. 一个[scrollTo](#scrollTo)的options对象
       2. 也可以是一个元素加上对其的相对偏移量
       3. 也可以是savedPosition，即保留并恢复到上次的滚动条位置，和浏览器的原生表现一致（history.scrollRestoration为auto）

        ```javascript
        const router = createRouter({
            scrollBehavior(to, from, savedPosition) {
                return { 
                    top: 0,
                    left: 0,
                    behavior: 'smooth'
                    //上面就是scrollTo的options的三大属性，后续文档中有详细介绍

                    // // 也可以这么写
                    // // el: document.getElementById('main'),
                    // el: '#main',
                    // top: -10
                }
            },
        })

        //返回一个Promise，使得切换路由时候稍作延迟以等待过度效果结束
        const router = createRouter({
            scrollBehavior(to, from, savedPosition) {
                return new Promise((resolve, reject) => {
                    setTimeout(() => {
                        resolve({ left: 0, top: 0 })
                    }, 500)
                })
            },
        })
        ```

25. 路由懒加载/动态导入：**令components接收一个返回Promise的函数**，Vue Router 只会在第一次进入页面时才会获取这个函数
    1. 不要在路由中使用异步组件。异步组件仍然可以在路由组件中使用，但路由组件本身就是动态导入的。

    ```javascript
    // 将
    // import UserDetails from './views/UserDetails.vue'
    // 替换成
    const UserDetails = () => import('./views/UserDetails.vue')

    const router = createRouter({
        // ...
        routes: [{ path: '/users/:id', component: UserDetails }],
    })

    //webpack魔法注释令多个组件打包到同一个异步模块中
    const UserDetails = () =>
    import(/* webpackChunkName: "group-user" */ './UserDetails.vue')
    const UserDashboard = () =>
    import(/* webpackChunkName: "group-user" */ './UserDashboard.vue')

    // vite
    // vite.config.js
    export default defineConfig({
        build: {
            rollupOptions: {
                // https://rollupjs.org/guide/en/#outputmanualchunks
                output: {
                    manualChunks: {
                    'group-user': [
                        './src/UserDetails',
                        './src/UserDashboard',
                        './src/UserProfileEdit',
                    ],
                    },
                },
            },
        },
    })

    ```

26. vue-router的不同路由组件在切换导航时会销毁和重建，导致数据丢失，这样可以避免组件之间状态的相互影响，提高性能，并且确保在切换路由时，上一个路由对应的组件实例被正确销毁，避免内存泄漏问题。但是也会导致原组件的数据、页面状态丢失，如果想要避免，可以使用`<keep-alive></keep-alive>`缓存路由组件保持组件活跃

    ```html
    <!-- 默认使用的router-view组件，不带插槽，每次切换路由时会销毁和重建组件，包括内部的生命周期等所有信息 -->
    <!-- <router-view/> -->
    <!-- 与下面的代码等价 -->
    <!-- 
    <router-view v-slot="{ Component }">
        <component :is="Component" />
    </router-view>
    -->

    <!-- 下面是缓存路由组件的例子 -->
    <router-view v-slot="{ Component }">
        <keep-alive>
            <component :is="Component" />
        </keep-alive>
    </router-view>
    ```

27. 动态路由：`router.addRoute()` 和 `router.removeRoute()`添加和删除路由
28. 对比vue-router 3.x 详见[这里](https://router.vuejs.org/zh/guide/migration/)
29. 如果调用 `history.replaceState()/pushState()` 时没有保留当前状态，你需要传递当前 `history.state`：因为**vue-router使用历史状态state来保存导航信息，如滚动位置，以前的地址等**

## HTML

### HTML基础

1. 没有关闭标签的空元素应该在开始标签中添加斜杠，比如`<br/>`、`<img />`、`<meta />`
2. html对标签/标签内属性的大小写不敏感，大小写效果一致
3. 标签内的title属性会添加鼠标悬浮提示，img标签的alt可以让纯文本浏览器/图片加载失败时显示设置的文字
4. img+map(area)可以实现将图片按照坐标“分割”为不同部分，每个部分可以映射一个超链接地址
5. id锚点配合a标签的href可以跳转到页面指定位置
6. `<noscript>抱歉，您的浏览器不支持 JavaScript！</noscript>`
7. meta：meta 元素被用于规定页面的描述、关键词、文档的作者、最后修改时间以及其他元数据，元数据可用于浏览器（如何显示内容或重新加载页面），搜索引擎（关键词等作为索引的依据），或其他 web 服务。
8. meta属性、值、作用：
    1. charset：定义文档使用的字符集。示例：`<meta charset="UTF-8">`
    2. name 和 content：用于设置元信息，例如描述页面的关键字、描述、作者等。示例：`<meta name="description" content="This is a webpage description.">`
    3. http-equiv 和 content：用于模拟 HTTP 头部字段，例如设置文档的过期时间、缓存控制等。示例：`<meta http-equiv="refresh" content="5;url=https://example.com">`设置定时刷新
    4. viewport：用于控制页面在移动设备上的视口（viewport）设置，以确保页面在不同设备上显示正常。示例：`<meta name="viewport" content="width=device-width, initial-scale=1.0">`设置（name为）视口，视口宽度为设备宽度，初始加载时不进行放大缩小
    5. http-equiv： content-security-policy、content-type、default-style、refresh
    6. name：     application-name、author、description、generator、keywords、viewport
9. **viewport meta与rem**
   1. `content="width=device-width, initial-scale=1.0"`，`width=device-width`设置了视口宽度为设备的宽度，这个设置很重要，因为不同设备的宽度不一样，不能把宽度写死以免造成显示问题；initial-scale表示初始的缩放倍数，默认是1
   2. 已知设备是iPhone 14Pro Max，若不做兼容处理，使用rem且initial-scale=1时，我们的文档会远远超出可视区域的大小，因为它的分辨率和像素比（DPR，为3）过高，因此页面需要缩放为`1/DPR`也就是`initial-scale=0.333333333`才会正常、清晰地显示
   3. 对于rem布局，我们需要获取设计稿的宽度，并且需要对像素做计算转化为对应的rem值。假设某个UI稿设计宽度为 750px，那么我们需要得到它与根元素html宽度（`document.documentElement.getBoundingClientRect().width`）的比例，然后再乘以当前设备默认字体的font-size，就得到了转换后的rem，也就是一个字的尺寸，这样就做到了在不同屏幕大小下都能按比例放大/缩小
10. 语义化标签
    header 定义文档或节的页眉
    nav 定义导航链接的容器
    section 定义文档中的节，content
    article 定义独立的自包含文章
    aside 定义内容之外的内容（比如侧栏）
    footer 定义文档或节的页脚
    details 定义额外的细节
    summary 定义 details 元素的标题
    main
    ···
11. html、head、body标签都是可以省略的，但是不推荐，[解释](https://www.w3school.com.cn/html/html5_syntax.asp)
12. ISO-8859-1字符集，避免字符被识别为标签，[全列表](https://www.w3school.com.cn/charsets/ref_html_8859.asp)
13. url编码（可用`encodeURIComponent`）： URL 常常会包含 ASCII 集合之外的字符，URL 必须转换为有效的 ASCII 格式。URL 编码使用 "%" 其后跟随两位的十六进制数来替换非 ASCII 字符。URL 不能包含空格。URL 编码通常使用 + 来替换空格。
14. `<!DOCTYPE html>`文档声明，文档类型包括多种：html、html5、xhtml等等
15. 条件注释(**实操好像无效**)： It（低于）、Ite（低于或等于）、gt（高于）、gte（高于或等于）
16. HTML5新增了很多元素，比如datalist标签：

    ```html
    <form>
        <input id="my_input" list="my_list"/>
        <datalist id="my_list">
            <option value="parrot">鹦鹉</option>
            <option value="pig">猪猪</option>
            <option value="cow">牛牛</option>
        </datalist>
    </form>
    ```

    结果如下
    <form>
        <input id="my_input" list="my_list"/>
        <datalist id="my_list">
            <option value="parrot">鹦鹉</option>
            <option value="pig">猪猪</option>
            <option value="cow">牛牛</option>
        </datalist>
    </form>
17. HTML5新增的输入类型（input的type值）：color date datetime datetime-local email month number range search tel time url week
18. [视频处理](https://www.w3school.com.cn/html/html_video.asp)、[音频处理](https://www.w3school.com.cn/html/html_audio.asp)
19. 获取地理位置：`navigator.geolocation.getCurrentPosition(showPosition,showError)`，还可以[显示地图结果](https://www.w3school.com.cn/html/html5_geolocation.asp)
20. HTML5拖放

    ```html
    <!-- html-->
    <img id="drag_img" ondragstart="drag(event)" draggable="true" src="./images/image.jpg" height="50" title="图片提示" alt="图片的文字替代">
    <div class="drag_container" ondragover="container_allow_drop(event)" ondrop="drop(event)">
        <span id="comment">将图片拖到此处</span>
    </div>
    ```

    ```javascript
    //js
    //被添加元素的 ondragover事件
    function container_allow_drop(e) {//默认地，数据/元素无法被放置到其他元素中。为了实现拖放，我们必须阻止元素的这种默认的处理方式。
        e.preventDefault();
    }
    //被添加元素的ondrop事件
    function drop(e) {//调用 preventDefault() 来阻止数据的浏览器默认处理方式（drop 事件的默认行为是以链接形式打开）
        e.preventDefault();
        e.target.appendChild(document.getElementById(e.dataTransfer.getData('drag_id')));
    }
    //被拖拽元素的ondragstart事件
    function drag(e) {
        e.dataTransfer.setData("drag_id", e.target.id);
    }
    ```

21. Web储存：localStorage、sessionStorage、cookie，浏览器支持：

    ```javascript
    if (typeof(Storage) !== "undefined") {
        // 针对 localStorage/sessionStorage 的代码
    } else {
        // 抱歉！不支持 Web Storage ..
    }
    ```

22. HTML5应用程序缓存
    1. 概念：通过创建 manifest 文件，可轻松创建 web 应用的离线版本
        1. 离线浏览 - 用户可在应用离线时使用它们
        2. 速度 - 已缓存资源加载得更快
        3. 减少服务器负载 - 浏览器将只从服务器下载更新过或更改过的资源
    2. 例子

        ```html
        <!DOCTYPE HTML>
        <html manifest="demo.appcache">

        <body>
        文档内容 ......
        </body>

        </html>
        ```

    3. 每个指定了 manifest 的页面在用户对其访问时都会被缓存。如果未指定 manifest 属性，则页面不会被缓存。manifest 文件的建议文件扩展名是：".appcache"。
    4. manifest 文件需要设置正确的 MIME-type，即 "text/cache-manifest"。必须在 web 服务器上进行配置
    5. manifest文件例子如下，三个选项依次是缓存、不缓存、资源无法访问时的代替资源

        ```bash
        CACHE MANIFEST
        # 2022年x月x日，注释，使用时间戳可以让浏览器更新缓存，只有manifest文件发生改变时浏览器才会更新缓存
        /theme.css
        /logo.gif
        /main.js

        NETWORK:
        login.asp

        FALLBACK:
        /html/ /offline.html
        ```

23. H5 Web Worker
    1. 创建后台线程，其指定的JavaScript文件会在后台线程中运行，不会影响页面的性能。
    2. 示例：

        ```javascript
        if(typeof(Worker) !== "undefined") {
            if(typeof(w) == "undefined") {
                w = new Worker("demo_workers.js");
            }
            w.onmessage = function(event) {//worker后台线程和主线程只能通过postmessage和onmessage进行通信
                document.getElementById("result").innerHTML = event.data;
            };
        } else {
            document.getElementById("result").innerHTML = "Sorry! No Web Worker support.";
        }
        //终止worker
        w.terminate();
        ```

    3. 限制：
        1. 必须同源，且谷歌无法加载本地的js(worker)文件
        2. document/window/parent/alert等都无法读取，可使用self代替window
        3. 和主线程需要通过postMessage方法来通信
24. H5 Server-Sent Event（SSE）以及webSocket
    1. 实现服务器主动推送信息的方式：
        1. SSE：发送的不是一次性的数据包，而是一个数据流（流信息），会连续不断地发送过来，这样客户端就不会关闭连接，比如视频播放。
        2. WebSocket：浏览器和服务器只需要完成一次握手，两者之间就直接可以创建持久性的连接，并进行双向数据传输。服务器可以主动向客户端推送信息，客户端也可以主动向服务器发送信息，是真正的双向平等对话，属于服务器推送技术的一种
    2. 上述两者的区别：（单双同源协轻重）
        1. **SSE单向通道（只能服务器发送数据，客户端无法像服务器发送数据，所以没有send方法），WebSocket是全双工通道，可以双向通信，更加强大、灵活（所以有ws.send(data)方法）**
        2. **SSE有同源限制，WebSocket没有同源限制，客户端可以与任意服务器通信**
        3. **SSE 使用 HTTP 协议，现有的服务器软件都支持。WebSocket 是一个独立协议。（SSE优点）**
        4. **SSE 属于轻量级，使用简单；WebSocket 协议相对复杂。**
        5. **SSE 默认支持断线重连，WebSocket 需要自己实现。**
        6. **SSE 一般只用来传送文本，二进制数据需要编码后传送，WebSocket 默认支持传送二进制数据。**
        7. **SSE 支持自定义发送的消息类型**
    3. 前端用法：
       1. SSE：`var source = new EventSource(url, { withCredentials: true });`
       2. websocket：`let ws = new WebSocket('url');`
       3. 都有的监听事件：onopen、onmessage、onerror
       4. websocket独有的方法：onclose、send

        ```javascript
        //SSE用法
        var source = new EventSource(url, { withCredentials: true });
        //第二个参数可省略，用于跨域时传递cookie

        source.onmessage = function (event) {
            var data = event.data; 
        };
        //onopen 当通往服务器的连接被打开
        //onmessage 当接收到消息
        //onerror 当发生错误

        // 另一种写法
        source.addEventListener('message', function (event) {
            var data = event.data;
        }, false);
        ```

        ```javascript
        //webSocket用法
        let ws = new WebSocket('url');
        switch (ws.readyState) {//下面几个预设常量的值分别为0，1，2，3
            case WebSocket.CONNECTING:
                console.log("websocket连接中......");
                break;
            case WebSocket.OPEN:
                console.log("连接成功，可以通信了");
                break;
            case WebSocket.CLOSING:
                console.log("正在关闭连接......");
                break;
            case WebSocket.CLOSED:
                console.log("连接已关闭");
                break;
        }
        //以下的on前缀监听事件也可以使用ws.addEventListener来监听
        ws.onopen = function () {
            ws.send("发送给服务器的数据");
            //发送blob对象数据
            var file = document
                .querySelector('input[type="file"]')
                .files[0];
            ws.send(file);
            //还可以发送ArrayBuffer等
        }
        ws.onmessage = function (event) {
            console.log(`持续接收到服务器的数据:event.data：\n ${event.data}`);
        }
        ws.onclose = function (event) {
            var code = event.code;
            var reason = event.reason;
            var wasClean = event.wasClean;
            console.log(`连接已经关闭，关闭代码：${code}，关闭原因：${reason}，关闭是否干净：${wasClean}`);
        }
        ws.onerror = function (event) {
            console.log(`连接出错了：${event}`);
        }
        ```

    4. 服务器配合：SSE的服务器必须把 "Content-Type" 报头设置为 "text/event-stream"
    5. 其它：自定义事件：服务器设置返回的字段event为自定义事件名称，详见[阮一峰](http://www.ruanyifeng.com/blog/2017/05/server-sent_events.html)
25. http状态，[完整列表](https://www.w3school.com.cn/tags/html_ref_httpmessages.asp)
    1. 100：GET 的会把data拼接在URL中发送服务器，服务器接收成功并返回200 ；**POST 是先将hearder发给服务器返回100**，**再发送data给到服务器，返回200**（http状态码100：服务器已收到请求第一部分，等待其余部分；200：服务器已成功处理请求）
    2. 200：成功
    3. 301：永久重定向
    4. 302：临时重定向
    5. 304：协商缓存服务器告诉客户端，原来的缓存文件可以直接使用而不用下载
    6. 400 Bad Request：服务器未能理解请求。
    7. 401 Unauthorized：被请求的页面需要用户名和密码
    8. 403 Forbidden：对页面的访问被禁止。
    9. 404 Not Found：服务器无法找到被请求的页面
    10. 500 Internal Server Error：请求未完成。服务器遇到不可预知的情况，有可能是后端代码出bug了
    11. 502 Bad Gateway：请求未完成。服务器从上游服务器收到一个无效的响应，项目构建发布的时候容易出现这个错误
26. [浏览器快捷键列表](https://www.w3school.com.cn/tags/html_ref_keyboardshortcuts.asp)

## CSS

### position定位

1. static（不脱离文档流）：
   1. 正常的布局行为，从上到下，**top/right/bottom/left/z-index无效**
2. relative（**不脱离**文档流）
   1. 相对其在正常文档流中的位置进行定位，使用top、right、bottom、left进行偏移定位，原先正常位置的空间将会保留（留为空白），不会影响其他元素的正常布局（下面的不会被挤下去，而是和本元素重叠），如果不设置偏移量，则与static布局一致
3. absolute（脱离文档流）
   1. 向上寻找非static的元素，相对其布局，外层一直没有非static元素则相对body布局
4. fixed（脱离文档流）
   1. 通过指定元素相对于屏幕视口（viewport）的位置来指定元素位置。元素的位置在屏幕滚动时不会改变。**当元素祖先的 transform、perspective、filter 或 backdrop-filter 属性非 none 时，容器由视口改为该祖先。所以会出现fixed不相对于窗口定位的情况**
5. sticky
   1. 元素根据正常文档流进行定位，设置top、right、bottom 和 left 偏移阈值后，上下滑动至超过偏移阈值时，会相对它的**最近滚动祖先**和最近块级祖先定位，固定在该偏移值位置。比如滚动父元素中有abcd4个元素高度都为400px，初始为c元素设置`position:sticky; top:100px;`，布局不会改变，此时向上滑动页面，abcd也随之在父元素中上向滚动，滚动至c元素距离父元素顶部只有100px时，c元素会固定在那不再向上移动；同理向下移动至距离超过100px时，c元素又会取消固定而回到原来文档中的位置

### 盒子模型

1. 把元素看成一个盒子，布局规范从外到内包含margin、border、padding、content
2. 标准盒子模型：`box-sizing: content-box`，一个盒子的宽度是：左右margin+左右border+左右padding+width
3. 怪异盒子模型：`box-sizing: border-box`， 盒子的宽度为margin+width，其中padding、border的宽度会包含在width内，flex容器内的元素也可以设置

### flex布局

1. flex布局：
   1. 容器属性：flex-direction、flex-wrap、flex-flow、justify-content、align-items、align-content；
   2. 容器内项目属性：order、flex-grow、flex-shrink、flex-basis、flex、align-self
2. 具体点击[这里](http://www.zouzhu.site/think/flex)

### 浮动

1. 为一个元素设置浮动为left/right时，允许它沿父容器**最左边或者最右边**放置（或者碰到另一个浮动的元素），直至该行装不下了换行，**和absolute一样，其它元素会无视它占据的空间直接在它所在位置下面布局并且被它覆盖**，**但是文字除外**，**所有的本应“被覆盖”文字会主动绕开它所占有的空间，从而围绕它布局（即使是普通div内部的文字也会被挤出原div，但是被挤出的文字不会影响后面的布局）**，因此可以这么形容float布局：它脱离文档流，但是不脱离*文本流*
2. 如何防止正常元素被值非none的float元素覆盖，正常元素设置`clear:left/right`来清除向左/右浮动元素导致的覆盖，或者直接设置clear:both来清除所有的浮动覆盖
3. 假如我们有这样一种情况：父容器宽度为800px，两个浮动子元素A、B一个向左浮动一个向右浮动，宽度都为500px，那么会导致A、B换行，A右边留白，B左边留白，此时对于AB后面的子元素C来说它们非文本部分会被覆盖，那么，这个时候设置C的`clear:left`，那么C则会往下移，不再被向左浮动的A元素覆盖，**而只被向右浮动的B覆盖**；如果设置`clear:right`，那么就会移动到B下面，不被向右浮动的B覆盖，更不会被A覆盖了，clear会让C元素在所清除影响的浮动元素下面一行开始布局，so不要想当然地认为会产生“断层”

### css其它

1. p:nth-last-child(2)和p:nth-last-of-type(2)，可以传参为2n（n从0开始）表示偶数位置的所有标签
   1. p:nth-last-child(2)：p标签，并且是父元素倒数第二个标签时该选择器生效，p:nth-child(2)同理，p:nth-child(1)等价于p:first-child，p:nth-last-child(1)等价于p:last-child
   2. p:nth-last-of-type(2)找到父标签下所有的p标签的倒数第二个并且生效，p:nth-of-type(2)同理
2. 属性选择器 `[attribute='value']` 用于选择具有特定属性值的元素，比如`p[v-clock='1']:first-child{}`会选出：具有 v-clock 属性，并且该属性的值为 '1'，同时是其父元素下的第一个子元素的p标签
3. css三大特性：
   1. 继承：即子类元素继承父类的样式（可继承：cursor、visibility、color、font等）;
   2. 优先级：多个样式设置相同的属性更高优先级的选择器会被优先应用;
   3. 层叠：具有多个样式，通过层叠(后者覆盖前者、优先级高的覆盖优先级低的、不同的属性会叠加)的样式
4. CSS变量和简单计算：

    ```CSS
    /**变量使用--前缀 */
    :root {
        --primary-color: #007bff;
        --secondary-color: #6c757d;
        --box-width: 200px;
    }
    /**使用变量用var()，计算用calc() */
    .box {
        color: var(--primary-color);
        background-color: var(--secondary-color);
        width: calc(var(--box-width) + 20px);
        height: calc(50vh + 10px);
        font-size: calc(12px * 1.5);
    }
    ```

5. BFC：一个独立的容器，不同的BFC区域之间是相互独立的，互不影响的。利用这个特性我们可以让不同BFC区域之间的布局不产生影响。每一个BFC区域只包括其子元素，不包括其子元素的子元素。
   1. 主要特点
      1. 内部的块级元素会在垂直方向上一个接一个地放置，形成一个垂直的流。
      2. **属于同一个 BFC 的相邻块级元素的 margin 会发生折叠**，导致它们之间的间距可能会比预期的小，可以为这两个元素创建两个单独的BFC（比如为其各自嵌套一个新的父元素并且设置`overflow:hidden`或者display:flex）
      3. BFC 可以包含浮动元素，并阻止浮动元素溢出到 BFC 外部（默认情况下浮动元素脱离了文档流，父元素中不会将其视为子元素的一部分而包括进去，所以如果浮动元素比父元素高，则会溢出父元素而不是撑高父元素）。
      4. BFC 的区域不会与浮动元素重叠，避免产生布局混乱。
   2. 常见创建BFC方式
      1. body根元素
      2. float 属性不为 none
      3. position 为 absolute 或 fixed，注意relative不符合
      4. display 为 flex，inline-block，table-cell等
      5. overflow 不为 默认值visible，可以设置为auto
6. `background-image: url("../../media/examples/lizard.png"),url("../../media/examples/star.png");`前一张背景图会覆盖后一个，从底往上依次为：lizard.png → star.png
7. 伪类（`:`），一个冒号，注意和伪元素区分，**指定元素在特殊状态下的样式**
   1. `a:hover`，鼠标悬浮的样式
   2. `a:active`，按下按钮和松开按钮之间的间隙
   3. `a:visited`，访问过该链接后的样式
   4. `:root`根元素，一般是html，可以用来声明全局CSS变量：`:root{--main-color: pink; --p-padding: 20px 40px;}`
   5. 还有好多，mdn上自己慢慢看
8. 伪元素（`::`），两个冒号注意和伪类区分
   1. 概念：一个附加至选择器末尾的关键词，**允许修改特定部分样式**，比如`p::first-line{}`对每个p元素的第一行设置样式
   2. 每个选择器只能有一个伪元素
   3. 常见伪元素：
      1. `a::after`，给a创建一个子元素，作为a的**最后一个子元素**，默认是行内元素。通常会配合**content**属性来为该元素添加装饰内容，这个content可以是一个字符串，可以是一个图片的url，也可以是`attr(prop)`，这个prop是父元素a的某个属性，比如href，甚至可以是一个自定义的属性，你可以随意在父元素上添加属性并且在伪元素的`content: attr(prop)`内使用；content还可以是none、normal、计数器等等其他东西
      2. `a::before`，创建一个子元素作为a的第一个子元素，默认行内元素
      3. `p::first-letter`，p第一行第一个字母
9. 实现三角形，设置空元素长宽为0，设置4个方向的border会发现每个border都是占用整体四分之一大小的三角形，设置不需要显示的border颜色为transparent即可
10. 如果文档宽度小于 300 像素则修改背景颜色(background-color):

    ```css
    @media screen and (max-width: 300px) {
        body {
            background-color:lightblue;
        }
    }
    ```

11. link引入的css会并行下载，会阻塞js的执行，引入的js会阻塞页面渲染
12. import 也是一种引入样式的方式，但是它通常用于在 CSS 文件中引入其他 CSS 文件，在当前样式表加载完成后再加载，这可能会导致样式表的加载顺序出现问题，不常用
13. 最常用的css选择器：标签选择器、.类选择器、ID选择器、属性选择器、伪类选择器。
14. 样式优先级：**!important > 行内样式> ID选择器 > 类选择器 > 标签 > 通配符 > 继承 > 浏览器默认属性**
15. 预处理器：less
    1. 使用 css 实现 js 一样的变量、等以及实现样式的复用时非常麻烦，并且代码比较臃肿，难以维护
    2. 使用less变量就很简单，直接`@`变量名进行定义，并且可以直接使用运算符进行四则运算，简洁
    3. 混合可以将一部分样式抽出，作为单独定义的模块，被很多选择器重复使用。
    4. 函数、选择器嵌套等特性，less让css变得更贴近一门“编程语言”，也让代码更具结构化和可读性，降低维护成本

## ES6

### let和const

1. ES6新增加了两个重要的 JavaScript 关键字: let 和 const。
2. let 声明的变量只在 let 命令所在的代码块内有效
3. const 声明一个只读的常量，一旦声明，常量的值就不能改变，因此**一旦声明必须初始化**，否则报错
4. **const其实保证的不是变量的值不变，而是保证变量指向的内存地址所保存的数据不允许改动**，对于简单类型（数值 number、字符串 string 、布尔值 boolean）,值就保存在变量指向的那个内存地址，因此 const 声明的简单类型变量等同于常量。而复杂类型（对象 object，数组 array，函数 function），变量指向的内存地址其实是保存了一个指向实际数据的指针
5. let、const和var的区别：
   1. let、const 只能声明一次 var 可以声明多次
   2. **let、const有块级作用域**（以花括号{}为界限，外部的不能访问内部的），**var只有函数作用域和全局作用域**，没有块级作用域（可以访问{}内部的变量，如while内的var变量在外部可以访问）
   3. let，const不存在变量提升，因此存在暂时性死区（声明之前不能访问，否则报错），而var没有
   4. **浏览器中，var声明的变量挂载在global（即window对象）上，因此可以通过this访问到**；而**let、const声明的变量挂载在**另一个和global（即window）同级别的**块级作用域（名为script）下**，因此无法通过this访问（浏览器中的this访问的就是globalThis也就是window，node中的this访问的是global）

### 箭头函数

1. **箭头函数没有自己的super、new.target（函数内部使用，用于判断函数是否通过new调用，是则返回当前构造函数，否则返回undefined）、this、arguments，不能被实例化（不能new一个箭头函数，否则报错，函数不是构造函数）**；在箭头函数里面取arguments/this都会向外层非箭头函数寻找；

    ```javascript
    class Parent {
        name;
        sex;
        constructor(n, s) {
            this.name = n;
            this.sex = s;
        }
    }
    class Child extends Parent {
        age;
        constructor(n, s, a) {
            // const arr_fn = function () {//在这里用super会报错-super unexpected here，只能用箭头函数；用this也会是undefined；
            const arr_fn = () => {
                super(n, s);//调用父类构造函数，除此外还能super.static_method访问父类静态方法
                this.age = a;//this从外层非箭头函数取得
                console.log(Array.from(arguments));//arguments从外层非箭头函数取得：[ '走珠', '1', 24 ]
            }
            arr_fn();
        }
        say=()=>{
            console.log("🚀 ~ Parent ~ say ~ this:", this);//输出实例本身
        }
    }
    let c = new Child('走珠', '1', 24);
    console.log(JSON.stringify(c));//{"name":"走珠","sex":"1","age":24}

    age = 123;
    const arr1 = () => { () => this.age }
    const arr2 = () => () => this.age;
    console.log(arr1());//undefined，这里注意，arr1本来就没有返回值的！因此为undefined
    console.log(arr2());//()=>this.age
    console.log(arr2()()); //123
    ```

### 新增

1. Set、Map、WeakMap、WeakSet数据类型（后面有详细说明）
2. Symbol
3. 类型化数组（TypedArray），比如Uint8Array类型化数组（Unit8Array一个具体的TypedArray，TypedArray可以看作一个抽象类，但是不是一个全局对象/声明/字段，它是众多类型化数组的统一名称），上传/下载文件的时候，就可以使用 `FileReader` 或 `Blob` 对象来读取文件内容并将其转换为Unit8Array
   1. 类型化数组可以在大规模数据操作时提供显著的性能优势。由于类型化数组是基于底层的二进制数据存储，所以对于诸如图像处理、音频处理、网络通信等涉及大量数据的应用场景，它们通常比传统的 JavaScript 数组更快
   2. 由于类型化数组是基于底层二进制数据的，它们在内存占用方面更加高效。相对于 JavaScript 数组，它们通常不需要额外的存储空间来存储对象引用和元信息，这使得它们更适合处理大量连续的数值数据
   3. 数据操作灵活，类型化数组允许您以不同的数据视图方式来处理底层数据。这意味着您可以对同一块数据使用不同的数据类型进行操作，同时避免了不必要的数据拷贝
   4. 更适合与底层 API 的互操作： 类型化数组非常适合与底层的 Web API（如 WebGL、Web Audio、File API 等）进行互操作，因为这些 API 通常涉及大量的二进制数据

   ```javascript
    // 通过数组初始化Int16Array数组，基于底层二进制的储存让数组的读写变得比正常数组更为高效
    let int16Array = new Int16Array([1, 2, 3, 4, 5]);
   ```

## JS-MDN

### 对象&类

#### 对象&类的基本概念

1. 创建一个对象/类

    ```javascript
    //创建对象的两种方法，一是对象字面量，二是通过new实例化一个对象
    //以下是一个字面量对象，不同于从类实例化得到一个对象
    let obj = {
        user_name: "巴拉巴拉",
        fun1() {//简写
            return `你好，我叫${this.user_name}`;
        },
        fun2: function () {
            return `爱你`;
        }
    };

    //类，底层仍然是通过原型实现
    class Parent {
        name//属性
        constructor(n) {
            this.name = n;
        }
        say_hi() {
            console.log(`Hi, I'm ${this.name}`);
        }
        method1() { }
        method2() { }
    }
    let obj2 = new Parent('Tom');

    ```

2. 构造函数constructor做了什么
   1. 创建一个新的对象
   2. 将 this 绑定到这个新的对象，你可以在构造函数代码中使用 this 来引用它
   3. 执行构造函数中的代码
   4. 构造函数返回了对象 ? 返回构造函数的对象 : 返回原来创建的新对象

   ```javascript
    //自己写一个new函数
    function my_new(ParentConstructor, ...args) {
        const obj = {};//创建一个空对象
        obj.__proto__ = ParentConstructor.prototype;//将空对象的原型指定为构造函数的prototype
        //以上两句也可以合并为一下一句
        //const obj = Object.create(ParentConstructor.prototype);//以构造函数的原型对象为原型创建一个对象
        const result = ParentConstructor.apply(obj, args);//构造函数的this绑定创建的对象并且执行构造函数
        return result instanceof Object ? result : obj;//构造函数显式返回了实例对象（引用类型），则创建的实例对象会被覆盖，返回该对象，如果构造器返回基本类型或者没有显式返回（返回undefined），那么返回原先新创建的对象
    }
   ```

3. 当一个方法拥有相同的函数名，但是在不同的类中可以具有不同的实现时，我们称这一特性为多态（polymorphism）。当一个方法在子类中替换了父类中的实现时，我们称之为子类重写/重载（override）了父类中的实现
4. 对象的内部状态保持了私有性，而外部代码只能通过对象所提供的接口访问和修改对象的内部状态，不能直接访问和修改对象的内部状态。保持对象内部状态的私有性、明确划分对象的公共接口和内部状态，这些特性称之为封装（encapsulation）
5. JS并不采用强制措施阻止外部代码访问对象的私有属性，在这种情况下，程序员们通常会采用一些约定俗称的命名方式来标记对象的私有部分，例如将以下划线开头的变量名看作是对象的私有部分
6. JS私有属性/类私有域：定义为`#prop_name`，读作哈希prop_name。从作用域之外引用 # 名称、内部在未声明的情况下引用私有字段、或尝试使用 delete 移除声明的字段都会抛出编译语法错误

    ```javascript
    class PrivateC {
        #private_prop1;
        constructor(p1) {
            this.#private_prop1 = p1;
        }
        set_p1(p1) {
            this.#private_prop1 = p1;
        }
        get_p1() {
            return this.#private_prop1;
        }
        delete_p1() {
            // delete this.#private_prop1;//编译报错，不可删除私有属性
        }
    }
    let private_obj = new PrivateC('zou');
    // console.log(private_obj.#private_prop1);//编译报错，私有属性不可在类之外访问
    console.log(private_obj.get_p1());// 'zou'
    private_obj.set_p1('mei');
    console.log(private_obj.get_p1());// 'mei'
    ```

#### 对象原型

1. 对象的原型--`Object.getPrototypeOf(obj)`（标准用法）或者`obj.__proto__`（非标准用法，不建议使用）：JS每个对象拥有一个原型对象，**对象以其原型为模板、从原型继承方法和属性**
2. 原型链：`obj.__proto__.__proto__.__proto__....`，在调用`obj.method()`形式时，首先会在obj本身寻找是否有method方法，如果找不到，则会往`obj.__proto__`上找，如果还是找不到，继续往`obj.__proto__.__proto__`上找，直到Object.prototype.method为止，不会继续往上，因为继续往上是`Object.prototype.__proto__`，其值为null，**Object.prototype是最基础的原型，所有对象原型链都有它**
3. 属性的“遮蔽”：对象自身设置了一个在其原型链上存在的属性，则会优先访问对象本身的属性而非原型链上的属性
4. 设置原型的方法
   1. `Object.create(parent)`:以parent为原型创建一个新对象，`child.__proto__===parent`

        ```javascript
        let parent = { a: 1 };

        let child = Object.create(parent);
        console.log(child.__proto__ === parent);//true
        console.log(child.a);//往原型上找到属性a的值为1
        ```

   2. 构造函数：将`构造函数.prototype`赋值为一个指定的原型对象

        ```javascript
        const personPrototype = {
            greet() {
                console.log(`你好，我的名字是 ${this.name}！`);
            }
        };
        function Person(name) {
            this.name = name;
        }
        let p1 = new Person("11");
        console.log(p1.greet);//undefined，调用greet()会报错

        Object.assign(Person.prototype, personPrototype);//赋值原型之后就能调用原型对象上的方法了，即设置原型成功
        console.log(p1.greet);//ƒ (){console.log("你好，我的名字是 ".concat(this.name,"！"))}
        ```

5. 每个实例对象都有一个constructor属性（从原型中继承而来），也可以通过`new obj.constructor()`来创建一个新的对象（一般不这么用，其实就相当于new 类名()）

   ```javascript
    console.log(`"abc".constructor：${'abc'.constructor}，'abc'.constructor===String：${'abc'.constructor === String}`);// String true

    console.log(`true.constructor：${true.constructor}，true.constructor===Boolean：${true.constructor === Boolean}`);//Boolean true
   ```

6. **更改原型链上游某个构造函数的prototype属性，所有的下游的对象都能访问到该新增属性**
7. 通常用法：构造器函数体中定义属性，prototype上定义方法

    ```javascript
    function Test_class(a, b) {
        //定义属性，避免在prototype定义属性实例中互相污染
    }
    //prototype属性上定义方法，方法不会互相污染。并且共用一个方法可以减少内存占用
    Test_class.prototype.method1 = function () { };
    Test_class.prototype.method2 = function () { };

    ```

8. JS原型链和其他语言的继承有什么区别：当一个子类完成继承时，由该子类所创建的对象既具有其子类中单独定义的属性，又具有其父类中定义的属性（以及父类的父类，依此类推）；而原型链中子类自身并不包含父类中定义的属性和方法，而是要求委派的对象（父类）执行

#### 继承

1. 原型链继承：使用prototype继承，**子实例更改其继承自父类中的引用类型（简单类型不会，因为简单类型属性直接赋值给了子实例本身）数据会污染其他的子实例**，会继承原型链上的方法：`Child.prototype=new Parent()`，所以我们一般把方法写在原型对象上，属性写在构造函数里
2. 构造函数继承：**子类构造函数中使用call/apply绑定所创建子实例的this以及父类构造函数，子实例更改其继承自父类中的引用类型数据不会互相污染，但是不会继承父类.prototype上的属性**：`function Child(){Parent.call(this)}`
3. 组合继承：构造函数继承 + 原型链继承：不污染也会继承父类.prototype，**先call再prototype，性能相对降低，并且 Child 和 Child.prototype 上面都有重复的Parent属性**：`function Child(){Parent.call(this)} ; Child.prototype=new Parent()`
4. 寄生组合继承：组合继承中的原型链继承改为Object.create(Parent.prototype)，这样子**Child和Child.prototype不会包含重复的父类方法，但是Child原始的prototype属性/方法会丢失，因为我们直接把Child.prototype赋值为一个新的对象了**：`function Child(){Parent.call(this)} ; Child.prototype=Object.create(Parent.prototype);`
5. ES6的class使用extends继承`Child.prototype=Object.create(Parent.prototype) ; Child.__proto__=Parent`，使用babel转码会发现class的extends语法糖和寄生组合继承的方式基本类似

    ```javascript
        class Parent {
        name;
        #secret;//私有属性，仅能在内部访问
        constructor(n, s = '私有属性') {//s设置默认值，s参数不传或者为undefined时使用默认值
            this.name = n;
            this.#secret = s;
        };
        walk() {
            console.log("我会走");
        }
        #secret_fun() {//私有方法，仅能在内部调用
            console.log("私有方法")
        };
        force_open_secret() {
            console.log("通过暴露接口查看私有属性/方法：");
            console.log(this.#secret);
            this.#secret_fun();
        }
    }
    let p1 = new Parent('邹竹');
    console.log(p1.name);//邹竹
    // console.log(p1.#secret);//报错
    // p1.#secret_fun();//报错
    p1.force_open_secret();//通过暴露接口查看私有属性/方法：  私有属性  私有方法

    //Child继承自Parent
    class Child extends Parent {
        extra;
        constructor(n, s, extra) {//有constructor必须调用super，否则报错
            super(n, s);
            this.extra = extra;
        }
    }
    let c1 = new Child('111', undefined, 11);
    c1.walk();//我会走
    c1.force_open_secret();//通过暴露接口查看私有属性/方法：  私有属性  私有方法
    ```

### 常用标准内置对象

#### Number

1. JavaScript 采用“遵循 IEEE 754 标准的双精度 64 位格式”表示数字，所以JS种其实除了BigInt并不存在整形/整数。因为浮点数的二进制表示法无法精确的表示某些十进制数字，JS的小数运算可能会产生误差，比如 0.1 + 0.2 结果为 0.30000000000000004 ，0.3-0.2也不是0.1，为了解决这个问题，可以采用如下解决方案：
   1. **将源操作数转化为整数进行计算**，最后再恢复为小数
   2. **四舍五入**，使用`toFixed()`/`toPrecision()`进行四舍五入指定精度
   3. 第三方库
2. [方法参考](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Number)

#### String

1. 注意区分原始值和字符串对象(使用`new String()`创建的对象)，原始值时没有属性/方法/原型的，当原始值访问属性/调用方法时，JS会自动包装成对应的包装对象（String、Boolean、Number都是如此）
2. **数组和字符串共有的属性/方法：`length、slice()、concat()、indexOf()、[Symbol.iterator]()`**等
3. 常用属性：
   1. length
4. 常用方法：
   1. `String.prototype[Symbol.iterator]()`：返回字符串的迭代器，每次迭代的value为对应位置的**单个字符**
   2. `str.concat(str2[, str3, ...])`：连接多个字符串并返回结果字符串，强烈建议使用 `+` 或者  `+=` 代替本方法
   3. `str.indexOf(search_str[, start_position])`：从start_position(默认为0)开始搜索得到search_str的索引，没找到返回-1
   4. `str.slice(start[, end])`：截取从start到end(不包括end本身的字符)的字符串，如果end不传参，则直接从start截取到字符串末尾，**start和end可以是负数，如果start位置>end位置（注意是字符位置比较，而非数值大小的比较），则会返回空字符串**
   5. `str.split(seperator[, max_num])`：将str以seperator为基准分割成一个数组，数组最多包含max_num个字符串，超过的部分停止分割，并且剩余部分字符串不会被包含在数组中，**seperator不存在str或者为undefined时会直接返回整个字符串作为唯一元素的数组**；当seperator出现在str开始和末尾位置时，该操作会让数组第一个元素/最后一个元素出现一个空串值
   6. `str.startsWith(str2[, start_position])`：从start_position开始，是否已str2开头
   7. `str.endsWith(str2[, end_position])`：从str截取end_position（默认为str的长度）长度后是否以str2结尾
   8. `str.subString(start[, end])`：截取从start到end（不包括end本身字符）的字符串，如果end不传参，则直接从start截取到字符串末尾，start和end谁大谁小效果都一致，并且当最小值为负数/NaN时，将其视作0；最大值大于str.length时，将其视作str.length，和slice方法都用于切片但是没有slice灵活，尽量别用这个

```javascript
let str = "hello world";
let str2 = " zack ";
console.log(str.concat(str2, str));
//强烈建议使用 + 或者 += 代替concat方法
console.log(str + str2 + str);
console.log(str.indexOf("lo", 2), str.indexOf("h", 2));//3 -1
console.log(str.endsWith("ld", str.length));//true
console.log(str.endsWith("ld", str.length - 2));//false
console.log(str.slice(-2, -1), str.slice(-1, -2), str.slice(5, 3));//"l"   ""   ""
console.log(str.substring(-2, 1), str.substring(1, 3), str.substring(3, 1));//"h"    "el"    "el"
console.log(str2.split('', 3), str2.split(''), "1,2,3".split(','), "1,2,3".split('99'));//[' ', 'z', 'a']      [' ', 'z', 'a', 'c', 'k', ' ']       ['1', '2', '3']        ['1,2,3']
console.log(",a,a,".split(','));// ['', 'a', 'a', '']
const str_iter = str[Symbol.iterator]();
let item = str_iter.next();
while (!item.done) {
    console.log(item.value);//逐行输出"hello world"每个字符
    item = str_iter.next();
}
```

#### 数组Array

1. 构造函数：**当且仅当只有一个参数时才将其视为长度，否则都将其视为数组元素**，长度必须是非负整数，小数/负数会报错，为非Number类型时会视作单个元素而非数组长度

    ```javascript
    //创建数组
    let arr = Array();//空数组
    let arr1 = Array(3).fill('a');//数组的长度是3，这里传一个正整数参数时表示数组长度
    let arr2 = Array("3");//传一个字符串"3"，因此其被视作元素而非长度
    let arr3 = Array(3, 4);//数组长度是2，包含3，4两个元素
    console.log(arr, arr1, arr2, arr3);
    // []     ['a', 'a', 'a']      ['3']      [3, 4]
    ```

2. **数组和字符串共有的属性/方法：`length、slice()、concat()、indexOf()、[Symbol.iterator]()`**等
3. 静态方法：
   1. `Array.from(array_like[, map_fn(element, index), this_arg])`：从**类数组对象/可迭代对象**array_like浅拷贝生成一个新数组，map_fn是对每个元素执行的回调函数，**其返回值才是新数组的元素**，element和index分别是当前处理的元素和下标，this_arg是map_fn中的this值（使用了this的情况下，map_fn不可以是箭头函数，否则会指向window而非this_arg，同理，高级函数map等参数中的回调函数也不要用箭头函数，否则同样会令this_arg失效）
   2. `Array.isArray(value)`
   3. `Array.of(e1[, e2, e3, ...])`：创建一个包含元素e1, e2,...的数组，和`Array()`区别在于，**只有一个正整数参数时Array.of()将其视为新数组元素，而Array()将其视为数组长度**

    ```javascript
    let array_like1 = [1, 3, 4];
    let this_arg = {
        add_one(item) {
            return ++item;
        }
    }
    let arr = Array.from(array_like1, item => item ** 2);
    console.log(arr);//[1, 9, 16]
    let arr2 = Array.from(array_like1, function (item) { return this.add_one(item) }, this_arg);
    console.log(arr2);// [2, 4, 5];

    let arr3 = Array.of(3);
    let arr4 = Array.of(4, 5);
    console.log(arr3, arr4);// [3]      [4, 5]
    ```

4. 其它常用方法：
   1. 返回值为新数组长度：
      1. `arr.push(item1[, item2, ...])`：将item1，item2...全部追加到数组尾部
      2. `arr.unshift(item1[, item2, ...])`：将item1，item2...全部追加到数组头部
   2. 返回值为被删除的数组元素：
      1. `arr.pop()`
      2. `arr.shift()`：数组头部
   3. 返回值为一个数组：
      1. `arr.splice(start[, delete_count, item1, item2, ...])`：从start开始删除delete_count个元素并且将item1, item2, ... 拼接到start后， **start可以为负数，delete_count如果省略，则直接删除从start到数组末尾所有元素**。返回被删除的所有元素组成的数组
      2. `arr.reverse()`：反转原数组并返回原数组引用
      3. `arr.sort([compare_fn(a, b)])`：排序原数组并返回原数组引用
      4. `arr.slice(start[, end])`：用法和String的slice一致，截取arr从start到end（不包括end元素）的元素并且返回它们组成的数组，start和end都可以是负数，如果**规范化后start位置>end位置，则返回空数组，类似于字符串的slice方法在该条件下会返回空串**
      5. `arr.concat([value1[, value2, ...]])`：连接多个**数组/非数组**（**元素是数组会展开第一层，非数组直接作为元素拼接**）并返回一个**新数组**，如果不传参数，则直接返回arr的浅拷贝，**concat貌似是唯一会解构数组的方法**（唯一一个传递数组作为参数并且会自动解构一层的方法）
      6. `arr.flat([depth])`：扁平化数组，depth表示要提取的深度，depth可设置为Infinity以将任何数组转化为一维，返回一个新数组
      7. `arr.flatMap(callback_fn(element, index, origin_arr), this_arg)`：等价于`arr.map(callback(...args)).flat()`
   4. 返回其它：
      1. `arr.join(seperator)`：拼接成字符串，**如果seperator省略，则自动以','连接**
   5. 迭代方法/高级函数
      1. 满足 `arr.method(callback_fn(element, index, origin_arr), this_arg)` 形式的方法（回调函数callback_fn也不要用箭头函数，否则同样会令this_arg失效）：
         1. map
         2. forEach：返回undefined
         3. filter
         4. some：返回true/false
         5. every：返回true/false
         6. find：返回满足条件的第一个元素
         7. findIndex：返回满足条件的第一个元素的索引
      2. 其它形式：
         1. `arr.reduce(callback_fn(pre, cur, index, origin_arr), init_val)`：将数组**多个元素**进行累计计算**得到一个值**，pre为上一个值，指定init_val时，pre最开始为init_val，cur为`arr[0]`，index为0；不指定init_val，pre最开始为`arr[0]`，cur为`arr[1]`，index为1。当数组为空数组并且无init_val时，会报错。

    ```javascript
    let arr = [];
    arr.push(...[1, 2], 3);
    console.log(arr);// [1, 2, 3]
    arr.unshift(...[-2, -1], 0);
    console.log(arr);// [-2, -1, 0, 1, 2, 3]
    arr.shift();
    arr.pop();
    console.log(arr);// [-1, 0, 1, 2]
    arr.splice(-1, 1);
    console.log(arr);// [-1, 0, 1]
    arr.splice(0, 2, 999, 888);
    console.log(arr);// [999, 888, 1]
    let arr2 = arr.concat([2], 3, [4, 5]);
    console.log(arr2);// [999, 888, 1, 2, 3, 4, 5]
    arr2.reverse();
    console.log(arr2);// [5, 4, 3, 2, 1, 888, 999]
    arr2.sort((a, b) => a - b);
    console.log(arr2);// [1, 2, 3, 4, 5, 888, 999]
    console.log(arr2.slice(-3, -1));// [5, 888]
    console.log(arr2.slice(-1, -3));// []，start<end因此返回空数组，类似于字符串的slice方法在该条件下会返回空串

    console.log(arr.join());//999,888,1
    console.log(arr.join(','));//999,888,1


    //高级方法
    //统一的回调函数形式如下：（reduce除外）
    function callback_map(item, index, origin_arr) {
        return item * 2;
    }
    function callback_foreach(item, index, origin_arr) {
        console.log(item, index, origin_arr);
    }
    function callback_filter(item, index, origin_arr) {
        return item.toString().length < 2;
    }
    console.log(arr.map(callback_map, undefined));//[1998, 1776, 2]
    arr.forEach(callback_foreach, undefined);
    //999 0 [999, 888, 1]
    //888 1 [999, 888, 1]
    //1 2 [999, 888, 1]
    console.log(arr.filter(callback_filter));//[1]
    console.log(arr.some(callback_filter, undefined));//true
    console.log(arr.every(callback_filter, undefined));//false
    console.log(arr.find(callback_filter, undefined));//1
    console.log(arr.findIndex(callback_filter, undefined));//2

    //reduce别树一帜
    const reduce_res = arr.reduce(function (pre, cur, index, origin_arr) {
        return pre + cur;
    }, 1000000);
    console.log(reduce_res);//1001888
    ```

#### Set（ES6新增）

1. Set概念 `const set = new Set([item1, item2,...items])`
   1. 储存唯一值（值可以是任意类型）
   2. NaN被视为相同值，尽管NaN!==NaN
   3. 构造方法new Set(array)，必须要加new否则报错
   4. **for of直接迭代set其实就是迭代`set[Symbol.iterator]()`迭代器，这个迭代器的迭代值与构造set的数组元素保持一致，对于Map也是如此，所以用for of迭代set会迭代出每个元素，迭代map会迭代出每个数组**
2. 常用方法/属性，以下都是定义在`Set.prototype`上的实例方法/属性，标粗体的都是与Map共有的属性/方法（map没有add方法，但是多了get、set方法）
   1. **`set.size`**：元素个数
   2. `set.add(item)`：添加元素，返回值是set本身，因此可以向Promise那样链式调用`set.add(1).add(2).add(3)`
   3. **`set.has(item)`**：是否包含item
   4. **`set.clear()`**：删除所有元素
   5. **`set.delete(item)`**：删除某个元素item，*成功删除返回true否则返回false*
   6. **`set.entries()`**：返回一个迭代器，迭代的value是`[item, item]`，因为set不像Map那样有key，因此**为了与与Map对象的 API 形式保持一致**，故使得每一个条目的 key 和 value 都拥有相同的值，**只有这一个迭代方法值为数组**，其它的迭代方法迭代值都是元素本身/key（set的key和元素本身视为一致）
   7. **`set.forEach(callback(element, key, origin_set), this_arg)`**：与`Array.prototype.forEach()`形式一致，但是因为set没有键（key/index）所以前面两个参数值都是element
   8. **`set.values()`**：返回一个新的迭代器对象，迭代的value是每一个元素值，该对象按插入顺序包含 Set 对象中每个元素的值
   9. **`set.keys()`**：与`set.values()`完全一致，因为set没有key
   10. **`set[Symbol.iterator]()`**：返回一个迭代器（即类于构建set的数组），迭代set中的每一个元素，和`set.values()`类似

    ```javascript
    // Set
    const set = new Set([1, 2]);
    console.log(set.size);// 2
    set.add("你好");
    set.add({});
    console.log(set.size);// 4
    set.clear();
    set.add(999);
    set.add(888);
    set.delete(999);
    console.log(set);//Set(1) {888}
    set.add(9999);
    console.log(set.has(9999));// true

    //迭代Set
    function go_all(iter) {
        let temp = iter.next();
        while (!temp.done) {
            console.log(temp.value);
            temp = iter.next();
        }
    }
    const entries_iterator = set.entries();//迭代值：[item, item]
    go_all(entries_iterator);//输出 [888, 888]  [9999, 9999]
    const values_iterator = set.values();
    go_all(values_iterator);// 888 9999
    const keys_iterator = set.keys();
    go_all(keys_iterator);// 888 9999
    const symbol_iterator = set[Symbol.iterator]();
    go_all(symbol_iterator);// 888 9999

    set.forEach(function (element, key, origin_set) {
        console.log(element);// 888 9999
    }, undefined)
    console.log(888 in set);//false，set/map不是对象，不具有索引，不能用in来判断元素
    ```

#### Map（ES6新增）

1. Map概念：`const map = new Map([[key1, value1], [key2, value2], ...])`或者`const map = new Map(origin_map)`复制origin_map，但是和origin_map不是同一个引用
   1. 保存键值，**与对象不同，它的键值对是有序的，并且键值可以是基本类型，也可以是引用类型（Object的键必须是字符串或者Symbol）**
   2. **使用for of迭代值的是一个数组`[key, value]`，不像set迭代的是单个元素，for of直接迭代map其实就是迭代`map[Symbol.iterator]()`迭代器，这个迭代器的迭代值与构造map的数组元素保持一致，对于Set也是如此，所以用for of迭代set会迭代出每个元素，迭代map会迭代出每个数组**
   3. 键唯一性和set一样，NaN视为同一值，+0、-0视为同一值
2. 对象和Map区别
    1. 键的类型：对象的键只能是字符串或 Symbol 类型，而 Map 的键可以是任意类型，包括对象、函数、基本数据类型等
    2. Map 会按照插入顺序保持键值对的顺序
    3. 迭代：对象的迭代是基于键的，可以使用 `for...in` 循环或 `Object.keys()`、`Object.values()`、`Object.entries()` 方法进行迭代。而 Map 提供了迭代器接口，可以使用 `for...of` 循环或 `Map.prototype.forEach()` 方法进行迭代
    4. Map 有 size 属性可以直接获取键值对的数量
3. 常用方法/属性，以下都是定义在Map.prototype上的实例方法/属性（**和set相比，map多了两个.get(),.set()方法， 而set多了一个.add()方法，其它方法名都一致**）
   1. **`map.size`**：键值对个数
   2. `map.get(key)`：获取key对应的值
   3. `map.set(key, value)`：设置键值对
   4. **`map.clear()`**：删除所有键值对
   5. **`map.delete(key)`**：删除某个键值对，*成功删除返回true否则返回false*
   6. **`map.has(key)`**：是否包含key对应的键值对
   7. **`map.keys()`**：返回包含所有单个key的迭代器，有序
   8. **`map.values()`**：返回包含所有单个value的迭代器，有序
   9. **`map.entries()`**：同`map[Symbol.iterator]()`：返回一个迭代`[key, value]`的迭代器
   10. **`map[Symbol.iterator]()`**：返回一个迭代器，迭代的是`[key, value]`数组（即类似于构建map的数组），有序
   11. **`map.forEach(callback(value, key, origin_map){}, this_arg)`**：遍历元素并执行回调，有序

    ```javascript
    // Map
    const map = new Map([[1, "呱唧呱唧"]]);
    map.set(true, "bibibib");
    console.log(map.get(1));// 呱唧呱唧
    console.log(map.has(true));// true
    map.delete(true);// 成功返回true
    function go_all(iter) {
        let temp = iter.next();
        while (!temp.done) {
            console.log(temp.value);
            temp = iter.next();
        }
    }
    const entries_iterator = map.entries();
    const values_iterator = map.values();
    const keys_iterator = map.keys();
    const symbol_iterator = map[Symbol.iterator]();
    go_all(entries_iterator);// [1, '呱唧呱唧']
    go_all(values_iterator);// 呱唧呱唧
    go_all(keys_iterator);// 1
    go_all(symbol_iterator);// [1, '呱唧呱唧']

    map.forEach(function (value, key, origin_map) {
        console.log(`key是${key}，value是：${value}`)
    }, undefined);
    // key是1，value是：呱唧呱唧
    ```

#### WeakSet（ES6新增）

1. WeakSet概念（**仅包含引用类型、弱引用、仅有3个方法**）
   1. 与Set一致储存唯一值，但是**只能储存引用类型**（对象/数组/函数等，null也不行），**并且是弱引用**（弱引用的数据会被垃圾回收，不像正常的引用，即强引用，存在的时候数据不会被回收）
2. 常用方法，**只有最基本的`add / delete / has`三个方法（增删查）**
   1. `weak_set.add(obj)`：添加obj
   2. `weak_set.delete(obj)`：删除obj
   3. `weak_set.has(obj)`：是否包含obj

    ```javascript
    //WeakSet
    const weak_set = new WeakSet([{}]);
    // weak_set.add(23);//报错，元素只能是引用类型
    // weak_set.add(null);//报错，元素只能是引用类型
    weak_set.add([]);
    weak_set.add(function () { });
    console.log(weak_set);//WeakSet {{}, ƒunction, []}
    //WeakSet {{…}, ƒ, Array(0)}
    ```

3. Set和WeakSet的区别：
   1. 元素类型：Set 可以包含任意类型的值作为其元素，包括基本数据类型和对象引用。而 WeakSet 只能包含对象引用作为其元素，不能包含基本数据类型。
   2. 引用关系：Set 中的元素是通过引用来存储的，而 WeakSet 中的元素是弱引用的。这意味着 WeakSet 中的元素不会阻止被引用对象的垃圾回收，**如果没有其他强引用指向对象，垃圾回收器会自动回收该对象**
   3. 迭代：WeakSet 不支持迭代操作
   4. 大小计数：Set 具有 size 属性，可以获取集合中的元素数量

#### WeakMap（ES6新增）

1. WeakMap概念：`const weak_map = new WeakMap([iterable])`，iterable可有可无，有的话必须是一个可迭代对象，并且其元素必须是键值对（**键必须是对象、弱引用、仅有4个方法**）
   1. 键值对合集，并且**键必须是对象**，如果使用原始数据类型（字符串/Symbol等），则会报错，并且键是**弱引用**
   2. **由于键是弱引用，键没有其它引用时，键对象会被回收，由此对应的值对象也会被回收**
   3. 也正因为键是弱引用，WeakMap的key是不可枚举的，也是不可迭代的，不能像Map一样通过`.keys()`，`values()`，`.entries()`来迭代其键/值/键值对
2. 常用方法，**只有最基本的`get / set / delete / has`四个方法（增删查）**
   1. `weak_map.get(key)`：获取key对应的值
   2. `weak_map.set(key, value)`：设置key和value键值对
   3. `weak_map.delete(key)`：删除键值对，*成功返回true否则返回false*
   4. `weak_map.has(key)`：是否包含key键

    ```javascript
    //WeakMap
    const obj = {};
    const weak_map = new WeakMap([[obj, "value1"], [parseInt, "我是parseInt对应值"]]);// key为非引用类型会报错
    console.log(weak_map.get(parseInt));// 我是parseInt对应值
    weak_map.set(parseFloat, "mememe是float");
    weak_map.delete(parseInt);
    console.log(weak_map.has(parseInt));// false
    console.log(weak_map);// WeakMap {{…} => 'value1', ƒ => 'mememe是float'}
    ```

3. Map和WeakMap区别
   1. 键类型：Map 的键可以是任意类型，包括基本数据类型和对象引用，而 WeakMap 的键只能是对象引用
   2. 引用关系：Map 中的键是强引用的，即使其他地方没有对键的引用，键值对也不会被垃圾回收。**WeakMap 中的键是弱引用的，如果没有其他强引用指向键对象，垃圾回收器会自动回收键对象及其对应的键值对**。
   3. 迭代：WeakMap 不支持迭代操作没有迭代接口
   4. 大小计数：WeakMap不能获取键值对数量

#### Object

##### Object概念

1. 几乎所有的对象都是 Object 的实例，除了null原型对象：`Object.create(null)`或定义了 `__proto__: null`的对象
2. 对象删除属性必须使用delete操作符

##### 方法

1. 原型方法
   1. `Object.prototype.valueOf()`：用于返回对象的原始值，有可能是基本类型，也有可能不是
   2. `Object.prototype.toString()`：返回一个表示该对象的字符串
2. 静态方法
   1. `Object.create(proto)`：以proto为原型创建一个新的对象
   2. `Object.defineProperty()`：在一个对象上定义一个新属性，或修改其现有属性，并且能够劫持属性的6个描述符：
      1. get：getter
      2. set：setter
      3. value：设置属性值，和get互斥，一起设置会报错
      4. configurable：是否可修改和删除
      5. enumerable：是否可枚举
      6. writeble：是否可用赋值运算符修改该属性，默认为false（意思是用Object.defineProperty定义一个属性时，默认值为false，而不是说一个普通对象默认无法为其属性赋值）

      ```javascript
        Object.defineProperty(
        obj,
        'prop_name',
        {
            //value: undefined, //设置属性值，和get互斥，一起设置会报错
            //writable: false, //是否可以被赋值运算符改变，为false改值操作无效
            //configurable: false, //是否能被改变或者该属性能否被删除
            set: undefined,
            get: undefined,
            //enumerable: false, //是否可以被枚举，为true时可以被for(let key in obj)或者Object.keys()遍历
        })
      ```

   3. `Object.getPrototypeOf()`以及`Object.setPrototypeOf()`用于读写目前已废弃的`__proto__`属性
   4. `Object.freeze()`：Object.freeze() 静态方法可以使一个对象被冻结。冻结对象可以防止扩展，并使现有的**直接属性**不可写入和不可配置（嵌套的内部对象不生效）。被冻结的对象不能再被更改：不能添加新的属性，不能移除现有的属性，不能更改它们的可枚举性、可配置性、可写性或值，对象的原型也不能被重新指定。freeze() 返回与传入的对象相同的对象。冻结一个对象是 JavaScript 提供的最高完整性级别保护措施。**冻结一个对象相当于阻止其扩展然后将所有现有属性的描述符的 configurable 特性更改为 false——对于数据属性，将同时把 writable 特性更改为 false**。无法向被冻结的对象的属性中添加或删除任何内容，任何这样的尝试都将失败。冻结对象可以保护数据不被篡改、提高安全性、提高性能（引擎访问属性时内部会做一些优化）
   5. `Object.assign`
      1. Object.assign(target, ...sources) ：**将sources中的可枚举的自身（非原型链上的）属性+方法复制到target**
      2. 该方法使用源对象的 `[[Get]]` 和目标对象的 `[[Set]]`，它会调用 getters 和 setters
      3. 该方法只是浅拷贝，如果sources的属性是对象，他只会复制对象的引用，因此不能直接适用深拷贝
      4. **sources如果是基本类型会被包装成对象，但是只有字符串包装器才有可枚举属性，因此其他基本类型不会被复制**

       ```javascript
       const target = { a: 1, b: 2 };
       const source1 = { b: 4, c: 5 };
       const source2 = {
           d: 88,
           fun: function () {
               console.log(this.d);
           }
       }
       Object.assign(target, source1, source2);
       console.log(target);//{a: 1, b: 4, c: 5, d: 88, fun: ƒ}
       //b被source1中的b覆盖，方法fun也会被复制

       const v1 = 'abc';
       const v2 = true;
       const v3 = 10;
       const v4 = Symbol('foo');

       const obj = Object.assign({}, v1, null, v2, undefined, v3, v4);
       console.log(obj); // { "0": "a", "1": "b", "2": "c" }
       //sources：基本类型会被包装成对象，null/undefined被忽略
       //只有字符串包装器才有可枚举属性因此"abc"被包装成{ "0": "a", "1": "b", "2": "c" }，其他没有可枚举属性的包装器不会被复制

       ```

#### Function

1. 属性
2. 方法
   1. `Function.prototype.apply(this_obj, args_array)`：相当于 `this_obj.fun(...args_array)`，其中this_obj是函数的this对象，args_array是函数的参数**数组**/类数组，直接执行了函数，当this_obj为null或者undefind时，会自动指向全局对象，apply和bind、call不一样，**apply是唯一一个接收参数数组的方法**
   2. `Function.prototype.call(this_obj, arg1, arg2, arg3, ...)`：和apply基本一致，除了参数形式是传一组数列而非参数数组，同样也直接执行函数
   3. `Function.prototype.bind(this_obj, arg1, arg2, arg3, ...)`：返回一个全新的绑定函数，函数的this指向this_obj，没有直接执行函数，需要后面再加括号才能执行
   4. **如果是箭头函数调用call/bind/apply会自动忽略第一个this参数不生效**

    ```javascript
    //**实现需求1：arr1内追加arr2的所有元素，不用循环push，也不能创建新的数组
    let arr1 = [1], arr2 = [2, 3];
    //concat会返回一个新的数组，不会修改arr1和arr2
    let concat_arr = arr1.concat(arr2, [4, 5]);
    concat_arr[0] = 99;
    console.log(concat_arr);//[99, 2, 3, 4, 5]
    console.log(arr1);//[1]

    //使用apply自然地实现了这个方法
    Array.prototype.push.apply(arr1, arr2);
    console.log(arr1);//[1, 2, 3]
    //当然你也可以使用数组解构
    arr1.push(...arr2);
    console.log(arr1);//[1, 2, 3, 2, 3]

    //**实现需求2：找到arr3内部的最大值/最小值，不能循环，也不能使用sort
    let arr3 = [1, 3, 88, Infinity, -Infinity];
    let max1 = Math.max.apply(null, arr3);//Infinity
    console.log(max1);
    //当然你同样可以使用数组解构
    let max2=Math.max(...arr3);
    console.log(max2);//Infinity
    ```

#### Promise

1. 一个Promise对象代表一个异步操作，有三种状态：
   1. pending：初始状态，操作进行中--未结束
   2. fulfilled：操作成功状态，说明promise执行的函数最后执行了resolve(msg)，msg参数传入.then(func(msg))中；
   3. rejected：操作失败状态，说明promise执行的函数最后执行了reject(error)，error参数传入.catch(func(error))中；
2. 本质上 Promise 是一个函数返回的对象，我们可以在它上面绑定回调函数，这样我们就不需要在一开始把回调函数作为参数传入这个函数了
3. `Promise.prototype.then()`和`Promise.prototype.catch()`方法返回的也是一个Promise对象，因此可以被链式调用；**`.catch()`其实就是没有预留handleResolve回调函数位置的`.then()`方法而已**
4. `.then()`方法的**参数如果不是函数，或者没有参数**，那么会直接执行下一个`.then()`，并且上一个环节返回的**参数会透传**至下一个环节
5. Promise通过`.then(handleResolve, handleRejectOrError)**`方法链式调用，可以避免多重异步操作导致的回调地狱，同样的道理，出错的时候‘回调地狱’需要调用n个失败回调，但是Promise只需要在尾部执行一次即可，同时，一个Promise可以执行多个`.then()`方法
6. 重要！！**`resolve(arg)`参数进入`.then()`的第一个回调函数handleResolve中；`reject(err)`或者异常报错（Error）进入会优先进入`.then()`的第二个回调函数handleRejectOrError中，如果`.then()`方法没有第二个回调函数，那么进入后面的`.catch()`中。若一个Promise不`resolve/reject/抛出异常`，那么Promise后的链式调用`.then()/.catch()`都不会执行**
7. `.then()`方法中的参数也可以是一个有返回值的**箭头函数**，箭头函数具有惰性求值和延迟计算的特性，`.then()`方法中的箭头函数`return(value)`的值相当于Promise中的`resolve(value)`；**也可以是一个普通的函数**
8. `resolve()/reject()`只是保证Promise链式调用能传入下一环，并不会终止当前函数的执行，也就是说，如果这两个方法后面还有代码，那么会继续执行直到当前函数执行完毕
9. **Promise没有执行完成之前，取其值都是pending**，除非执行完了最后一个节点，并且**Promise链返回值是最后一个非finally节点的Promise**，注意finally不接收任何参数，只负责做一些自定义的处理并且返回它的上一个Promise
10. Promise的缺点：一旦创建，就会马上执行，并且不知道代码执行到了哪一步，因此基于Promise实现的fetch API相较于XHR而言有一个缺点就是无法获取请求的进度

```javascript
new Promise((resolve, reject) => {
    let to = '美丽1';
    console.log(`第一个Promise，正常resolve，发出${to}`);
    resolve(to);
})
.then((msg) => {
    let to = '我我';
    console.log(`第二个Promise，箭头函数，收到 ${msg}`);
    console.log(`第二个Promise，箭头函数，发出${to}`);
    return to;
}, () => {/**这里面写reject的处理，本例中不会执行*/ })
.then(function (msg) {
    let to = 'tata';
    console.log(`第三个Promise，普通函数，收到 ${msg}`);
    console.log(`第三个Promise，普通函数，发出${to}`);
    return to;
}, () => { })
.then()//这里的.then()不传函数参数为空，上一步的返回值发生透传，传值下一个.then()中
.then(111)//传一个非函数对象也同样会透传
.then(function (msg) {//这里的.then()传了函数，但是不返回，下一个会收到undefined
    console.log(`第六个Promise，箭头函数，收到${msg}，但是函数不返回（不发出）`);
})
.then((msg) => {
    console.log(`第七个Promise，箭头函数，收到${msg}`);
})
// 第一个Promise，正常resolve，发出美丽1
// 第二个Promise，箭头函数，收到 美丽1
// 第二个Promise，箭头函数，发出我我
// 第三个Promise，普通函数，收到 我我
// 第三个Promise，普通函数，发出tata
// 第六个Promise，箭头函数，收到tata，但是函数不返回（不发出）
// 第七个Promise，箭头函数，收到undefined

// 可以但是不要像下面这样在.then()方法中返回Promise，then中直接写两个函数并且函数中直接return解决值就可以，因为这种写法分析起来有点复杂，具体看后续的例子
Promise.resolve().then(function handle_resolve() {
    return new Promise((resolve, reject) => {
        console.log('handle_resolve');
        resolve('resolve返回值');
    })
})
.then((msg) => {
    console.log('我是', msg);
});

//.then()方法返回Promise的情况
Promise.resolve().then(() => {
    console.log(0);
    // return Promise.resolve(4);
    return new Promise((resolve) => {
        resolve(4);
    });
    //上面两种返回方式完全等价
}).then((res) => {
    console.log(res);
});
Promise.resolve().then(() => {
    console.log(1);
}).then(() => {
    console.log(2);
}).then(() => {
    console.log(3);
}).then(() => {
    console.log(5);
}).then(() => {
    console.log(6);
})
//在这里需要尤为注意的是，当一个Promise（假设名为p0）的.then()方法中返回一个Promise（假设名为p4）时，其实是做了这样一件事，即把如下代码放入微队列中：p4.then(完成p0并且结果传给p0)，注意是把这个表达式放入微队列中，而非直接执行！相当于为p0多添加了两个.then()链方法，所以上述代码输出0之后，会把 p4.then(完成p0并且结果传给p0) 放入微队列，然后输出1  → 把2放入微队列 → 解决p4 → 把 完成p0 放入微队列中 → 输出2 → 把3放入微队列中 → 完成p0 → 把p0的then内容放入微队列中 → 输出3 → 把5放入微队列中 → p0.then()方法接收到的res值为p4传递过来的4，因此输出4 → 输出5 → 把6放入微队列中 → 输出6
// 综上所述，最终代码输出结果为0 1 2 3 4 5 6

//第一部分可以抽象成下面这种写法
Promise.resolve().then(() => {
    //p0开始
    console.log(0);
}).then(() => {
    return 4;
}).then(() => {
    //p0结束
    return 4;
}).then((res) => {
    console.log(res);
})

```

##### 常用的静态方法

1. `Promise.resolve(msg)`：
   1. 创建一个返回msg的成功Promise，`Promise.resolve(4)`完全等价于 `new Promise((resolve) => { resolve(4) })`
2. `Promise.reject(err)`：
   1. 创建一个返回err的拒绝（失败）Promise
3. `Promise.any([...p_arr])`：
   1. 四个Promise并发方法之一，和`Promise.all()`**相反**，`Promise.all()`是出现拒绝就立马返回，都不拒绝就返回所有的成功值数组；而**`Promise.any()`是出现成功就立马返回，都拒绝才返回所有的拒绝值数组**
4. `Promise.all([...p_arr])`：
   1. 四个Promise并发方法之一，接受一个Promise 的iterable类型（比如数组/Map/Set，不可以为undefined，会报错），返回一个结果Promise。**数组内全成功，结果Promise状态成功，参数为数组各个返回值组成的数组；数组内有拒绝，结果Promise状态拒绝，参数为数组第一个被拒绝的返回值；如果元素不是Promise**
   2. 元素是empty、null、undefined、function、空resolve（`Promise.resolve()`）时，对应的返回值是null而不是undefined，其它非Promise元素返回参数就是本身

    ```javascript
    Promise.all([
        import('a.js'),
        import('b.js'),
        import('c.js')
    ]).then(([a, b, c]) => {
    // 使用 a, b, c 模块
    });

    function check_promise(promises_arr) {
        Promise.all(promises_arr)
            .then((msg) => console.log(`resolve！参数为${JSON.stringify(msg)}`))
            .catch((err) => console.log(`reject！，参数为${JSON.stringify(err)}`));
    }
    let suc_arr = [Promise.resolve(), Promise.resolve(1), Promise.resolve(2), '非promise的字符串（直接返回）'];
    check_promise(suc_arr);//resolve！参数为[null, 1, 2, "非promise的字符串（直接返回）"]
    //不传参数或者参数为undefined对应的元素最终返回null，但是如果是Promise.resolve().then()直接返回则不会变为null，而是undefined
    check_promise([null, undefined, 1, true, false, "string", function(){}, [1], {a : 1}, Promise.resolve(333333), Promise.resolve(), , ,]);
    // resolve！参数为[null, null, 1, true, false, "string", null, [1], {"a":1}, 333333, null, null, null]

    let fail_arr = [Promise.resolve(1), Promise.reject(2), Promise.reject(3), 4];
    check_promise(fail_arr);//reject！，参数为2
    ```

5. `Promise.allSettled([...p_arr])`：
   1. 四个Promise并发方法之一，和`Promise.all()`类似，不过它**返回的是数组每一个Promise的结果**，不管是成功还是拒绝，结果内容包括状态-status，成功返回值-value，失败原因-reason，使用`result = await Promise.allSettled()`不管成功还是失败都会成功赋值给result，其元素是包含status和value属性的对象以及包含status和reason属性的对象。（**Promise.all()只有全部成功才会返回数组，只要有失败就返回第一个失败的结果并且使用`result = await Promise.all([...pArr])`不会令result的值改变**）
   2. 和`Promise.all()`还有一个区别就是，**`Promise.allSettled()`对于非Promise元素（简单值、对象都如此）都会成功并且返回该元素本身**，而`Promise.all()`元素是empty、null、undefined、function、空resolve（`Promise.resolve()`）时，对应的返回值是null

    ```javascript
    Promise.allSettled([
        Promise.reject(00),
        Promise.resolve(11),
        Promise.resolve()
    ])
        .then((msg) => { console.log(msg) });
    // //输出
    // [
    //     { "status": "rejected", "reason": 0 },
    //     { "status": "fulfilled", "value": 11 },
    //     { "status": "fulfilled", "value": undefined }
    // ]
    Promise.allSettled([]).then((msg) => { console.log(msg) });//[]
    ```

6. `Promise.race([...p_arr])`
   1. 四个Promise并发方法之一，同样接受一个iterable类型参数（比如Promise数组，Map，Set等），返回一个Promise，该Promise的状态（**成功/失败**）和返回值与数组中最先完成的Promise一致，和**Promise.allSettled()一样，非Promise元素成功返回本身**

    ```javascript
    Promise.race([Promise.reject(2), Promise.resolve(1), 999999]).then((msg) => { console.log('成功', msg) }, (msg) => { console.log('失败', msg) });
    //失败 2
    Promise.race([Promise.reject(2).then(()=>{throw new Error("ddddddddd")}), Promise.resolve(1).then(()=>{}), 999999]).then((msg) => { console.log('成功', msg) }, (msg) => { console.log('失败', msg)});
    // 成功 999999
    ```

##### 常用的实例方法

1. `p.then(fun(msg), fun(err))`
2. `p.catch(fun(err))`
   1. **`.catch()`其实就是没有预留handleResolve回调函数位置的`.then()`方法而已**
3. `p.finally()`
   1. **finally 的回调函数中不接收任何参数**，它仅用于无论最终结果如何都要执行的情况
   2. **finally 也返回一个Promise，但是这个Promise是它前一个Promise的值**

       ```javascript
       let pro = Promise.resolve(1).then((res) => {
           console.log('1成功', res, pro);
       }).then((res) => {
           setTimeout(() => {
               console.log('2setTimeout', pro);
           }, 1000)
           console.log('2成功', res, pro);
           return '2返回值'
       }).then(res => {
           console.log('3', res, pro);
           return '3返回值'
       }).finally(res => {
           console.log('finally', res);
       })
       //正确结果
       // 1成功 1 Promise {<pending>}
       // 2成功 undefined Promise {<pending>}
       // 3 2返回值 Promise {<pending>}
       // finally undefined
       // 2setTimeout Promise {<fulfilled>: '3返回值'}
       Promise.resolve(1).finally(() => { return 111 });// Promise {<fulfilled>: 1}
       Promise.resolve(1).then(() => { return 111 })// Promise {<fulfilled>: 111}
       Promise.reject(1).then(() => { return 111 })// Promise {<rejected>: 1}      UnCaught错误
       Promise.resolve(1).finally(() => { console.log('finally'); return '111111' }).then((res) => { console.log(res); return 222 });//finally 1 Promise {<fulfilled>: 222}
       ```

#### async/await

1. async 和 await 关键字让我们可以用一种更简洁的方式写出基于 Promise 的异步行为，而无需刻意地链式调用 promise
2. await 会造成阻塞，**必须放在async函数内使用，在async之外使用会报语法错误**
3. **async函数会返回一个Promise，该Promise的值为async函数返回值（成功）或者抛出的异常（失败）**(和`Promise.prototype.then()`类似)
4. await返回从 Promise 实例或 thenable 对象取得的处理结果。如果等待的表达式不符合 thenable（**比如是一个普通同步函数而非Promise），则返回表达式本身的值**，是普通函数则会直接返回该函数而不执行它
5. 使用 async/await 关键字就可以在异步代码中使用普通的 try/catch 代码块来捕获reject或者错误
6. **从第一行代码直到（并包括）第一个 await 表达式（如果有的话）都是同步运行的**。这样的话，一个**不含 await 表达式的 async 函数是会同步运行的**。然而，**如果函数体内有一个 await 表达式，async 函数就一定会异步执行**
7. **好好理解这句话**：在 await 表达式之后的代码可以被认为是存在在链式调用的 then 回调中，多个 await 表达式都将加入链式调用的 then 回调中，返回值将作为最后一个 then 回调的返回值。 —— **意思就是await下一行代码开始直到函数结束的部分都相当于放进了.then()中**
8. 在async/await中的promise 链不是一次就构建好的，相反，promise 链是分阶段构造的，因此在处理异步函数时必须注意对错误函数的处理。 —— **也就是只有当await的Promise成功resolve之后（异步操作成功处理之后）才会开始把await后的代码包装成promise链中的`.then()`**
9. Promise和Async的区别：
   1. async/await是基于promise实现的，async会返回一个promise
   2. async更简洁，return的值就是返回的promise的resolve参数，报的错就是返回promise的catch参数
   3. async/await让`try catch`可以同时处理同步错误和异步错误，但是promise只能用`.catch()`来处理

```javascript
function dec_pro() {
    return new Promise((resolve, reject) => {
        console.log(3);
        setTimeout(() => {
            console.log(4);
            resolve(2);
        });
        console.log(5);
    })
}
async function a_fun() {
    console.log(1);
    console.log(await dec_pro());
    console.log(6);
}
a_fun();
Promise.resolve(1).then(() => {
    console.log(8);
    setTimeout(() => {
        console.log(9);
    });
})
console.log(7)
//自己做： 1 3 5 7 8 4 2 6 9
//正确！
//思路：    首先 输出1 → 输出3 → 参数为4的setTimeout在0秒后放入宏队列 → 输出5 → await等待resolve，其后的6也跟着await等待 → 继续同步代码，遇到.then()放入微队列 → 输出7 → 检查微队列，输出8 → 参数为9的setTimeout放入宏队列 → 微队列执行完毕检查宏队列 → 输出4 → resolve(2)将await后面的代码放入微队列 → 优先执行微队列 → 输出2 → 输出6 → 微队列执行完毕检查宏队列 → 输出9
```

```javascript
function pp() {
    return new Promise(function (resolve, reject) {
        setTimeout(() => {
            console.log('a');
            reject(2);//如果是resolve，那么await得到的值会是2
        }, 2000);
        console.log('b');
    })
}
async function my_async() {
    console.log(1);
    const result = await pp();
    // const result = await pp().catch((err) => { console.log('错误', err) });
    console.log(result);
}
my_async();
//输出：1 b (2秒后)a  Uncaught (in promise) 2
//假如上面的result赋值语句改为： const result = await pp().catch((err) => { console.log('错误', err) });
//则输出为： 1 b a (2秒后)错误 2
//因为catch方法内的函数没有返回值，因此最后my_async函数是一个undefined的promise

// async function my_async_try_catch() {
//     console.log(1);
//     try {// 可以使用try catch来像同步代码一样捕获await返回的错误/拒绝值
//         const result = await pp();
//         console.log(result);
//     } catch (err) {
//         console.log("出错", err);
//     }
// }
// my_async_try_catch();
// // 1 b a 出错 2
```

#### Proxy + Reflect

1. Proxy
   1. `const p = new Proxy(obj, handler);`
      1. obj: 要使用 Proxy 包装的目标对象（可以是任何类型的对象，包括原生数组，函数，甚至另一个代理）。
      2. handler: handler 对象是一个容纳一批特定属性的占位符对象。属性就是 Proxy 的各个捕获器，比如get、set、defineProperty、deleteProperty、has、ownKeys、construct、getPrototypeOf、setPrototypeOf等，各自的用法详见：[这里](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/get)
   2. `const p = Proxy.revocable(obj, handler)`，可通过`p.revoke()`撤销对obj的代理，撤销之后，任何对p代理的操作都会报错

2. Reflect
   1. Reflect称为反射。它也是ES6中为了操作对象而提供的新的API，用来替代直接调用Object的方法。它提供拦截 JavaScript 操作的方法
   2. 对于每个可被 Proxy 捕获的内部方法，在 Reflect 中都有一个对应的方法，其名称和参数与 Proxy 捕捉器相同。 这一点很关键，13种捕获器可以与Reflect方法一一对应，简化 Proxy 的创建。这也是Reflect 的出现的原因之一：完美的与Proxy搭配使用
   3. 13种捕获器方法：
      1. `Reflect.get(target, propertyKey[, receiver])`：获取对象身上某个属性的值，类似于 `target[name]`
      2. `Reflect.set(target, propertyKey, value[, receiver])`：将值分配给属性的函数。返回一个Boolean，如果更新成功，则返回true。
      3. `Reflect.ownKeys(target)`：返回一个包含**所有自身属性**（不包含继承属性）的数组。(类似于 `Object.keys()`, 但不会受enumerable 影响，`Object.keys()` 只返回目标对象自身的可枚举属性的键；`Reflect.ownKeys()` 返回目标对象自身的所有键，包括字符串类型和符号类型的键)

            ```javascript
            const arr = [1, 3, 5];
            console.log(Object.keys(arr));//['0', '1', '2']
            console.log(Reflect.ownKeys(arr));//['0', '1', '2', 'length']
            ```

      4. `Reflect.apply(target, thisArgument, argumentsList)`：对一个函数进行调用操作，同时可以传入一个数组作为调用参数。和 Function.prototype.apply() 功能类似。
      5. `Reflect.construct(target, argumentsList[, newTarget])`：对构造函数进行 new 操作，相当于执行 `new target(...args)`
      6. `Reflect.defineProperty(target, propertyKey, attributes)`：和 `Object.defineProperty()` 类似。如果设置成功就会返回 true
      7. `Reflect.deleteProperty(target, propertyKey)`：作为函数的delete操作符，相当于执行 `delete target[name]`。
      8. `Reflect.getOwnPropertyDescriptor(target, propertyKey)`：类似于 `Object.getOwnPropertyDescriptor()`。如果对象中存在该属性，则返回对应的属性描述符，否则返回 undefined。
      9. `Reflect.getPrototypeOf(target)`：类似于 `Object.getPrototypeOf()`。
      10. `Reflect.has(target, propertyKey)`：判断一个对象是否存在某个属性，和 `in` 运算符 的功能完全相同。
      11. `Reflect.isExtensible(target)`：类似于 Object.isExtensible().
      12. `Reflect.preventExtensions(target)`：类似于 Object.preventExtensions()。返回一个Boolean。
      13. `Reflect.setPrototypeOf(target, prototype)`：设置对象原型的函数。返回一个 Boolean，如果更新成功，则返回 true。
3. 元编程：借助Proxy + Refelct，你可以在 JavaScript 元级别进行编程，它们允许你拦截并定义基本语言操作的自定义行为（例如，属性查找，赋值，枚举，函数调用等）。
4. 示例：

    ```javascript
    let obj = {
        a: 2,
        _cat: "动物",
        get cat() {
            console.log("触发obj对象内部自定义的getter拦截器，拦截属性cat");
            return this._cat;
        }
    };
    let arr = [1, 3, [88, 99]];
    const handler = {
        get(target, prop) {
            console.log(`代理拦截了get，读取属性${prop}`);
            return Reflect.get(target, prop);
        },
        //新增属性+修改属性都是使用set
        set(target, prop, value) {
            console.log("代理拦截了set，并且该操作是新增属性/修改已有属性");
            if (prop in target) {//只要存在该属性，即使值是undefined也会返回true
                console.log(`修改属性${prop},值为${value}`);
            } else {
                console.log(`新增属性${prop},值为${value}`);
            }
            return Reflect.set(target, prop, value);
        },
        deleteProperty(target, prop) {
            console.log(`代理拦截了deleteProperty，删除属性${prop}`);
            return Reflect.deleteProperty(target, prop);
        }
    }
    const handler_receiver = {
        get(target, prop, receiver) {
            console.log(`receiver代理拦截了get，读取属性${prop}`);
            return Reflect.get(target, prop, receiver);
            //或者
            // return Reflect.get(...arguments);
        }
    }
    const p = new Proxy(obj, handler);
    const p_arr = new Proxy(arr, handler);
    const p_receiver = new Proxy(obj, handler_receiver);

    //基础用法——对象
    console.log(p.a);//代理拦截了get，读取属性a  2
    console.log(p.b);//代理拦截了get，读取属性b undefined
    console.log(obj.b);//undefined
    p.b = "bbbb";//代理拦截了set，并且该操作是新增属性/修改已有属性 新增属性b,值为bbbb
    console.log(p.b);//代理拦截了get，读取属性b bbbb
    //修改了代理对象的属性，原对象属性也被修改~
    console.log(obj.b);//bbbb
    p.b = "****";// 代理拦截了set，并且该操作是新增属性/修改已有属性 修改属性b,值为****
    delete p.b; //代理拦截了deleteProperty，删除属性b
    console.log(p.b);//代理拦截了get，读取属性b undefined
    console.log(obj.b);//undefined

    //基础用法——数组
    console.log(p_arr[0]);//代理拦截了get，读取属性0 1
    console.log(p_arr[2]);//代理拦截了get，读取属性2 [88, 99]
    p_arr[0] = 8888;//代理拦截了set，并且该操作是新增属性/修改已有属性 修改属性0,值为8888
    //下面这一行，因为其也是一个数组，并且没有进行代理，因此不会有任何输出
    p_arr[2][0] = 9999;
    console.log(p_arr[0]);//代理拦截了get，读取属性2 代理拦截了get，读取属性0 8888
    console.log(p_arr[2]);//代理拦截了get，读取属性2 [9999, 99]
    delete p_arr[2];//代理拦截了deleteProperty，删除属性2
    console.log(p_arr[2]);//代理拦截了get，读取属性2 undefined
    console.log(arr[2]);//undefined
    console.log(p_arr);//Proxy(Array) {0: 8888, 1: 3}
    console.log(arr);// [8888, 3, empty]
    p_arr[2] = 999;//代理拦截了set，并且该操作是新增属性/修改已有属性 新增属性2,值为999


    const obj_child = {
        _cat: "猫猫 for obj_child"
    }
    const proxy_child = {
        _cat: "猫猫 for proxy_child"
    }
    const receiver_child = {
        _cat: "猫猫 for receiver_child"
    }
    obj_child.__proto__ = obj;
    proxy_child.__proto__ = p;
    receiver_child.__proto__ = p_receiver;
    //receiver用于保留调用者的this
    //当我们调用*_child.cat时，会调用父类的getter而输出this._cat，我们本应该输出*_child._cat，但是当对象的原型是一个代理的时，会输出父类obj._cat，为此我们需要为元操作符getter传递第三个参数receiver，才能保证始终this始终指向子类
    //当对象原型是一个正常对象，即使原型有getter拦截器，this仍然会指向调用者也就是子对象，获取的是子对象的this._cat
    console.log(obj_child.cat);//触发obj对象内部自定义的getter拦截器，拦截属性cat 猫猫 for obj_child
    //当对象原型是一个代理对象，往原型链上找到代理并且触发属性cat的getter，但是这里由于代理的目标对象是父对象（setter的target指向的是obj这个父对象），所以获取的cat属性是父对象的this._cat
    console.log(proxy_child.cat);//代理拦截了get，读取属性cat 触发obj对象内部自定义的getter拦截器，拦截属性cat 动物
    //为了纠正上面的代理可能引起的this指向问题，在代理的setter中加入receiver参数，这样即使对象原型是一个代理对象，this也会指向子对象而非父对象
    console.log(receiver_child.cat);//receiver代理拦截了get，读取属性cat 触发obj对象内部自定义的getter拦截器，拦截属性cat 猫猫 for receiver_child

    //receiver更加简单地理解 —— 针对 target 对象中指定的 propertyKey 属性存在一个 getter 函数的情形来指定正确的this，如果 target 对象中的属性不是一个 getter，receiver 参数将没有任何效果
    let obj = {
        get foo() {
            return this.bar;
        }
    };
    let receiverObject = {
        bar: 'Hello'
    };
    console.log(Reflect.get(obj, 'foo', receiverObject)); // 输出: "Hello"
    //obj 对象有一个 getter 属性 foo。当我们通过 Reflect.get 读取 obj.foo 时，正常情况下这个 getter 会在 obj 的上下文中执行，其内部的 this 值指向 obj。但是，当我们用 Reflect.get 并传递了 receiverObject 作为第三个参数时，getter 会在 receiverObject 的上下文中执行，因此 this.bar 会返回 receiverObject.bar 的值，即 Hello。这样可以使得你能够在特定的上下文中调用getter，即使属性在原型链上。
    ```

### 常用表达式/方法

1. 参数是一个数组的方法有：
   1. `Function.prototype.apply()`
   2. `Array.prototype.concat()`，如果参数是数组则会自动解构第一层
   3. `Promise.any()`
   4. `Promise.all()`
   5. `Promise.allSettled()`
   6. `Promise.race()`
2. 从左至右计算表达式：`1 + 2 + "abc" + 1 + 2 == "3abc12"`
3. in：`prop in obj`返回true或者false，判断obj是否有prop属性，不论prop是否可枚举/是否继承
4. **遍历对象/遍历数组的常见方法及区别**：只有`for in`会遍历继承的属性；`for in`， `Object.keys()`，`Object.entries()`遍历可枚举；`Reflect.ownKeys()`遍历所有自身属性（可枚举/不可枚举/Symbol/字符串）；`Object.getOwnPropertyNames()`遍历所有自身属性（可枚举/不可枚举，Symbol除外）；Symbol类型属性名是不可枚举的

    ```javascript
    //遍历对象属性的方法
    const parent = {
        p: 999
    };
    const child = Object.create(parent);
    child.a = 111;
    console.log(child);//{a: 111}，展开后可在[[Prototype]]上发现p属性
    //for in：遍历对象的所有可枚举属性，会继承
    for (let key in child) {
        console.log("for in");
        console.log(child[key]);//111 999
    }
    //Object.keys()：遍历对象的可枚举属性，不继承
    for (let key of Object.keys(child)) {
        console.log("Object.keys()");
        console.log(child[key]);//111
    }
    //Object.entries()： 方法返回一个由对象自身的可枚举属性的 [key, value] 对组成的数组，遍历自身的可枚举属性，不继承
    //注意Object.entries()返回的是一个二维数组[[key1, value1],[key2, value2],...]，通过for of迭代该数组得到[keyi, valuei]
    //而数组的Array.prototype.entries()方法返回的是一个数组迭代器，不过同样可以通过for of进行迭代得到[indexi, valuei]，详见数组部分
    for (let arr of Object.entries(child)) {
        console.log("Object.entries()");
        console.log(arr[1]);//111
    }
    //Reflect.ownKeys()：遍历所有自身属性（可枚举/不可枚举/Symbol/字符串），不继承
    //Reflect.ownKeys(target)等同于 Object.getOwnPropertyNames(target).concat(Object.getOwnPropertySymbols(target))
    for (let key of Reflect.ownKeys(child)) {
        console.log("Reflect.ownKeys()");
        console.log(child[key]);//111
    }
    //Object.getOwnPropertyNames()：遍历所有属性（包括可枚举，除Symbol外的不可枚举），不继承
    for (let key of Object.getOwnPropertyNames(child)) {
        console.log("Object.getOwnPropertyNames");
        console.log(child[key]);//111
    }

    //一个特殊的表达式：in，不能遍历对象，但是可以判断属性以及方法是否存在于对象内，会继承，并且可枚举/不可枚举都可以判断（没有任何限制条件）
    const arr = [1, 2, 3];
    console.log(1 in arr, 3 in arr);//true  false
    console.log("length" in arr);//true
    console.log("toString" in arr);//true
    ```

5. delete运算符：delete 运算符用于删除对象的一个**自身属性**（成功返回true，失败返回false）；**如果该属性的值是一个对象，并且没有更多对该对象的引用，该属性所持有的对象最终会自动释放**
   1. 私有属性永远不能被删除
   2. 不可配置的属性不能被移除。这意味着像 Math、Array、Object 这些内置对象的属性以及使用 `Object.defineProperty()` 方法设置为不可配置的属性不能被删除
   3. 任何使用 let 或 const 声明的属性不能够从它被声明的作用域中删除，因为它们没有附加到任何对象上
   4. 任何使用 var 声明的属性不能从全局作用域或函数的作用域中删除，因为即使它们可能附加到全局对象上，它们也是不可配置的
   5. 删除的属性不存在，那么 delete 将不会起任何作用，但仍会返回 true
   6. delete 操作符与直接释放内存没有关系。内存管理是通过破坏引用间接完成的
   7. **delete 只影响自身属性**。如果对象的原型链上有一个与待删除属性同名的属性，那么删除属性之后，对象会使用原型链上的那个属性
   8. 当你删除一个数组元素时，数组的长度（length）不受影响。即便你删除了数组的最后一个元素也是如此（delete数组元素会创建一个稀疏数组——包含empty，空缺的元素变为undefined）
6. **undefined并不是一个关键字**，而是window的一个属性，所以你甚至可以设置一个变量名为undefined：`let undefined = 99`，所以，代码中为了避免有这种逆天的代码，应该使用`void 0`来表示undefined（void+任何表达式都可以，它们返回值一定是undefined，void就是用来创建一个undefined），而不是直接将一个变量赋值为undefined
7. **`[Symbol.toPrimitive](hint)`**方法，返回一个原始值，被所有的强类型转换制算法优先调用。hint表示预期的原始值类型，可以是 "number"、"string" 和 "default" 中的任意一个，对于对象类型a+b以及a==b来说，总是优先使用`[Symbol.toPrimitive](hint)`（**hint为"default"**），然后是`valueOf()`，最后是`toString()`来进行转换。当然如果对象并没有定义`[Symbol.toPrimitive](hint)`，那么就优先`valueOf()`，然后再是`toString()`

    ```javascript
    const x = {
      [Symbol.toPrimitive](hint) {
        if (hint === "number") {
          return 10;
        }
        if (hint === "string") {
          return "hello";
        }
        return 3333;
      }
    };
    const y = {}, a = {}, b = {};
    x.valueOf = y.valueOf = function () { return 1111 };
    x.toString = y.toString = function () { return 2222 };
    console.log("🚀 ~ x+y:", x + y);//4444  x的default类型Symbol.toPrimitive + y的valueOf()
    console.log("🚀 ~ a==b:", a == b);//false 不是同一个引用的对象不相等
    console.log("🚀 ~ x==3333:", x == 3333);//true  x的default类型Symbol.toPrimitive值为3333
    console.log("🚀 ~ x+true:", x + true);//3334
    console.log("🚀 ~ x+'abc':", x + 'abc');//'3333abc'
    console.log("🚀 ~ x+1:", x + 1);//3334
    console.log("🚀 ~ String(x):", String(x));//'hello'  强制类型转换hint为"string"
    console.log("🚀 ~ Number(x):", Number(x));//10    强制类型转换hint为"number"
    ```

8. `valueOf()`方法的返回值：
   1. 基本数据类型（Number、String、Boolean、Symbol，**不包括undefined和null**）：返回对应的原始值。
   2. **对象（包括Array）：默认返回对象本身**，除非自定义重写。
9. `toString()`方法的返回值（**记住toString一定返回一个字符串，而valueOf遇到数组/对象不会转化为字符串**）：
   1. 基本数据类型（Number、String、Boolean、Symbol，**不包括undefined和null，因为它们不能调用任何方法，包括下面的`valueOf()`也是**）：返回对应的**字符串**表示
   2. **Array对象：返回数组元素的逗号分隔字符串，多层嵌套数组会被展平为一维字符串**，例如：`[1,3,[4,[],[6]]].toString()`会返回 `1,3,4,,6`，而`[].toString()`返回空串
   3. **普通对象：默认返回"[object Object]"字符串**
   4. Function对象：返回函数的源代码字符串。
   5. RegExp对象：返回正则表达式的字符串表示。
   6. Error对象：返回错误信息的字符串表示。
   7. Date对象：返回表示日期和时间的字符串。
10. 加法（+）
    1. +只能够执行两种操作：数字相加、字符串连接。**因此计算之前会强制将两个操作值转化为基本类型（调用`valueOf()`或者`toString()`方法）**，转化规则如下：
          1. **字符串优先：两方有一方是字符串，全转化为字符串**（true为"true"，false为"false"，null为"null"，undefined为"undefined"）
          2. 双方都是BigInt（比Number能表示的最大值更大的数字），执行BigInt加法；一方是一方不是，报错
          3. **其余情况都转化为Number**，并执行加法（执行`Number(val)`，**true为1；false为0；null为0，undefined为NaN，并且undefined与NaN执行任何的+运算结果都为NaN**）
    2. 当一个对象需要转换为基本类型时（**如使用 + 运算符或 == 比较不同类型值时**），JavaScript 会自动按顺序调用对象的`[Symbol.toPrimitive](hint)`（hint为 "default"），`valueOf()` 和 `toString()` 方法将对象转换为基本类型。如果对象没有 valueOf() 方法，或者它的 valueOf() 方法返回的不是基本类型值，JavaScript 会尝试调用对象的 toString() 方法来获取**字符串类型**值。如果对象的 toString() 方法也不是返回基本类型值，那么 JavaScript 就会抛出一个类型错误。
    3. 特殊的加法示例

        ```javascript
        let arr = [];
        let obj = {};
        console.log(arr.toString());//''
        console.log(arr.valueOf());// [] 即数组的valueOf()方法返回它本身
        console.log(obj.toString());//[object Object]
        console.log(obj.valueOf());//{} 即对象的valueOf()方法返回它本身 obj.valueOf()===obj返回true
        console.log([] + 3);//"3"
        console.log(arr + 3);//"3"
        console.log({} + 3);//[object Object]3
        console.log(obj + 3);//[object Object]3
        console.log({ a: 3 } + 3);//[object Object]3
        console.log(null + 3);//3
        console.log(undefined + 3);//NaN
        console.log(NaN + 23);//NaN，NaN进行任何数学计算值都为NaN
        ```

        ```javascript
        let obj1 = {
            a: 1,
            valueOf() {
                return 111;
            }
        };
        let obj2 = {
            b: 2,
            valueOf() {//优先调用，但是返回不是基本类型，于是调用其次的toString方法
                return [2, 3, 4];
            },
            toString() {//后调用
                return 'obj2_str';
            }
        }
        function add_all(...args) {
            let res = args.reduce(function (pre, cur) {
                return pre + cur;
            });
            console.log(res);
        }
        add_all(1, 2); //3
        add_all("str1", "str2"); //"str1str2"
        //有字符串全转化为字符串
        add_all(1, 'str'); //"1str"
        add_all(true, 'str'); //"truestr"
        add_all(false, 'str'); //"falsestr"
        add_all(null, ''); //"null"
        add_all(undefined, ''); //"undefined"
        //没有字符串，则全部转化为BigInt/Number类型
        add_all(1, true); //2
        add_all(true, false); //1
        add_all(true, null); //1
        add_all(true, true); //2
        add_all(true, undefined); //NaN
        add_all(1, undefined); //NaN

        //多项相加，从左按顺序执行+运算，注意过程中可能结果的类型会变化
        add_all(1, 2, null, true, false, 'str', null, true, false, undefined, 5); //"4strnulltruefalseundefined5"
        //对象等会使用valueOf()以及toString()方法转化为基本类型值
        add_all(obj1, 'str'); //111str
        add_all(obj1, 1); //112
        add_all(obj2, 'str'); //obj2_strstr
        add_all(obj2, 999); //obj2_str999
        ```

11. 相等运算符（`==`）
    1. 类型相同：
       1. **都是对象，必须指向同一个引用才会返回true**
       2. +0 == -0
       3. NaN !== NaN，NaN与任何值都不相等都返回false
    2. 类型不同：
       1. **如果其中一个操作数为 null 或 undefined，另一个操作数也必须为 null 或 undefined 才会返回 true。否则返回 false**，因此`0==null`或者`undefined==0`会返回false！
       2. 如果都是基本类型并且类型不同，会**将基本类型都尽量转化为数字**：**true转为1，false会转化为0，''转化为0**（null和undefined除外，因为它们必须两边的都为null和undefined才为真）；**这个和加法+不一样**，加法计算不同基本类型时存在字符串会优先将两者转化为字符串，其余情况转化为Number
       3. 如果其中一个操作数是对象，另一个是基本类型，按此顺序使用对象的 `[Symbol.toPrimitive](hint)`（hint为 "default"），`valueOf()` 和 `toString()` 方法将对象转换为基本类型。（**这个原始值转换与加法+中使用的转换相同**）

    ```javascript
    "1" == 1; // true
    "" == 0; //true
    "abc" == 1; // false
    1 == "1"; // true
    "" == false; // true  *******************!!!
    0 == false; // true
    0 == null; // false，null和undefined必须对应null或者undefined
    0 == undefined; // false
    null == undefined; // true
    0 == !!null; // true, 看看逻辑非运算符
    0 == !!undefined; // true, 看看逻辑非运算符
    1 == !!999; // true

    const number1 = new Number(3);
    const number2 = new Number(3);
    number1 == 3; // true
    number1 == number2; // false

    const a = [1, 2, 3];
    const b = "1,2,3";
    a == b; // true, `a` converts to string

    const c = [true, 0.5, "hey"];
    const d = c.toString(); // "true,0.5,hey"
    c == d; // true
    ```

12. 全等（===）：等值等型
13. `Object.is(value1, value2)`：
    1. 与==不一样，它**不会**进行类型转换
    2. 与===也不一样，它判断NaN是相等的，而判断+0和-0是不相等的
14. new操作符

    ```javascript
    //下面两个是构造函数，一个不返回（返回undefined），一个返回一个引用对象
    function Parent_Normal(name) {
        this.name = name;
    }
    function Parent_Arr(size) {
        const result = [];
        result.length = size;
        return result;
    }
    //自己写一个new函数
    function my_new(ParentConstructor, ...args) {
        const obj = {};//创建一个空对象
        obj.__proto__ = ParentConstructor.prototype;//将空对象的原型指定为构造函数的prototype
        //以上两句也可以合并为一下一句
        //const obj = Object.create(ParentConstructor.prototype);//以构造函数的原型对象为原型创建一个对象
        const result = ParentConstructor.apply(obj, args);//构造函数的this绑定创建的对象并且执行构造函数
        return result instanceof Object ? result : obj;//构造函数显式返回了实例对象（引用类型），则创建的实例对象会被覆盖，返回该对象，如果构造器返回基本类型或者没有显式返回（返回undefined），那么返回原先新创建的对象
    }
    const p1_my_new = my_new(Parent_Normal, '邹竹');
    const p1 = new Parent_Normal('邹竹');
    console.log(p1 instanceof Parent_Normal);//true
    console.log(p1_my_new instanceof Parent_Normal);//true

    const p2 = new Parent_Arr(5);
    const P2_my_new = my_new(Parent_Arr, 5);
    //这里的p2或者p2_my_new其实都是Parent_Arr函数返回的一个数组，而非一个实例对象
    console.log(p2 instanceof Parent_Arr);//false
    console.log(P2_my_new instanceof Parent_Arr);//false
    console.log(p2 instanceof Array, P2_my_new instanceof Array);//true true
    //let a = [] 和 let a = new Array(size)本质上都是一样的，都是Array的实例对象
    ```

15. if...else：**对象/数组是true，非零是true，非空字符串是true，其余的都是false**
16. 可选链运算符（`?.`）：当a为null/undefined时，`a?.b?.c.d.e`不会报错，会在a处短路返回`undefined`
17. `for of`：在**可迭代对象**（包括 Array，Map，Set，**String**，TypedArray，arguments 对象， DOM 元素集合NodeList等等）上创建一个迭代循环，调用自定义迭代钩子，并为每个不同属性的值执行语句
18. 关闭 for of 迭代：可以由 **break**, throw 或 return 终止，**并且不会改变原数组**。for of和for in都可以用break终止，只有forEach不行

    ```javascript
    //Map特殊一些，因为迭代的元素是数组
    let map = new Map([["a", 1], ["b", 2], ["c", 3]]);//Map迭代的是一个[key, value]组成的数组，因此可以用[key, value]解构其值
    //以下是普通的
    let arr = [1, 2, 3];//数组每一项迭代的是数组的元素，虽然有item+=1语句，但是item传递的是基本类型值，不是引用，因此不会对原数组造成污染
    let str = "abc";//字符串迭代的是每一个字符
    let set = new Set([1, 1, 2, 2, 3, 3]);//Set迭代的是Set中的每一个元素
    // 其它：
    // arguments迭代的是每一个参数
    function iter(iterable) {
        for (let item of iterable) {
            // item += 1;
            console.log(item);
        }
        console.log(iterable);
    }
    // iter(arr);
    // iter(str);
    // iter(map);
    iter(set);

    //字符串使用for of遍历会得到类似数组的下标！！
    const str = "abcd";
    for (let c in str) {
      console.log("🚀 ~ c:", c);
    }
    //0 1 2 3
    for (let c in str) {
      console.log("🚀 ~ str[c]:", str[c]);
    }
    //a b c d
    ```

19. `requestAnimationFrame(callback)`
    1. 和setTimeout看起来非常相似，但是setTimeout并不精确（事件循环原理），而`requestAnimationFrame`是精确根据屏幕刷新率来执行回调函数的，不会执行太快，导致过度开销，也不会太慢导致卡顿
    2. 不可见的元素不会重绘/回流（也就是重排）
    3. 切换到其它标签会暂停，不会浪费性能和资源，也不会损耗电池寿命
20. 解构赋值 & 剩余属性(...rest)

    ```javascript
    // // 数组的解构：
    // let [a, b, c] = Array(3); // a=undefined// b=undefined // c=undefined
    // [...Array(3)] // [undefined, undefined, undefined] Array(3)每一项都是empty，无法使用map遍历，但是通过数组结解构，会变成undefined
    // let [a, b, c] = [1, 2, 3];  // a = 1// b = 2// c = 3
    // let [a, [[b], c]] = [1, [[2], 3]]; // a = 1// b = 2// c = 3
    // let [a, , b] = [1, 2, 3]; // a = 1// b = 3
    // let [a = 1, b] = []; // a = 1, b = undefined //不完全解构
    // let [a, ...b] = [1, 2, 3]; //a = 1//b = [2, 3] //剩余运算符
    // let [a, b, c, d, e] = 'hello'; // a = 'h'// b = 'e'// c = 'l'// d = 'l'// e = 'o'
    // let [a = 2] = [undefined]; // a = 2 解构默认值
    // let [a = 3, b = a] = [];     // a = 3, b = 3
    // let [a = 3, b = a] = [1];    // a = 1, b = 1
    // let [a = 3, b = a] = [1, 2]; // a = 1, b = 2

    // // 对象模型的解构（Object）
    // let { foo, bar } = { foo: 'aaa', bar: 'bbb' }; // foo = 'aaa'// bar = 'bbb'
    // let { baz: foo } = { baz: 'ddd' }; // foo = 'ddd'
    // let obj = { p: ['hello', { y: 'world' }] };
    // let { p: [x, { y }] } = obj;
    // // x = 'hello'// y = 'world'
    // let obj = { p: ['hello', { y: 'world' }] };
    // let { p: [x, { }] } = obj;
    // // x = 'hello'
    // let obj = { p: [{ y: 'world' }] };
    // let { p: [{ y }, x] } = obj;
    // // x = undefined// y = 'world'
    // let { a, b, ...rest } = { a: 10, b: 20, c: 30, d: 40 };// a = 10// b = 20// rest = {c: 30, d: 40}
    // let { a = 10, b = 5 } = { a: 3 }; // a = 3; b = 5;
    // let { a: aa = 10, b: bb = 5 } = { a: 3 }; // aa = 3; bb = 5;
    ```

21. 按位异或 `^` ：全真或全假都为false，一真一假为true
22. `parseInt(str, radix)`：把str转换为十进制数字，radix表示str本身的进制，radix为0或者undefined时，通常判定为其默认值10（表示字符串是十进制），除非是0x开头或者0开头，因此一定要设置一个radix
23. 获取函数签名的参数个数：`parseInt.length`或者`Array.prototype.reduce.length`
24. `isNaN(a)`：判断变量是否为NaN
25. `addEventListener(type, listener[, options/useCapture])`: listener函数中的this是e.currentTarget，默认是冒泡阶段顺序触发

    ```javascript
    //dom节点由外至内以此为grand，parent，child
    const event_elements = ['grand', 'parent', 'child'];
    function listen(options) {
        console.log("监听器参数为：", options);
        event_elements.forEach(function (id) {
            document.getElementById(id).addEventListener("click",
                function (e) {
                    console.log(`${id}被触发！`);
                    console.log(e.currentTarget === this); // 输出 `true`
                },
                options
            )
        })
    }
    onMounted(() => {
        listen({
            capture: true,//捕获阶段触发，默认是false，即冒泡阶段触发，和传递单个useCapture布尔值效果一致
            once: false,//只调用一次后便移除
            passive: true,//监听是否被动，默认为false，设置为 true 时，表示listener函数永远不会（不能）调用 preventDefault()，如果强行调用会报错。触摸事件和滚轮事件的事件监听器的存在使得页面滚动时可能阻塞主线程，从而降低页面性能，将其设置为true告诉浏览器该监听器不会调用 preventDefault()，从而允许浏览器进行性能优化，提高某些事件的性能表现，但同时也意味着监听器无法取消事件的默认行为
        })
        //grand parent child ，即按照捕获阶段触发的顺序，由外至内触发

        // listen(true);//只传true相当于{capture: true}
    })
    ```

26. `event.preventDefault()`：阻止事件的默认动作，但是不会阻止事件流进一步传播，比如点击按钮（click事件）、输入文字（keypress事件）
27. `event.stopPropgation()`：阻止事件流（捕获阶段→目标阶段→冒泡阶段）进一步传播，propgation本身就是“传播”的意思
28. `event.stopImmediatePropagation()`：阻止事件流进一步传播**并且**阻止监听同一事件的其他事件监听器被调用：同一个事件有多个监听器，事件触发时会按顺序调用监听器，如果某个监听器中调用了本方法，则剩下的监听器不会被调用
29. <a id="scrollTo"></a>`window.scroll()`/`window.scrollTo()`：这两个方法用法和作用一模一样
    1. `window.scrollTo(left, top)`：滚动至文档坐标(left, top)
    2. `window.scrollTo(options)`：options包含3个属性：top（等价于y坐标）、left（等价于x坐标）、behavior滚动行为。其中behavior支持三个String参数：auto（默认值）、instant（瞬间滚动）、`smooth`（平滑滚动）

        ```javascript
        // 设置滚动行为改为平滑的滚动
        window.scrollTo({
            top: 1000,
            behavior: "smooth"
        });
        ```

30. **History API**
    1. `history.pushState(state, unused, url)`：不刷新的情况下在页面历史中添加一条新的**同源**url记录（假设是`/abc`），并且**`/abc`**下（注意是push的url而不是当前的url）保存了一个状态state，如果下一次通过浏览器前进后退（history.go(-1)）而跳转到`/abc`时，页面会触发onpopstate事件，在该事件中可以获取push入`/abc`的state
    2. 属性
       1. `history.length`：该整数表示会话历史中元素的数目，包括当前加载的页
       2. `history.scrollRestoration`：**滚动恢复属性**允许 web 应用程序在历史导航上显式地设置默认滚动恢复行为，有两个值：
          1. auto：将**恢复/保留**用户已滚动到的页面上的位置，刷新页面默认会保留
          2. manual：未还原页上的位置。用户必须手动滚动到该位置
       3. `history.state`：返回栈顶（当前url）的state对象（通过`history.pushState()`或者`history.replaceState()`方法设置），如果未手动设置，state值为null
    3. 方法
       1. `history.pushState(state, unused, url)`：
          1. state：是自定义对象，限制大小最大 16 MiB，如果需要更多的空间，建议使用 sessionStorage 和/或 localStorage
          2. unused：由于历史原因，该参数存在且不能忽略；传递一个空字符串是安全的，以防将来对该方法进行更改
          3. **url：必须同源**，否则报错，可以是hash值（比如#abc）
          4. 异步完成
          5. 调用pushState不会加载url页面，除非手动刷新
          6. pushState不会引起`window.onhashchange`事件，即使url是一个hash
          7. 对比 hash（锚点）
             1. 两者都会在当前文档创建一个新的历史记录
             2. pushState只要是同源的页面都可以添加到历史记录中，而hash会停留在同一个文档中
             3. pushState可以添加重复的url记录，但是hash不能重复添加当前的hash记录，仅有被添加的hash值与当前不同时才会添加到历史记录中（因为hash本身就只是个锚点）
             4. pushState可以使用你的新历史条目关联任意数据。使用基于 hash 的方式，你需要将所有相关的数据编码为一个短字符串。
       2. `history.replaceState()`：与pushState唯一区别是直接用新的url替换当前url而非新增历史记录
       3. `history.back()`：相当于点击浏览器回退按钮，等价于`history.go(-1)`
       4. `history.forward()`：相当于点击浏览器前进按钮，等价于`history.go(1)`
       5. `history.go(num)`：前进/后退指定步数，没有参数或者num为0时会刷新页面
31. **window.onpopstate**
    1. 例子：

        ```javascript
        window.onpopstate = function(event) {
            alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
        };
        ```

    2. 每当激活同一文档中不同的历史记录条目时，popstate 事件就会在对应的 window 对象上触发
    3. 如果当前处于激活状态的历史记录条目是由 `history.pushState()` 方法创建的或者是由 `history.replaceState()` 方法修改的，则 `popstate` 事件的 `state` 属性包含了这个历史记录条目的 `state` 对象的一个拷贝
    4. **调用 `history.pushState()` 或者 `history.replaceState()` *不会*触发 `popstate` 事件**。`popstate` 事件只会在浏览器某些行为下触发，比如点击后退按钮（或者在 JavaScript 中调用 `history.back()` 方法）。即，在同一文档的两个历史记录条目之间导航会触发该事件
32. hashchange事件：`#hash`值改变时触发
33. setTimeout原理：在等待设置的毫秒数之后，把函数添加到消息队列末尾，此时如果消息队列中还有其他任务再执行，则需要一直等待其他任务执行完毕才执行，所以**等待时间可能会超过设置的时间**

### web术语概念

1. 模块化
   1. 需要把 type="module" 放到 `<script>` 标签中，来声明这个脚本是一个模块`<script type="module" src="main.js"></script>`
   2. 只能在模块内部使用import和export，无法在普通script标签内使用，除非添加type="module"

        ```html
        <!-- type="importmap" 声明 JavaScript 模块的导入映射关系，其实就是对导入路径设置一个别名，后面再使用import进行导入的时候可以直接导入别名来代替这个路径 -->
        <script type="importmap">
            {
                "imports": {
                    //别名 : 实际路径
                    "three": "../build/three.module.js",
                    "three/addons/": "./jsm/"
                }
            }
        </script>

        <!-- 注意！！这里的script要加上type="module"，表示使用es6的模块语法import/export -->
        <script type="module">
            //可以直接导入上面imports里面定义的别名
            import * as THREE from 'three';
            import TWEEN from 'three/addons/libs/tween.module.js';
        </script>
        ```

   3. 模块之间互相独立，可以互相导入和调用
       **CommonJS的模块导入导出规范**

       ```javascript
       //****************CommonJS规范，导入导出有等于"="号（NodeJS默认使用该规范）

       //demo.js
       //模块化--导出
       //CommonJS规范（NodeJS默认使用该规范）
       const obj = { a: 1, b: () => { console.log('我是obj.b') } };
       module.exports = obj;
       //或者如下，和上面一行的效果一模一样
       // module.exports = { a: 1, b: () => { console.log('我是obj.b') } };

       //index.js
       //模块化--导入，下面两种方式都可以成功导入
       //导入了demo之后，webpack.config.js的配置中的人口文件就不应该有demo，会执行2次demo文件，但是如果有的话，可以提取公共部分解决问题
       const obj = require('./demo');
       console.log(obj);//{a: 1, b: ƒ}
       //或者
       const { a, b } = require('./demo');
       console.log(a, b);// 1   ƒ (){console.log("我是obj.b")}
       ```

       **ES6模块导入导出规范**

       ```javascript
       //data.js 导出模块
       let single_user = 'admin_single', single_pwd = 123, mult_user = 'admin_mult', mult_pwd = 456;
       //单独的export可以有多个，并且导出之后解构赋值，必须使用一个/多个同名变量来接入（多对多），即：import { single_user, single_pwd } from './data'
       export {
           single_user,
           single_pwd
       };
       const data = {
           mult_user,
           mult_pwd
       }
       //export default 一个模块只能有一个，并且只能用一个变量名来接（一对一），即：import any_alias from './data'
       export default data;

       //index.js 导入模块
       //模块化--导入 ES6规范
       import any_alias from './data';//对应 export default
       console.log(any_alias.mult_user, any_alias.mult_pwd);

       import { single_user, single_pwd } from './data';//解构，对应 export
       console.log(single_user, single_pwd);
       ```

   4. import有两种导入方式：标准导入、动态导入
      1. 标准导入：`import { getUsefulContents } from "/modules/file.js";`
         1. 同步导入
         2. 可选择仅导入需要的接口/函数，便于初始化和tree-shaking
      2. 动态导入：`import("/modules/my-module.js").then()`
         1. 动态/异步导入，返回一个Promise
         2. 可以在需要的时候再请求服务器加载该模块（按需加载）
         3. 只能导入整个模块
   5. require和import的区别
      1. require 是 CommonJS规范引入方式
      2. import是es6的一个语法标准，如果要兼容浏览器的话必须转化成es5的语法
      3. require是运行时调用，所以require理论上可以运用在代码的任何地方
      4. import是编译时调用，所以**同步导入时**必须放在文件最外层
      5. require是赋值过程，其实require的结果就是对象、数字、字符串、函数等，再把require的结果赋值给某个变量
      6. import是解构过程，但是目前所有的引擎都还没有实现import，我们在node中使用babel支持ES6，也仅仅是将ES6转码为ES5再执行，import语法会被转码为require
2. **序列化**：序列化是将一个对象或数据结构转换为适合网络传输或存储的格式（如数组缓冲区或文件格式）的过程。例如在 JavaScript 中，你可以通过调用 `JSON.stringify()` 函数将某个值序列化为 JSON 格式的字符串。CSS 值可以通过调用 `CSSStyleDeclaration.getPropertyValue()` 函数来序列化。
3. 防抖/节流
   1. 防抖（Debouncing）：（**延迟执行**，有clearTimeout）
      1. 当事件被触发后，**等待一段时间（例如 300 毫秒）后再执行**函数。
      2. 如果在等待时间内再次触发了同样的事件，则重新计时，重新等待一段时间后执行函数。
      3. 如果在等待时间内触发了不同的事件，则重新计时，等待一段时间后执行函数。
      4. 这种机制确保只有在事件停止触发一段时间后，才执行最后一次触发的函数。
      5. 防抖常用于处理如输入框输入、窗口调整大小等事件，以减少事件处理的频率。

      ```javascript
        var debounce = function(fn, t) {
            let timer;
            return function(...args) {
                if(timer){//仍处于上一次事件触发等待期，清除计时器，重新开始计时
                    clearTimeout(timer);
                }
                timer = setTimeout(fn, t, ...args);
            }
        };
      ```

   2. 节流（Throttling）：（**立即执行**，没有clearTimeout）
      1. 当事件被触发后，**立即执行**函数，并在一定时间内（例如每隔 300 毫秒）阻止函数再次执行。
      2. 如果在等待时间内再次触发了同样的事件，则被忽略，函数不会执行。
      3. 如果在等待时间内触发了不同的事件，则立即执行函数，并重置计时器。
      4. 这种机制确保函数在一定时间间隔内最多执行一次。
      5. 节流常用于处理如滚动事件、请求发送等频繁触发的事件，以限制函数执行的频率。
      6. 这两种技术都有各自的应用场景，具体使用哪种取决于实际需求。防抖适用于需要等待一段时间后执行最后一次触发的函数，而节流适用于限制函数的执行频率。

      ```javascript
        var throttle = function(fn, t) {
            let available = true, stored_args;
            return function callback(...args) {
                stored_args = args;//用于保存新的参数，当间隔时间过后会直接执行最新参数函数
                if(available){//当前可直接执行
                    fn(...stored_args);
                    available = false;
                    stored_args = null;
                    setTimeout(()=>{
                        available = true;
                        stored_args&&callback(...stored_args);
                    }, t);
                }
            }
        };

      ```

4. **深拷贝**：
   * 对于可序列化对象：
     1. `JSON.stringify(obj)`

        ```javascript
        let obj = {
            a() {
                console.log(this);
            },
            b: 111,
            c: null,
            d: undefined,
            e: new Date()
        }
        let trans_obj = JSON.parse(JSON.stringify(obj))
        console.log(trans_obj);//{"b":111,"c":null,"e":"2023-05-11T08:12:30.849Z"}
        console.log(typeof obj.e, typeof trans_obj.e);//object string
        //函数、undefined等不会被复制，Date等对象也会有问题变成了string类型
        ```

     2. `structuredClone(obj)`

   * 对于不可序列化对象：
     1. 递归+判断属性是否是对象，是对象则继续递归，否则直接赋值

        ```javascript
        const copyObj = (obj = {}) => {
            let newobj = null; //变量先置空
            //判断是否需要继续进行递归
            if (typeof (obj) == 'object' && obj !== null) {
                newobj = obj instanceof Array ? [] : {}; //进行下一层递归克隆
                for (var i in obj) {
                    newobj[i] = copyObj(obj[i])
                }
            } else newobj = obj; //如果不是对象直接赋值
            return newobj;
        }
        ```

     2. 第三方库方法：`lodash.cloneDeep()`，实际也是递归封装了一下
     3. jquery的extend()方法：`$.extend(true, {}, obj)`
5. 全局变量：通过 `globalThis` 访问，在浏览器中是`window`，在node中是`global`，在其他环境中可能有不同名称
6. 变量提升 & 函数提升:
   1. 变量提升所有的声明都会提升到当前作用域的最顶上去
   2. 函数声明的优先级高于变量声明的优先级，并且函数声明和函数定义的部分一起被提升
   3. 变量提升，但是赋值操作不会提升！！因此提升后直接取值会是undefined
   4. let 、const存在一个暂时性死区（暂时性死区：创建了变量但是没有初始化之前，没法使用变量，报错；“Cannot access 'value' before initialization”），只在变量创建阶段有提升，在初始化阶段没有提升，形成的暂时性死区。

    ```javascript
    fn();
    function fn() {
        console.log(1);
    }
    fn();
    function fn() {
        console.log(2);
    }
    fn();
    var fn = function () {
        console.log(3);
    }
    fn();
    function fn() {
        console.log(4);
    }
    fn();
    function fn() {
        console.log(5);
    }
    fn();
    //输出：555333
    //4个函数声明分别输出1，2，4，5依次被提升到作用于顶部（会携带函数体一起提升）
    //还有1个变量提升输出3，提升后值为undefined
    //函数提升优先级比变量提升高，并且5处于最后会覆盖掉124的提升以及undefined
    //所以在3初始化之前的3个fn()调用，都输出5
    //3初始化之后的3个fn()调用，都输出3
    ```

7. 迭代器和生成器：
   1. 迭代器：
      1. 可迭代对象（包括 Array，Map，Set，String，TypedArray，arguments 对象等等）
      2. 迭代操作:
         1. **常规方式：`for of` 循环迭代**
         2. **手动通过 `.next()` 执行迭代**，通过调用迭代器的.next()方法来迭代元素，返回一个包含value和done属性的对象，value即是迭代元素的值，done是迭代结束标志

               ```javascript
                   const arr = [1, 2, 3];
                   const iterator = arr[Symbol.iterator]();

                   console.log(iterator.next()); // { value: 1, done: false }
                   console.log(iterator.next()); // { value: 2, done: false }
                   console.log(iterator.next()); // { value: 3, done: false }
                   console.log(iterator.next()); // { value: undefined, done: true }
               ```

      3. 自定义迭代器，实现一个next()内部方法，并且自己要维护每个返回值，如果需要使用for of进行迭代，后文有实现`function* [Symbol.iterator]()`方法的例子

            ```javascript
            //自定义一个迭代器，从start到end，理论上不给end非Inifinity值可以无限迭代下去，每次向前迭代step大小
            function my_iterator(start = 0, end = 10, step = 1) {
                let current = start;
                const iter_obj = {
                    next() {
                        if (current <= end) {
                            let result = current;
                            current += step;
                            return {
                                value: result,
                                done: false
                            };
                        } else {
                            return {
                                value: undefined,
                                done: true
                            }
                        }
                    }
                }
                return iter_obj;
            }
            const iter = my_iterator(3, 21, 5);//从3开始每次迭代加5
            let item = iter.next();
            while (!item.done) {
                console.log(item.value);
                item = iter.next();
            }
            //输出：3 8 13 18
            ```

   2. 生成器：generator函数（**`function*`定义，`yield`单个元素，`yield*`托管给另一个迭代器**）
      1. 自定义迭代器比较麻烦，需要显式地维护其内部状态（比如定义`.next()`方法，返回对象必须包含value和done属性等等），因此更强大的生成器应运而生
      2. 用法：

            ```javascript
            const arr2 = [1, 2, 3];
            function* my_generator() {
                // for (let i = 0; i < arr2.length; i++) {
                //     yield arr2[i];
                // }
                yield* arr2;//上面的for循环也可以用这一句代替，yield* 会暂停当前生成器的执行，并将控制权转移给另一个生成器或可迭代对象，这里也就是相当于合并了另一个迭代对象arr2[Symbol.iterator]
            }
            let g1 = my_generator();
            console.log(g1.next());// { value: 1, done: false }
            console.log(g1.next());// { value: 2, done: false }
            console.log(g1.next());// { value: 3, done: false }
            console.log(g1.next());// { value: undefined, done: false }
            ```

   3. 可迭代对象
      1. **对于一个可迭代对象，它本身或者原型链上某个对象必须包含一个Symbol.iterator()方法**，这意味着所有的可迭代对象（**String（）**、Array、TypedArray、Map 和 Set）都可以通过 `obj[Symbol.iterator]()` **方法**来获取其内置的迭代器

            ```javascript
            const str = "你好吗", str_iter = str[Symbol.iterator]();
            const arr = [5, 6, 7], arr_iter = arr[Symbol.iterator]();
            const map = new Map([['a', 1], [8, '8_value']]), map_iter = map[Symbol.iterator]();
            const set = new Set([1, 1, 1, 334, 6]), set_iter = set[Symbol.iterator]();
            function itering(iter) {
                let obj = iter.next();
                while (!obj.done) {
                    console.log(obj.value);
                    obj = iter.next();
                }
            }
            itering(str_iter);// 你 好 吗
            //注意数组的Symbol.iterator方法和entries方法的区别：Symbol.iterator方法的每一项的value是 item，而entries方法的每一项的value是 [index, item]，即索引＋元素组成的数组
            itering(arr_iter);// 5 6 7
            itering(arr.entries());// [0, 5] [1, 6] [2, 7]
            itering(map_iter);// ['a', 1] [8, '8_value']
            itering(set_iter);// 1 334 6
            ```

      2. 自定义一个可迭代对象（不是迭代器，迭代器需要自定义next()方法并返回包含value、done属性的对象，前面有代码）

            ```javascript
            //自定义可迭代对象
            const my_iterator = {
                [Symbol.iterator]: function* () {
                    yield 1;
                    yield 2;
                    yield 3;
                }
            }
            //通过for of迭代
            for (let item of my_iterator) {
                console.log(item);// 1 2 3
            }
            //通过[Symbol.iterator]()方法迭代
            const iter = my_iterator[Symbol.iterator]();
            let next = iter.next();
            while (!next.done) {
                console.log(next.value);// 1 2 3
                next = iter.next();
            }
            ```

8. `for in` 、 `for of` 、`forEach`
   1. `for in` 和 `for of` 都是迭代语句，`for in` 用于遍历一个对象的所有可枚举属性（自身+继承）；`for of` 只能遍历一个可迭代对象的每一个元素（比如遍历数组、字符串、Set、Map的每个元素），如果`for of`后面的对象不可迭代会报错
   2. `for in` 返回的是对象属性名，`for of` 返回元素的值
   3. `for in` 适合遍历对象，不适用于遍历数组（因为遍历数组时会遍历数组的非数字属性），而`for of` 适用于遍历数组、字符串等可迭代对象，遍历一个普通的对象会报错（not iterable）
   4. **`forEach`不能用`break`跳出循环，而`for in`和`for of`可以**
   5. 总的来说，`for in`适用于迭代对象的属性，而`for of`适用于迭代对象的值，处理字符串、数组的时候，`for of` 更加方便、安全，能保证迭代顺序和值的类型，`forEach`仅适用于**数组、Map、Set等**，适用于需要对数组的每个元素执行回调函数的情况，无法使用 `break` 或 `continue` 控制循环流程。

    ```javascript
    let arr = [5, 4, '你好'], obj = { a: 222, b: 333, c: 444 };

    const for_in_fun = function (arg) {
        for (let item in arg) {
            console.log(`${item} `);
        }
    }
    const for_of_fun = function (arg) {
        for (let item of arg) {
            console.log(`${item} `);
        }
    }
    for_in_fun(arr);// 0 1 2
    // for_in_fun(obj);// a b c
    // for_of_fun(arr);// 5 4 你好
    // for_of_fun(obj);// 报错，obj是不可迭代对象

    //字符串使用for of遍历得到的是类似数组的下标！！
    const str = "abcd";
    for (let c in str) {
      console.log("🚀 ~ c:", c);
    }
    //0 1 2 3
    for (let c in str) {
      console.log("🚀 ~ str[c]:", str[c]);
    }
    //a b c d
    ```

9. 跳出foreach循环：使用`try {arr.foreach((item)=>{//写条件，满足条件throw new Error()})} catch(err){//捕获}`，**不能**使用break，for of和for in可以
10. 手写instanceof

    ```javascript
    function my_instance_of(obj, fun) {
        if ((obj == null || typeof obj != 'object') && typeof obj != 'function') {//对基本类型以及function类型都要做特殊处理
            return false;
        }
        while (obj != fun.prototype) {
            if (obj === null) {
                return false;
            }
            obj = obj.__proto__;
        }
        return true;
    }
    const compare = (obj, fun) => {
        const correct_asw = obj instanceof fun, my_asw = my_instance_of(obj, fun);
        console.log(correct_asw, my_asw);
    }
    class Parent { }
    class Child extends Parent {
        constructor() {
            super();
        }
    }
    let child1 = new Child();
    compare(child1, Child);// true true
    compare(child1, Parent);// true true
    compare(Function, Object);// true true
    compare(child1, Object);// true true
    compare(Child, Function);// true true
    compare(child1, Function);// false false
    compare(1, Number);// false false
    ```

11. 基本类型与引用类型
    1. **基本类型**数据直接存放在**栈**内存中
    2. 引用类型**栈**内存中存放的是一个引用/指针，这个指针指向的**堆**内存中存放的才是实际数据
    3. **基本类型没有原型、属性、方法，但是基本类型（null/undefined除外）访问其属性时会自动被包装成对应的包装类型（被转化为一个对象，null和undefined除外）再获取其原型/属性/方法**。一旦属性访问或方法调用结束，这个临时对象就会被丢弃。

    ```javascript
    let str = "";
    let str_obj = new String("");
    console.log(str.__proto__ === String.prototype);//true ，这里本来str是基本类型，没有原型/属性，但是包装器将其转化为一个String对象从而具有原型/属性
    console.log(str instanceof String);//false ，只有实例对象才能成立，因此这里即便String再str的原型链上（因为实际上判断的是String在str包装对象的原型链上），也仍然返回false
    console.log(str_obj.__proto__ === String.prototype);//true ，引用类型有__proto__，自然是成立的
    console.log(str_obj instanceof String)//true ，实例对象，因此也成立
    ```

12. **弱类型：允许隐式类型转换而不报错**
13. **动态类型：变量可以被赋值或者重复赋值为任意类型**
14. script标签的defer和async：浏览器解析到一个`<script>`标签时，会立即停止解析 HTML，然后请求相应的 JS 文件并开始下载。**如果是`<script>`标签引入的 JS 文件，则会阻塞 HTML 的渲染（因为浏览器的GUI渲染线程和JS引擎线程是互斥的），直到 JS 文件下载完成并执行后才继续（已经做过实验求证过，确实如此，同步的JS循环会阻塞页面渲染）**。这可能会导致页面加载速度变慢。defer和async是两种处理 JS 文件的方式，可以让 JS 文件的下载不会阻塞页面的渲染。
    1. defer会使浏览器在 **HTML 解析完毕后**，**DOMContentLoaded 事件触发前**，按照文件出现顺序下载并且延迟执行，不阻塞页面渲染，使用webpack打包后的html文件引入js就是采用这种方式。（**渲染完再执行、顺序下载、顺序执行、延迟执行，不阻塞**），defer属性对模块脚本（含type = "module"标志）没有用，模块脚本默认defer
    2. async则是当 JS 文件下载完成后立即执行，并不保证文件顺序执行（谁快谁先上）。也就是说如果多个async的 JS 文件依赖于其他 JS 文件中的变量或函数或者DOM结构，可能会出现问题。async脚本可能在DOMContentLoaded之后执行，也可能在其之前执行。（**先下载完就先执行，无序，执行过程会中断渲染**）
    3. 小结：
       1. async 和 defer 都**指示浏览器在一个单独的线程中下载脚本**，而页面的其他部分（DOM 等）正在下载，因此在获取（下载）过程中页面加载不会被阻塞
       2. **async 属性的脚本将在下载完成后立即执行。这将阻塞页面，并不保证任何特定的执行顺序**
       3. 带有 **defer** 属性的脚本指示浏览器应该延迟执行这个脚本，直到整个HTML文档都被解析完毕，**defer脚本按照它们的顺序加载**，并且只有在所有脚本加载完毕后才会执行，
       4. 如果脚本无需等待页面解析，且无依赖独立运行，那么应使用 async。
       5. 如果脚本需要等待页面解析，且依赖于其他脚本，调用这些脚本时应使用 defer，将关联的脚本按所需顺序置于 HTML 的相应 `<script>` 元素中
15. 重绘与重排（**重排也叫回流**）
    1. 重绘（repaint）是指当元素的样式发生变化，但不影响其几何属性时，浏览器会重新绘制该元素。这意味着浏览器只需更新元素的视觉外观，而无需重新计算布局。**即布局不变，仅仅外观变化**。比如更改元素的字体颜色、背景颜色、透明度等等
    2. 重排（reflow）是指当 DOM 结构发生变化或元素的尺寸、位置等属性发生变化时，浏览器会重新计算渲染树，然后重新布局**整个**页面。**即布局改变，整个页面重新渲染**。比如元素的宽度高度、位置发生改变，新增/删除可见元素，内部内容改变，初始渲染、窗口resize等等
    3. 重绘不一定重排，重排一定会重绘（当浏览器完成重排之后，将会重新绘制受到此次重排影响的部分）
    4. 重排优化：集中改变样式、使用absolute/fixed
16. JS内存管理
    1. 自动分配/自动释放
    2. JS垃圾回收机制（主要依赖于引用的概念）
       1. 引用计数法——最初的垃圾收集算法：对象被引用一次（赋值一次）计数器加一，被取消引用一次，计数器减一，计数器为0被回收器清除，但是在处理`循环引用/互相引用`时会出问题（比如obj1.a=obj2; obj2.a=obj1，函数执行完毕后彼此的引用都存在因此不回收），因此大部分js引擎都不采用本方法
       2. 标记清除算法：通过**遍历**对象引用关系，标记所有活动对象，当对象不再被任何的变量/属性引用（**无法获取/不可达**）的时候，该对象会被清除。（函数执行完毕，对象在函数外围/全局变量中**无法获取**，此时会自动清除）。清除后可能会产生不连续的内存空间，因此后面还需要内存整理。

            ```javascript
            //以下代码运行在全局（作用域），因为函数（作用域）内部的变量/对象在函数执行完毕之后会被自动清除，因此不需要手动赋值为null
            let obj1 = { a: 1 };//创建了一个对象，并且被变量obj1引用
            let obj2 = obj1;//对象被obj2引用
            obj1 = null;//取消obj1的引用
            obj2 = null;//取消obj2的引用
            //此时这个对象没有被任何变量引用，下一次JS引擎定期检查和回收的时候该对象会被回收

            //使用闭包来解释标记清除法
            function fun(a) {
                let saved = 99;
                let unsaved = 88;
                return function (b) {
                    return a + b + saved;
                }
            }
            let me = fun(1);
            //此时，执行完了fun函数，回收器要开始回收fun创建的三个作用域对象：a，saved，unsaved
            //但是，这里的a、saved被返回的函数引用着，因此无法被回收，一直留在内存中，直到没有任何引用指向返回的匿名函数（即me被回收）
            me = null;
            //手动将me置为null，此时上面返回的函数引用被消除，函数被回收，其保存的闭包变量a，saved也被回收
            ```

       3. 对于**仅在某一个函数内部使用**的变量，在函数执行结束后，其作用域也就随之结束，变量及其引用的对象也就成为垃圾对象，等待垃圾回收机制自动回收。因此，在这种情况下，不需要手动将变量赋值为null。
17. 函数柯里化（使用闭包保存前一个参数）
    1. 参数复用：柯里化可以通过固定部分参数的方式，创建一个新的函数，该函数可以重复使用，传递剩余的参数进行调用。这样可以减少重复的代码，提高代码的可重用性。
    2. 延迟执行：柯里化可以将函数的部分参数进行提前传递，留下剩余参数的空缺，以后再传递。这样可以实现延迟执行函数的效果，根据需要动态地提供参数，使得函数的调用更加灵活。
    3. 函数组合：柯里化可以方便地实现函数的组合，将多个函数组合成一个新的函数，以实现更复杂的功能。通过将部分参数绑定在柯里化的函数中，可以在函数组合过程中传递剩余参数，并且可以轻松地重组和重用函数。
    4. 高阶函数的使用：柯里化是函数式编程中的重要概念，它能够与其他高阶函数（如 map、filter、reduce 等）配合使用，提供更加灵活和可组合的编程方式。
    5. 部分应用：通过柯里化，可以将多个参数的函数转化为接受更少参数的函数。这种部分应用的方式可以使得函数的调用更加简洁和清晰，而不需要一次性提供所有参数。
    6. 总的来说，柯里化在函数式编程中起着重要的作用，可以提高代码的可重用性、可组合性和灵活性，使得函数的调用更加简洁和可读。它在实际应用中广泛用于函数组合、参数复用、延迟执行等方面，为编程带来了许多优势。

    ```javascript
    function add(a, b) {
        return a + b;
    }
    add(1, 2);//直接在括号里写所有的参数
    function add_currying(a) {
        return function (b) {
            return a + b;
        }
    }
    let first = add_currying(1);//可以实现参数1的复用，或者一些其它计算判断条件的复用，避免每次调用函数重复判断
    let result1 = first(2);
    let result2 = first(3);
    ```

18. 事件委托：**多个子元素绑定的事件委托给父元素处理**，比如多个li的click事件绑定到父元素ul上，ul的click事件中的`e.target`就是所点击的子元素li，也就是事件捕获过程中的最小的元素，在这个过程中，由于子元素内部很有可能嵌套了更多子元素，有可能出现e.target并不是所期望的子元素，这个时候我们可以使用`event.target.closest("a")`来精确找到我们所需要的子元素，其中，closest内部参数是一个css选择器，这个例子是找到最近的祖先元素中标签是a的元素
19. 事件循环
    1. JS中的任务会被添加到任务队列（也就是消息队列）中，只有当一个消息完整地执行后，其它消息才会被执行，不会被抢占。任务执行完毕→任务队列取出下一个任务执行，如此重复循环，直到任务队列清空，这是最简单的事件循环的理解
    2. JS是单线程执行的，后一个任务需要等待前一个任务执行完毕之后才能继续执行，这个模式是合理的设计，但是也有缺点，就是当处理setTimeout、Ajax、Dom时间交互时，可能出现长时间的等待，因此就出现了异步，将任务交给异步模块去处理，大大提高主线程的效率。
    3. 同步代码不属于宏任务（macrotasks）也不属于微任务（microtasks），它们是在JavaScript事件循环的主执行流中直接执行的任务
    4. 异步任务队列分为宏任务队列、微任务队列，JS执行顺序为：**先执行同步代码，遇到微任务放入微任务队列，遇到宏任务放入宏任务队列，主线程同步代码执行完毕后优先执行微任务队列的所有任务，然后再执行宏任务队列中的任务**，当宏任务队列执行过程中遇到了微任务，那么当前宏任务执行完毕之后，会再次优先执行微任务，如此循环往复直至任务队列执行完毕
       1. 常见宏任务：
          1. setTimeout
          2. setInterval
          3. 网络请求
       2. 常见微任务：
          1. Promise的`then()`和`catch()`
          2. NodeJS中的`process.nextTick`：`process.nextTick` 可以将一个函数的执行优先级提升到当前微任务队列中的最高，在微任务队列中的所有其他微任务之前执行(当前执行栈结束之后、下一个事件循环之前执行)
    5. 理解`setTimeout`：时间值参数代表了消息被实际**加入到队列**的**最小延迟时间**。如果队列中没有其它消息并且栈为空，在这段延迟时间过去之后，消息会被马上处理。但是，如果有其它消息，setTimeout 消息必须等待其它消息处理完。因此第二个参数仅仅表示最少延迟时间，而非确切的等待时间
20. 进程与线程：
    1. 任何程序必须有一个及以上的进程，进程必须有一个及以上的线程，线程范围更小
    2. 进程有独立的内存，而多个线程是可以共享内存的，提高了效率
    3. 进程有独立的入口/出口，可以独立运行，线程必须在进程内运行
21. AJAX，即异步 JavaScript 和 XML（Asynchronous JavaScript and XML），浏览器原生支持的有两个异步请求方式：**XHR 、 fetch**
    1. XHR（旧），即XMLHttpRequest
       1. 比如jQuery的`ajax()`，以及axios
       2. 设置 `withCredentials:true` 会影响底层的XHR对象的行为，具体来说就是会让请求头携带cookie传送给服务器，并不是说请求里会带上withCredentials字段

            ```javascript
            $.ajax({
                url: 'https://api.example.com/data',
                method: 'GET',
                xhrFields: {
                    withCredentials: true
                },
                success: function(response) {
                    // 处理响应数据
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // 处理错误
                }
            });
            ```

    2. fetch（新）
       1. 基于Promise
       2. 与`jQuery.ajax()`主要有以下不同
          1. 收到代表错误的http状态码时，不会被标记为reject，即使是404或者500。相反，它会把状态码不在200~299的resolve返回值的`ok`属性设置为false，仅当网络故障/请求被阻止时，才会标记reject
          2. fetch无法获取上传进度
       3. 无法获取**上传/请求**进度：
          1. fetch无法监控发出**请求/上传**的进度，因为fetch基于Promise，上传数据相当于创建一个Promise，而Promise创建完毕就立即执行，无法获取其执行进度
          2. 而XHR可以通过`xhr.upload.onprogress=function(event){}`事件来监听**请求/上传**的进度，其中`event.total`是总数据大小，`event.loaded`表示已经传输的大小
       4. 可以获取**下载/响应**进度
          1. fetch可以通过**重复执行**`await response.body.getReader().read()`来逐块获取已经下载的部分数据(就是一点点地拿)，可以与总数据大小（`response.headers.get('content-length')`）进行比较从而得到下载进度

                ```javascript
                // 代替 response.json() 以及其他方法
                const reader = response.body.getReader();

                // 在 body 下载时，一直为无限循环
                while(true) {
                    // 当最后一块下载完成时，done 值为 true
                    // value 是块字节的 Uint8Array类型化数组（即一个具体的TypedArray，TypedArray可以看作一个抽象类，但是不是一个全局对象/声明，它是众多类型化数组的统一名称）
                    const {done, value} = await reader.read();
                    if (done) {
                        break;
                    }
                    console.log(`Received ${value.length} bytes`)
                }
                ```

          2. XHR通过`xhr.onprogress=function(event){}`监听**响应/下载**进度，其中`event.total`是总数据大小，`event.loaded`表示已经传输的大小
       5. [用法](https://developer.mozilla.org/zh-CN/docs/Web/API/Fetch_API/Using_Fetch)：

            ```javascript
            const options = {
                method: "POST", // GET、PUT、DELETE 等等
                body: JSON.stringify(dataObject)    // 请求体，用于 POST 或 PUT 请求发送数据，注意 GET 或 HEAD 方法的请求不能包含 body 信息
                headers: {
                    'Content-Type': 'application/json',  // 请求头，用来指定发送的数据类型
                    'Authorization': 'Bearer <token>'    // 可以包含其他自定义的请求头
                },
                // credentials: 'same-origin',         // 控制是否发送跨域请求时携带 cookies，还可以是include，设置include时，相当于jq.ajax()中的withCredentials为true，也就是允许携带cookie，但此时后端的响应头里Access-Control-Allow-Origin 字段不能使用通配符 "*"，而必须是当前的域名；还可以为omit，标识忽略不发送cookie等凭据
                // mode: 'cors',                       // 请求的模式，控制 CORS 行为
                // cache: 'no-cache',                  // 控制请求是否缓存
                // redirect: 'follow',                 // 控制请求遇到重定向时的处理方式
                // referrerPolicy: 'no-referrer',      // 控制请求的 referrer 信息
            };
            fetch("http://abc.com/api/efg", options)
                .then((response) => response.json())
                .then((data) => console.log(data));
            ```

22. 文件的切片上传：
    1. `<input type="file" />`可以选择文件，并且通过元素的`files`获取已经选择的文件列表，列表包含每个文件的详细信息，是一个File对象
    2. File对象是Blob对象的一个子类，Blob对象标识二进制的数据（图片、音频等），File中包括文件名、类型、大小、最后修改时间等等信息
    3. File对象有一个slice方法，类似于字符串的slice：`file.slice(start, end, file_type)`，其返回值也是一个Blob对象，即截取的数据组成的“部分文件”
    4. FileReader对象读取BLob数据，`reader.readAsDataURL(sliced_blob)`可以读取前面截取的“部分文件”，`reader.onload`完成后通过`event.target.result`获取该文件读取结果
    5. FormData，表单数据，用来盛放数据的对象，通过formData.append(key, value)来向对象中添加一条数据，可以直接把第一步获取的file放进formData中：`formData.append(""file"", file)`
    6. 将3中通过slice得到的“部分文件”（即文件切片）通过**一次请求**传输给后端，循环这个过程，直至slice的end参数大于文件本身大小
    7. 代码如下：

        ```html
        <!DOCTYPE html>
        <html lang="en">
        <body>
        <div class="container">
            <h1>大文件上传</h1>
            <input type="file" id="fileInput" accept="image/*">
            <button id="uploadButton">切片上传</button>
            <br>
        </div>
        <script>
        //chunk就是一个切片，也就是小文件
            async function uploadChunk(chunk) {
                const formData = new FormData();
                formData.append('file', chunk);

                //这里的地址可以替换为你的后端地址
                const response = await fetch('https://file.io', {
                    method: 'POST',
                    body: formData
                });

                const result = await response.json();
                return result;
            }

            document.getElementById('uploadButton').addEventListener('click', async function() {
                const fileInput = document.getElementById('fileInput');
                const file = fileInput.files[0];
                const chunkSize = 100 * 1024; // 设置切片大小100KB
                const totalChunks = Math.ceil(file.size / chunkSize);

                //循环切片过程，直到整个文件都被切完
                for (let i = 0; i < totalChunks; i++) {
                    const start = i * chunkSize;
                    const end = Math.min(start + chunkSize, file.size);
                    const chunk = file.slice(start, end);
                    //上传一个切片
                    const result = await uploadChunk(chunk);
                }
            });
        </script>
        </body>
        </html>
        ```

23. preload和prefetch：
    1. **preload**：页面需要马上使用的资源，**强制浏览器尽快加载**，属于关键资源：`<link rel="preload" href="style.css" as="style" /><link rel="preload" href="main.js" as="script" />`，除了要设置rel="preload"外，**还需指定href路径和as资源类型**（style、script、image、font等等）
       1. 比如字体，当某个按钮有特定字体时，**如果不预加载字体，那么就会先加载CSS并且使用默认字体，直到字体文件请求成功后才会一瞬间替换成指定字体，造成FOUT字体闪动**（Flash of Unstyled Text）；那么这个时候则应该使用预加载字体文件：**先加载字体（虽然加载完了但是还没有被使用），然后加载CSS，CSS解析到需要使用字体文件，则直接从缓存中获取该字体，避免重新请求**，从而不会发生闪动问题
    2. **prefetch**：利用浏览器**空闲时间**来下载或**预取**用户在不久的将来可能访问的文档（可能要用到，但是也可能不用到，但是没有preload优先级高，空闲的时候下载下来就行了，要用的时候直接从缓存里取）：`<link rel="prefetch" href="night_sky.jpg"/>`
       1. 比如一个带图片背景的隐藏组件（display:none），点击某个按钮后显示组件（display:block），如果不预取/预加载，则会直接发出一次请求，等待请求完成才会显示图片，从而留白影响用户体验；但是如果使用预取prefetch，则会在空闲的时候下载该图片，当用户点击按钮则会直接使用prefetch缓存（请求状态码仍然是200，但是Size那一栏会显示Prefetch Cache）
24. get/post的区别：
    1. 本质都是TCP连接，仅仅是格式不同，只是一个数据放在URL里，一个放在request body里
    2. GET请求在URL中传送的参数不能超过2048个字符，而POST没有限制
    3. GET在浏览器刷新/回退时是无害的，而POST会再次提交请求
    4. GET数据明文可见，传输安全性差，敏感信息不要用GET
    5. GET可以被缓存也可以被收藏，POST不行
    6. GET 是将数据中的hearder 和 data 一起发送给服务端，返回200code ；POST 是先将hearder发给服务器返回100，再发送data给到服务器，返回200（http状态码100：服务器已收到请求第一部分，等待其余部分；200：服务器已成功处理请求）
    7. GET编码格式只能用ASCII码，POST无限制
25. 网络相关、http、https
    1. 分层模型：应用层、传输层、网络层、数据链路层、物理层
    2. http和https的区别
       1. http数据是明文的，https加密过，数据变成了乱码，安全性更高。https就是在http基础上添加了TLS/SSL加密（TLS是新一代的加密方式，SSL是旧的加密方式大部分现在的浏览器都不支持，但是SSL名气更大，是TLS的前身，可以看成同一种加密即SSL加密）
       2. https需要申请证书用以验证网站的身份
       3. http更快
       4. http端口80，https端口443
       5. https需要建立安全连接，**建立安全连接阶段使用的是非对称加密**；得到会话密钥后后续通信都使用会话密钥进行加密（即**对称加密**），这个会话密钥只会在当前会话中生效
       6. 对称/非对称两种加密方式：
          1. 使用相同的密钥进行加密、解密，这叫对称加密
          2. 非对称加密：
             1. **两端都生成自己的公钥、私钥**：服务器使用随机数生成自己的公钥、私钥，客户端同样使用随机数生成自己的公钥、私钥，这两个随机数基本不可能一致，所以服务器和客户端两边产生的公钥/私钥一定会不同；
             2. **公钥任何人都可以拿到**，甚至黑客也能拿到，而**私钥必须牢牢掌握在各自的手中**
             3. **公钥加密的东西，只能够被相对应的私钥解密**，所以中间有黑客截取到了请求，没有私钥也无法解密
             4. 客户端需要和服务器加密时，首先需要得到服务器的公钥，将文件用公钥加密传给服务器，这个加密后的文件只有服务器的私钥可以读取
26. 常见的请求头：
    1. cookie
    2. host：指定请求的目标服务器的主机名和端口号
    3. user-agent
    4. Referer：引荐人（服务器：“你从哪知道我们？”），**Referer是一个禁止修改的请求头，无法通过JS进行操作修改**，强行添加这个头会报错，此外还有一些其它的请求头比如Host、Connection、Cookie、Date等等，都是禁止修改的请求头。用户在地址栏输入网址，或者选中浏览器书签，就不发送Referer字段；用户点击网页链接、发送表单、加载JS和图片等静态资源，那么浏览器会自动将当前网址作为Referer字段，放在 HTTP 请求的头信息发送（可以告诉目标网站，我做了好事，给你引流来了亲亲）。这个可以用来作为CRFS的一种防御手段，后台验证请求的Referer，不是同域名则当作攻击请求而忽略，不过浏览器本身可以修改Referer头（尤其是老浏览器），从这点来看，本质也不是绝对安全，但是不可否认是一种手段。
    5. Connection：是否使用持续连接，在现在主流的http1.1中值默认为Keep-Alive持久连接，意味着对同一个服务器的请求可以继续在该连接上完成，而在http1.0中是非持久的（值为close），所以http1.0每进行一次请求都要重新连接
    6. Get/Post:请求类型
    7. Content-Type：发送给后端的数据格式（比如application/json）
    8. Cache-Control：缓存控制
27. 常见的响应头：
    1. Access-Control-Allow-Credentials：允许cookie
    2. Access-Control-Allow-Origin：允许跨域
    3. Set-Cookie：设置与页面关联的Cookie
    4. Server：服务器名称
    5. Content-Type：表示后面的文档属于什么MIME类型
    6. Expires：文档过期时间
    7. Refresh：表示浏览器应该在多少时间之后刷新文档，以秒计
28. http缓存
    1. **强缓存**（cache-control、expires**响应头**控制，Cache-Control优先级更高）：访问某个资源时浏览器有缓存副本且有效，则直接取缓存，不向服务器发起请求。
       1. **`expires`**：过期时间（比如5月31日0点），未过期则直接从缓存中取数据，也就是我们说的强缓存，属于http1.0，优先级低于Cache-Control的max-age
       2. **`cache-control`**：属于http1.1
          1. 值为no-cache时表示跳过强缓存走协商缓存，每次请求时需要检查新鲜度，如果缓存是最新的，服务器返回304，表示读取缓存，否则服务器返回最新的资源并且返回状态码200，也就是我们说的协商缓存；
          2. 值为no-store则表示完全不走缓存，直接从服务器获取资源；
          3. 值为max-age=3，则资源被缓存3秒，请求之后3秒内资源缓存有效，超过3秒缓存被清除，需要重新请求
    2. **协商缓存**（两对：**Etag/If-None-Match**、**Last-Modified/If-Modified-Since**）：访问某个资源时，浏览器缓存在但是失效，浏览器会发起请求询问缓存是否可以继续使用，可以继续使用则返回304，该资源发生了变化了则返回新的资源，其状态码200
       1. **Etag：响应头**，表示资源的唯一标识符，服务器生成，优先级比Last-Modified高
       2. **If-None-Match：请求头**，浏览器再次发起请求时发送该http头，值为Etag，服务器会对浏览器发送的Etag与自身的Etag进行对比，两者相同返回304，否则返回数据和200状态码
       3. **Last-Modified**：响应头：资源最后一次修改时间
       4. **If-Modified-Since**：请求头：浏览器再次请求资源会带上该http头，值为浏览器保存的Last-Modified值，服务器会将其与自身的Last-Modified做对比，如果不一致，会返回新的内容以及新的Last-Modified，如果一致，返回304
    3. 一般来说，html使用协商缓存，js、css等资源使用强缓存，但是每次修改代码后，需要对资源使用唯一指纹来命名，比如开启filenameHashing选项，保证每次修改后能够获取到资源的唯一引用
    4. 用于检测缓存新鲜度的http头（强缓存）：
       1. expires：过期时间（比如5月31日0点），未过期则直接从缓存中取数据，也就是我们说的强缓存
       2. cache-control：
          1. 值为no-cache时表示跳过强缓存走协商缓存，但是每次请求时需要检查新鲜度，如果缓存是最新的，服务器返回304，表示读取缓存，否则服务器返回最新的资源并且返回状态码200，也就是我们说的协商缓存；
          2. 值为no-store则表示完全不走缓存，直接从服务器获取资源；
          3. 值为max-age=3，则资源被缓存3秒，请求之后3秒内资源缓存有效，超过3秒缓存被清除，需要重新请求
       3. pragma：值只能是no-cache，设定此值就会规定浏览器不使用缓存
    5. 用于校验缓存的值的http头（协商缓存）
       1. Etag：响应头，表示资源的唯一标识符，服务器生成，优先级比Last-Modified高
       2. If-None-Match：请求头，浏览器再次发起请求时发送该http头，值为Etag，服务器会对浏览器发送的Etag与自身的Etag进行对比，两者相同返回304，否则返回数据和200状态码
       3. Last-Modified：响应头：资源最后一次修改时间
       4. If-Modified-Since：请求头：浏览器再次请求资源会带上该http头，值为浏览器保存的Last-Modified值，服务器会将其与自身的Last-Modified做对比，如果不一致，会返回新的内容以及新的Last-Modified，如果一致，返回304
29. 区分连接 和 请求
    1. TCP连接：客户端和服务器通信需要首先建立一个“通道”，这个通道就是一个TCP连接：建立连接（三次握手） → 发送、接收请求 → 断开连接（四次挥手）。详见<https://www.cnblogs.com/onesea/p/13053697.html>
       1. 三次握手 —— 建立连接
           1. 本地：**请求建立连接**
           2. 服务器：针对客户端的SYN的**确认**应答并请求建立连接
           3. 本地：针对服务器端的SYN的**确认**应答
       2. 四次挥手 —— 断开连接
          1. 本地：请求断开连接
          2. 服务器：针对客户端的FIN的确认应答，准备关闭连接，但此时可能还有一些请求未完成，因此还会等一会
          3. 服务器：请求断开连接，告诉客户端已经可以关闭了
          4. 针对服务器端的PIN的确认应答，成功关闭连接
    2. 请求：客户端向服务端发送一个请求以获取服务、响应，比如网页中获取JS、CSS以及图片资源，请求需要通过事先建立的TCP连接来发送和接收。http请求由三个部分组成：
       1. 请求行：`POST /index.html HTTP/1.1`，请求方式、URL、协议版本
       2. 请求头：报文header
       3. 请求体：即手动发送的数据
30. chrome查看当前请求的协议：控制台 → network → 请求列表顶部标题tab（包括Name/Status/Size等）右键单击 → Protocol，如果protocol是h2则表示该请求是http2
31. http1.1和http2.0
    1. 队头阻塞 和 **多路复用**：
       1. http1.1中一个TCP连接建立后，**同一连接同一时间只能发送一个请求**，也就是说一个请求需要独占一个TCP连接，从而容易导致队头**阻塞**，那么如果需要并发请求，浏览器和服务器之间会**建立多个TCP连接，来支持多个并发请求**，但是在同一时间内对同一个域名的TCP连接有数量限制（比如Chrome4+允许建立6个连接）。因此一些站点会有多个静态资源 CDN 域名，目的就是变相的解决浏览器针对同一域名的请求限制阻塞问题
       2. http2.0使用了多路复用技术，**对于一个TCP连接，可以同时并发多个请求**（比如同时请求100张图片）
    2. **二进制分帧**：
       1. HTTP/2 会**将所有传输的信息分割为更小的消息和帧**（frame）,并对它们采用二进制格式的编码 ，其中 HTTP1.x 的首部信息会被封装到头帧（HEADER frame），而相应的请求体则封装到数据帧（DATA frame）里面，又因为**http2.0的一个TCP连接可以承载任意数量的双向数据流**，因此同时发送更多更小的“帧”比一次发送更少更大的文本格式更快
    3. **服务器主动推送资源**（http2.0中唯一一个需要开发者自己配制的功能）
       1. 在http1.1中，请求一个页面通常流程是：请求HTML页面 → 解析页面 → 请求CSS → 请求JS...，也就是说对任意一个需要使用的资源，都需要客户端首先发起一个请求才能收到服务器的推送
       2. 在http2.0中，客户端请求HTML页面时，服务端通过配置可以直接向客户端额外推送CSS文件，从而减小请求次数。比如Nginx中可以做如下配置：

            ```conf
            location /test.html { 
                http2_push /test.css; 
            }
            ```

    4. **首部压缩**：
       1. http1.1虽然也能使用GZip压缩，但是请求头仍然是纯文本，请求多的时候压力越大，http2特定的压缩程序HPACK可以压缩头帧，减小请求头的大小
    5. 头部优先级和依赖：
       1. HTTP/2.0 可以设置请求头部的优先级和依赖关系，以确定服务器响应的顺序
32. http2.0缺点：HTTP/2 是基于 TCP 协议来传输数据的，TCP 是字节流协议，TCP 层必须保证收到的字节数据是完整且连续的，这样内核才会将缓冲区里的数据返回给 HTTP 应用，那么当「前 1 个字节数据」没有到达时，后收到的字节数据只能存放在内核缓冲区里，只有等到这 1 个字节数据到达时，HTTP/2 应用层才能从内核中拿到数据，这就是 HTTP/2 队头阻塞问题。有没有什么解决方案呢？既然是 TCP 协议自身的问题，那干脆放弃 TCP 协议，转而使用 UDP 协议作为传输层协议，这个大胆的决定， HTTP/3 协议做了！
33. TCP(Transmission Control Protocol，传输控制协议) UDP(User Datagram Protocol，用户数据报协议)
    1. 是否面向连接? TCP是面向连接的。即发送数据之前需要建立一条完整的连接；比如打电话要先拨号建立连接； UDP是面向无连接的。即发送数据之前不需要建立连接；例如发送邮件，ip电话
    2. 是否提供可靠服务? TCP提供可靠的服务，保证数据的连续和完整性；UDP尽最大的努力交付，即不保证可靠的交付
    3. 字节流还是报文? **TCP是字节流协议，有序传输，传输的不一定是个完整的数据单元；UDP面向报文，无序传输，传输的是一个完整的数据单元，可能会丢包、乱序**
    4. 一对一还是一对多? TCP只能是一对一，而UDP支持一对一、一对多、多对一或者多对多
    5. 首部开销： TCP 20字节，UDP 8字节
    6. 通信信道： TCP使用全双工的可靠信道 UDP使用不可靠的信道
34. 前端常用的设计模式：
    1. 工厂模式
       1. 就像一个工厂一样，根据提供的原材料，工厂方法函数产生不同的产品实例
       2. 比如，提供（游客、已登录未订阅、已订阅）三种用户身份，工厂方法进行switch判断，产生对应按钮的功能为（跳转登录、付费购买、展示涨跌幅）
    2. 单例模式：闭包保存第一次创建的实例并返回
    3. 代理模式：ES6的proxy
    4. 观察者模式
       1. vue源码的Dep（发布者）和Watcher（订阅者）
    5. 模块模式、策略模式等等
35. babel
    1. babel转化ES6语法为ES5语法，内部使用将ES6语法转成ES6的AST→ES6的AST转为ES5的AST→ES5的AST转为ES5语法
    2. babel默认只转码语法，不会转码新的API，需要使用babel-polyfill运行新的API（Map、Reflect、Set、Symbol等等）
36. 混合应用开发：jsbridge
    1. jsbridge实现web端和native之间互相通信的桥梁，比如同花顺许多app前端页面也会用到jsbridge，比如调用协议打开微信进行分享，js获取用户是否安装微信的状态并且执行回调等等，这里面的“协议”就是native和web段通信的一种机制
    2. 以JavaScript引擎或Webview容器作为媒介，通过协定协议进行通信，实现Native端和Web端双向通信的一种机制
    3. Web调用Native端主要有两种方式
       1. 拦截Webview请求的URL Schema（比如同花顺<http://eqhexin/changeUser吊起登录框）>
       2. 直接向Webview中注入JS API，js中会将这个api当作全局变量使用（比如同花顺的一些协议，通过全局调用的方式直接使用）
    4. Native调用Web端比较简单，直接将拼接的js代码传入webview中执行即可
37. js是**静态作用域**
    1. 静态作用域：执行之前就确定了它的作用域
    2. 动态作用域：作用域是函数执行的时候才决定的

    ```javascript
    function foo() {
        console.log(a);
    }
    function bar() {
        let a = 3;
        foo();
    }
    let a = 2;
    bar();
    //这里因为foo定义时是在全局作用域，因此bar中调用foo输出的a是全局的a而不是bar中的a
    ```

38. JS是**动态类型**（没有编译阶段，执行时才会类型报错），TS是静态类型（需要编译成JS后运行，编译时就会报错）
39. instancof和typeof区别
    * typeof只用于检测**基础类型（null除外，返回object） + 函数类型**
    * instanceof用于检测**实例**对象
    * `obj instanceof constructor`：判断constructor.prototype是否出现在实例对象obj的原型链上，但是这里的obj必须是一个实例对象，如果不是实例对象而是一个基本数据类型，那么即便constructor.prototype出现在原型链上，也会返回false，这是因为基本类型（没有属性、方法，除了null/undefined）被被包装成了一个对象（**包装类型**）从而具有了对象的属性、方法

    ```javascript
    console.log(typeof NaN); //number

    let str = "";
    let str_obj = new String("");
    console.log(str.__proto__ === String.prototype);//true ，这里本来str是基本类型，没有原型/属性，但是包装器将其转化为一个String对象从而具有原型/属性
    console.log(str instanceof String);//false ，只有实例对象才能成立，因此这里即便String再str的原型链上（因为实际上判断的是String在str包装对象的原型链上），也仍然返回false
    console.log(str_obj.__proto__ === String.prototype);//true ，引用类型有__proto__，自然是成立的
    console.log(str_obj instanceof String)//true ，实例对象，因此也成立
    ```

40. px、rem、em的区别？
    1. px表示css中的像素，px是固定长度单位，不随其它元素的变化而变化，不能适应浏览器缩放时产生的变化，因此一般不用于响应式网站
    2. em是根据自身的font-size来计算尺寸的，网上通传的根据父元素的font-size来计算是因为元素本身没有设置font-size从而导致继承了父元素的font-size产生的“相对父元素的fontsize进行计算”假象
    3. rem表示root em相对于html的font-size进行尺寸定制，**任意浏览器的默认字体都是16px**
    4. 为了方便计算，时常在`<html>`元素中设置font-size:值为62.5%,也就是16*62.5%=10px，那么后面的css里面的rem值就以1rem =10px来换算
41. 安全相关
    1. 登录的用户名密码加密
    2. token
       1. token（令牌）时服务端生成的一个字符串，为客户端请求的一个标识。用户登录成功之后服务端会返回一个token给前端，前端需要把token暂存在本地（cookie或者localStorage等），每次**请求时在header中带上token就无需再带上账号和密码**
       2. 简单的token是由 userid + 时间戳 + sign签名等使用hash算法压缩成的十六进制字符串
    3. XSS跨站点脚本攻击——黑客在输入框注入JS脚本从而导致加载页面时执行JS脚本（对输入内容进行编码、过滤、特殊字符转义等等）
    4. CFRS跨站点请求伪造，正常网页是登陆状态，恶意页面通过注入脚本等方式在页面内伪造正常请求，并且会携带用户认证信息传送给服务端（或者恶意页面通过其他手段获取认证信息，在其页面内部带上该信息并发出请求），服务端以为是正常请求从而进行处理导致攻击成功：**script、image、iframe的src都不受同源策略的影响。所以我们可以借助这一特点，实现跨域src链接带上cookie。对于需要 POST 请求的端点，可以在加载页面时以编程方式触发 `<form>` 提交**
       1. 防御手段1：**服务端验证Refer访问来源**，设置必须同域才正常处理请求，但是某些浏览器可能会限制或修改Referer值，本质上来说不算绝对安全，只是一种手段
       2. 防御手段2：使用token，每次请求时都**手动添加token**，并且token及时更新
42. This指针
    1. 非箭头函数独立调用（即函数前面没有前缀）的时候this一定指向window；
    2. 使用function定义的函数，this的指向调用者；
    3. 箭头函数中的this指向是固定不变的，一直指向的是定义函数的环境（外层的非箭头函数），如果箭头函数外面没有包裹的非箭头函数，则恒指向window（**外面包裹着的是对象同样this也会指向window，包裹的一定要是函数**）
    4. 自执行匿名函数的this恒指向window，自执行箭头函数的this指向其环境的this
43. 闭包
    1. 闭包可以用来实现许多有用的功能，例如：
       1. 封装变量和方法：可以使用闭包将变量和方法封装在一个私有作用域内，防止其被其他代码意外访问或修改。
       2. 模块化开发：可以使用闭包将模块中的变量和方法私有化，避免全局作用域污染，同时暴露需要对外使用的接口。
       3. 缓存函数值：可以使用闭包来缓存某些函数的计算结果，避免重复计算浪费时间。
       4. 实现回调函数：可以使用闭包实现回调函数，将一些数据和逻辑传递给回调函数使用。
       5. 保存状态：可以使用闭包保存函数执行时的状态，以便后续使用。
       6. 总的来说，闭包是一种强大的编程工具，可以用来解决许多实际问题。但是也需要注意闭包的缺点，如内存泄漏、性能问题等。
    2. 只要闭包被创建并**持续引用着函数外部的变量**，就可能导致内存泄漏。比如闭包内使用 setInterval/setTimeout 和事件监听器是常见的比较容易引起内存泄漏的场景，因此页面中使用完之后要手动取消计时器、监听器
44. 跨域：请求的协议/域名/端口号与当前url页面不同即为跨域
    1. CORS（Cross-origin resource sharing）
       1. 简单请求：
          1. 请求方式为GET、POST、HEAD（与GET请求类似，但HEAD请求只获取目标资源的头部信息而不获取响应体）
          2. 浏览器会在头信息中添加origin字段；
          3. Access-Control-Allow-Origin：*或者具体同源域名
          4. Access-Control-Allow-Credentials：允许客户端请求携带cookie，为true的时候origin字段不可以为*，必须指定具体的同源域名，同时前端AJAX请求中打开withCredentials属性；
       2. 非简单请求：
          1. 请求方式是PUT、DELETE，或者Content-Type字段的类型是application/json；请求之前会先发送OPTIONS预检请求；
          2. 预检请求未通过则不会返回Access-Control-Allow-Origin等字段，通过才会返回这些字段；
       3. 与JSONP的区别：CORS比JSONP更强大，JSONP只支持get请求，CORS支持所有类型请求，但是JSONP支持老式浏览器，以及可以向不支持CORS的网站请求数据。
    2. JSONP：前端传一个回调函数给后端，后端补充参数，再返回给前端，前端执行该函数，利用的是**script标签**的src可以进行跨域请求，并且请求的数据会立马被浏览器当作javascript语句去执行，从而只要后端返回的是一个符合JS语法的函数即可。缺点：**只能Get请求**
    3. 代理：比如webpack的`devServer.proxy`对象、fiddler、nginx
    4. websocket：双向通信，支持跨域
45. Babel转义es6语法成为es5
46. 浏览器缓存：Cookie、localStorage、sessionStorage
    1. cookie：
       1. 后端需要写cookie时会在返回头里面加入set-cookie字段（多条cookie有多个set-cookie）
       2. 一般由服务器生成，发送请求会携带在http头中，可以设置失效时间，数据大小不超过4KB
    2. localStorage：
       1. 需要保存数据时手动写入浏览器缓存，持久保存，除非手动清除，不超过5MB
    3. sessionStorage：
       1. 使用方法、数据大小同localStorage，但是只有（1）当前浏览器的（2）当前标签页能访问，即使只是跨标签也不能访问，并且关闭页面/浏览器数据清除
47. 浏览器工作原理：
    1. 导航，
       1. 输入一个url
       2. DNS解析（如果需要），根据域名定位到服务器IP，具体如下：
          1. 浏览器缓存：查看浏览器缓存是否有该域名对应的IP地址；
          2. 操作系统缓存：如果在浏览器缓存中不包含这个记录，则会使系统调用操作系统，获取操作系统的记录(保存最近的DNS查询缓存)；
          3. 路由器缓存：如果上述两个步骤均不能成功获取DNS记录，继续搜索路由器缓存；
          4. ISP缓存：若上述均失败，继续向ISP搜索。
          5. 网络运营商、根域名服务器、顶级域名服务器···
          6. 保存结果到缓存
       3. 建立TCP连接，三次握手
       4. 如果是https，还需要TLS协商，使用非堆成加密建立安全连接
       5. 请求
    2. 响应
    3. 解析 —— 构建 DOM 和 CSSOM 的步骤
       1. **解析html生成DOM树：不会阻塞渲染**，假设 HTML 文件很大，一个往返时延(RTT，Round-Trip Time)只能得到一部分，浏览器得到这部分之后就会开始构建 DOM，并不会等到整个文档就位才开始渲染
       2. **解析css生成CSSOM树**：会阻塞渲染，遇到`<link>`标签时，浏览器会请求对应的 CSS 文件，当 CSS 文件就位时便开始解析它，CSSOM 则必须等到所有字节收到才开始构建，因此和DOM不一样，CSSOM会阻塞渲染，在谷歌控制台的performance选项卡中
          1. 如果是内联样式，CSSOM 构建包含在Parse HTML过程中
          2. 如果是外部样式，包含在Parse Stylesheet过程中
          3. 如果没有设置样式，使用User Agent Style，则包含在Parse HTML过程中
       3. DOM树和CSSOM树关联起来构成渲染树（RenderTree）
       4. 在以上过程中：
          1. DOM树的构建是不会阻塞渲染的，而CSSOM的构建会阻塞渲染，并且，如果在页面头部有未添加defer的script同步脚本，同样会直接执行JS并且阻塞解析、渲染，这是因为浏览器的GUI线程和JS引擎线程是互斥的，所以为了防止JS引起的渲染阻塞，应该在script中加上defer以延迟解析/执行；但是应该注意的是，**无论是否添加defer/async，`<script>`脚本都和样式表一样，是立即下载的，defer/async用额外的线程下载脚本下载过程不产生阻塞，但是不同值会影响它的解析/执行的时机**
          2. CSS、图片的下载不会阻塞解析，JS的下载会阻塞解析
    4. 渲染
       1. 解析步骤中创建的 **CSSOM 树和 DOM 树组合成一个渲染树**，浏览器按照渲染树进行布局（Layout）
       2. 最后一步通过绘制显示出整个页面。
    5. JS交互
48. 浏览器底层
    1. 浏览器渲染引擎（也成为浏览器内核，使用C++编写，比如webkit/Gecko）根据html文档的标签描述进行渲染
    2. Chrome的内核只有html渲染的部分来自于Webkit，其JS部分使用自创的V8引擎，Chrome使用WebKit Glue与Webkit衔接，使其在未来能够使用更好的渲染引擎
    3. V8为何高效
       1. 传统引擎：为运行中的对象建立一个属性字典，然后每次在脚本中存取对象属性的时候，就去查这个字典，查到了就直接存取，查不到就新建一个属性
       2. V8引擎：使用hidden class，在脚本中每次为对象添加一个新的属性的时候，就以上一个hidden class为父类，创建一个具有新属性的hidden class的子类，如此往复递归进行，以后再遇到相同对象的时候，直接把最终版本的hidden class子类拿来用就是了，不用维护一个属性字典，也不用重复创建。
    4. JS底层：JS不同于C++/Java等语言，这些语言执行前需要通篇编译成机器码，而JS不需要编译成中间码，可以直接在浏览器中运行
    5. JS底层运行机制：`https://www.ucloud.cn/yun/107350.html`
    6. 浏览器进程/线程
       1. 多进程浏览器，chrome浏览器包含了Browser Process（地址栏、书签栏、前进后退、底层网络请求和文件访问等）、Renderer Process（呈现页面）、Plugin Process（负责控制插件）、GPU Process等
       2. 某一渲染进程出问题不会影响其他进程；更为安全，在系统层面上限定了不同进程的权限
       3. 导航过程发生了什么：
          1. Tab外的工作主要有Browser Process负责，Browser Process进程包含多个线程：
             1. UI thread ： 控制浏览器上的按钮及输入框；
             2. network thread: 处理网络请求，从网上获取数据；
             3. storage thread: 控制文件等的访问；
          2. 步骤：
             1. 地址栏输入文字
             2. UI thread 需要判断用户输入的是 URL 还是 query
             3. 回车键：UI thread 通知 network thread 获取网页内容（DNS解析、TCP连接、发送数据、处理响应），并控制 tab 上的 spinner 展现，表示正在加载中
             4. 相应内容是Html，数据传给Renderer Process，是zip，数据传给下载管理器
             5. network thread 会通知 UI thread 数据已经准备好，UI thread 会查找到一个 Renderer Process 进行网页的渲染（构建 DOM、加载次级的资源、加载次级的资源、JS 的下载与执行、样式计算、获取布局、绘制各元素、合成帧）
             6. 渲染结束后Renderer Process发送信号到 Browser process， UI thread 会停止展示 tab 中的 spinner

## NodeJS

### 第一个nodejs应用

1. 一个最简单的nodejs例子

    ```javascript
    let http = require('http');

    http.createServer(function (request, response) {
        response.writeHead(200, { 'Content-Type': 'text/plain' });//状态码，其它头部
        response.end(`Hi, I'm content`);
    }).listen(8888);//监听8888端口
    console.log('Server running at http://127.0.0.1:8888/');
    ```

### Node.js REPL(交互式解释器)

1. 类似于linux shell以及浏览器的console控制台
2. 可以在命令行输入node来启动REPL
3. node的终端，可以输入js命令并执行
4. 可使用下划线(_)获取上一个表达式的运算结果

### Nodejs事件循环、events.EventEmitter

1. Node.js使用观察者模式，单线程类似进入一个while(true)的事件循环，直到没有事件观察者退出，每个异步事件都生成一个事件观察者，如果有事件发生就调用该回调函数
2. events模块只有一个对象`EventEmitter`，需要实例化才能使用，实例包含多个[事件方法](https://www.runoob.com/nodejs/nodejs-event.html)，包括on、emit（这两个类似于Vue2的on和emit，但是Vue3中on被取消了）、`addListener(event, listener)`、`once(event, listener)`、`removeListener(event, listener)`、`removeAllListeners([event])`等等
3. 一个事件可以注册多个监听器，语法如下：

    ```javascript
    const events = require('events');
    const event_emitter = new events.EventEmitter();
    const listener1 = arg => console.log(`监听器 1 收到参数${arg}`);
    const listener2 = arg => console.log(`监听器 2 收到参数${arg}`);
    const listener3 = arg => console.log(`监听器 3 收到参数${arg}`);
    event_emitter.on('my_event', listener1);
    event_emitter.on('my_event', listener2);
    event_emitter.addListener('my_event', listener3);

    setTimeout(() => {
        event_emitter.emit('my_event', 'blabla');
        event_emitter.removeAllListeners('my_event');
        event_emitter.emit('my_event', '嘿嘿');//所有监听器都已经被移除了所以不会再被触发
    }, 2000);
    ```

### 缓冲区：Buffer

1. JavaScript 语言自身只有字符串数据类型，没有二进制数据类型。但在处理像TCP流或文件流时，必须使用到二进制数据。因此在 Node.js内部定义了一个 Buffer 类，该类用来创建一个专门存放二进制数据的缓存区。

### 流：Stream

1. Stream是一个抽象接口，Node 中有很多对象实现了这个接口。例如，对http 服务器发起请求的request 对象就是一个 Stream，还有stdout（标准输出）
2. 所有的 Stream 对象都是 EventEmitter 的实例。常用的事件有：
   1. data - 当有数据可读时触发。
   2. end - 没有更多的数据可读时触发。
   3. error - 在接收和写入过程中发生错误时触发。
   4. finish - 所有数据已被写入到底层系统时触发。

### Nodejs模块系统

1. 代码` require('./hello') `引入了当前目录下的 hello.js 文件，` require('hello') `没有`./`表示引入 node_modules 下的hello.js或者hello/index.js
2. Node.js 提供了 exports 和 require 两个**对象**，module.exports表示模块暴露的对象，require('模块路径')获取该模块暴露的对象，其中模块路径中的模块名的.js后缀可以省略，因为nodejs默认的后缀就是.js

    ```javascript
    //a.js
    const hello = { a: 1 };
    module.exports = hello;

    //b.js
    const hello = require('./a');
    console.log(hello.a);
    ```

3. 除了module.exports用于暴露对象外，也可以用exports.method_name = function(){} 来暴露一个方法或者属性，但是不要同时使用，因为exports中的方法仅仅是module.exports的一个引用，对于暴露同一个方法method，会在exports上添加一个同名的方法并进行引用赋值（exports.method = method），而module.exports会把整个原方法直接复制给module.exports（module.exports = method）

### NodeJS路由

1. 代码示例

    ```javascript
    // server.js
    var http = require("http");
    var url = require("url");

    function start(route) {
        function onRequest(request, response) {
            const full_path = `${request.connection.encrypted ? 'https' : 'http'}://${request.headers.host}${request.url}`;
            let pathname = new url.URL(full_path).pathname;
            console.log("Request for " + pathname + " received.");

            route(pathname);

            response.writeHead(200, { "Content-Type": "text/plain;charset=utf-8" });
            response.write("滴滴滴滴");
            response.end();
        }
        http.createServer(onRequest).listen(8888);
        console.log("Server has started at http://127.0.0.1:8888");
    }

    exports.start = start;

    // router.js
    function route(pathname) {
        console.log("About to route a request for " + pathname);
    }

    exports.route = route;

    //index.js
    let server = require('./server');
    let router = require('./router');

    server.start(router.route);
    ```

### 全局变量

#### 全局对象

1. 在浏览器 JavaScript 中，通常 window 是全局对象， 而 Node.js 中的全局对象是 global

#### __filename

__filename 表示当前正在执行的脚本的文件名。它将输出文件所在位置的绝对路径，且和命令行参数所指定的文件名不一定相同。 如果在模块中，返回的值是模块文件的路径。

#### __dirname

__dirname 表示当前执行脚本所在的目录。

#### process
>
>process 是一个全局变量，即 global 对象的属性。它用于描述当前Node.js 进程状态的对象，提供了一个与操作系统的简单接口

1. 用法示例：

    ```javascript
    process.on('exit', function(code) {

        // 以下代码永远不会执行
        setTimeout(function() {
            console.log("该代码不会执行");
        }, 0);
        
        console.log('退出码为:', code);
    });
    console.log("程序执行结束");
    ```

2. 可以监听的事件有：exit,beforeExit,uncaughtException,signal事件，完整事件、退出状态码、属性、方法[详见](https://www.runoob.com/nodejs/nodejs-global-object.html)
3. `process.nextTick(callback)`：一旦当前事件循环结束，立即调用回调函数（将函数的执行优先级提升到当前微任务队列中的最高，在微任务队列中的所有其他微任务之前执行(当前执行栈结束之后、下一个事件循环之前执行)）

### 常用工具库：util

### 文件系统：fs

1. 常用的几个文件操作

    ```javascript
    const fs = require('fs');
    //下面的4个读写操作属于高级的读写，不需要手动打开/关闭文件，无需手动管理文件描述符或偏移量，直接读/写全部内容
    fs.readFile('input.txt', function (err, data) { })//异步读
    fs.writeFile('input.txt', '我是通 过fs.writeFile 写入文件的内容', function (err) { });//异步写
    var data = fs.readFileSync('input.txt');//同步的readFile，会阻塞
    fs.writeFileSync(filename, data, options)//同步的writeFile，会阻塞

    fs.open('input.txt', 'r+', function (err, fd) { });//异步打开文件
    fs.close(fd, function(err){ });//异步关闭文件
    fs.stat('input.txt', function (err, stats) { });//异步获取文件信息

    //下面2个读写需要手动打开/关闭文件，并且需要指定要读/写的数据长度和偏移量
    fs.read(fd, buffer, offset, length, position, callback)
    fs.write(fd, buffer, offset, length, position, callback)
    ```

### 处理GET/POST请求

1. GET/POST请求示例：

    ```javascript
    var http = require('http');
    //var querystring = require('querystring');
    var util = require('util');
    var url = require('url');
    
    http.createServer(function(req, res){
        //如果是GET请求
        // console.log(new url.URL(full_path).searchParams);//get请求后的查询字符串对象

        // 定义了一个post变量，用于暂存请求体的信息
        var post = '';     
    
        // 通过req的data事件监听函数，每当接受到请求体的数据，就累加到post变量中
        req.on('data', function(chunk){    
            post += chunk;
        });
    
        // 在end事件触发后，通过querystring.parse将post解析为真正的POST请求格式，然后向客户端返回。
        req.on('end', function(){    
            //post = querystring.parse(post);

            //请求接收完毕，接着处理数据并且返回客户端
            // 设置响应头部信息及编码
            res.writeHead(200, {'Content-Type': 'text/html; charset=utf8'});
            res.write(`<h1>我是标题</h1>`);
            res.write(util.inspect(post));//util.inspect转化成字符串
            res.end();
        });
    }).listen(3000);
    ```

2. post请求监听data事件来获取前端传入参数，GET请求使用URL解析传入的参数
3. Content-Type表明服务器要传递的数据类型：

    ```txt
    text/plain 　　　　　　　　文本类型
    text/css  　　　　　　　　 css类型
    text/html 　　 　　　　　　html类型
    application/x-javascript 　　 js类型
    application/json　　　　　    json类型
    image/png jpg gif　　　　　   image/*
    (/.+.(png|jpg|gif)$/.test(pathname))    匹配到图片
    ```

### 常用工具模块

1. os模块，`var os = require("os")`，提供基本系统操作函数，如` os.type() `返回操作系统名，os模块各API及用法详见[这里](https://www.runoob.com/nodejs/nodejs-os-module.html)
2. path模块，如常用的`path.join(dir1,dir2)`连接两个路径；`path.resolve([from ...], to)`解析from+to为绝对路径；详见[这里](https://www.runoob.com/nodejs/nodejs-path-module.html)
3. net模块，提供了一些用于底层的网络通信的小工具，包含了创建服务器/客户端的方法。详见[这里](https://www.runoob.com/nodejs/nodejs-net-module.html)
4. [dns模块](https://www.runoob.com/nodejs/nodejs-dns-module.html)
5. [domain模块](https://www.runoob.com/nodejs/nodejs-domain-module.html)

### Nodejs的Web模块

1. 创建一个服务器简例：

    ```javascript
    var http = require('http');
    var fs = require('fs');
    var url = require('url'); 
    
    // 创建服务器
    http.createServer( function (request, response) {  
    // 解析请求，包括文件名
    var pathname = url.parse(request.url).pathname;
    
    // 输出请求的文件名
    console.log("Request for " + pathname + " received.");
    
    // 从文件系统中读取请求的文件内容
    fs.readFile(pathname.substr(1), function (err, data) {
        if (err) {
            console.log(err);
            // HTTP 状态码: 404 : NOT FOUND
            response.writeHead(404, {'Content-Type': 'text/html'});
        }else{             
            response.writeHead(200, {'Content-Type': 'text/html'});    
            // 响应文件内容
            response.write(data.toString());        
        }
        //  发送响应数据，比如前端请求的是http://localhost:8888/index.html 则会返回文件index.html
        response.end();
    });   
    }).listen(8080);
    
    // 控制台会输出以下信息
    console.log('Server running at http://127.0.0.1:8080/');
    ```

2. 创建一个客户端简例

    ```javascript
    var http = require('http');
    
    // 用于请求的选项
    var options = {
        host: 'localhost',
        port: '8080',
        path: '/index.html'  
    };
    
    // 处理响应的回调函数
    var callback = function(response){
        // 不断更新数据
        var body = '';
        response.on('data', function(data) {
            body += data;
        });
        
        response.on('end', function() {
            // 数据接收完成
            console.log(body);
        });
    }
    // 向服务端发送请求
    var req = http.request(options, callback);
    req.end();
    ```

### Nodejs的Express框架

1. Express 是一个简洁而灵活的 node.js Web应用框架，可以用于创建各种Web应用并提供了丰富的http工具
   1. 响应http请求
   2. 定义路由表执行不同的http请求动作
   3. 可以通过向模板传递参数来动态渲染 HTML 页面
2. 安装

    ```bash
    npm install express --save
    npm install body-parser cookie-parser multer --save
    ```

    其中：
    1. body-parser - node.js 中间件，用于处理 JSON, Raw, Text 和 URL 编码的数据。
    2. cookie-parser - 这就是一个解析Cookie的工具。通过req.cookies可以取到传过来的cookie，并把它们转成对象。
    3. multer - node.js 中间件，用于处理 enctype="multipart/form-data"（设置表单的MIME编码）的表单数据。
3. **使用`app.get('/path', callback(req, res))`来处理get请求，`app.post()`处理post请求；get请求通过req.query来获取入参，post请求通过req.body来获取入参**
4. 使用范例：

    ```javascript
    let express = require('express');
    let cookie_parser = require('cookie-parser');
    let util = require('util');
    let app = new express();
    app.use(cookie_parser());
    function start_express() {
        app.get('/', function (req, res) {
            res.send(`传入的cookies是：${util.inspect(req.cookies)}`);
        });
        app.get('/md', function (req, res) {//请求md路径，获取参数
            let file_name = req.query.md;//get请求通过req.query来获取入参
            res.set({
                'Content-Type': 'text/html; charset=utf8',
                'Access-Control-Allow-Origin': '*'
            });
            res.sendFile(__dirname + "/md/" + file_name);
        });
        app.post('/md', function (req, res) {
            let file_name = req.body.md;//post请求通过req.body来获取入参
            res.set({
                'Content-Type': 'text/html; charset=utf8',
                'Access-Control-Allow-Origin': '*'
            });
            res.sendFile(__dirname + "/md/" + file_name);
        })

        const server = app.listen(9999, 'localhost', function () {
            console.log(`应用实例2运行地址为：http://${server.address().address}:${server.address().port}`)
        });
    }
    ```

5. 更多express API和用法点击[这里](https://www.runoob.com/nodejs/nodejs-express-framework.html)

## webpack

### webpack概念

[官网](https://webpack.docschina.org)

1. webpack只是一个打包工具：
   1. 把项目中的**模块（一切文件皆模块）**——css、less、es6、图片打包成**css、js（es5语法）、图片**，也就是打包成浏览器能使用的**静态资源**，同时可以把这些模块压缩、合并处理成一个或多个js文件包（也就是所谓的bundle），并且会自动导入这些打包后的文件。
   2. 并且可以把整个项目进行打包以及部署自己的服务
   3. 通过Loader转换文件，通过Plugin注入钩子
2. 常见的打包/构建工具对比
   1. Npm Script
      1. `npm run xxx`，xxx可以是package.json中的scripts字段中的某个属性，比如，以下配置命令行运行`npm run my_dev`会执行`node dev.js`这个命令

            ```JSON
            "scripts": {
                "test": "echo \"Error: no test specified\" && exit 1",
                "dev": "webpack-dev-server",
                "build": "webpack",
                "my_dev": "node dev.js"
            }
            ```

      2. Npm Script优点是内置，无需额外安装，缺点是功能太简单
   2. Gulp：集成度不高，需要写很多配置后才能用，无法做到开箱即用
   3. Webpack：
      1. 专注于处理模块化项目，开箱即用
      2. 通过Plugin扩展，完整好用又不失灵活
      3. 使用场景不仅限于web开发
      4. 社区庞大活跃，紧跟时代发展
      5. 良好的开发体验
3. 安装
   1. 先安装好node、npm，以为webpack基于node
   2. 安装webpack、webpack-cli ：

        ```shell
        cnpm i webpack webpack-cli -g
        //-S 安装到线上环境，即--save
        //-D 安装到开发环境，即--save-dev，发布到线上的时候这个包不需要，比如webpack/webpack-cli，上线时我们只需要它们打包后的代码即可，这两个包本身就不被需要了
        //-g 安装到全局，即--global
        ```

### webpack基本配置

即JS文件会通过 `http://[devServer.host]:[devServer.port]/[output.publicPath]/[output.filename]` 进行访问，假如设置了output.publicPath为"/assets"那么，产生的js包会通过./assets/index3dsbd87d.js这样的方式引入而非./index3dsbd87d.js

#### 入口

#### 出口

#### loader（加载器）

1. 作用：转码 —— ES6转ES5，Less转Css等，webpack只能识别JS和JSON，那需要将非JS的模块转变为JS
2. **[loader大全](https://webpack.docschina.org/loaders/)**
3. loader特性：
   1. loader 支持链式传递。定义了**多个 loader 会逆序执行**（从数组最后一个loader开始转换直到第一个为止）。上一个转换结果回传给下一个loader，链中的最后一个 loader，返回 webpack 期望 JavaScript。
   2. 逆序是因为loader分为两种：normalLoader和pitchLoader，而大部分的loader都是normalLoader。在loader-runner库中，判断pitchLoader时的index是递增的，因此pitchLoader会顺序执行；而对于我们通常使用的normalLoader，index是递减的，因此会逆序执行
   3. loader 可以是同步的，也可以是异步的。
   4. loader 运行在 Node.js 中，并且能够执行任何 Node.js 能做到的操作。
   5. loader 可以通过 options 对象配置（仍然支持使用 query 参数来设置选项，但是这种方式已被废弃）。
   6. 除了常见的通过 package.json 的 main 来将一个 npm 模块导出为 loader，还可以在 module.rules 中使用 loader 字段直接引用一个模块。
   7. 插件(plugin)可以为 loader 带来更多特性。
   8. loader 能够产生额外的任意文件。
4. 常见的loader及其作用
   1. 文件处理类型：
      * `file-loader` 将**图片等静态文件发送到输出文件夹，并返回（相对）URL**
      * `url-loader` 像 file loader 一样工作，但如果文件小于限制，可以返回 base64
      * ref-loader 手动创建所有文件之间的依赖关系
      * `raw-loader` 加载文件原始内容（utf-8）
      * val-loader 将代码作为模块执行，并将 exports 转为 JS 代码
   2. 转译
      * `babel-loader` 加载 ES2015+ 代码，然后使用 Babel 转译为 ES5，安装方式`npm install -D babel-loader @babel/core @babel/preset-env`
      * `ts-loader` 或 awesome-typescript-loader 像 JavaScript 一样加载 TypeScript 2.0+
   3. 模板相关（编码模板为html）
      * `html-loader` 导出 HTML 为字符串，需要引用静态资源
      * `markdown-loader` 将 Markdown 转译为 HTML
      * markup-inline-loader 将内联的 SVG/MathML 文件转换为 HTML。在应用于图标字体，或将 CSS 动画应用于 SVG 时非常有用
   4. 样式相关
      * `style-loader` 将模块的导出作为内联样式添加到 DOM 的`<style>`标签中
      * `css-loader` 解析 CSS 文件后，使用 import 加载，并且返回 CSS 代码
      * `less-loader` 加载和转译 LESS 文件
      * sass-loader 加载和转译 SASS/SCSS 文件
      * postcss-loader 使用 PostCSS 加载和转译 CSS/SSS 文件
      * stylus-loader 加载和转译 Stylus 文件
   5. 代码检查和测试
      * mocha-loader 使用 mocha 测试（浏览器/NodeJS）
      * `eslint-loader` PreLoader，使用 ESLint 清理代码
      * `jshint-loader` PreLoader，使用 JSHint 清理代码
      * jscs-loader PreLoader，使用 JSCS 检查代码样式
      * coverjs-loader PreLoader，使用 CoverJS 确定测试覆盖率
   6. 前端框架相关
      * `vue-loader` 加载和转译 Vue 组件
      * polymer-loader 使用选择预处理器(preprocessor)处理，并且 require() 类似一等模块(first-class)的 Web 组件
      * angular2-template-loader 加载和转译 Angular 组件

#### plugins（插件）

1. 作用：其它的某些功能需求，比如自动生成html，每次打包前自动清理build文件夹，自动开启服务等等，可以通过第三方插件实现
2. **[插件大全](https://webpack.docschina.org/plugins/)**

#### 开启服务

```javascript
//webpack.config.js

//打包的配置信息
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');//复制静态文件到输出目录中
const CompressionWebpackPlugin = require('compression-webpack-plugin');//gzip压缩插件
// const webpack=require("webpack");//需要使用webpack内置插件的时候才引

module.exports = {
    //入口文件的路径，缺省该字段默认打包src/index.js，其余不会打包
    // entry: './src/index.js',//单入口，除此外还可以是数组、对象形式
    // entry: ['./src/index.js', './src/demo.js'],//数组，多个入口，打包到一个文件中
    entry: {//如果是对象形式，那么output出口必须是多出口（不指定则会在dist下生成aaa.js，bbb.js），不能只指定一个输出文件，比如指定为一个输出文件main.js会报错
        aaa: './src/index.js',
        bbb: './src/demo.js',//index.js中引入了demo，因此不需要重复引入，但是如果重复引入了，可以分离--如何分离（待解决）
    },

    //出口
    output: {// 默认输出到/dist/main.js，可以指定输出的文件夹以及JS文件名
        // filename: 'not_main.js',
        filename: '[name][chunkhash:8].js',//[name]单出口文件输出main.js，多出口文件名（此时entry必定是一个对象）和入口文件属性保持一致
        //[hash]：每次生成一个hash，只要有一个文件的内容改变了，所有的文件hash都会改变
        //[chunkhash]：只有内容改变的那个文件的hash会改变
        //[hash:8]：规定生成的hash值长度为8
        path: path.resolve(__dirname, 'dist'),//指定输出文件夹（通常手动指定为build），默认为dist
        clean: true,//可以代替clean-webpack-plugin在每次构建前删除dist目录
    },

    //loader，通常只需要安装到开发环境即可即 npm install --save-dev xxx
    module: {
        rules: [
            {
                test: /\.css$/,//正则：以.css结尾的文件，都是用下面的loader进行处理，以便能够正常引入，被webpack成功识别
                use: [//使用的loader，可以是每个loader的名字组成的字符串数组，也可以是一个对象数组
                    'style-loader',//导出的内容/样式直接添加到html中（也就是css全部放在JS中），但是实际开发过程中，css文件是独立的，全放在js中会导致JS过大，因此通常不会使用style-loader，而是使用MiniCssExtractPlugin.loader，压缩css并生成一个独立的css并引入
                    // MiniCssExtractPlugin.loader,//这里生成独立的css文件，从JS中分离出来减小体积，但是！！！这个loader会导致css样式分离开发时无法进行热更新，因此尽量样式开发完成之后再使用，而且，如果不将CSS分离，某些样式的设置会出问题，比如v-cloak，由于css和JS合并了，css无法优先加载并且应用v-cloak的隐藏样式，模板字符串代码{{}}还是会直接显示在页面上
                    'css-loader',//用于加载css文件，并处理@import和url()，就像js解析import/require()一样
                ]
            }, {
                test: /\.less$/,
                use: [
                    {
                        loader: 'style-loader',
                        // loader: MiniCssExtractPlugin.loader//同上css，不适用style-loader
                    }, {
                        loader: 'css-loader'
                    }, {
                        loader: 'less-loader'
                    }
                ]
            }, {
                test: /\.js$/,
                // exclude: /(node_modules|bower_components)/,//babel很慢，所以把node_modules等不需要转码的文件夹排除，或者也可以使用include仅包含src目录即可
                include: path.resolve(__dirname, './src'),
                use: [
                    {//未设置cacheDerectory：2506 ms
                        loader: 'babel-loader?cacheDirectory=true',//将es6语法转码为es5，否则es6语法会保持原样，低版本浏览器会报错
                        options: {
                            // sourceType: 'unambiguous',
                            presets: ['@babel/preset-env']
                        }
                    }
                ]
            },
            {
                test: /\.(jpg|png|gif|jpeg|webp)$/,
                use: [
                    {
                        loader: 'file-loader',//将**图片等静态文件发送到输出文件夹，并返回（相对）URL**
                        options: {
                            name: '[path][name].[ext]',//不设置默认为一串 很长hash.后缀名，这里设置为 原路径/文件名.后缀名
                        }
                    }
                ]
            },
            //下面是url-loader示例，与file-loader冲突，因此不要同时用
            // {
            //     test: /\.(jpg|png|gif|jpeg|webp)$/,
            //     use: [
            //         {
            //             loader: 'url-loader',
            //             options: {
            //                 name: '[name].[ext]',
            //                 outputPath: 'images/', // 指定输出目录
            //                 esModule: false, // 关闭ES模块语法
            //                 limit: 8192,//小于8192b，
            //             }
            //         }
            //     ],
            //     type: 'javascript/auto'//这个属性如果没有设置，则会生成两张图片(如果你的页面只引入了一张图片)
            //     /**
            //      * 详见-资源模块：https://webpack.docschina.org/guides/asset-modules/#root
            //      * 当在 webpack 5 中使用旧的 assets loader（如 file-loader/url-loader/raw-loader 等）和 asset 模块时，
            //      * 你可能想停止当前 asset 模块的处理，并再次启动处理，这可能会导致 asset 重复，你可以通过将 asset 模块的类型设置为 'javascript/auto' 来解决。
            //      */
            // },
        ]
    },

    //插件
    plugins: [
        new CompressionWebpackPlugin(),//开启压缩，默认使用gzip压缩。对生成的文件进行gzip压缩，以进一步减小文件大小，从而加快页面加载速度。它通常在生产模式下使用，可以将压缩后的文件保存在输出目录中
        new CleanWebpackPlugin(),//每次构建之前清除输出目录"/build"
        // new MiniCssExtractPlugin({
        //     filename: '[name][chunkhash:8].css'
        // }),// 实现css分包提速效果：CSS 提取到单独的文件中，为每个包含 CSS 的 JS 文件创建一个 CSS 文件，并且支持 CSS 和 SourceMaps 的按需加载。但是！！！这个loader会导致css样式分离开发时无法进行热更新，因此尽量样式开发完成之后再使用
        new HtmlWebpackPlugin({
            // filename: 'ninin.html',//设置了非index.html名字的页面，则开发环境的页面地址为localhost:3000/ninin.html，需要手动加上页面名称。 filename可以设置为../index.html用以打包时将html复制到JS的父目录而非同级，但是这样设置在开发环境启动服务的时候可能引起Cannot GET /  、 404错误 、 Refused to execute script from 'xx.js' because its MIME type ('text/html') is not executable, and strict MIME type checking is enabled错误
            template: './public/my_own_html_template.html',//复制一份模板，并且注入编译后的文件
            // chunks: ['aaa'],//仅包含aaa入口的包，bbb不会引入，多页面时配置多个HtmlWebpackPlugin时会用到，避免多个不同页面的js引入到一个html内
        }),//自动生成html文件，并且引入所有所有的编译后的文件
        new CopyWebpackPlugin({//1. 将静态资源文件夹从源目录复制到打包后的目标目录中 2. 将公共的 HTML、CSS、JS 文件从源目录复制到打包后的目标目录中
            patterns: [
                {
                    from: path.resolve(__dirname, 'public'),
                    to: path.resolve(__dirname, 'dist'),
                    globOptions: {
                        ignore: ['**/flex.html', '**/index.html', '**/my_three.html'] // 如果不需要复制 .html 文件，必须加入忽略列表，否则会出错，比如通过htmlwebpackPlugin生成了页面，就不能再把public中的相同页面复制进去了
                    },
                },
            ],
        }),
        // new webpack.HotModuleReplacementPlugin()//设置了devServer.hot:true会自动添加该插件，不需要手动添加
    ],

    //开启webpack服务，devServer其他字段详见：https://webpack.docschina.org/configuration/dev-server/
    devServer: {
        static: {
            directory: path.join(__dirname, './dist')//将 dist 目录下的文件 serve 到 localhost:8080 下
        },
        port: 3000,
        compress: true,//一切服务都启用gzip压缩
        open: true,//自动打开浏览器
        hot: true,//热更新
        //代理/api接口到target地址，访问/api/xxx相当于访问http://testm.10jqka.com.cn/xxx，可以解决接口跨域问题
        proxy: {
          '/api': {
            // target: 'http://testm.10jqka.com.cn',
            target: 'https://vaserviece.10jqka.com.cn/dehydrationResearch/homePage',
            ws: false,
            changeOrigin: true,// 默认情况下，代理时会保留主机头的来源，可以将 changeOrigin 设置为 true 以覆盖此行为。如果非同源，不设置changeOrigin为true会导致代理工具报错
            pathRewrite: {
              '^/api': '',// 访问的时候不带/api这段字符，防止访问地址多出/api导致404
            },
          },
        },
        //经过上述的proxy配置之后，下面的代码会在本地开发环境成功运行并且请求到vaserviece接口数据，浏览器页面以及控制台请求的都是localhost域名，但实际上控制台请求已经被转发到https://vaserviece.10jqka.com.cn/dehydrationResearch/homePage/findList
        /**
        async function test() {
            const fetch_options = {
                method: "POST"
            };
            // const url="https://vaserviece.10jqka.com.cn/dehydrationResearch/homePage/findList"//这是原地址，使用代理我们只要用/api开头即可
            const url = "/api/findList";
            const get_data = await fetch(url, fetch_options);//返回一个Promise
            const result = await get_data.json();//get_data.json()仍然是一个Promise，因此还需要await/then()一次
            console.log("🚀 ~ test ~ result:", result);
        }
        test();
         */
    },

    //补充，多入口时需要设置 optimization.runtimeChunk: "single"，热更新报错：[HMR] Update failed: ChunkLoadError: Loading hot update chunk 139 failed.
    //optimization.runtimeChunk设置为true，可以避免同一个模块被实例化多次，将所有生成的 runtime 代码打包到一个单独的文件中，以便在多个页面之间共享，减少了每个页面的加载时间和代码冗余。举个例子，假如common.js中有一个{a:1}，假设one.js和two.js都引用了common.js并且都执行了a++，那么两个模块的a默认都会是2，因为common.js被实例化了2次，但是如果加上该配置，两个模块会共享同一个a，最终a值为3
    optimization: {
        runtimeChunk: 'single',
        minimize: true,//告知 webpack 使用 TerserPlugin 或其它在 optimization.minimizer定义的插件压缩和混淆JS代码，以减小文件大小并提高加载速度。这个选项是Webpack内置的优化功能
    },

    //打包模式：开发模式development / 生产模式production（默认）
    // mode: 'production',// 生产模式，去掉注释，精简代码
    mode: 'development',//开发模式，不会精简代码，直接eval执行代码，包括注释+所有代码
}
```

#### 发布

1. webpack测试环境、正式环境区分：.env文件，.env.development内配置的环境变量在npm run serve时生效，.env.production内配置的环境变量在npm run build时生效，npm run build生成的是一个dist目录，可以通过http-server dist/进行运行
    1. 可以通过配置.env.development、.env.pruduction中配置一个VUE_APP_CURRENT_MODE=dev/pro/pre来作为不同环境下的一个唯一key，然后新建一个config文件夹分别配置三个环境下的url、接口等配置，通过`process.env.CURRENT_MODE`来自动获取不同环境下的key
    2. 也可以通过域名来区分测试环境，正式环境

### webpack优化

#### 构建优化

1. 减少构建的资源体积
    1. 合理使用loader，用 include（仅包含） 或 exclude（排除node_modules等） 来帮我们避免不必要的文件夹构建
    2. 缓存编译结果：`loader: 'babel-loader?cacheDirectory=true'`
    3. **cdn引包 + externals选项排除打包**：比如通过cdn引入vue.js，把资源从打包列表中排除，速度会有明显的提升
    4. 预编译：通过DllPlugin来对那些我们引用但是绝对不会修改的npm包来进行预编译，再通过DllReferencePlugin忽略已经预编译的文件。
    5. 按需引入，如elementUI只引入Button、Input、Datapicker减小体积
2. 其它
   1. 使用thread-loader构建多线程加速：`test:/.js$/,use:[thread-loader,xx-loader]`thread-loader放在耗时的loader前面，通过多进程模型，来加速代码构建
   2. 关闭生产环境的sourceMap选项：productionSourceMap设置为 false 以加速生产环境构建

#### 页面性能优化

1. WARNING in webpack performance recommendations: You can limit the size of your bundles by using import() or require.ensure to lazy load some parts of your application.For more info visit <https://webpack.js.org/guides/code-splitting/>
2. WARNING in asset size limit: The following asset(s) exceed the recommended size limit (244 KiB) This can impact web performance. .my_threef97b3f9c.js 778.1522f779fcf7f465850d.hot-update.js
3. **代码分离**
   1. **动态导入/加载**：import()--返回Promise，**在需要引入的时候再触发即可实现按需引入**，并且配合webpack的魔法注释`const v=await import(/* webpackChunkName: "my_vue" */ "./test_bundle")`即可实现代码分割
      1. 未动态前：都打包到一个大的js中

            ```javascript
            import v from "./test_bundle";//同步引入，打包到my_three的包中，造成包过大
            ```

            ```bash
            assets by path js/*.js 5.1 MiB
            asset js/my_three5467fbf0.js 4.96 MiB [emitted] [immutable] (name: my_three) 1 related asset
            ...

            # devTools 加载数据
            17 requests 4.7 MB transferred  7.8 MB resources    Finish: 853 ms  DOMContentLoaded: 398 ms    Load: 779 ms
            ```

      2. 动态导入后：生成了两个包，降低了单个包大小（小即是快）

            ```javascript
            async function load_vue(){
                const v=await import(/* webpackChunkName: "my_vue" */ "./test_bundle");//魔法注释设置打包名称
                console.log(`我引入了${v.default}`);//注意，v必须指向模块的 .default 值，因为它才是 promise 被处理后返回的实际的 module 对象
            }
            load_vue();//使用import()语法动态引入
            ```

            ```bash
            assets by path js/*.js 5.15 MiB
            asset js/my_three6a45a519.js 3.71 MiB [emitted] [immutable] (name: my_three) 1 related asset
            asset js/my_vuebe3ef2c7.js 1.28 MiB [emitted] [immutable] (name: my_vue) 1 related asset
            ...

            # devTools 加载数据，可见DOMContentLoaded时间明显加快
            19 requests 4.7 MB transferred  7.8 MB resources Finish: 861 ms  DOMContentLoaded: 119 ms    Load: 743 ms
            ```

      3. 懒加载（即动态加载）：打包的时候不会把所有的代码和依赖打包到一个大的文件中，而是分割成更小的代码块，需要的时候再载入，提高了页面的性能和加载速度
         1. 理解：通常在index.js中导入代码：`import a from './test_bundle'`，它是**同步的**，并且会把a和index文件一起打包为一个大文件；懒加载：`let a = import('./test_bundle'); a.then(res => { res.xxx })`，这里是直接import()，会返回一个promise，导入分以下情形（假定test_bundle导出一个名为my_fun的函数）：
            1. 通过`export default my_fun`导出，使用`res.default()`来执行
            2. 通过`export default {my_fun}`导出，使用`res.default.my_fun()`来执行
            3. 通过`export { my_fun }`导出，使用`res.my_fun()`执行
         2. 当要进行懒加载的代码过大，超过了babel预设的最大值（500KB），则不会被babel优化（但是该分开打包还是分开打包了，只不过单个包太大），会有`[big]`黄色提示以及超限警告，此时可以考虑将该模块分成更小的模块来进行异步加载
   2. **optimization.splitChunks**分包、**提取多个包之间的公共代码**：
      1. 为分包之前，打包效果：

            ```bash
            assets by path js/*.js 3.86 MiB
                asset js/my_threef927be0b.js 3.71 MiB [emitted] [immutable] (name: my_three) 1 related asset
                asset js/flexfc405916.js 90.1 KiB [emitted] [immutable] (name: flex) 1 related asset
                asset js/index334044cb.js 44.9 KiB [emitted] [immutable] (name: index) 1 related asset
                asset js/runtimeb21a19f9.js 14.2 KiB [emitted] [immutable] (name: runtime) 1 related asset
            ```

      2. 分包后的打包效果：

            ```javascript
            //webpack.config.js
            optimization: {
                runtimeChunk: 'single',
                splitChunks: {
                    cacheGroups: {
                        three_base: {
                            test: /[\\/]node_modules[\\/](three)[\\/]/,
                            name: 'three_base',
                            chunks: 'all',
                        },
                        //three中的addons文件
                        three_other:{
                            test: /[\\/]node_modules[\\/](three[\\/]examples[\\/]jsm)[\\/]/,
                            name: 'three_other',
                            chunks: 'all',
                            priority: 2
                        }
                    },
                },
            },
            ```

            ```bash
            assets by path js/*.js 3.86 MiB
                asset js/three_base49858cc5.js 3.07 MiB [emitted] [immutable] (name: three_base) (id hint: three_base) 1 related asset
                asset js/three_otherc1c90315.js 457 KiB [emitted] [immutable] (name: three_other) (id hint: three_other) 1 related asset
                asset js/my_three5ff3e70d.js 208 KiB [emitted] [immutable] (name: my_three) 1 related asset
                asset js/flexfc405916.js 90.1 KiB [emitted] [immutable] (name: flex) 1 related asset
                + 2 assets
            ```

4. **图片压缩**，tinyPng或者其它压缩工具：
   1. 未压缩：29 requests   4.7 MB transferred  7.8 MB resources
   2. 压缩：19 requests 2.7 MB transferred  6.3 MB resources    Finish: 808 ms  DOMContentLoaded: 406 ms    Load: 756 ms
5. **合理的预取、预加载**：prefetch或者preload
6. **开启gzip**——安装compression-webpack-plugin插件+服务器开启gzip并且做一定配置：
   1. compression-webpack-plugin
      1. 未安装：webpack打包速度：2527 ms，总包大小：3.82MB 纯未压缩页面代码
      2. 安装后：打包速度：2902 ms，总包大小：3.82MB的未压缩打包代码 + 830KB 的gzip压缩文件
   2. 服务器gzip配置
      1. nginx配置如下，如果项目是运行在容器内，那么我们需要在宿主机上映射一份nginx.conf文件以修改nginx配置，流水线作业中的创建容器那一步需要挂载宿主机的nginx.conf文件到容器内部，即

        ```yml
        job_deploy:
        stage: deploy
        image: docker
        tags:
            - dont_cry_tag
        script:
            - docker build -t zacks_home_image .
            - if [ $(docker ps -aq --filter name=zacks_home_container) ]; then docker rm -f zacks_home_container;fi
            - docker run -d -p 8083:80  -v /srv/gitlab-runner/new_backup_nginx.conf:/etc/nginx/nginx.conf --name zacks_home_container zacks_home_image
        ```

        ```conf
        http {
            # 添加协商缓存
            add_header Cache-Control no-cache;

            # 开启 gzip 压缩
            gzip  on;

            # 开启静态压缩，如果不开启本开关，则会对每一次请求都用nginx默认的压缩方式进行压缩（动态压缩），而非使用我们webpack已经压缩过的文件
            # 压缩的资源会被缓存下来，下次请求时就直接使用缓存
            # 使用compression-webpack-plugin压缩后已经有了.gz压缩文件，因此设置了gzip_static会直接使用该.gz压缩包，这个过程也叫静态压缩；而对应的还有一种叫做动态压缩，也就是开启了gzip后对于每一次请求，都会执行一次文件压缩，叫做动态压缩，请求量大的时候这个过程很显然会消耗系统的CPU资源
            gzip_static  on;

            # 使用 gzip 压缩的文件类型
            # 此外，text/html 是自带的，不用写上
            gzip_types text/plain text/css application/javascript application/json text/xml application/xml application/xml+rss image/jpeg image/png image/gif;
            
            # 小于 256 字节的不压缩
            # 这是因为压缩是需要时间的，太小的话压缩收益不大
            gzip_min_length 256;
        }
        ```

   3. 配置完上面两个后的效果对比：
      1. 未配置： 16 requests   7.8 MB transferred  7.2 MB resources    Finish: 30.20 s DOMContentLoaded: 11.30 s    Load: 30.21 s
      2. 配置后： 16 requests   4.1 MB transferred  7.2 MB resources    Finish: 18.84 s DOMContentLoaded: 1.44 s    Load: 18.84 s
      3. 未配置2：js 792KB
      4. 配置后2：js 218KB

7. **页面响应速度：loading动画**
    1. loading动画：页面留白过长时间（超过3s，最好控制在2s以内）的等待会让用户流失，首屏优化，减少页面留白时间，让用户觉得快，才是真的快
8. **tree-shaking**
   1. 使用ES6的模块语法（**export + import** 而不是 CommonJS的 module.exports + require）
   2. 启用打包工具的生产模式：在Webpack配置中，你需要配置mode选项为production，这会自动开启Tree shaking优化
   3. 在package.json中将 sideEffects 属性设置为false（或者仅包含有副作用的文件数组，比如`"sideEffects": ["*.css","*.less"] // 其他有副作用的文档类型可以追加`）
9. cdn + externals ：提高打包速度 + 网页速度
   1. 未排除打包之前：

        ```bash
        assets by path js/*.js 3.82 MiB
        asset js/my_three74e9f92d.js 3.68 MiB [emitted] [immutable] (name: my_three) 1 related asset
        ...
        ```

   2. 排除打包之后（需要在页面中加入cdn引入的方式）：

        ```javascript
        externals:{
            'three': 'THREE',//模块名称:真实变量名称 从打包列表中排除THREE库
        }
        ```

        ```bash
        assets by path js/*.js 777 KiB
        asset js/my_three31da49d2.js 628 KiB [emitted] [immutable] (name: my_three) 1 related asset
        ...
        ```

## Vite

1. 相比webpack优势：
   1. 打包/不打包：webpack所有文件整体打包，vite使用ES Module，不需要打包，浏览器用到哪个模块就请求、编译那个模块
   2. 热更新方面：webpack会把所有的模块重新编译一遍，而vite只会请求改动过的那个模块
   3. 底层：webpack基于nodejs，vite基于es build预构建的，使用go语言编写，速度快很多（go语言--纳秒级 比 JS--毫秒级 快很多）
   4. JS单线程，go多线程，开发环境构建速度也更快
   5. 缓存：vite利用浏览器的缓存策略，针对源码模块（我们自己写的代码）做了协商缓存处理，针对依赖模块（第三方库）做了强缓存处理，这样我们项目的访问的速度也就更快了

## 性能分析/优化

1. 如何使用Chrome开发工具进行性能分析：[Analyze runtime performance](https://developer.chrome.com/docs/devtools/performance/)
2. 使用Vue框架的应用也可以使用[Vue开发者扩展](https://cn.vuejs.org/guide/scaling-up/tooling.html#browser-devtools)进行分析
3. [页面快速加载优化](https://web.dev/fast/)

## ThreeJs

1. 几何体（Geometry）和材质（Material）是可以重用的，但是需要绘制几个对象就要实例化几个网格（Mesh）

    ```javascript
    const helper_obj = new THREE.Mesh(
        new THREE.SphereGeometry(0.5),
        new THREE.MeshLambertMaterial({
            color: 'red',
            wireframe: true
        })
    )
    ```

2. `MeshNormalMaterial({ color: 'blue' })`中的color不会生效，因为该材料的颜色是由它的位置决定的（彩虹色并且随位置而渐变）
3. 点积 `A.dot(B)` : 是一个数字/标量，表示A在B上的投影与B的乘积，值大于0表示夹角在90度以内；值等于0表示A与B垂直，值小于零表示值大于90度小于180度
4. 叉乘 `new THREE.Vector3().crossVectors(A, B)` ，运算结果是一个向量，并且与这两个向量都垂直，是这两个向量所在平面的法线向量

## Nginx

### Nginx介绍

1. Nginx (engine x) 是一个高性能的**HTTP代理**和**反向代理**web服务器，同时也提供了IMAP/POP3/SMTP服务
2. 当请求并发量增大的时候，一台服务器不够用，于是增加到多台服务器，这个时候就需要一个**代理服务器**来帮助转发和处理请求
3. Nginx接收用户的请求，并且转发到不同的服务器节点上（用户只访问代理服务器，转发过程对于用来说是无感知的）
4. Nginx功能
    1. 正向代理、反向代理：**代理服务器如果配置在客户端即为正向代理,如果配置在服务端即为反向代理**，和机器个数没有关系
        1. 正向代理代替客户端去发送请求，反向代理代替服务端接受请求
        2. **反向代理**：**客户端请求代理服务器**，代理服务器将发送给服务器的请求做转发、负载均衡等等，nginx通常被用来作为反向代理服务器
        3. 正是因为正向代理代替客户端发送请求,正向代理服务器和客户端对外表现为一个客户端,所以正向代理隐藏了真实的客户端；反向代理代替服务端接受请求,反向代理服务器和真实服务器对外表现为一个服务端,所以反向代理服务器隐藏真实的服务端
        4. 综上，本质上代理服务器还是那个代理服务器,如果替客户端干活就是正向代理,如果替服务端干活就是反向代理

            ```conf
            # 反向代理服务器配置示例
            server {
            listen 5000;
            server_name localhost;

            # 客户端请求http://localhost:5000/，会转发到http://localhost:3000/；客户端请求http://localhost:5000/api/时，转发到http://localhost:4000/api/
            location / {
                proxy_pass http://localhost:3000;
            }
            location /api/ {
                proxy_pass http://localhost:4000;
            }
            }
            ```

    2. 负载均衡
        1. 轮询：多个请求依次转发给不同的服务器（**有多个服务器**，相当于一个遍历过程），然后再循环。比如有三个服务器ABC，10个请求0~9，那么顺序为：0→A、1→B、2→C、3→A、4→B   ···等等
        2. 加权轮询：某个服务器的性能较好（权重更高），则会为其转发更多的请求，处理的请求就更多
        3. iphash：客户端请求的ip进行hash操作，然后根据hash结果将同一个客户端ip的请求分发给同一台服务器进行处理，可以解决session不共享的问题（一般使用redis来实现，不重要）
    3. 动静分离
        1. 把静态资源（比如某些js、css、图片等）和经常变的资源区分开来，对静态资源做缓存操作，提高资源响应的速度

## 算法

### 回溯

1. 原理：递归，是一种纯暴力的搜索方式。按照深度优先搜索的策略，探索到某一结点时，要先判断该结点是否包含问题的解，如果包含，就从该结点出发继续探索下去，如果该结点不包含问题的解，则逐层向其祖先结点回 溯
2. 解决的问题：
    1. **组合**：比如给一个集合，找出数组长度为2的组合
    2. **切割**：比如给定一个字符串，如何切割保证每个字符串都回文
    3. **子集**：比如求所有的子集
    4. **排列**：有顺序的组合
    5. **棋盘问题**：N皇后、解数独
3. 算法：抽象成一棵树，宽度使用for循环遍历，深度使用递归遍历

    ```javascript
    let is_end;//终止条件
    let current_level=[];//当前宽度
    let result=[];
    let temp=[];
    function deal(){}
    function back_track(strs){
        if(is_end){
            re sult.push('');//处理结果
            return;//当前深度已经终止了，记得return
        }
        for(let i=0;i<current_level.length;i++){//遍历当前宽度
            temp.push(current_level[i]);
            deal(current_level[i]);//处理每一个节点
            back_track();//递归
            temp.pop();//撤销该节点的处理情况
        }
    }
    ```

### 贪心算法

1. 原理：局部最优→全局最优
2. 例子：给定一个金额和一组硬币的面额，要求找到凑成该金额所需的最少硬币数量，思路就是从最大的单个金额（局部最优）开始累计，依次向小的金额累计，直到找到满足条件的“最优解”（为什么没最优解要引号标注呢？以为这种总是以局部最优为指标的算法最终得到的解不一定是最优解，比如下面的coins变为`[50, 30, 1]`，总金额变为60，用该算法得到的结果是11，即【50, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1】，而事实上，最优解应该是2，即【30， 30】）

```javascript
function greedy_coin_change(coins, amount) {
    coins.sort((a, b) => b - a);  // 从大到小排序硬币面额
    let coin_count = 0;
    let i = 0;

    while (amount > 0 && i < coins.length) {
        if (coins[i] <= amount) {
            const num_coins = Math.floor(amount / coins[i]);  // 计算可以使用的硬币数量
            coin_count += num_coins;
            amount -= num_coins * coins[i];
        }
        i++;
    }

    if (amount === 0) {
        return coin_count;
    } else {
        return -1;  // 无法凑成指定金额
    }
}

// 示例
const coins = [25, 10, 5, 1];
const amount = 63;
const result = greedy_coin_change(coins, amount);
if (result !== -1) {
    console.log(`需要的最少硬币数量为: ${result}`);
} else {
    console.log("无法凑成指定金额");
}
```

### 动态规划

1. 原理

### 排序算法

1. 各排序算法以及其时间复杂度：`https://www.cnblogs.com/fengff/p/10983733.html`
2. 例子：
   1. 冒泡排序，$O(N^2)$

        ```javascript
        function bubble_sort(arr) {
            let arr_length = arr.length;
            for (let i = 0; i < arr_length; i++) {
                for (let j = 0; j < arr_length - 1; j++) {
                    if (arr[j] > arr[j + 1]) {
                        let temp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            return arr;
        }
        console.log(bubble_sort([1, 34, 5, 6, 7, 122]));//[1, 5, 6, 7, 34, 122]
        ```

   2. 插入排序排序，$O(N^2)$

        ```javascript
        function insert_sort(arr) {
            const arr_length = arr.length;
            if (arr_length <= 1) return arr;
            const result = [arr[0]];
            for (let i = 1; i < arr_length; i++) {
                for (let j = 0; j < result.length; j++) {
                    if (arr[i] < result[j]) {
                        result.splice(j, 0, arr[i]);
                        break;
                    }
                    if (j === result.length - 1) {//到最后一个了
                        result.push(arr[i]);
                        break;
                    }
                }
            }
            return result;
        }

        console.log(insert_sort([1, 34, 5, 6, 7, 122]));
        ```

   3. 快速排序，$O(N*log_2N)$

        ```javascript
        function quick_sort(arr) {
            if (arr.length <= 1) return arr;
            const base = arr[0], left = [], right = [];
            for (let i = 1; i < arr.length; i++) {
                if (arr[i] > base) {
                    right.push(arr[i]);
                } else {
                    left.push(arr[i]);
                }
            }
            //已经将base的顺序确定好，接下来通过递归来对两边的数组分别执行相同的操作
            return [...quick_sort(left), base, ...quick_sort(right)];
        }
        console.log(quick_sort([1, 34, 5, 6, 7, 122]));//[1, 5, 6, 7, 34, 122]
        ```

## 其它项目问题

1. 微信分享、客户端分享
   1. 可定义的信息
      1. 分享时显示的LOGO；
      2. 分享LOGO的宽度；
      3. 分享LOGO的高度；
      4. 分享出去显示的标题（默认调用网页标题）；
      5. 分享出去显示的描述（默认调用网页标题）；
      6. 分享链接(默认为当前页面的URL)。
      7. 分享微信的APPID(一般为空)。
   2. 具体步骤
      1. 使用微信的JS-SDK，引入jweixin.js
      2. 在config配置所有要调用的API：`wx.config(jsApiList:[xxx])`
      3. 在wx.ready中配置各个api：`wx.ready(function(){ wx.onMenuShareTimeLine({})     wx.onMenuShareAppMessage({}) })` 分别配置朋友圈和微信好友分享的标题、内容、图片等，成功或者失败都可以传入回调函数
      4. 虽然微信提供了JSSDK，但是这不意味着你可以用自定义的按钮来直接打开微信的分享界面，这套JSSDK只是把微信分享接口的内容定义好了，实际还是需要用户点击右上角的菜单按钮进行主动的分享，用户点开分享界面之后，出现的内容就会是你定义的分享标题、图片和链接。所以需要自己手动配置一个引导用户分享的遮罩

    ```javascript
    //微信分享：
    //使用前引入<script type="text/javascript" src='//res.wx.qq.com/open/js/jweixin-1.0.0.js'></script>
    wx.config({
        debug: false,
        jsApiList: [
            // 所有要调用的 API 都要加到这个列表中
            'checkJsApi',
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
            'onMenuShareWeibo',
            'onMenuShareQZone'
        ]
    });

    wx.ready(function () {
        //分享到朋友圈
        wx.onMenuShareTimeline({
            title: '分享到朋友圈标题', // 分享标题
            desc:"内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容",
            link:window.location.href,
            imgUrl: "http://ozone.thsbx.cn/dehydrationReport/images/index_banner.png", // 分享图标
            success: function () {
                // 分享成功执行此回调函数
                alert('success');
            },
            cancel: function () {
                alert('cancel');
            }
        });

        //分享给朋友
        wx.onMenuShareAppMessage({
            title: '分享给朋友标题标题', // 分享标题
            desc: '内容内容',
            link:window.location.href,
            imgUrl: "http://ozone.thsbx.cn/dehydrationReport/images/index_banner.png", // 分享图标
            trigger: function (res) {
                // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
            },
            success: function (res) {
                // 分享成功执行此回调函数
                alert('已分享');
            },
            cancel: function (res) {
                alert('已取消');
            },
            fail: function (res) {
                alert(JSON.stringify(res));
            }
        });
        console.log("完成配置");
    });
    ```

2. 微信回流：使用scheme跳转(可以通过`<a href="weixin://">`打开微信APP，并且支持打开不同的功能，如公众号、小程序等等，详见微信开放文档)，从H5跳转app，app会识别并且打开复制的链接
3. Vue3中引入markdown文件并且显示在页面上：
   1. 不好的插件： vite-plugin-md，markdown文件没错也容易报错
   2. 可以用的插件：
      1. unplugin-vue-markdown，但是样式不够多，比如代码样式不高亮

## React

### 入门

1. 创建一个新的React项目：`npx create-react-app my-app` 或 `npx create-next-app@latest`（使用next框架）
2. React 应用程序是由 组件 组成的，React 组件是返回标签的 JavaScript 函数

    ```jsx
    function MyButton() {
        return (
            <button>
                I'm a button
            </button>
        );
    }

    export default function MyApp() {
        return (
            <div>
                <h1>Welcome to my app</h1>
                <MyButton />
            </div>
        );
    }
    ```

3. React 组件必须以大写字母开头，而 HTML 标签则必须是小写字母，JSX 比 HTML 更加严格。你必须闭合标签，如 `<br />`。你的组件也不能返回多个 JSX 标签。你必须将它们**包裹到一个共享的父级**中，比如 `<div>...</div>` 或使用空的 `<>...</>` 包裹
4. 标签和 return 关键字不在同一行，则必须把它包裹在一对括号中，没有括号包裹的话，任何在 return 下一行的代码都 将被忽略！
5. className为标签指定一个class
6. JSX 会让你把标签放到 JavaScript 中。而大括号会让你 “回到” JavaScript 中，这样你就可以从你的代码中嵌入一些变量并展示给用户，比如下面的user.name。（Vue中是`{{}}`，而React中是`{}`，在React中`{{}}`仅仅代表一个JS对象，只是对象需要被`{}`包裹）
   1. 同理，如果是组件的参数，函数内形参也要使用`{}`

    ```jsx
    return (
        <h1>
            {user.name}
        </h1>
    );

    //组件函数参数也要用{}
    function Square({ value }) {
        return <button className="square">{value}</button>;
    }
    ```

7. 绑定属性也是用`{}`，例如：`src={user.imageUrl}`，下面的style中的`{}`内是一个对象，所以看起来有两对花括号

    ```jsx
    return (
        <img
            className="avatar"
            src={user.imageUrl}
            style={{
                width: user.imageSize,
                height: user.imageSize
            }}
        />
    );
    ```

8. 没有条件语法/指令，**直接用JS的if判断**，或者用`?:`三元运算符（没有v-if），再或者没有else可以用`&&`运算符

    ```jsx
    let content;
    if (isLoggedIn) {
        content = <AdminPanel />;
    } else {
        content = <LoginForm />;
    }
    return (
        <div>
            {content}
        </div>
    );
    <div>{is_login?(<Pannel />):(<Login />)}</div>
    <div>{is_login && <Pannel />}</div>
    ```

9. 列表渲染，**使用for循环**，同Vue的`v-for`一样，**需要为每个子项提供一个key**
   1. 列表渲染只需要返回一个数组即可，因此可以使用数组的map方法，或者直接用一个空数组，把一个一个的组件`push`进去

    ```jsx
    const listItems = products.map(product =>
        <li key={product.id}>
            {product.title}
        </li>
    );

    return (
        <ul>{listItems}</ul>
    );

    const rows = [];
    for (let i = 0; i < 3; i++) {
        rows.push(<div>{i}</div>)
    }
    return (<>{rows}</>);
    ```

10. **关于React的key，不仅能用在列表中，更加可以用于在状态更新时重置组件**，比如以下两个Chat组件，未添加key的那一个当to.email改变时并不会发生变化，即使其输入框已经输入了应该发给前一个联系人的信息，切换了联系人（邮箱）的时候信息草稿仍然存在；但是添加了`key={to.email}`的组件当to.mail改变时，会重置组件状态，表明“这已经是一个全新的聊天界面”了（**每当 key变化时，React 将重新创建 DOM，并 重置组件和它的所有子组件的 state**）

    ```jsx
    <Chat contact={to} />

    <Chat key={to.email} contact={to} />
    ```

11. 事件处理：直接传递函数名即可。
    1. 按照惯例，通常将事件处理程序命名为 handle，后跟事件名称
    2. **疑问！！！那么需要传参的时候怎么办？？？？？**  对于需要传参的事件，不能够通过`onClick = {handleClick(0)}`这种形式取调用，因为这样写会直接调用`handleClick(0)`（这一点和Vue不一样，Vue中有形参应该传参，若不传则默认把`$event`传给第一个形参，如果传一个空列表则把参数都看作undefined；无形参则传或者不传都可以），而handleClick中很有可能调用了setxxx来改变组建的状态state，这将导致组件重新渲染，而重新渲染又会运行`handleClick(0)`，从而导致无限循环。如何解决？可以通过传入一个调用`handleClick(0)`的箭头函数来解决
    3. 当元素和父元素都有onClick监听事件时，事件默认会使用冒泡阶段顺序触发两次（和原生addEventListener监听器保持一致），因此可以使用`event.stopPropagation()`来避免事件继续传播，从而只会触发一次。如果想要在捕获阶段触发事件，可以使用事件名后面+Capture来实现：比如`onClickCapture`
    4. 同理要想阻止默认行为也可以调用`event.preventDefault()`。这两种方案在Vue中使用`.stop、.prevent`修饰符来简写
    5. 与渲染函数不一样，事件处理函数是执行副作用（更新state）的最佳位置

    ```jsx
    return (
        <button onClick={handleClick}>
            Click me
        </button>
    );

    return (
        <button onClick={()=>handleClick(0)}>
            Click me
        </button>
    );

    ```

12. 父子组件通信：
    1. 父传子：prop。下面的count和handleClick都是父组件中的数据和方法，可以通过prop把这些**属性/方法**传给子组件，子组件调用即可（这个和Vue是类似的，只不过不需要使用`@`和`:`）。**注意当传递多个参数时，多个形参参数props是包含在同一个`{}`中的**。
       1. React的props也是单项数据流（同Vue）
       2. React 组件函数接受一个参数，一个 props 对象，但是呢，我们通常不需要整个props对象，并且写起来也麻烦，因此可以将它结构为单独的属性
       3. 也能为每个prop指定一个默认值，`function MyButton({ count = 0, onClick }){}`
       4. 展开语法：`<Avatar {...props} />`可以将props每个属性都转发给子组件而不用手动给每个属性赋值，让代码更简洁，*但是请克制地使用展开语法*，通常它表示应该拆分组件，将子组件作为JSX传递
       5. 特殊属性：`props.children`，表示子组件/子元素，需要在父组件定义中手动渲染（可以是组件，也可以是html元素/JSX，类似于Vue中的插槽）
       6. props是不可变的，当需要改变props时，只能传递一个新的对象，旧的props对象将被抛弃并清除，不要尝试“更改 props”
       7. **把props镜像到state中会导致该state与props并不保持同步更新！**

            ```jsx
            function Message({ messageColor }) {
                const [color, setColor] = useState(messageColor);
                // 如果父组件稍后传递不同的 messageColor 值（例如，将其从 'blue' 更改为 'red'），则 color state 变量将不会更新！ 
            }
            ```

       8. 示例

        ```jsx
        return (
            <div>
                <h1>Counters that update together</h1>
                <MyButton count={count} onClick={handleClick} />
                <MyButton count={count} onClick={handleClick} />
            </div>
        );

        //子组件
        //function MyButton(props){} //解构成下面的形式
        function MyButton({ count = 0, onClick }) {
            return (
                <button onClick={onClick}>
                    Clicked {count} times
                </button>
            );
        }

        //特殊属性：children，类似Vue的插槽
        export default function Parent(props) {
            console.log("父组件中：", props);
            return (
                <>
                    <p>我是父组件，参数prop1：{props.prop1}</p>
                    {props.children}
                </>
            )
        }
        ```

    2. 子传父：通过父组件传递一个事件（函数名）给子组件，子组件触发该事件即可

        ```jsx
        function Child({ msg, onChangeMsg }) {
            return (
                <>
                <p>父组件状态：{msg}</p>
                <button onClick={() => onChangeMsg("新值")}>触发事件改变夫组件状态为：“新值”</button>
                </>
            )
        }

        function Parent() {
            const [msg, setMsg] = useState("初始化——未修改");
            function changeMsg(newMsg) {
                setMsg(newMsg);
            }
            return (<div>
                <Child msg={msg} onChangeMsg={changeMsg} />
            </div>)
        }
        ```

13. 组件：
    1. 组件应该是纯粹的（纯函数）
       1. 只负责自己的任务。 它不会更改在该函数调用前就已存在的对象或变量。
       2. 输入相同，则输出相同。 给定相同的输入，组件应该总是返回相同的 JSX。
    2. 不应该改变任何用于组件渲染的输入。这包括 props、state 和 context。通过 “设置” state 来更新界面，而**不要改变预先存在的对象**
14. 多个子组件需要通信，应该改为在共同父组件中声明共享state，也就是状态提升
15. 组件定义函数不要嵌套，每一个组件都应该定义在顶层
16. hook：`useState` 以及任何其他以“use”开头的函数都被称为 Hook，只在 React 渲染时有效。useState 是 React 提供的一个内置 Hook。你可以在 [React API 参考](https://react.jscn.org/reference/react) 中找到其他内置的 Hook。你也可以通过组合现有的 Hook 来编写属于你自己的 Hook。**Hooks只能在你的组件（或其他 Hook）的最顶层** 调用
    1. 按规定只在顶层调用 Hooks，Hooks 将始终以相同的顺序被调用
    2. **在 React 内部，为每个组件保存了一个数组，其中每一项都是一个 state-setter() 对。它维护当前 state 对的索引值**，每一个setter()在setters数组中的位置都和其对应的state在state数组中的位置相同，**不能在条件、循环、或其他嵌套函数内中调用“use”钩子**，因为我们正在处理指向一组数组的游标，所以如果更改渲染中调用的顺序，游标将与数据不匹配，并且 use 调用将不会指向正确的数据或处理程序。
17. 组件状态state：使用`const [data, set_data] = useState(arg)`，data保存了数据的值，`set_data(data + 1)`是修改数据的方法，同一个组件多次渲染，它们的state都是独立的不会互相影响
    1. state **完全私有**于声明它的组件，其它组件都不知道该组件有什么state
    2. **不变性**：更改数据（比如一个数组）的时候，使用`.slice()`等方法创建数组的副本而不是直接修改原数组，这两个做法结果是一样的，但是保持不变性有两个好处：
        1. 方便撤销/重做这样的常见功能，可以保持当前版本的数据完好性
        2. 父组件state变化会导致所有子组件重新渲染，不管子组件是否受到此变化影响。不变性可以让组件比较其数据是否改变的成本非常低
    3. **组件的state指向的内容是不会变动的，设置它（setXxx）并不改变你已有的状态变量，而是触发一次重新渲染。这在一开始可能会让人感到惊讶！**，当使用setXxx的时候，**state会指向一个新的内容对象**，因此下面的代码三个输出都会是1而不会输出任何的222，**setTimeout中的count由于闭包的原因，输出的仍然是旧的count变量的引用**（也就是下面所说的“快照”）而非最新状态 —— **请反复观看以下代码**（其实由代码使用const修饰count也能看出来，count这个变量本身是不会变的）

        ```jsx
        const [count, setCount] = useState(1);
        function change_count() {
            console.log("🚀 ~ Parent ~ count:", count);
            setCount(222);
            setTimeout(() => console.log("🚀 ~ Parent setTimeout ~ count:", count)
                , 2000);
            console.log("🚀 ~ Parent ~ count:", count);
        }
        ```

    4. state 在其表现出的特性上更像是一张快照。设置它不会更改你已有的 state 变量，但会触发重新渲染
    5. **设置 state 需要下一次渲染时才会生效（好好体会这句话）**，因此以下代码点击一次按钮只会+1而不会+3，***这点和Vue中不一样，Vue中数据的改变是即时的，不会等待下次渲染再修改，因此Vue中会直接+3***

        ```jsx
        const [number, setNumber] = useState(0);

        <button onClick={() => {
        setNumber(number + 1); // 当前number是0，React 准备在下一次渲染时将 number 更改为 1
        setNumber(number + 1); // 当前number是0，React 准备在下一次渲染时将 number 更改为 1
        setNumber(number + 1); // 当前number是0，React 准备在下一次渲染时将 number 更改为 1
        }}>+3</button>
        ```

        ```vue
        <!-- Vue中 -->
        <template>
            <p>{{r}}</p>
            <button @click="add_3(1)">+3</button>
        </template>

        <script setup>
        let r = ref(0);
        function add_3(num = 1) {
            r.value += num;
            r.value += num;
            r.value += num;
        }
        </script>
        ```

    6. 由上我们已经知道直接设置state会在下次更新的时候才生效，那么如何在下次渲染前多次更新同一个state呢？我们可以传递一个**状态更新函数**给setNumber，像`setNumber(n => n + 1)`而不是`setNumber(number + 1)`

        ```jsx
            <button onClick={() => {
                setCount(n => n + 1);
                setCount(n => n + 1);
                setCount(n => n + 1);
            }}>真的+3</button>
        ```

    7. 上述两点可以用一下算法表示，即React计算最终state的算法为：

        ```javascript
        //queue是类似[5, n => n+1, 42]的setXxx()方法的参数组成的数组
        export function getFinalState(baseState, queue) {
            let finalState = baseState;

            for (let update of queue) {
                if (typeof update === 'function') {
                    // 调用更新函数
                    finalState = update(finalState);
                } else {
                    // 替换下一个 state
                    finalState = update;
                }
            }

            return finalState;
        }
        ```

    8. **React 会等到事件处理函数中的 所有 代码都运行完毕再处理你的 state 更新**，这个过程也叫批处理
    9. React的state是数组/对象时，不应该直接修改原数组/对象，需要创建一个新的对象（或复制现有的对象），然后用这个副本来更新状态：通常使用`...`展开运算符来复制，解构对象和新增的属性重复时会以更底部的为准。
       1. **对于存放在state中的对象/数组来说，它应该被视作只读的**，就像基本类型一样。如果**直接修改对象的属性**（也叫突变/mutation），相当于是修改上一次渲染的对象，又因为**没有使用设置函数setXxx，因此并不会触发新的渲染**
       2. 如果嫌展开语法繁琐，可以使用[Immer](https://github.com/immerjs/use-immer)来实现新对象的创建，而语法简洁，类似于mutation：

        ```jsx
        // 以下监听回调函数不会生效，因为它直接修改了对象的属性而不是整体替换掉对象
        onPointerMove={e => {
            position.x = e.clientX;
            position.y = e.clientY;
        }}

        // 下面的写法才是正确、有效的写法
        onPointerMove={e => {
            setPosition({
                x: e.clientX,
                y: e.clientY
            });
        }}
        
        //使用Immer以使用类似mutation的简洁语法创建新的对象
        import { useImmer } from "use-immer";

        const [person, updatePerson] = useImmer({
            name: "Michel",
            age: 33
        });
        updatePerson(draft => {
            draft.artwork.city = 'Lagos';
        });

        ```

    10. 新数组也是类似的使用新数组

        ```jsx
        const [list, setList] = useState(initialList);
        function handleToggle(artworkId, nextSeen) {
            setList(list.map(artwork => {
                if (artwork.id === artworkId) {
                    return { ...artwork, seen: nextSeen };
                } else {
                    return artwork;
                }
            }));
        }

        // 使用Immer
        const [list, updateList] = useImmer(initialList);
        function handleToggle(artworkId, nextSeen) {
            updateList(draft => {
                const artwork = draft.find(a =>
                    a.id === artworkId
                );
                artwork.seen = nextSeen;
            });
        }
        ```

    11. **使用setXxx改变状态会导致重新渲染，也就是会重新执行一遍组件函数，与该状态相关的逻辑/副作用会自动重新执行一遍，因此React并不需要类似于Vue中的watch函数**。假设有以下的逻辑代码，当调用`setIsActive()`时会触发函数组件重新执行，并且自动重新计算 backgroundClassName 和 pictureClassName 的值。并且注意这里的backgroundClassName和pictureClassName并不需要设置为state，因为它们的变化并不依赖于组件内部的状态或者其他副作用，而是仅仅根据 isActive 这个状态的变化来动态改变的。

        ```jsx
        const [isActive, setIsActive] = useState(false);
        let backgroundClassName = 'background';
        let pictureClassName = 'picture';
        if (isActive) {
            pictureClassName += ' picture--active';
        } else {
            backgroundClassName += ' background--active';
        }
        ```

    12. 同样的道理，*这种重新计算的特性使得React也不需要像Vue那样的computed属性*
    13. 对于仅仅依赖某个state而改变、并且不依赖其它状态的数据，不需要设置为state，比如由firstName+lastName两个state组合而成的fullName就不需要设置为state，而可以直接使用`const fullName = firstName + ' ' + lastName;`，fullName会自动根据两个state改变而重新计算，`类似于Vue中的computed属性`
    14. 深层嵌套的state应该[扁平化其数据结构](https://react.jscn.org/learn/choosing-the-state-structure)
    15. 组件的state在组件被移除的时候也会被清除：`{showB && <Counter />}`，当showB为false时，组件被移除，其state被清空，即使showB又切换为true也不会恢复（*这一点类似于Vue的`v-if`指令，直接销毁/新建组件*）。类似的`{isPaused ? (<p>待会见！</p> ) : (<Counter />)}`当isPaused状态变化时，Counter组件也会被销毁和重建，状态也会被清除（相同位置不同组件不会让state保留）
    16. 相同位置不同组件不保留state，为什么你不应该把组件函数的定义嵌套起来的原因：假如Child组件定义在Parent组件内，当Parent本身的状态改变时会触发重新渲染，从而导致Child组件函数被重新执行创建了一个新的、不同的Child组件，因此Parent的状态会保留，但是Child组件是同一个位置的不同组件，因此其state会被清除
    17. UI 树中**相同位置的相同组件**会保留state：`{isFancy ? (<Counter isFancy={true} /> ) : (<Counter isFancy={false} /> )}`，这里切换isFancy状态，两个Counter都会使用同一个state
18. 组件状态整合reducer：`const [data, dispatch] = useReducer(reducerFunction, initialState)`，当组件拥有的state过多过于复杂的时候，可以将组件的所有状态更新逻辑整合到reducer函数中，增加可读性、减少代码量、方便调试，**data代替了原来的状态**
    1. 比如组件中包含对状态的添加、删除、修改等多种操作，仅用`useState()`的**setXxx()**来更新数据会让人很难一眼看清组件更新逻辑
    2. 通过以下步骤将 `useState()` 迁移到 `useReducer()`
       1. 把`setXxx()`修改成`dispatch()`，dispatch是`useReducer()`的第二个元素，类似于`useState()`的第二个元素，用于更新状态：dispatch一个“action”来指明“用户刚刚做了什么”，并且提供必要的参数。useReducer和useState很像，但是它有两个参数并且state放在第二位

            ```jsx
            //tasks表示状态，dispatch表示更新状态的函数，有点像useState()，但是用法不一样
            const [tasks, dispatch] = useReducer(
                tasksReducer,// reducer函数，对每一种action进行对应的状态更新，并且把更新后的新状态返回
                initialTasks // 初始状态
            );
            function handleDeleteTask(taskId) {
                dispatch(
                    // "action" 对象：一个普通对象而已，结构是由你决定的，但通常来说，它应该至少包含可以表明 发生了什么事情 的信息，比如下面的type，按照惯例，我们通常会添加一个字符串类型的 type 字段来描述发生了什么
                    {
                        type: 'deleted',// type表明这是一个删除操作
                        id: taskId,
                    }
                );
            }
            ```

       2. 编写一个reducer函数，以处理不同的逻辑操作，**Reducer函数包含两个参数：当前的状态state，以及action对象**，记住哦是两个参数！！

            ```jsx
            function yourReducer(state, action) {
                // 对于每种action的类型，处理逻辑并返回更新后的状态
            }

            // 对于每种action都要处理并且返回更新后的状态，React建议将每个 case 块包装到 { 和 } 花括号中，这样在不同 case 中声明的变量就不会互相冲突。此外，case 通常应该以 return 结尾。如果你忘了 return，代码就会 进入 到下一个 case，这就会导致错误！
            function tasksReducer(tasks, action) {
                switch (action.type) {
                    case 'added': {
                        return [
                            ...tasks,
                            {
                                id: action.id,
                                text: action.text,
                                done: false,
                            },
                        ];
                    }
                    // case ...:{return ...}
                    default: {
                        throw Error('未知 action: ' + action.type);
                    }
                }
            }

            // reducer函数其实是来源于数组的reduce()方法，接收目前状态和action，然后返回下一个状态
            //甚至可以通过使用一个actions数组，通过数组的reduce()方法并传递一个初始状态initialState以及一个reducer函数来直接计算进行actions内部所有操作之后的最终状态：
            let initialState = [];
            let actions = [
                {type: 'added', id: 1, text: '参观卡夫卡博物馆'},
                {type: 'added', id: 2, text: '看木偶戏'},
                {type: 'deleted', id: 1},
                {type: 'added', id: 3, text: '打卡列侬墙'},
            ];

            let finalState = actions.reduce(tasksReducer, initialState);
            /**最终状态如下
            [
                {
                    "id": 2,
                    "text": "看木偶戏",
                    "done": false
                },
                {
                    "id": 3,
                    "text": "打卡列侬墙",
                    "done": false
                }
            ]
            */
            ```

       3. 在组件中使用reducer

            ```jsx
            import { useReducer } from 'react';//先导入
            //const [tasks, setTasks] = useState(initialTasks);
            const [tasks, dispatch] = useReducer(tasksReducer, initialTasks);// 使用userReducer替换上面的useState
            // 此时tasks代替了useState的状态
            ```

    3. **由于 reducer 函数接受 state（tasks）作为参数，因此你可以 在组件之外声明它**。**可以将你定义的reducer函数移动到一个新的文件中**，这减少了代码的缩进级别，提升了代码的可读性；并且会将关注点分离，更便于理解组建的逻辑：事件处理程序只通过派发 action 来指定 发生了什么，而 reducer 函数通过响应 actions 来决定 状态如何更新
    4. **useState和useReducer能做的事情是一样的**，useReducer也不是没有缺点，当逻辑足够简单时，用useState的代码量更少，可读性也还行，喜欢用哪个/同时用都可以
    5. 类似在使用useState时可以通过Immer来简化state的更新代码，我们也可以使用Immer来简化reducers函数：`import { useImmerReducer } from 'use-immer';const [tasks, dispatch] = useImmerReducer(tasksReducer, initialTasks);`
    6. useReducer()实现：

        ```jsx
        import { useState } from 'react';
        export function useReducer(reducer, initialState) {
            const [state, setState] = useState(initialState);
            /**
            function dispatch(action) {
                const nextState = reducer(state, action);
                setState(nextState);
            }
             */
            //虽然大部分情况下不重要，但是更加准确的实现是下面这样的，传一个状态更新函数
            function dispatch(action) {
                setState((s) => reducer(s, action));
            }
            return [state, dispatch];
        }
        ```

19. Context：`const LevelContext = createContext(1)`，允许父组件向其下层无论多深的任何组件提供信息，而无需通过 props 显式传递/逐级透传
    1. 使用步骤：在 Context.js 中创建并导出 `ImageSizeContext`。然后用 `<ImageSizeContext.Provider value={imageSize}>` 包裹住整个列表来向下传递值，最后在 子组件中使用 `useContext(ImageSizeContext)` 来读取它。
       1. **首先需要创建一个context**，并将其放在一个单独的文件中导出。注意：这一步**创建context并不需要我们把所有的状态都初始化好**，因为context的值是在**标签`<context名.Provider value={xxx}>`**中通过value属性传递的，它可以是任意的值
       2. **子组件中引入并且使用 useContext Hook** （`useContext(context)`可以让我们获取创建的context数据）以及刚刚创建的 context，**只有子组件需要useContext，父组件不需要**
       3. **父组件提供一个context**，若不提供，会使用创建context时的默认值，用 context provider **标签`<context名.Provider>`**裹起来，并通过value属性传递值

       ```jsx
        // 创建context
        // LevelContext.js 文件
        import { createContext } from 'react';
        export const LevelContext = createContext(1);

        // 子组件Heading中引入context  
        // Heading.js文件
        import { useContext } from 'react';
        import { LevelContext } from './LevelContext.js';

        export default function Heading({ children }) {
            const level = useContext(LevelContext);
            switch (level) {
            }
        }

        // 父组件Section提供context，使用value给context赋值，并且用<context名.Provider>标签包裹
        // Section.js 文件
        import { LevelContext } from './LevelContext.js';

        export default function Section({ level, children }) {
            return (
                <section className="section">
                <LevelContext.Provider value={level}>
                    {children}
                </LevelContext.Provider>
                </section>
            );
        }
       ```

    2. 子组件使用 `useContext(LevelContext)` 访问的永远是最近父级的 LevelContext 提供的值，因此可以通过提供一个类似`<LevelContext.Provider value={level + 1}></LevelContext.Provider>`并且重复嵌套同一个组件来实现内部的同个组件levelContext值层层递增的效果
    3. Context 会穿过所有（不提供context的）中间层级的组件，有点类似于CSS中的样式继承，比如`color: red`，除非中间有相同的样式覆盖，否则，内部的元素会继承最外面元素的color；类似地，在 React 中，覆盖来自上层的某些 context 的唯一方法是将子组件包裹到一个提供不同值的 context provider 中，并且不同的 React context 不会覆盖彼此
    4. 克制地使用Context：
       1. 传递props会让哪些组件使用了哪些数据变得十分清晰，便于维护
       2. 如果你通过很多层不使用该数据的中间组件（并且只会向下传递）来传递数据，这通常意味着你在此过程中忘记了抽象组件，比如：当Layout组件并不直接需要posts这个prop时，你应该在Layout中使用children参数并且使用`<Layout><Posts posts={posts} /></Layout>`而不是`<Layout posts={posts} />`，这样就减少了定义数据的组件和使用数据的组件之间的层级。
    5. context使用场景：更改主题、保存账户信息、保存路由信息、应用顶部状态管理
20. 使用Reducer 和 Context 结合拓展你的应用
    1. Reducer 可以整合组件的状态更新逻辑。Context 可以将信息深入传递给其他组件。你可以**结合**使用它们来共同管理一个复杂页面的状态。
    2. `useReducer(reducer, initialState)`仅能够在顶级组件中使用，后代组件必须通过触发传递的props才能够触发（逐级props），而Context刚好能解决这个问题！
    3. **把 状态 和 dispatch 函数都 放入 context**。这样，所有在 TaskApp 组件树之下的组件都不必一直往下传 props 而可以直接读取 状态 和 dispatch 函数；使用的时候需要用**两个标签**（`<TasksContext.Provider value={tasks}><TasksDispatchContext.Provider value={dispatch}></TasksDispatchContext.Provider></TasksContext.Provider>`）进行嵌套（如果状态和dispatch函数都需要的话）
    4. 具体实现步骤：
       1. 创建两个context，用于存放`useReducer()`的状态（TasksContext）和dispatch函数
       2. 将 state 和 dispatch 函数 通过`Xxx.Provider`标签放入 context
       3. 在子组件内读取 TasksContext 状态 或者 调用 TasksDispatchContext

            ```jsx
            // 创建两个context
            // TaskContext.js 文件
            import { createContext } from 'react';

            export const TasksContext = createContext(null);
            export const TasksDispatchContext = createContext(null);

            // 给context传递
            // 父组件中
            import { TasksContext, TasksDispatchContext } from './TasksContext.js';

            export default function TaskApp() {
                const [tasks, dispatch] = useReducer(tasksReducer, initialTasks);
                // ...
                return (
                    <TasksContext.Provider value={tasks}>
                    <TasksDispatchContext.Provider value={dispatch}>
                        //后代组件
                    </TasksDispatchContext.Provider>
                    </TasksContext.Provider>
                );
            }

            // 子组件内读取两个context
            // 子组件中
            import { useContext } from 'react';
            import { TasksContext, TasksDispatchContext } from './TasksContext.js';

            export default function TaskList() {
                const tasks = useContext(TasksContext);
                const dispatch = useContext(TasksDispatchContext);
            }
            ```

    5. 进阶 —— 将reducer以及context迁移到一个文件中，把数据和组件视图彻底解耦（*有VueX那味儿了*）
       1. 同时还可以将`<Xxx.Provider></Xxx.Provider>`标签进行整合，形成一个新的、自定义的TasksProvider组件，能够简化调用context时的代码量
       2. 新建的TasksProvider组件可以接收一个children prop并且直接渲染即可，children可以获取提供的所有context
       3. 在内部还能够导出一些自定义的Hooks方便使用、派送状态

       ```jsx
        import { createContext, useContext, useReducer } from 'react';

        const TasksContext = createContext(null);
        const TasksDispatchContext = createContext(null);

        export function TasksProvider({ children }) {
            const [tasks, dispatch] = useReducer(
                tasksReducer,
                initialTasks
            );
            return (
                <TasksContext.Provider value={tasks}>
                <TasksDispatchContext.Provider value={dispatch}>
                    {children}
                </TasksDispatchContext.Provider>
                </TasksContext.Provider>
            );
        }

        // 注意，下面定义了两个自定义Hook
        export function useTasks() {
            return useContext(TasksContext);
        }
        export function useTasksDispatch() {
            return useContext(TasksDispatchContext);
        }

        function tasksReducer(tasks, action) {
            switch (action.type) {
                case 'added': {
                    return null;
                }
            }
        }

        const initialTasks = [
            { id: 0, text: 'Philosopher’s Path', done: true },
            { id: 1, text: 'Visit the temple', done: false },
            { id: 2, text: 'Drink matcha', done: false }
        ];

       ```

21. props vs state :（类似于Vue的props和data选项）
    1. props 类似于传递给函数的参数
    2. state 类似于组件的内存
    3. props 和 state 并不是唯一的响应式值。从它们计算出的值也是响应式的。如果 props 或 state 发生变化，组件将重新渲染，从中计算出的值也会随之改变
    4. **state 和 props 更新都会触发重新渲染**，这就是为什么下面的代码即使className并没有设置为state也会随着时间改变而改变，因为time这个prop一直在变，一直导致重新渲染，函数一直在执行，所以className变化也会及时响应到视图上

        ```jsx
        export default function Clock({ time }) {
            let hours = time.getHours();
            let className;
            if (hours >= 0 && hours <= 6) {
                className = 'night';
            } else {
                className = 'day';
            }
            return (
                <h1 className={className}>
                {time.toLocaleTimeString()}
                </h1>
            );
        }
        ```

22. 受控组件和非受控组件：组件中的重要信息（比如是否激活）由props控制，则是“受控组件”，由自身的state控制，则是“非受控组件”
23. 在[现有页面的一部分使用React](https://react.jscn.org/learn/add-react-to-an-existing-project)
24. 组件导入、导出规则，一个文件中只能有一个默认导出，但是可以有多个具名导出
    1. 默认导出：`export default function Button() {}`，**导入时可以使用任意别名**，比如：`import Banana from './Button.js'`
    2. 具名导出：`export function Button() {}`，导入时必须同名：`import { Button } from './Button.js';`

### 脱围机制——“走出” React 并连接到外部系统（DOM等）

1. ref引用值：
   1. `const ref = useRef(0)`，组件能像“记住”state一样“记住”ref保存的值，但是**设置ref不会导致重新渲染，而设置state会**
   2. 通过 `ref.current` 属性访问该 ref 的当前值：`ref.current = ref.current + 1`
   3. **React 保证 每轮渲染中调用 useRef 所产生的引用对象时，获取到的对象引用总是相同的，即获取到的对象引用永远不会改变**
   4. ref 就像组件的一个不被 React 追踪的秘密口袋。例如，可以使用 ref 来存储 timeout ID、DOM 元素 和其他不影响组件渲染输出的对象。
   5. 使用时机：
      1. 需要操作DOM的时候使用：（比如聚焦到某个输入框、滚动到某个节点）用法*和Vue中的ref非常类似*，声明一个与DOM元素ref值同名的ref即可
         1. 如果DOM节点是一个列表，不能通过`return <li ref={useRef(null)} />`这样的写法来为每个子项指定ref属性，因为Hooks只能在组件函数的顶层被调用，不能再循环、判断、map函数、组件函数外部被调用，此时可以为每一项的ref属性传入一个回调函数，参数就是每个子项的DOM节点
         2. React 不允许组件访问其他组件的 DOM 节点。甚至自己的子组件也不行。所以为自定义组件添加ref的行为是无效的：`<MyInput ref={inputRef} />`，默认无法成功获取到子组件MyInput的DOM节点，**除非子组件同意父组件操作自己的DOM节点**（使用`forwardRef((props, ref)=>{})`定义）
         3. 子组件还能限制父组件操作DOM节点的哪些API，详见[这里](https://react.jscn.org/learn/manipulating-the-dom-with-refs)
         4. 强制React同步更新DOM：使用`flushSync`函数来包裹state的设置函数

           ```jsx
           // React
           import { useRef } from 'react';
           <input ref={inputRef} />
           const inputRef = useRef(null);
           inputRef.current.focus();
             
           //Vue3
           <input ref="input" />
           const input = ref(null)
           input.value.focus();

           // 为DOM列表每一项指定ref
           <li
               key={cat.id}
               ref={node => {
                   const map = getMap();
                   if (node) {
                       // 添加到 Map
                       map.set(cat.id, node);
                   } else {
                       // 从 Map 删除
                       map.delete(cat.id);
                   }
               }}
           >

           // 只有使用forwardRef定义才能够让父组件操作自身的DOM
           // 子组件中
           const MyInput = forwardRef((props, ref) => {
               return <input {...props} ref={ref} />;
           });
           // 父组件中
           <MyInput ref={inputRef} />

           // 强制React同步更新DOM：使用`flushSync`函数来包裹state的设置函数
           flushSync(() => {
               setText('');
               setTodos([ ...todos, newTodo]);      
           });
           //下面操作的DOM是最新的DOM元素
           ```

      2. 需要保存不会引起渲染的数据时要用：以下的代码**如果使用普通的变量来盛放setInterval，会导致定时器无法被停止**，并且每按一次开始，会增加一个定时器，导致num的增加速度越来越快，因为虽然每次handleStart都将定时器ID赋值给了该变量，但是**React重新渲染重新执行函数时，函数组件中的非状态（state）变量和非持久性引用（如 useRef 创建的引用）都会被重置为初始值**，变量又会变为0，导致无法被clearInterval成功清除原本“已经保存”的定时器

           ```jsx
           const [num, setNum] = useState(0);
           let intervalRef = useRef(0);
           function handleStart() {
               setNum(0);
               clearInterval(intervalRef.current);
               intervalRef.current = setInterval(() => setNum(n => n + 1), 150);// 这里setNum必须要传一个更新函数，不能是setNum(num+1)，这样会导致num一直为0+1
           }
           function handleStop() {
               clearInterval(intervalRef.current);
           }
           ```

   6. **useRef可以理解成不用设置函数的`useState({ current: initialValue })`**（其实很好理解，state会在每次重新渲染时都保存状态，而不用设置函数就不会引起视图渲染，不就是ref的定义 —— 记住保存的值，但是不会引起重新渲染吗）
   7. **ref其实就是一个普通的对象，因此不会像设置state那样，需要在下一次渲染的时候才会改变（并且当前的state变量会被替换），每当更改了ref.current属性值，它就立即生效了**
2. 不要试图在渲染期间操作DOM，以下代码会报错：

    ```jsx
    // 错误代码示例1：
    function VideoPlayer({ src, isPlaying }) {
        const ref = useRef(null);
        if (isPlaying) {
            ref.current.play();  // 渲染期间不能调用 `play()`。 
        } else {
            ref.current.pause(); // 同样，调用 `pause()` 也不行。
        }
        // 试图在渲染期间对 DOM 节点进行操作导致报错，并且，第一次调用的时候，DOM节点都不存在，因此也不会有play()或者pause()方法
        // 解决办法是 使用 useEffect 包裹副作用，把它分离到渲染逻辑的计算过程之外
        return <video ref={ref} src={src} loop playsInline />;
    }
    //解决方案：
    useEffect(() => {
        if (isPlaying) {
            ref.current.play();
        } else {
            ref.current.pause();
        }
    });

    // 错误代码示例2：
    export default function Clock({ time }) {
        const [classState, setClassState]=useState('night');
        let hours = time.getHours();
        if (hours >= 0 && hours <= 6) {
            setClassState('night');// 无限重复渲染导致报错
        } else {
            setClassState('day');
        }
        
        return (
            <h1 id="time" className={classState}>
            {time.toLocaleTimeString()}
            </h1>
        );
    }
    ```

3. Effect
   1. 有些组件需要与外部系统同步。例如，需要根据 React 状态控制非 React 组件、设置服务器连接或在组件出现在屏幕上时发送分析日志
   2. `useEffect()`回调函数内的代码**每次渲染完成后都会执行**，而setState()会导致重新渲染，因此下面的代码会陷入死循环报错

        ```jsx
        const [count, setCount] = useState(0);
        useEffect(() => {
            setCount(count + 1);
        })
        ```

   3. Effect VS 事件Event
      1. Event：事件处理程序包含由特定用户操作（例如按钮点击或键入）引起的“副作用”（它们改变了程序的状态） —— **由特定用户操作引起**
      2. Effect：允许你指定由渲染本身，而不是特定事件引起的副作用 —— **不需要特定的用户操作/事件引起**
   4. 编写Effect
      1. 声明 Effect 。默认情况下（不传依赖数组），**useEffect 会把内部的代码放到屏幕每次更新渲染之后再执行**

            ```jsx
            // 错误代码示例：
            function VideoPlayer({ src, isPlaying }) {
                const ref = useRef(null);
                if (isPlaying) {
                    ref.current.play();  // 渲染期间不能调用 `play()`。 
                } else {
                    ref.current.pause(); // 同样，调用 `pause()` 也不行。
                }
                // 试图在渲染期间对 DOM 节点进行操作导致报错，并且，第一次调用的时候，DOM节点都不存在，因此也不会有play()或者pause()方法
                // 解决办法是 使用 useEffect 包裹副作用，把它分离到渲染逻辑的计算过程之外
                return <video ref={ref} src={src} loop playsInline />;
            }

            // 代码改成
            // 把调用 DOM 方法的操作封装在 Effect 中，你可以让 React 先更新屏幕，确定相关 DOM 创建好了以后然后再运行 Effect
            useEffect(() => {
                if (isPlaying) {
                    ref.current.play();
                } else {
                    ref.current.pause();
                }
            });
            ```

      2. 指定 Effect 依赖。大多数 Effect 应该按需执行，而不是在每次渲染后都执行
         1. 前面说过，**每次渲染之后，useEffect 都会执行内部的代码**，但是事实上很多时候不需要如此，而是应该为useEffect函数第二个参数一个依赖数组，**当依赖发生改变才会重新运行effect**（*类似于Vue的watch*）
         2. 依赖数组可以包含多个依赖项，只有所有依赖项都和上次渲染一致时才会跳过effect的重新执行
         3. 当effect存在依赖项但是数组传入一个空数组也会导致报错：没有依赖数组作为第二个参数，与依赖数组位空数组 `[]` 的行为是不一致的，传入`[]`表明代码只会在组件挂载后执行

            ```jsx
            useEffect(() => {
                // 这里的代码会在每次渲染后执行
            });

            useEffect(() => {
                // 这里的代码只会在组件挂载后执行，挂载的意思就是组件首次出现在页面上
            }, []);

            useEffect(() => {
                //这里的代码只会在每次渲染后，并且 a 或 b 的值与上次渲染不一致时执行
            }, [a, b]);
            ```

         4. ref 具有 稳定 的标识：**React 保证 每轮渲染中调用 useRef 所产生的引用对象时，获取到的对象引用总是相同的，即获取到的对象引用永远不会改变**，所以它不会导致重新运行 Effect，因此**ref可以传入依赖列表但是没必要**（但是如果是存在可能传入不同的ref的情况 —— 比如从父元素有条件的传入不同的ref之一，则需要指定其为依赖）
      3. 必要时添加清理（cleanup）函数。指定如何停止、撤销，或者清除它的效果
         1. 比如以下代码：Effect会在，每次组件挂载的时候执行，但是假如组件重新挂载（比如切换到其它组件后又切换回该组件），会重复发出请求，而第一次的请求并未销毁，因此为了保证“每次重新执行 Effect 之前，或者组件被卸载时，都断开连接”，我们可以在Effect中返回一个清理函数，清除上一次的Effect副作用，这样，**每次重新执行 Effect 之前，或者组件被卸载时，都会执行清理函数**

            ```jsx
            useEffect(() => {
                const connection = createConnection();
                connection.connect();

                // 此时应该返回一个清理函数
                return () => {
                    connection.disconnect();
                };
            }, []);
            ```

         2. **开发环境中，React 有意重复挂载你的组件（但是组件的 state 与 创建的 DOM 都会被保留）**，以查找像上面示例中的**Effect**被执行两次的错误，正确的态度是“如何修复 Effect 以便它在重复挂载后能正常工作”，而不是“如何只运行一次 Effect”。所以有需要的话还是实现并返回一个清理函数，比如Effect订阅了一个事件则应该在清理函数中取消、打开了一个小弹窗则应该在清理函数中关闭.
   5. **仅仅只是改变内部状态的话，不需要Effect**，以下代码会陷入死循环（渲染结束执行Effect → Effect内部更新state会触发渲染 → 渲染完毕继续执行Effect ...）

        ```jsx
        const [count, setCount] = useState(0);
        useEffect(() => {
            setCount(count + 1);
        });
        ```

   6. **需要依靠事件触发的内容也不要依靠Effect来处理**
   7. 对于耗时的计算，可以使用`useMemo()`缓存计算结果，类似于Vue中的computed属性：

        ```jsx
        const visibleTodos = useMemo(() => {
            return getFilteredTodos(todos, filter); // 如果 todos 或 filter 没有发生变化将跳过执行
        }, [todos, filter]);
        ```

   8. 开发环境中，useEffect会执行两次，如果某些逻辑必须在 每次应用加载时执行一次，而不是在 每次组件挂载时执行一次，可以添加一个顶层变量来记录它是否已经执行过了

        ```jsx
        let didInit = false;

        function App() {
            useEffect(() => {
                if (!didInit) {
                    didInit = true;
                    // ✅ 只在每次应用加载时执行一次
                    loadDataFromLocalStorage();
                    checkAuthToken();
                }
            }, []);
            // ...
        }
        ```

   9. Effect vs 类组件的生命周期
      1. 函数式组件的初始化useState hook其实就是类组件的constructor周期，代替构造函数来初始化状态
      2. useEffect传入一个空数组，会在每次组件挂载后执行一次Effect，可以模拟类组件的componentDidMount周期
      3. useEffect不传入参数会在每次渲染后执行，来模拟 componentDidUpdate
      4. useEffect返回的清理函数可以模拟类组件的componentWillUnmount周期
      5. useMemo 来得到类似 shouldComponentUpdate 的优化效果
      6. 事实上，Effect只关心内部的代码如何开始/如何停止，只专注于单个启动/停止周期，并不会考虑组件的生命周期（挂载/更新）
   10. 每个 Effect 应该代表一个独立的同步过程，比如对于roomId这个依赖项，有两个Effect：建立连接 + 记录日志 ，应该把两者分开成两个Effect单独编写，而不能为了图方便写到一个useEffect hook中

        ```jsx
        function ChatRoom({ roomId }) {
            useEffect(() => {
                logVisit(roomId);
            }, [roomId]);

            useEffect(() => {
                const connection = createConnection(serverUrl, roomId);
                // ...
            }, [roomId]);
            // ...
        }
        ```

   11. 从组件的角度来看，空的 `[]` 依赖数组意味着这个 Effect 仅在组件挂载时连接到聊天室，并在组件卸载时断开连接。（请记住，在开发环境中，React 仍会 额外执行一次 来对逻辑进行压力测试。）
   12. 依赖列表项一定是**有可能改变的值**，如果把一个硬编码的const常量（非计算而来的常量）作为依赖列表项，是没有意义的；但如果是通过计算而来的常量（可能随着重新渲染而改变值，因此也是响应式的）
   13. **依赖项必须包括 Effect 中读取的每个 响应式值**：包括 props、state 和组件体内的变量**甚至是用到的组件内的函数**，但是应该避免使用函数作为响应式依赖项（会报错），**因此如果Effect依赖了函数，应该把函数放到useEffect的回调内，使其不再是要给响应式的值**
   14. useEffectEvent是一个正在建设的不稳定的API，因此不详细描述，可以理解成useEffect中不需要响应式的一部分，也就是说**useEffectEvent内部访问的响应式状态量不需要添加到依赖中，即使这些状态量发生了变化也不会导致内部重新执行**
       1. useEffectEvent读取的总是最新的状态值，如果你想要它读取旧的值，可以为其创建形参并且在useEffect内部调用时传入一个旧的实参

        ```jsx
        const onVisit = useEffectEvent(visitedUrl => {
            logVisit(visitedUrl, numberOfItems);
        });

        useEffect(() => {
            onVisit(url);
        }, [url]);
        ```

   15. 自定义Hooks可以用来提取组件的公共逻辑，减少重复编码，你只需要关注 “这些公共的部分最终都是需要一个什么样的值？”，并且把逻辑抽离到Hook中，把这个值在Hook中返回就可以了
       1. 对于内部没有调用Hook的函数，就不能被定义成Hook，因为它不是一个Hook，只是一个常规函数
       2. 命名规范：对于Hook来说命名必须符合`useXxx`的形式，而对于组件来说，名称必须以大写字母开头
       3. **自定义Hook共享的是逻辑，而非状态**，调用自定义Hook其实就是相当于把原先提取到Hook中的公共代码“还原到”它们调用的地方，**对于不同组件中同一个Hook调用是彼此完全独立、不会互相干扰的**
       4. **每当组件重新渲染，自定义 Hook 中的代码就会重新运行**。这就是组件和自定义 Hook 都 需要是纯函数 的原因。我们应该把自定义 Hook 的代码看作组件主体的一部分
       5. 给自定义Hook传参：可以传入一个响应式的值，也可以传一个事件（传一个事件是不稳定特性，和 `useEffectEvent` 一样正在建设中）：比如给useChatRoom这个Hook传一个回调函数`onReceiveMessage`，表示收到信息之后的处理函数，但是把`onReceiveMessage`添加到依赖并不理想，可以把他包裹到`useEffectEvent`来排除依赖

            ```jsx
            import { useEffect, useEffectEvent } from 'react';
            // ...

            export function useChatRoom({ serverUrl, roomId, onReceiveMessage }) {
                const onMessage = useEffectEvent(onReceiveMessage);

                useEffect(() => {
                    const options = {
                        serverUrl: serverUrl,
                        roomId: roomId
                    };
                    const connection = createConnection(options);
                    connection.connect();
                    connection.on('message', (msg) => {
                        onMessage(msg);
                    });
                    return () => connection.disconnect();
                }, [roomId, serverUrl]); // ✅ 声明所有依赖
            }

            // 其他组件内使用该Hook
            useChatRoom({
                roomId: roomId,
                serverUrl: serverUrl,
                onReceiveMessage(msg) {
                    showNotification('New message: ' + msg);
                }
            });
            ```

       6. 自定义Hook举例
          1. 一个实时检测网络状态的自定义Hook例子：这个Hook每次被调用的时候返回的都是独立的isOnline state，只不过这个state碰巧同一时间值一样，总之自定义Hook一定符合“共享逻辑而不共享状态”的原则

               ```jsx
               import { useState, useEffect } from 'react';

               export function useOnlineStatus() {
                   const [isOnline, setIsOnline] = useState(true);
                   useEffect(() => {
                       function handleOnline() {
                           setIsOnline(true);
                       }
                       function handleOffline() {
                           setIsOnline(false);
                       }
                       // 下面两个监听器使用了两个比较少用到的事件类型：online、offline，这两个监听器实际上是监听Navigator.online属性，为true的时候触发online事件，为false时触发offline事件
                       window.addEventListener('online', handleOnline);
                       window.addEventListener('offline', handleOffline);
                       return () => {
                           window.removeEventListener('online', handleOnline);
                           window.removeEventListener('offline', handleOffline);
                       };
                   }, []);
                   return isOnline;
               }
               ```

          2. 自定义fetch请求并且更新数据的Hook示例：**理想情况下，应用程序中的大多数 Effect 最终都应该由自定义 Hook 替代**，无论是由你自己编写还是由社区提供。自定义 Hook 隐藏了同步逻辑，因此调用组件不知道 Effect 的存在。随着你继续开发应用程序，你将开发出一套可供选择的 Hooks，并且最终将不再经常在组件中编写 Effect。

               ```jsx
               function useData(url) {
                   const [data, setData] = useState(null);
                   useEffect(() => {
                       let ignore = false;
                       fetch(url)
                       .then(response => response.json())
                       .then(json => {
                           if (!ignore) {
                               setData(json);
                           }
                       });
                       return () => {
                           ignore = true;
                       };
                   }, [url]);
                   return data;
               }
               ```
