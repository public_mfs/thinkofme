import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'weather',
      component: () => import('../components/weather.vue')
    }, {
      path: '/md',
      name: 'md',
      component: () => import('../components/md.vue')
    }, {
      path: '/three',
      name: 'three',
      component: () => import('../components/three.vue')
    }, {
      path: '/flex',
      name: 'flex',
      component: () => import('../components/flex.vue')
    }
  ]
})

export default router;
