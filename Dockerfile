FROM node:latest as builder
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build

FROM nginx:latest

RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
# 设置容器为北京时间，方便查看日志

COPY --from=builder /app/dist /usr/share/nginx/html